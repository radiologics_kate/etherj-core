/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etherj.math;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class ComplexTest
{
	
	public ComplexTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of add method, of class Complex.
	 */
	@Test
	public void testAdd_double()
	{
		System.out.println("add");
		double f = 1;
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(2, -2);
		Complex result = instance.add(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of add method, of class Complex.
	 */
	@Test
	public void testAdd_Complex()
	{
		System.out.println("add");
		Complex other = new Complex(3, 4);
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(4, 2);
		Complex result = instance.add(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of conj method, of class Complex.
	 */
	@Test
	public void testConj()
	{
		System.out.println("conj");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(1, 2);
		Complex result = instance.conj();
		assertEquals(expResult, result);
	}

	/**
	 * Test of cube method, of class Complex.
	 */
	@Test
	public void testCube()
	{
		System.out.println("cube");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-11, 2);
		Complex result = instance.cube();
		assertEquals(expResult, result);
	}

	/**
	 * Test of divide method, of class Complex.
	 */
	@Test
	public void testDivide_double()
	{
		System.out.println("divide");
		double f = 2;
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(0.5, -1);
		Complex result = instance.divide(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of divide method, of class Complex.
	 */
	@Test
	public void testDivide_Complex()
	{
		System.out.println("divide");
		Complex other = new Complex(3, 4);
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-0.2, -0.4);
		Complex result = instance.divide(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of isReal method, of class Complex.
	 */
	@Test
	public void testIsReal()
	{
		System.out.println("isReal");
		Complex instance = new Complex(1, -2);
		boolean expResult = false;
		boolean result = instance.isReal();
		assertEquals(expResult, result);
		instance = new Complex(1, 0);
		assertEquals(true, instance.isReal());
	}

	/**
	 * Test of modulus method, of class Complex.
	 */
	@Test
	public void testModulus()
	{
		System.out.println("modulus");
		Complex instance = new Complex(1, -2);
		double expResult = Math.sqrt(5);
		double result = instance.modulus();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of multiply method, of class Complex.
	 */
	@Test
	public void testMultiply_double()
	{
		System.out.println("multiply");
		double f = 2;
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(2, -4);
		Complex result = instance.multiply(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of multiply method, of class Complex.
	 */
	@Test
	public void testMultiply_Complex()
	{
		System.out.println("multiply");
		Complex other = new Complex(3, 4);
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(11, -2);
		Complex result = instance.multiply(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of negate method, of class Complex.
	 */
	@Test
	public void testNegate()
	{
		System.out.println("negate");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-1, 2);
		Complex result = instance.negate();
		assertEquals(expResult, result);
	}

	/**
	 * Test of phase method, of class Complex.
	 */
	@Test
	public void testPhase()
	{
		System.out.println("phase");
		Complex instance = new Complex(1, -2);
		double expResult = Math.atan(-2);
		double result = instance.phase();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of power method, of class Complex.
	 */
	@Test
	public void testPower()
	{
		System.out.println("power");
		double power = 4;
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-7, 24);
		Complex result = instance.power(power);
		assertEquals(expResult, result);
	}

	/**
	 * Test of sq method, of class Complex.
	 */
	@Test
	public void testSq()
	{
		System.out.println("sq");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-3, -4);
		Complex result = instance.sq();
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class Complex.
	 */
	@Test
	public void testSqrt()
	{
		System.out.println("sqrt");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(Math.sqrt((1+Math.sqrt(5))*0.5),
			-Math.sqrt((-1+Math.sqrt(5))*0.5));
		Complex result = instance.sqrt();
		assertEquals(expResult, result);
	}

	/**
	 * Test of subtract method, of class Complex.
	 */
	@Test
	public void testSubtract_double()
	{
		System.out.println("subtract");
		double f = 2;
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-1, -2);
		Complex result = instance.subtract(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of subtract method, of class Complex.
	 */
	@Test
	public void testSubtract_Complex()
	{
		System.out.println("subtract");
		Complex other = new Complex(3, 4);
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(-2, -6);
		Complex result = instance.subtract(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of equals method, of class Complex.
	 */
	@Test
	public void testEquals()
	{
		System.out.println("equals");
		Object other = new Complex(1, -2);
		Complex instance = new Complex(1, -2);
		boolean expResult = true;
		boolean result = instance.equals(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of inverse method, of class Complex.
	 */
	@Test
	public void testInverse()
	{
		System.out.println("inverse");
		Complex instance = new Complex(1, -2);
		Complex expResult = new Complex(0.2, 0.4);
		Complex result = instance.inverse();
		assertEquals(expResult, result);
	}

}
