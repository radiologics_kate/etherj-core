/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etherj.math;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class ComplexFTest
{
	
	public ComplexFTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of add method, of class ComplexF.
	 */
	@Test
	public void testAdd_float()
	{
		System.out.println("add");
		float f = 1;
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(2, -2);
		ComplexF result = instance.add(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of add method, of class ComplexF.
	 */
	@Test
	public void testAdd_ComplexF()
	{
		System.out.println("add");
		ComplexF other = new ComplexF(3, 4);
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(4, 2);
		ComplexF result = instance.add(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of conj method, of class ComplexF.
	 */
	@Test
	public void testConj()
	{
		System.out.println("conj");
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(1, 2);
		ComplexF result = instance.conj();
		assertEquals(expResult, result);
	}

	/**
	 * Test of cube method, of class ComplexF.
	 */
	@Test
	public void testCube()
	{
		System.out.println("cube");
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-11, 2);
		ComplexF result = instance.cube();
		assertEquals(expResult, result);
	}

	/**
	 * Test of curt method, of class ComplexF.
	 */
	@Test
	public void testCurt()
	{
		System.out.println("curt");
		ComplexF instance = new ComplexF(2, 0);
		ComplexF expResult = new ComplexF(
			(float) (Math.pow(2, 1.0/3.0)),
			(float) 0);
		ComplexF result = instance.curt();
		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of curt method, of class ComplexF.
	 */
	@Test
	public void testCurt2()
	{
		System.out.println("curt2");
		ComplexF instance = new ComplexF(0, -1);
		ComplexF expResult = new ComplexF(
			(float) (Math.pow(2, 1.0/3.0)),
			(float) 0);
		ComplexF result = instance.curt();
		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of divide method, of class ComplexF.
	 */
	@Test
	public void testDivide_float()
	{
		System.out.println("divide");
		float f = 2;
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(0.5f, -1);
		ComplexF result = instance.divide(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of divide method, of class ComplexF.
	 */
	@Test
	public void testDivide_ComplexF()
	{
		System.out.println("divide");
		ComplexF other = new ComplexF(3, 4);
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-0.2f, -0.4f);
		ComplexF result = instance.divide(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of isReal method, of class ComplexF.
	 */
	@Test
	public void testIsReal()
	{
		System.out.println("isReal");
		ComplexF instance = new ComplexF(1, -2);
		boolean expResult = false;
		boolean result = instance.isReal();
		assertEquals(expResult, result);
		instance = new ComplexF(1, 0);
		assertEquals(true, instance.isReal());
	}

	/**
	 * Test of modulus method, of class ComplexF.
	 */
	@Test
	public void testModulus()
	{
		System.out.println("modulus");
		ComplexF instance = new ComplexF(1, -2);
		float expResult = (float) Math.sqrt(5);
		float result = instance.modulus();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of multiply method, of class ComplexF.
	 */
	@Test
	public void testMultiply_float()
	{
		System.out.println("multiply");
		float f = 2;
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(2, -4);
		ComplexF result = instance.multiply(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of multiply method, of class ComplexF.
	 */
	@Test
	public void testMultiply_ComplexF()
	{
		System.out.println("multiply");
		ComplexF other = new ComplexF(3, 4);
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(11, -2);
		ComplexF result = instance.multiply(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of negate method, of class ComplexF.
	 */
	@Test
	public void testNegate()
	{
		System.out.println("negate");
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-1, 2);
		ComplexF result = instance.negate();
		assertEquals(expResult, result);
	}

	/**
	 * Test of phase method, of class ComplexF.
	 */
	@Test
	public void testPhase()
	{
		System.out.println("phase");
		ComplexF instance = new ComplexF(1, -2);
		float expResult = (float) Math.atan(-2);
		float result = instance.phase();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of power method, of class ComplexF.
	 */
	@Test
	public void testPower()
	{
		System.out.println("power");
		double power = 4.0;
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-7, 24);
		ComplexF result = instance.power(power);
		assertEquals(expResult, result);
	}

	/**
	 * Test of sq method, of class ComplexF.
	 */
	@Test
	public void testSq()
	{
		System.out.println("sq");
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-3, -4);
		ComplexF result = instance.sq();
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class ComplexF.
	 */
	@Test
	public void testSqrt()
	{
		System.out.println("sqrt");
		ComplexF instance = new ComplexF(1, 1);
		ComplexF expResult = new ComplexF(
			(float) (Math.sqrt(Math.sqrt(2))*Math.cos(instance.phase()/2)),
			(float) (Math.sqrt(Math.sqrt(2))*Math.sin(instance.phase()/2)));
		ComplexF result = instance.sqrt();
//		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class ComplexF.
	 */
	@Test
	public void testSqrt2()
	{
		System.out.println("sqrt2");
		ComplexF instance = new ComplexF(1, -1);
		ComplexF expResult = new ComplexF(
			(float) (Math.sqrt(Math.sqrt(2))*Math.cos(instance.phase()/2)),
			(float) (Math.sqrt(Math.sqrt(2))*Math.sin(instance.phase()/2)));
		ComplexF result = instance.sqrt();
//		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class ComplexF.
	 */
	@Test
	public void testSqrt3()
	{
		System.out.println("sqrt3");
		ComplexF instance = new ComplexF(-1, -1);
		ComplexF expResult = new ComplexF(
			(float) (Math.sqrt(Math.sqrt(2))*Math.cos(instance.phase()/2)),
			(float) (Math.sqrt(Math.sqrt(2))*Math.sin(instance.phase()/2)));
		ComplexF result = instance.sqrt();
//		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class ComplexF.
	 */
	@Test
	public void testSqrt4()
	{
		System.out.println("sqrt4");
		ComplexF instance = new ComplexF(-1, 1);
		ComplexF expResult = new ComplexF(
			(float) (Math.sqrt(Math.sqrt(2))*Math.cos(instance.phase()/2)),
			(float) (Math.sqrt(Math.sqrt(2))*Math.sin(instance.phase()/2)));
		ComplexF result = instance.sqrt();
//		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of sqrt method, of class ComplexF.
	 */
	@Test
	public void testSqrt5()
	{
		System.out.println("sqrt5");
		ComplexF instance = new ComplexF(0, -1);
		ComplexF expResult = new ComplexF(
			(float) (Math.sqrt(2)/2),
			(float) (-Math.sqrt(2)/2));
		ComplexF result = instance.sqrt();
//		System.out.println("Result: "+result.r+" "+result.i+"i");
		assertEquals(expResult, result);
	}

	/**
	 * Test of subtract method, of class ComplexF.
	 */
	@Test
	public void testSubtract_float()
	{
		System.out.println("subtract");
		float f = 2;
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-1, -2);
		ComplexF result = instance.subtract(f);
		assertEquals(expResult, result);
	}

	/**
	 * Test of subtract method, of class ComplexF.
	 */
	@Test
	public void testSubtract_ComplexF()
	{
		System.out.println("subtract");
		ComplexF other = new ComplexF(3, 4);
		ComplexF instance = new ComplexF(1, -2);
		ComplexF expResult = new ComplexF(-2, -6);
		ComplexF result = instance.subtract(other);
		assertEquals(expResult, result);
	}
	
}
