/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etherj.math;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class Matrix3x3FTest
{
	float[] refArray = new float[] {-1,2,2,2,2,-1,2,-1,2};
	Matrix3x3F ref = new Matrix3x3F(refArray);
	
	public Matrix3x3FTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of asArray method, of class Matrix3x3F.
	 */
	@Test
	public void testAsArray()
	{
		System.out.println("asArray");
		Matrix3x3F instance = ref;
		float[] expResult = refArray;
		float[] result = instance.asArray();
		assertTrue(Arrays.equals(expResult, result));
	}

	/**
	 * Test of determinant method, of class Matrix3x3F.
	 */
	@Test
	public void testDeterminant()
	{
		System.out.println("determinant");
		Matrix3x3F instance = ref;
		float expResult = -27;
		float result = instance.determinant();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of eigenValues method, of class Matrix3x3F.
	 */
	@Test
	public void testEigenValues()
	{
		System.out.println("eigenValues");
		Matrix3x3F instance = ref;
		float[] expResult = new float[] {-3,3,3};
		float[] result = instance.eigenValues();
		System.out.println(Arrays.toString(result));
		assertTrue(Arrays.equals(expResult, result));
	}

	/**
	 * Test of eigenValues method, of class Matrix3x3F.
	 */
	@Test
	public void testEigenValues2()
	{
		System.out.println("eigenValues2");
		float[] mxArray = new float[] {0,0,0,0,0,0,0,0,1};
		Matrix3x3F mx = new Matrix3x3F(mxArray);
		Matrix3x3F instance = mx;
		float[] expResult = new float[] {0,0,1};
		float[] result = instance.eigenValues();
		System.out.println(Arrays.toString(result));
		assertArrayEquals(expResult, result, 1e-6f);
	}

	/**
	/**
	 * Test of eigenValues method, of class Matrix3x3F.
	 */
	@Test
	public void testEigenValues3()
	{
		System.out.println("eigenValues3");
		float[] mxArray = new float[] {
			0.00191075f, 0.00156862f, 0.000180632f,
			0.00156862f, -0.0216533f, -0.000992447f,
			0.000180632f, -0.000992447f,  -0.00698847f};
		Matrix3x3F mx = new Matrix3x3F(mxArray);
		Matrix3x3F instance = mx;
		float[] expResult = new float[] {-0.0218250f, -0.00692222f, 0.00201618f};
		float[] result = instance.eigenValues();
		System.out.println(Arrays.toString(result));
		assertArrayEquals(expResult, result, 1e-6f);
	}

	/**
	/**
	 * Test of eigenValues method, of class Matrix3x3F.
	 */
	@Test
	public void testEigenValues4()
	{
		System.out.println("eigenValues4");
		float[] mxArray = new float[] {
			-0.00298428f, 0.000594925f, 0.000695135f,
			0.000594925f, -0.00546715f, 0.00326622f,
			0.000695135f, 0.00326622f, -0.0122325f};
		Matrix3x3F mx = new Matrix3x3F(mxArray);
		Matrix3x3F instance = mx;
		float[] expResult = new float[] {-0.0135689f, -0.00456069f, -0.00255430f};
		float[] result = instance.eigenValues();
		System.out.println(Arrays.toString(result));
		assertArrayEquals(expResult, result, 1e-6f);
	}

	/**
	 * Test of inverse method, of class Matrix3x3F.
	 */
	@Test
	public void testInverse()
	{
		System.out.println("inverse");
		Matrix3x3F instance = ref;
		float[] invArr = Arrays.copyOf(refArray, 9);
		for (int i=0; i<invArr.length; i++)
		{
			invArr[i] /= 9;
		}
		Matrix3x3F expResult = new Matrix3x3F(invArr);
		Matrix3x3F result = instance.inverse();
		assertEquals(expResult, result);
	}

	/**
	 * Test of multiply method, of class Matrix3x3F.
	 */
	@Test
	public void testMultiply()
	{
		System.out.println("multiply");
		Matrix3x3F other = ref;
		Matrix3x3F instance = ref;
		Matrix3x3F expResult = new Matrix3x3F(
			new float[] {9,0,0,0,9,0,0,0,9});
		Matrix3x3F result = instance.multiply(other);
		assertEquals(expResult, result);
	}

	/**
	 * Test of transpose method, of class Matrix3x3F.
	 */
	@Test
	public void testTranspose()
	{
		System.out.println("transpose");
		Matrix3x3F instance = ref;
		Matrix3x3F expResult = ref;
		Matrix3x3F result = instance.transpose();
		assertEquals(expResult, result);
	}
	
}
