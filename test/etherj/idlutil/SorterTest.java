/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etherj.idlutil;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class SorterTest
{
	
	public SorterTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of absSort method, of class Sorter.
	 */
	@Test
	public void testAbsSort()
	{
		System.out.println("absSort");
		float[] expected = new float[] {1.0f, 2.0f, 3.0f};
		float[] array = new float[] {1.0f, 2.0f, 3.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
		array = new float[] {1.0f, 3.0f, 2.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
		array = new float[] {2.0f, 1.0f, 3.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
		array = new float[] {2.0f, 3.0f, 1.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
		array = new float[] {3.0f, 2.0f, 1.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
		array = new float[] {3.0f, 1.0f, 2.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);

		expected = new float[] {1.0f, 2.0f, -3.0f};
		array = new float[] {-3.0f, 1.0f, 2.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);

		expected = new float[] {-3.0f, 3.0f, 3.0f};
		array = new float[] {3.0f, 3.0f, -3.0f};
		Sorter.absSort(array);
		assertArrayEquals(expected, array, 1e-6f);
	}
	
}
