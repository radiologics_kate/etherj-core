/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.crypto;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class CryptoToolkit
{
	private static final String Default = "default";
	private static final Logger logger = LoggerFactory.getLogger(
		CryptoToolkit.class);
	private static final Map<String,CryptoToolkit> toolkitMap = new HashMap<>();
	private final CryptoFactory cryptoFactory = new DefaultCryptoFactory();

	static
	{
		toolkitMap.put(Default, new CryptoToolkit());
	}

	/**
	 *
	 * @return
	 */
	public static CryptoToolkit getDefaultToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 *
	 * @return
	 */
	public static CryptoToolkit getToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public static CryptoToolkit getToolkit(String key)
	{
		return toolkitMap.get(key);
	}

	/**
	 *
	 * @param key
	 * @param toolkit
	 * @return
	 */
	public static CryptoToolkit setToolkit(String key, CryptoToolkit toolkit)
	{
		CryptoToolkit tk = toolkitMap.put(key, toolkit);
		logger.info(toolkit.getClass().getName()+" set with key '"+key+"'");
		return tk;
	}

	/**
	 *
	 * @param passPhrase
	 * @param salt
	 * @return
	 * @throws icr.etherj.crypto.CryptoException
	 */
	public CryptoCodec createCryptoCodec(String passPhrase, byte[] salt)
		throws CryptoException
	{
		return cryptoFactory.createCryptoCodec(passPhrase, salt);
	}

	public CryptoCodec createCryptoCodec(String passPhrase, byte[] salt,
		int iterCount, int keyLength) throws CryptoException
	{
		return cryptoFactory.createCryptoCodec(passPhrase, salt, iterCount,
			keyLength);
	}

	public KeyStore createKeyStore()
		throws CryptoException
	{
		return cryptoFactory.createKeyStore();
	}

	public KeyStore createKeyStore(int saltLength, int iterCount,
		int keyLength) throws CryptoException
	{
		return cryptoFactory.createKeyStore(saltLength, iterCount, keyLength);
	}

	public byte[] createSalt()
	{
		return cryptoFactory.createSalt();
	}

	public byte[] createSalt(int length)
	{
		return cryptoFactory.createSalt(length);
	}

	public interface CryptoFactory
	{
		public CryptoCodec createCryptoCodec(String passPhrase, byte[] salt)
			throws CryptoException;

		public CryptoCodec createCryptoCodec(String passPhrase, byte[] salt,
			int iterCount, int keyLength) throws CryptoException;

		public KeyStore createKeyStore()
			throws CryptoException;

		public KeyStore createKeyStore(int saltLength, int iterCount, int keyLength)
			throws CryptoException;

		public byte[] createSalt();

		public byte[] createSalt(int length);

	}
}