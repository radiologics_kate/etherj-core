/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.crypto;

import com.google.common.io.BaseEncoding;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author jamesd
 */
class DefaultCryptoCodec implements CryptoCodec
{
	private final int BLOCKSIZE = 256;
	private final int LENGTH_BYTES = 4;

	private final BaseEncoding base64 = BaseEncoding.base64();
	private final Cipher cipher;
	private final byte[] salt;
	private final int iterCount;
	private final int keyLength;
	private final SecretKey key;
	private final SecureRandom sc = new SecureRandom();

	public DefaultCryptoCodec(String passPhrase, byte[] salt)
		throws CryptoException
	{
		this(passPhrase, salt, 8192, 128);
	}

	public DefaultCryptoCodec(String passPhrase, byte[] salt, int iterCount,
		int keyLength) throws CryptoException
	{
		this.salt = Arrays.copyOf(salt, salt.length);
		this.iterCount = iterCount;
		this.keyLength = keyLength;
		try
		{
			SecretKeyFactory factory = SecretKeyFactory.getInstance(
				"PBKDF2WithHmacSHA512");
			KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), this.salt,
				this.iterCount, this.keyLength);
			SecretKey tmp = factory.generateSecret(spec);
			key = new SecretKeySpec(tmp.getEncoded(), "AES");
			// ToDo: Update to JDK8 and use Authenticated Encryption mode instead
			// of CBC mode
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		}
		catch (InvalidKeySpecException ex)
		{
			throw new CryptoException(CryptoCode.InvalidKeySpecification, ex);
		}
		catch (NoSuchAlgorithmException ex)
		{
			throw new CryptoException(CryptoCode.NoSuchAlgorithm, ex);
		}
		catch (NoSuchPaddingException ex)
		{
			throw new CryptoException(CryptoCode.NoSuchPadding, ex);
		}
	}

	@Override
	public String decrypt(String cipherText) throws CryptoException
	{
		if (cipherText == null)
		{
			throw new CryptoException(CryptoCode.NullCipherText);
		}
		String[] tokens = cipherText.split(java.util.regex.Pattern.quote(DELIM));
		if (tokens.length != 2)
		{
			throw new CryptoException(CryptoCode.BadIvFormat);
		}
		String clearText;
		try
		{
			byte[] iv = base64.decode(tokens[0]);
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
			byte[] decryptedData = base64.decode(tokens[1]);
			byte[] utf8 = cipher.doFinal(decryptedData);
			clearText = unpackOutputBytes(utf8);
		}
		catch (InvalidKeyException ex)
		{
			throw new CryptoException(CryptoCode.InvalidKey, ex);
		}
		catch (InvalidAlgorithmParameterException ex)
		{
			throw new CryptoException(CryptoCode.InvalidAlgorithmParameter, ex);
		}
		catch (IllegalBlockSizeException ex)
		{
			throw new CryptoException(CryptoCode.IllegalBlockSize, ex);
		}
		catch (BadPaddingException ex)
		{
			throw new CryptoException(CryptoCode.BadPadding, ex);
		}
		catch (UnsupportedEncodingException ex)
		{
			throw new CryptoException(CryptoCode.UnsupportedEncoding, ex);
		}
		return clearText;
	}

	@Override
	public String encrypt(String clearText) throws CryptoException
	{
		String cipherText;
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, key);
			AlgorithmParameters params = cipher.getParameters();
			byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
			byte[] inputBytes = packInputBytes(clearText);
			byte[] utf8EncryptedData = cipher.doFinal(inputBytes);
			cipherText = new StringBuilder()
				.append(base64.encode(iv).trim())
				.append(DELIM)
				.append(base64.encode(utf8EncryptedData).trim())
				.toString();
		}
		catch (InvalidKeyException ex)
		{
			throw new CryptoException(CryptoCode.InvalidKey, ex);
		}
		catch (InvalidParameterSpecException ex)
		{
			throw new CryptoException(CryptoCode.InvalidParameterSpecification, ex);
		}
		catch (IllegalBlockSizeException ex)
		{
			throw new CryptoException(CryptoCode.IllegalBlockSize, ex);
		}
		catch (BadPaddingException ex)
		{
			throw new CryptoException(CryptoCode.BadPadding, ex);
		}
		return cipherText;
	}

	@Override
	public byte[] getSalt()
	{
		return salt;
	}

	@Override
	public int getIterationCount()
	{
		return iterCount;
	}

	@Override
	public int getKeyLength()
	{
		return keyLength;
	}

	private int byteArrayToInt(byte[] lengthArray)
	{
		return (((int)lengthArray[0]) << 24) +
			(((int)lengthArray[1]) << 16) +
			(((int)lengthArray[2]) << 8) +
			(int)lengthArray[3];
	}

	private byte[] intToByteArray(int value)
	{
		return new byte[]
		{
			(byte)(value >>> 24),
			(byte)(value >>> 16),
			(byte)(value >>> 8),
			(byte) value
		};
	}

	private byte[] packInputBytes(String clearText)
	{
		byte[] clearBytes = clearText.getBytes();
		// Don't leak the length of the clearText
		int nBytes = BLOCKSIZE*((clearBytes.length/BLOCKSIZE)+1)+LENGTH_BYTES;
		byte[] random = new byte[nBytes];
		byte[] packed = new byte[nBytes];
		// Generate random data
		sc.nextBytes(random);
		// First LENGTH_BYTES encode actual length of clearBytes
		System.arraycopy(
			intToByteArray(clearBytes.length), 0,
			packed, 0,
			LENGTH_BYTES);
		// Copy clearBytes
		System.arraycopy(
			clearBytes, 0,
			packed, LENGTH_BYTES,
			clearBytes.length);
		// Copy random bytes to approximate a constant time operation irrespective
		// of length of clearText. Mitigates timing attacks
		int nValid = LENGTH_BYTES+clearBytes.length;
		System.arraycopy(
			random, 0,
			packed, nValid,
			nBytes-nValid);

		return packed;
	}

	private String unpackOutputBytes(byte[] packed)
		throws UnsupportedEncodingException
	{
		byte[] lengthArray = new byte[LENGTH_BYTES];
		System.arraycopy(packed, 0,
			lengthArray, 0,
			LENGTH_BYTES);
		int nClear = byteArrayToInt(lengthArray);
		byte[] clearBytes = new byte[nClear];
		System.arraycopy(packed, LENGTH_BYTES,
			clearBytes, 0,
			nClear);
		return new String(clearBytes, "UTF8");
	}
	
}
