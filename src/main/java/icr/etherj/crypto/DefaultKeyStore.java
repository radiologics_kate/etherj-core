/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.crypto;

import com.google.common.io.BaseEncoding;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author jamesd
 */
class DefaultKeyStore implements KeyStore
{
	private static final String ALIAS_COUNT = "AliasCount";
	private static final String ALIAS = "Alias";
	private static final String ITER_COUNT = "IterCount";
	private static final String KEY = "Key";
	private static final String KEY_LENGTH = "KeyLength";
	private static final String PASSWORD_HASH = "PasswordHash";
	private static final String PASSWORD_SALT = "PasswordSalt";
	private static final String SALT = "Salt";
	private static final String SALT_LENGTH = "SaltLength";

	private final BaseEncoding base64 = BaseEncoding.base64();
	private final Map<String,Entry> entries = new HashMap<>();
	private int iterCount;
	private int keyLength;
	private final Entry nullEntry;
	private byte[] passwordSalt;
	private int saltLength;
	private final CryptoToolkit toolkit = CryptoToolkit.getToolkit();

	DefaultKeyStore()
	{
		this(64, 8192, 128);
	}

	DefaultKeyStore(int saltLength, int iterCount, int keyLength)
	{
		this.saltLength = saltLength;
		nullEntry = new Entry(new byte[saltLength], "");
		this.iterCount = iterCount;
		this.keyLength = keyLength;
		this.passwordSalt = toolkit.createSalt(saltLength);
	}

	@Override
	public void clear()
	{
		entries.clear();
	}

	@Override
	public String getKey(String alias)
	{
		return entries.getOrDefault(alias, nullEntry).getData();
	}

	@Override
	public void load(InputStream is, String password)
		throws IOException, CryptoException
	{
		Properties p = new Properties();
		p.load(is);
		int loadSaltLength = getIntProperty(p, SALT_LENGTH, 8);
		int loadIterCount = getIntProperty(p, ITER_COUNT, 1024);
		int loadKeyLength = getIntProperty(p, KEY_LENGTH, 128);
		byte[] loadPasswordSalt;
		try
		{
			loadPasswordSalt = base64.decode(getStringProperty(p, PASSWORD_SALT));
		}
		catch (IllegalArgumentException ex)
		{
			throw new CryptoException("Base64 encoding error",
				CryptoCode.FormatError, ex);
		}
		if (loadPasswordSalt.length != loadSaltLength)
		{
			throw new CryptoException(
				String.format("Salt length mismatch. Expected: %d Actual: %d",
					loadSaltLength, loadPasswordSalt.length),
				CryptoCode.FormatError);
		}
		checkPasswordHash(p, password, loadPasswordSalt);
		int loadAliasCount = getIntProperty(p, ALIAS_COUNT, 0);
		Map<String,Entry> loadEntries = new HashMap<>();
		for (int i=0; i<loadAliasCount; i++)
		{
			loadEntry(p, i, loadEntries, loadSaltLength, loadIterCount,
				loadKeyLength, password);
		}
		synchronized(this)
		{
			saltLength = loadSaltLength;
			iterCount = loadIterCount;
			keyLength = loadKeyLength;
			entries.clear();
			entries.putAll(loadEntries);
		}
	}

	@Override
	public void setKey(String alias, String key)
	{
		if ((alias == null) || alias.isEmpty())
		{
			throw new IllegalArgumentException("Null or empty alias not permitted");
		}
		if ((key == null) || key.isEmpty())
		{
			throw new IllegalArgumentException("Null or empty key not permitted");
		}
		entries.put(alias,
			new Entry(toolkit.createSalt(saltLength), key));
	}

	@Override
	public int size()
	{
		return entries.size();
	}

	@Override
	public void store(OutputStream os, String password)
		throws IOException, CryptoException
	{
		Properties p = new Properties();
		p.setProperty(SALT_LENGTH, String.valueOf(saltLength));
		p.setProperty(ITER_COUNT, String.valueOf(iterCount));
		p.setProperty(KEY_LENGTH, String.valueOf(keyLength));
		p.setProperty(PASSWORD_SALT, base64.encode(passwordSalt));
		byte[] hash = hashPassword(password, passwordSalt);
		p.setProperty(PASSWORD_HASH, base64.encode(hash));
		int nAlias = entries.size();
		p.setProperty(ALIAS_COUNT, String.valueOf(nAlias));
		int idx = 0;
		for (String alias : entries.keySet())
		{
			Entry entry = entries.get(alias);
			String stub = String.format("%s%d.", ALIAS, idx+1);
			p.setProperty(stub+ALIAS, alias);
			byte[] salt = entry.getSalt();
			p.setProperty(stub+SALT, base64.encode(salt));
			CryptoCodec codec = toolkit.createCryptoCodec(password, salt,
				iterCount, keyLength);
			p.setProperty(stub+KEY, codec.encrypt(entry.getData()));
			idx++;
		}
		p.store(os, "ether.crypto.DefaultKeyStore");
	}

	private void checkPasswordHash(Properties p, String password, byte[] salt)
		throws CryptoException
	{
		byte[] storedHash = base64.decode(getStringProperty(p, PASSWORD_HASH));
		byte[] hash = hashPassword(password, salt);
		if (!Arrays.equals(hash, storedHash))
		{
			throw new CryptoException(CryptoCode.BadPassword);
		}
	}

	private int getIntProperty(Properties p, String name, int minValue)
		throws CryptoException
	{
		int value = 0;
		try
		{
			value = Integer.parseInt(p.getProperty(name));
			if (value < minValue)
			{
				throw new CryptoException(
					String.format("Property '%s' must be >= %d", name, minValue),
					CryptoCode.FormatError);
			}
		}
		catch (NumberFormatException ex)
		{
			throw new CryptoException(String.format("Property '%s' invalid: "
					+p.getProperty(name), name),
				CryptoCode.FormatError, ex);
		}
		return value;
	}

	private String getStringProperty(Properties p, String name)
		throws CryptoException
	{
		String value = p.getProperty(name, "");
		if (value.isEmpty())
		{
			throw new CryptoException(
				String.format("Property '%s' missing or empty", name),
					CryptoCode.FormatError);
		}
		return value;
	}

	private byte[] hashPassword(String hashPassword, byte[] hashSalt)
		throws CryptoException
	{
		try
		{
			SecretKeyFactory skf = SecretKeyFactory.getInstance(
				"PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(hashPassword.toCharArray(), hashSalt,
				iterCount, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;
		}
		catch (InvalidKeySpecException ex)
		{
			throw new CryptoException(CryptoCode.InvalidKeySpecification, ex);
		}
		catch (NoSuchAlgorithmException ex)
		{
			throw new CryptoException(CryptoCode.NoSuchAlgorithm, ex);
		}
	}	

	private void loadEntry(Properties p, int idx, Map<String, Entry> loadEntries,
		int loadSaltLength, int loadIterCount, int loadKeyLength, String password)
		throws CryptoException
	{
		String stub = String.format("%s%d.", ALIAS, idx+1);
		String alias = getStringProperty(p, stub+ALIAS);
		byte[] salt = base64.decode(getStringProperty(p, stub+SALT));
		if (salt.length != loadSaltLength)
		{
			throw new CryptoException(
				String.format("Salt length mismatch. Expected: %d Actual: %d",
					loadSaltLength, salt.length),
				CryptoCode.FormatError);
		}
		CryptoCodec codec = toolkit.createCryptoCodec(password, salt,
			loadIterCount, loadKeyLength);
		String cipherText = getStringProperty(p, stub+KEY);
		String data = codec.decrypt(cipherText);
		loadEntries.put(alias, new Entry(salt, data));
	}

	private class Entry
	{
		protected final byte[] salt;
		protected final String data;

		public Entry(byte[] salt, String data)
		{
			this.salt = Arrays.copyOf(salt, salt.length);
			this.data = data;
		}

		/**
		 * @return the data
		 */
		public String getData()
		{
			return data;
		}

		/**
		 * @return the salt
		 */
		public byte[] getSalt()
		{
			return salt;
		}
	}
}
