/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.crypto;

import icr.etherj.AbstractExceptionCode;

/**
 *
 * @author jamesd
 */
public class CryptoCode extends AbstractExceptionCode
{
	public static final CryptoCode Success = new CryptoCode("Success", "00000");
	// 01 Underlying Java Crypto
	public static final CryptoCode InvalidKeySpecification = new CryptoCode(
		"Invalid key specification", "01001");
	public static final CryptoCode NoSuchAlgorithm = new CryptoCode(
		"No such algorithm", "01002");
	public static final CryptoCode NoSuchPadding = new CryptoCode(
		"No such padding", "01003");
	public static final CryptoCode InvalidKey = new CryptoCode("Invalid key",
		"01004");
	public static final CryptoCode InvalidAlgorithmParameter = new CryptoCode(
		"Invalid algorithm parameter", "01005");
	public static final CryptoCode IllegalBlockSize = new CryptoCode(
		"Illegal block size", "01006");
	public static final CryptoCode BadPadding = new CryptoCode(
		"Bad padding", "01007");
	public static final CryptoCode UnsupportedEncoding = new CryptoCode(
		"Unsupported encoding", "01008");
	public static final CryptoCode InvalidParameterSpecification =
		new CryptoCode("Invalid parameter specification", "01009");
	// 02 Codec
	public static final CryptoCode NullCipherText = new CryptoCode(
		"Null cipher text", "02001");
	public static final CryptoCode BadIvFormat = new CryptoCode("Bad IV format",
		"02002");
	// 08 - KeyStore
	public static final CryptoCode BadPassword = new CryptoCode("Bad password",
		"08001");
	public static final CryptoCode FormatError = new CryptoCode("Format error",
		"08002");

	private CryptoCode(String reason, String code)
	{
		super(reason, code);
	}
}
