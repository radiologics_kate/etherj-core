/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;

/**
 * RtRoiObservation from DICOM IOD.
 * @author jamesd
 */
public interface RtRoiObservation extends Displayable, Validatable
{
	/** RT ROI interpreted type from DICOM IOD: External patient contour. */
	public static final String External = "EXTERNAL";
	/** 
	 * RT ROI interpreted type from DICOM IOD: Planning Target Volume (as defined
	 * in ICRU50).
	 */
	public static final String PTV = "PTV";
	/**
	 * RT ROI interpreted type from DICOM IOD: Clinical Target Volume (as defined
	 * in ICRU50).
	 */
	public static final String CTV = "CTV";
	/**
	 * RT ROI interpreted type from DICOM IOD: Gross Tumor Volume (as defined in 
	 * ICRU50).
	 */
	public static final String GTV = "GTV";
	/** 
	 * RT ROI interpreted type from DICOM IOD: Treated Volume (as defined in 
	 * ICRU50).
	 */
	public static final String TreatedVolume = "TREATED_VOLUME";
	/** 
	 * RT ROI interpreted type from DICOM IOD: Irradiated Volume (as defined in 
	 * ICRU50).
	 */
	public static final String IrradVolume = "IRRAD_VOLUME";
	/**
	 * RT ROI interpreted type from DICOM IOD: Patient bolus to be used for
	 * external beam therapy.
	 */
	public static final String Bolus = "BOLUS";
	/**
	 * RT ROI interpreted type from DICOM IOD: Region in which dose is to be 
	 * minimised.
	 */
	public static final String Avoidance = "AVOIDANCE";
	/** RT ROI interpreted type from DICOM IOD: Patient organ. */
	public static final String Organ = "ORGAN";
	/**
	 * RT ROI interpreted type from DICOM IOD: Patient marker or marker on a 
	 * localiser.
	 */
	public static final String Marker = "MARKER";
	/** RT ROI interpreted type from DICOM IOD: Registration ROI. */
	public static final String Registration = "REGISTRATION";
	/**
	 * RT ROI interpreted type from DICOM IOD: Treatment isocenter to be used for
	 * external beam therapy.
	 */
	public static final String Isocentre = "ISOCENTER";
	/**
	 * RT ROI interpreted type from DICOM IOD: Volume into which a contrast agent
	 * has been injected.
	 */
	public static final String ContrastAgent = "CONTRAST_AGENT";
	/** RT ROI interpreted type from DICOM IOD: Patient anatomical cavity. */
	public static final String Cavity = "CAVITY";
	/** RT ROI interpreted type from DICOM IOD: Brachytherapy channel. */
	public static final String BrachyChannel = "BRACHY_CHANNEL";
	/** RT ROI interpreted type from DICOM IOD: Brachytherapy accessory device. */
	public static final String BrachyAccessory = "BRACHY_ACCESSORY";
	/** RT ROI interpreted type from DICOM IOD: Brachytherapy source applicator. */
	public static final String BrachySourceApplicator = "BRACHY_SRC_APP";
	/** RT ROI interpreted type from DICOM IOD: Brachytherapy channel shield. */
	public static final String BrachyChannelShield = "BRACHY_CHNL_SHLD";
	/** RT ROI interpreted type from DICOM IOD: External patient support device. */
	public static final String Support = "SUPPORT";
	/**
	 * RT ROI interpreted type from DICOM IOD: External patient fixation or
	 * immobilisation device.
	 */
	public static final String Fixation = "FIXATION";
	/**
	 * RT ROI interpreted type from DICOM IOD: ROI to be used as a dose reference.
	 */
	public static final String DoseRegion = "DOSE_REGION";
	/**
	 * RT ROI interpreted type from DICOM IOD: ROI to be used in control of dose
	 * optimisation and calculation.
	 */
	public static final String Control = "CONTROL";

	/**
	 * Returns the observation number uniquely identifying it within the list of
	 * observations, type 1.
	 * @return the observation number
	 */
	int getObservationNumber();

	/**
	 * Returns the referenced ROI number, type 1.
	 * @return the ROI number
	 */
	int getReferencedRoiNumber();

	/**
	 * Returns the ROI interpreter, type 2.
	 * @return the ROI interpreter
	 */
	String getRoiInterpreter();

	/**
	 * Returns the observation description, type 3.
	 * @return the observation description
	 */
	String getRoiObservationDescription();

	/**
	 * Returns the observation label, type 3.
	 * @return the observation label
	 */
	String getRoiObservationLabel();

	/**
	 * Returns the RT ROI interpreted type, type 2.
	 * @return ROI interpreted type
	 */
	String getRtRoiIntepretedType();

	/**
	 * Sets the observation number uniquely identifying it within the list of
	 * observations, type 1.
	 * @param number the observation number to set
	 */
	void setObservationNumber(int number);

	/**
	 * Sets the referenced ROI number, type 1.
	 * @param number the ROI number to set
	 */
	void setReferencedRoiNumber(int number);

	/**
	 * Sets the ROI interpreter, type 2.
	 * @param interpreter the ROI interpreter to set
	 */
	void setRoiInterpreter(String interpreter);

	/**
	 * Sets the ROI observation description, type 3.
	 * @param description the observation description to set
	 */
	void setRoiObservationDescription(String description);

	/**
	 * Sets the ROI observation label, type 3.
	 * @param label the observation label to set
	 */
	void setRoiObservationLabel(String label);

	/**
	 * Sets the RT ROI interpreted type, type 2, defined terms:
	 * {@link RtRoiObservation#External},
	 * {@link RtRoiObservation#PTV},
	 * {@link RtRoiObservation#CTV},
	 * {@link RtRoiObservation#GTV},
	 * {@link RtRoiObservation#TreatedVolume},
	 * {@link RtRoiObservation#IrradVolume},
	 * {@link RtRoiObservation#Bolus},
	 * {@link RtRoiObservation#Avoidance},
	 * {@link RtRoiObservation#Organ},
	 * {@link RtRoiObservation#Marker},
	 * {@link RtRoiObservation#Registration},
	 * {@link RtRoiObservation#Isocentre},
	 * {@link RtRoiObservation#ContrastAgent},
	 * {@link RtRoiObservation#Cavity},
	 * {@link RtRoiObservation#BrachyChannel},
	 * {@link RtRoiObservation#BrachyAccessory},
	 * {@link RtRoiObservation#BrachySourceApplicator},
	 * {@link RtRoiObservation#BrachyChannelShield},
	 * {@link RtRoiObservation#Support},
	 * {@link RtRoiObservation#Fixation},
	 * {@link RtRoiObservation#DoseRegion},
	 * {@link RtRoiObservation#Control}.
	 * 
	 * @param type the interpreted type to set
	 * @throws IllegalArgumentException if the type is not one of the defined terms
	 */
	void setRtRoiInterpretedType(String type) throws IllegalArgumentException;

}
