/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;

/**
 *
 * @author jamesd
 */
public class DefaultEnhancedGeneralEquipmentModule
	extends DefaultGeneralEquipmentModule
	implements EnhancedGeneralEquipmentModule
{
	@Override
	public void setDeviceSerialNumber(String deviceSerial)
		throws IllegalArgumentException
	{
		if ((deviceSerial == null) || (deviceSerial.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Device serial number must not be null or empty");
		}
		super.setDeviceSerialNumber(deviceSerial);
	}

	@Override
	public void setManufacturer(String manufacturer)
		throws IllegalArgumentException
	{
		if ((manufacturer == null) || (manufacturer.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Manufacturer must not be null or empty");
		}
		super.setManufacturer(manufacturer);
	}

	@Override
	public void setManufacturersModelName(String modelName)
		throws IllegalArgumentException
	{
		if ((modelName == null) || (modelName.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Model name must not be null or empty");
		}
		super.setManufacturersModelName(modelName);
	}

	@Override
	public void setSoftwareVersion(String softwareVersion)
		throws IllegalArgumentException
	{
		if ((softwareVersion == null) || (softwareVersion.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Software version must not be null or empty");
		}
		super.setSoftwareVersion(softwareVersion);
	}
	
}
