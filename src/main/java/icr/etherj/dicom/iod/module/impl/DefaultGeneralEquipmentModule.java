/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.MathUtils;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralEquipmentModule extends AbstractDisplayable
	implements GeneralEquipmentModule
{
	private String deviceSerial;
	private String manufacturer = "";
	private String modelName;
	private int pixPadding = Integer.MIN_VALUE;
	private String softwareVersion;
	private double spatialRes = Double.NaN;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if ((manufacturer != null) && !manufacturer.isEmpty())
		{
			ps.println(pad+"Manufacturer: "+manufacturer);
		}
		if ((modelName != null) && !modelName.isEmpty())
		{
			ps.println(pad+"ManufacturerModelName: "+modelName);
		}
		if ((deviceSerial != null) && !deviceSerial.isEmpty())
		{
			ps.println(pad+"DeviceSerialNumber: "+deviceSerial);
		}
		if ((softwareVersion != null) && !softwareVersion.isEmpty())
		{
			ps.println(pad+"SoftwareVersion: "+softwareVersion);
		}
		if (MathUtils.isFinite(spatialRes))
		{
			ps.println(pad+"SpatialResolution: "+spatialRes);
		}
		if (pixPadding != Integer.MIN_VALUE)
		{
			ps.println(pad+"PixelPaddingValue: "+pixPadding);
		}
	}

	@Override
	public String getDeviceSerialNumber()
	{
		return deviceSerial;
	}

	@Override
	public String getManufacturer()
	{
		return manufacturer;
	}

	@Override
	public String getManufacturersModelName()
	{
		return modelName;
	}

	@Override
	public int getPixelPaddingValue()
	{
		return pixPadding;
	}

	@Override
	public String getSoftwareVersion()
	{
		return softwareVersion;
	}

	@Override
	public double getSpatialResolution()
	{
		return spatialRes;
	}

	@Override
	public void setDeviceSerialNumber(String deviceSerial)
	{
		this.deviceSerial = deviceSerial;
	}

	@Override
	public void setManufacturer(String manufacturer)
	{
		this.manufacturer = (manufacturer != null) ? manufacturer : "";
	}

	@Override
	public void setManufacturersModelName(String modelName)
	{
		this.modelName = modelName;
	}

	@Override
	public void setPixelPaddingValue(int value)
	{
		// PixelPaddingValue VR is US or SS, unset if out of range.
		if ((value < Short.MIN_VALUE) || (value >= (2 << 15)))
		{
			pixPadding = Integer.MIN_VALUE;
			return;
		}
		pixPadding = value;
	}

	@Override
	public void setSoftwareVersion(String softwareVersion)
	{
		this.softwareVersion = softwareVersion;
	}

	@Override
	public void setSpatialResolution(double resolution)
	{
		if (!MathUtils.isFinite(resolution) || (resolution <= 0))
		{
			spatialRes = Double.NaN;
			return;
		}
		spatialRes = resolution;
	}

	@Override
	public boolean validate()
	{
		String clazz = "GeneralEquipmentModule";
		boolean manuOk = Validation.type2(clazz, manufacturer, Tag.Manufacturer);

		return manuOk;
	}
	
}
