/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.CodingSchemeIdentifier;
import java.util.List;

/**
 * SopCommon module in DICOM IOD.
 * @author jamesd
 */
public interface SopCommonModule extends Displayable
{
	CodingSchemeIdentifier addCodingSchemeIdentifier(CodingSchemeIdentifier csi);

	List<CodingSchemeIdentifier> getCodingSchemeIdentifierList();

	/**
	 * Returns the content qualification, type 3. Enumerated values: PRODUCT,
	 * RESEARCH, SERVICE.
	 * @return
	 */
	String getContentQualification();

	/**
	 * Returns the instance number, type 3.
	 * @return
	 */
	int getInstanceNumber();

	/**
	 * Returns longitudinal temporal information modified, type 3. Enumerated
	 * values: UNMODIFIED, MODIFIED, REMOVED.
	 * @return
	 */
	String getLongitudinalTemporalInformationModified();

	/**
	 * Returns the SOP class UID, type 1.
	 * @return
	 */
	String getSopClassUid();

	/**
	 * Returns the SOP instance UID, type 1.
	 * @return
	 */
	String getSopInstanceUid();

	/**
	 * Sets the content qualification, type 3. Enumerated values: PRODUCT,
	 * RESEARCH, SERVICE.
	 * @param qualification
	 * @throws IllegalArgumentException
	 */
	void setContentQualification(String qualification)
		throws IllegalArgumentException;

	/**
	 * Sets the longitudinal temporal information modified, type 3. Enumerated
	 * values: UNMODIFIED, MODIFIED, REMOVED.
	 * @param value
	 * @throws IllegalArgumentException
	 */
	void setLongitudinalTemporalInformationModified(String value)
		throws IllegalArgumentException;

	/**
	 * Sets the SOP instance UID, type 1.
	 * @param uid
	 */
	void setSopInstanceUid(String uid) throws IllegalArgumentException;

	/**
	 *	Set the instance number, type 3.
	 * @param number
	 */
	void setInstanceNumber(int number);
}
