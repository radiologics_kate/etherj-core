/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;

/**
 * GeneralEquipment module in DICOM IOD.
 * @author jamesd
 */
public interface GeneralEquipmentModule extends Displayable, Validatable
{

	/**
	 * Returns the device serial number, type 3.
	 * @return
	 */
	String getDeviceSerialNumber();

	/**
	 * Returns the manufacturer, type 2.
	 * @return
	 */
	String getManufacturer();

	/**
	 * Returns the manufacturer's model name, type 3.
	 * @return
	 */
	String getManufacturersModelName();

	/**
	 * Returns the pixel padding value, type 1C.
	 * @return
	 */
	int getPixelPaddingValue();

	/**
	 * Returns the software version, type 3.
	 * @return
	 */
	String getSoftwareVersion();

	/**
	 * Returns the spatial resolution, type 3.
	 * @return
	 */
	double getSpatialResolution();

	/**
	 * Sets the device serial number, type 3.
	 * @param deviceSerial
	 */
	void setDeviceSerialNumber(String deviceSerial);

	/**
	 * Sets the manufacturer, type 2.
	 * @param manufacturer
	 */
	void setManufacturer(String manufacturer);

	/**
	 * Sets the pixel padding value, type 1C.
	 * @param value
	 */
	void setPixelPaddingValue(int value);

	/**
	 * Sets the manufacturer's model name, type 3.
	 * @param modelName
	 */
	void setManufacturersModelName(String modelName);

	/**
	 * Sets the software version, type 3.
	 * @param softwareVersion
	 */
	void setSoftwareVersion(String softwareVersion);

	/**
	 * Sets the spatial resolution, type 3.
	 * @param resolution
	 */
	void setSpatialResolution(double resolution);
}
