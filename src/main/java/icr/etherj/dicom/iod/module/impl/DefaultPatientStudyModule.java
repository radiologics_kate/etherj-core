/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.MathUtils;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultPatientStudyModule extends AbstractDisplayable
	implements PatientStudyModule
{
	private String age = null;
	private double size = Double.NaN;
	private double weight = Double.NaN;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if ((age != null) && !age.isEmpty())
		{
			ps.println(pad+"PatientAge: "+age);
		}
		if (MathUtils.isFinite(size))
		{
			ps.println(pad+"PatientSize: "+size);
		}
		if (MathUtils.isFinite(weight))
		{
			ps.println(pad+"PatientWeight: "+weight);
		}
	}

	@Override
	public String getPatientAge()
	{
		return age;
	}

	@Override
	public double getPatientSize()
	{
		return size;
	}

	@Override
	public double getPatientWeight()
	{
		return weight;
	}

	@Override
	public void setPatientAge(String age) throws IllegalArgumentException
	{
		if ((age == null) || age.isEmpty())
		{
			this.age = null;
			return;
		}
		if (!DicomUtils.isValidAgeString(age))
		{
			throw new IllegalArgumentException("Invalid age: "+age);
		}
		this.age = age;
	}

	@Override
	public void setPatientSize(double size)
	{
		if (!MathUtils.isFinite(size) || (size <= 0))
		{
			this.size = Double.NaN;
			return;
		}
		this.size = size;
	}

	@Override
	public void setPatientWeight(double weight)
	{
		if (!MathUtils.isFinite(weight) || (weight <= 0))
		{
			this.weight = Double.NaN;
			return;
		}
		this.weight = weight;
	}
	
}
