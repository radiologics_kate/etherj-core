/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.MathUtils;
import icr.etherj.StringUtils;
import icr.etherj.dicom.Coordinate2D;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.CodingSchemeIdentifier;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.ContentRelationship;
import icr.etherj.dicom.iod.ContentTemplate;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.ContourImageContainer;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.FrameContent;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.HeirarchicalSeriesReference;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultContainerItem;
import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultCodeItem;
import icr.etherj.dicom.iod.impl.DefaultCodingSchemeIdentifier;
import icr.etherj.dicom.iod.impl.DefaultContentRelationship;
import icr.etherj.dicom.iod.impl.DefaultContentTemplate;
import icr.etherj.dicom.iod.impl.DefaultContour;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultDerivationImage;
import icr.etherj.dicom.iod.impl.DefaultDimensionIndex;
import icr.etherj.dicom.iod.impl.DefaultFrameContent;
import icr.etherj.dicom.iod.impl.DefaultHeirarchicalSeriesReference;
import icr.etherj.dicom.iod.impl.DefaultHeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.impl.DefaultImageItem;
import icr.etherj.dicom.iod.impl.DefaultNumericItem;
import icr.etherj.dicom.iod.impl.DefaultPersonNameItem;
import icr.etherj.dicom.iod.impl.DefaultPixelMeasures;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultReferencedInstance;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedStudy;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import icr.etherj.dicom.iod.impl.DefaultSourceImage;
import icr.etherj.dicom.iod.impl.DefaultSpatialCoordItem;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultSegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.impl.DefaultSegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultTextItem;
import icr.etherj.dicom.iod.impl.DefaultUidRefItem;
import icr.etherj.dicom.iod.impl.GenericContentItem;
import icr.etherj.dicom.iod.module.impl.DefaultCommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultEnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.impl.DefaultFrameOfReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralImageModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultImagePixelModule;
import icr.etherj.dicom.iod.module.impl.DefaultMultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultRoiContourModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtRoiObservationsModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultSopCommonModule;
import java.util.List;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory and utilities class for modules from DICOM IOD.
 * @author jamesd
 */
public class Modules
{
	private static final Logger logger = LoggerFactory.getLogger(Modules.class);

	/**
	 * Returns a new CommonInstanceReferenceModule.
	 * @return the new module
	 */
	public static CommonInstanceReferenceModule commonInstanceReferenceModule()
	{
		return new DefaultCommonInstanceReferenceModule();
	}

	/**
	 * Returns a new EnhancedGeneralEquipmentModule.
	 * @return the new module
	 */
	public static EnhancedGeneralEquipmentModule enhancedGeneralEquipmentModule()
	{
		return new DefaultEnhancedGeneralEquipmentModule();
	}

	/**
	 * Returns a new FrameOfReferenceModule.
	 * @return the new module
	 */
	public static FrameOfReferenceModule frameOfReferenceModule()
	{
		return new DefaultFrameOfReferenceModule();
	}

	/**
	 * Returns a new GeneralEquipmentModule.
	 * @return the new module
	 */
	public static GeneralEquipmentModule generalEquipmentModule()
	{
		return new DefaultGeneralEquipmentModule();
	}

	/**
	 * Returns a new GeneralImageModule.
	 * @return the new module
	 */
	public static GeneralImageModule generalImageModule()
	{
		return new DefaultGeneralImageModule();
	}

	/**
	 * Returns a new GeneralSeriesModule.
	 * @return the new module
	 */
	public static GeneralSeriesModule generalSeriesModule()
	{
		return new DefaultGeneralSeriesModule();
	}

	/**
	 * Returns a new GeneralStudyModule.
	 * @return the new module
	 */
	public static GeneralStudyModule generalStudyModule()
	{
		return new DefaultGeneralStudyModule();
	}

	/**
	 * Returns a new ImagePixelModule.
	 * @return the new module
	 */
	public static ImagePixelModule imagePixelModule()
	{
		return new DefaultImagePixelModule();
	}

	/**
	 * Returns a new MultiframeFunctionalGroupsModule.
	 * @param sopClassUid
	 * @return the new module
	 * @throws IllegalArgumentException
	 */
	public static MultiframeFunctionalGroupsModule multiframeFunctionalGroupsModule(
		String sopClassUid) throws IllegalArgumentException
	{
		SharedFunctionalGroups sharedGroups = null;
		PerFrameFunctionalGroups perFrameGroups = null;
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				sharedGroups = new DefaultSegmentationSharedFunctionalGroups();
				perFrameGroups = new DefaultSegmentationPerFrameFunctionalGroups();
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID: "+sopClassUid);
		}
		return new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
			sharedGroups, perFrameGroups);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialSeriesModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialStudyModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		String id = module.getClinicalTrialTimePointId();
		if (id == null)
		{
			return;
		}
		dcm.putString(Tag.ClinicalTrialTimePointID, VR.LO, id);
		String desc = module.getClinicalTrialTimePointDescription();
		if (desc != null)
		{
			dcm.putString(Tag.ClinicalTrialTimePointDescription, VR.ST, desc);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialSubjectModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		String sponsor = module.getClinicalTrialSponsorName();
		if (sponsor == null)
		{
			return;
		}
		dcm.putString(Tag.ClinicalTrialSponsorName, VR.LO,
			module.getClinicalTrialSponsorName());
		dcm.putString(Tag.ClinicalTrialProtocolID, VR.LO,
			module.getClinicalTrialProtocolId());
		dcm.putString(Tag.ClinicalTrialProtocolName, VR.LO,
			module.getClinicalTrialProtocolName());
		dcm.putString(Tag.ClinicalTrialSiteID, VR.LO,
			module.getClinicalTrialSiteId());
		dcm.putString(Tag.ClinicalTrialSiteName, VR.LO,
			module.getClinicalTrialSiteName());
		String subjId = module.getClinicalTrialSubjectId();
		if (subjId != null)
		{
			dcm.putString(Tag.ClinicalTrialSubjectID, VR.LO, subjId);
		}
		else
		{
			String readId = module.getClinicalTrialSubjectReadingId();
			if (readId == null)
			{
				throw new IllegalArgumentException(
					"Subject ID and subject reading ID cannot both be null");
			}
			dcm.putString(Tag.ClinicalTrialSubjectReadingID, VR.LO, readId);
		}	
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(CommonInstanceReferenceModule module,
		DicomObject dcm)
	{
		List<ReferencedSeries> refSeriesList = module.getReferencedSeriesList();
		if (refSeriesList.isEmpty())
		{
			return;
		}
		dcm.putSequence(Tag.ReferencedSeriesSequence);
		DicomElement refSerSq = dcm.get(Tag.ReferencedSeriesSequence);
		for (ReferencedSeries refSeries : refSeriesList)
		{
			DicomObject item = new BasicDicomObject();
			packReferencedSeries(refSeries, item);
			refSerSq.addDicomObject(item);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(EnhancedGeneralEquipmentModule module,
		DicomObject dcm)
	{
		pack((GeneralEquipmentModule) module, dcm);
		dcm.putString(Tag.Manufacturer, VR.LO, module.getManufacturer());
		dcm.putString(Tag.ManufacturerModelName, VR.LO, 
			module.getManufacturersModelName());
		dcm.putString(Tag.DeviceSerialNumber, VR.LO, module.getDeviceSerialNumber());
		dcm.putString(Tag.SoftwareVersions, VR.LO, module.getSoftwareVersion());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(FrameOfReferenceModule module, DicomObject dcm)
	{
		String forUid = module.getFrameOfReferenceUid();
		if (forUid == null)
		{
			return;
		}
		dcm.putString(Tag.FrameOfReferenceUID, VR.UI, forUid);
		dcm.putString(Tag.PositionReferenceIndicator, VR.LO,
			module.getPositionReferenceIndicator());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralEquipmentModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Manufacturer, VR.LO, module.getManufacturer());
		String modelName = module.getManufacturersModelName();
		if ((modelName != null) && !modelName.isEmpty())
		{
			dcm.putString(Tag.ManufacturerModelName, VR.LO, modelName);
		}
		String serial = module.getDeviceSerialNumber();
		if ((serial != null) && !serial.isEmpty())
		{
			dcm.putString(Tag.DeviceSerialNumber, VR.LO, serial);
		}
		String software = module.getSoftwareVersion();
		if ((software != null) && !software.isEmpty())
		{
			dcm.putString(Tag.SoftwareVersions, VR.LO, software);
		}
		double spatialRes = module.getSpatialResolution();
		if (MathUtils.isFinite(spatialRes))
		{
			dcm.putDouble(Tag.SpatialResolution, VR.DS, spatialRes);
		}
		int pixPadding = module.getPixelPaddingValue();
		if (pixPadding != Integer.MIN_VALUE)
		{
			dcm.putInt(Tag.PixelPaddingValue, VR.US, pixPadding);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralImageModule module, DicomObject dcm)
	{
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		putIfExists(module.getPatientOrientation(), dcm, Tag.PatientOrientation,
			VR.CS);
		putIfExists(module.getContentDate(), dcm, Tag.ContentDate, VR.DA);
		putIfExists(module.getContentTime(), dcm, Tag.ContentTime, VR.TM);
		putIfExists(module.getImageType(), dcm, Tag.ImageType, VR.CS);
		putIfExists(module.getAcquisitionDate(), dcm, Tag.AcquisitionDate, VR.DA);
		putIfExists(module.getAcquisitionTime(), dcm, Tag.AcquisitionTime, VR.TM);
		putIfExists(module.getImageComments(), dcm, Tag.ImageComments, VR.LT);
		putIfExists(module.getBurnedInAnnotation(), dcm, Tag.BurnedInAnnotation,
			VR.CS);
		if (module.getLossyImageCompression())
		{
			dcm.putString(Tag.LossyImageCompression, VR.CS, "01");
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralReferenceModule module, DicomObject dcm)
	{
		List<ReferencedInstance> refImages = module.getReferencedImageList();
		if (!refImages.isEmpty())
		{
			dcm.putSequence(Tag.ReferencedImageSequence);
			DicomElement refSq = dcm.get(Tag.ReferencedImageSequence);
			for (ReferencedInstance inst : refImages)
			{
				packReferencedInstance(inst, refSq);
			}
		}
		putIfExists(module.getDerivationDescription(), dcm,
			Tag.DerivationDescription, VR.ST);
		List<Code> derivCodes = module.getDerivationCodeList();
		if (!derivCodes.isEmpty())
		{
			dcm.putSequence(Tag.DerivationCodeSequence);
			DicomElement codeSq = dcm.get(Tag.DerivationCodeSequence);
			for (Code code : derivCodes)
			{
				packCode(code, codeSq);
			}
		}
		List<SourceImage> sourceImages = module.getSourceImageList();
		if (!sourceImages.isEmpty())
		{
			dcm.putSequence(Tag.SourceImageSequence);
			DicomElement srcSq = dcm.get(Tag.SourceImageSequence);
			for (SourceImage image : sourceImages)
			{
				packSourceImage(image, srcSq);
			}
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralSeriesModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		int number = module.getSeriesNumber();
		dcm.putString(Tag.SeriesNumber, VR.IS,
			(number > 0) ? Integer.toString(number) : "");
		putIfExists(module.getLaterality(), dcm, Tag.Laterality, VR.CS);
		putIfExists(module.getSeriesDate(), dcm, Tag.SeriesDate, VR.DA);
		putIfExists(module.getSeriesTime(), dcm, Tag.SeriesTime, VR.TM);
		putIfExists(module.getSeriesDescription(), dcm, Tag.SeriesDescription,
			VR.LO);
		putIfExists(module.getOperatorsName(), dcm, Tag.OperatorsName, VR.PN);
		putIfExists(module.getProtocolName(), dcm, Tag.ProtocolName, VR.LO);
		putIfExists(module.getPatientPosition(), dcm, Tag.PatientPosition, VR.CS);
		int minPix = module.getSmallestPixelValueInSeries();
		if (minPix >= 0)
		{
			dcm.putInt(Tag.SmallestPixelValueInSeries, VR.US, minPix);
		}
		int maxPix = module.getLargestPixelValueInSeries();
		if (maxPix >= 0)
		{
			dcm.putInt(Tag.LargestPixelValueInSeries, VR.US, maxPix);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralStudyModule module, DicomObject dcm)
	{
		dcm.putString(Tag.StudyInstanceUID, VR.UI, module.getStudyInstanceUid());
		dcm.putString(Tag.StudyDate, VR.DA, module.getStudyDate());
		dcm.putString(Tag.StudyTime, VR.TM, module.getStudyTime());
		dcm.putString(Tag.ReferringPhysicianName, VR.PN,
			module.getReferringPhysicianName());
		dcm.putString(Tag.StudyID, VR.SH, module.getStudyId());
		dcm.putString(Tag.AccessionNumber, VR.SH, module.getAccessionNumber());
		putIfExists(module.getStudyDescription(), dcm, Tag.StudyDescription, VR.LO);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(ImagePixelModule module, DicomObject dcm)
	{
		dcm.putBytes(Tag.PixelData, VR.OB, module.getPixelData());
		dcm.putInt(Tag.SamplesPerPixel, VR.US, module.getSamplesPerPixel());
		dcm.putString(Tag.PhotometricInterpretation, VR.CS,
			module.getPhotometricInterpretation());
		dcm.putInt(Tag.Rows, VR.US, module.getRowCount());
		dcm.putInt(Tag.Columns, VR.US, module.getColumnCount());
		dcm.putInt(Tag.BitsAllocated, VR.US, module.getBitsAllocated());
		dcm.putInt(Tag.BitsStored, VR.US, module.getBitsStored());
		dcm.putInt(Tag.HighBit, VR.US, module.getHighBit());
		dcm.putInt(Tag.PixelRepresentation, VR.US, module.getPixelRepresentation());
		if (module.getSamplesPerPixel() > 1)
		{
			dcm.putInt(Tag.PlanarConfiguration, VR.US,
				module.getPlanarConfiguration());
		}
		int value = module.getSmallestPixelValue();
		if (value >= 0)
		{
			dcm.putInt(Tag.SmallestImagePixelValue, VR.US, value);
		}
		value = module.getLargestPixelValue();
		if (value >= 0)
		{
			dcm.putInt(Tag.LargestImagePixelValue, VR.US, value);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param parentDcm the target
	 */
	public static void pack(MultiframeDimensionModule module,
		DicomObject parentDcm)
	{
		parentDcm.putSequence(Tag.DimensionOrganizationSequence);
		DicomElement dimOrgSq = parentDcm.get(Tag.DimensionOrganizationSequence);
		for (String uid : module.getDimensionOrganisationUidList())
		{
			DicomObject dcm = new BasicDicomObject();
			dcm.putString(Tag.DimensionOrganizationUID, VR.UI, uid);
			dimOrgSq.addDicomObject(dcm);
		}
		putIfExists(module.getDimensionOrganisationType(), parentDcm,
			Tag.DimensionOrganizationType, VR.CS);

		parentDcm.putSequence(Tag.DimensionIndexSequence);
		DicomElement dimIdxSq = parentDcm.get(Tag.DimensionIndexSequence);
		for (DimensionIndex idx : module.getDimensionIndexList())
		{
			packDimensionIndex(idx, dimIdxSq);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(MultiframeFunctionalGroupsModule module, DicomObject dcm)
	{
		String sopClassUid = module.getSopClassUid();
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				packSegmentationStorage(
					(SegmentationMultiframeFunctionalGroupsModule) module, dcm);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID");
		}
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		dcm.putString(Tag.ContentDate, VR.DA, module.getContentDate());
		dcm.putString(Tag.ContentTime, VR.TM, module.getContentTime());
		dcm.putInt(Tag.NumberOfFrames, VR.IS, module.getNumberOfFrames());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(PatientModule module, DicomObject dcm)
	{
		dcm.putString(Tag.PatientName, VR.PN, module.getPatientName());
		dcm.putString(Tag.PatientID, VR.LO, module.getPatientId());
		dcm.putString(Tag.PatientBirthDate, VR.DA, module.getPatientBirthDate());
		String sex = module.getPatientSex();
		if ((sex != null) && !sex.isEmpty())
		{
			dcm.putString(Tag.PatientSex, VR.CS, sex);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(PatientStudyModule module, DicomObject dcm)
	{
		putIfExists(module.getPatientAge(), dcm, Tag.PatientAge, VR.AS);
		putIfFinite(module.getPatientSize(), dcm, Tag.PatientSize, VR.DS);
		putIfFinite(module.getPatientWeight(), dcm, Tag.PatientWeight, VR.DS);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if no contours found
	 */
	public static void pack(RoiContourModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		List<RoiContour> list = module.getRoiContourList();
		if (list.size() < 1)
		{
			throw new IllegalArgumentException("No RoiContours present");
		}
		dcm.putSequence(Tag.ROIContourSequence);
		DicomElement rcSq = dcm.get(Tag.ROIContourSequence);
		for (RoiContour rc : list)
		{
			DicomObject rcItem = new BasicDicomObject();
			rcItem.putInt(Tag.ReferencedROINumber, VR.IS,
				rc.getReferencedRoiNumber());
			int[] rgb = rc.getRoiDisplayColour();
			if ((rgb != null) && (rgb.length == 3))
			{
				rcItem.putInts(Tag.ROIDisplayColor, VR.IS, rgb);
			}
			rcSq.addDicomObject(rcItem);
			List<Contour> conList = rc.getContourList();
			if (conList.isEmpty())
			{
				continue;
			}
			packContours(conList, rcItem);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if no observations found
	 */
	public static void pack(RtRoiObservationsModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		List<RtRoiObservation> list = module.getRtRoiObservationList();
		if (list.isEmpty())
		{
			throw new IllegalArgumentException("No RtRoiObservations present");
		}
		dcm.putSequence(Tag.RTROIObservationsSequence);
		DicomElement obsSq = dcm.get(Tag.RTROIObservationsSequence);
		for (RtRoiObservation obs : list)
		{
			DicomObject obsItem = new BasicDicomObject();
			obsItem.putInt(Tag.ObservationNumber, VR.IS,
				obs.getObservationNumber());
			obsItem.putInt(Tag.ReferencedROINumber, VR.IS,
				obs.getReferencedRoiNumber());
			String label = obs.getRoiObservationLabel();
			if ((label != null) && !label.isEmpty())
			{
				obsItem.putString(Tag.ROIObservationLabel, VR.SH, label);
			}
			String desc = obs.getRoiObservationDescription();
			if ((desc != null) && !desc.isEmpty())
			{
				obsItem.putString(Tag.ROIObservationDescription, VR.ST, desc);
			}
			obsItem.putString(Tag.RTROIInterpretedType, VR.CS,
				obs.getRtRoiIntepretedType());
			obsItem.putString(Tag.ROIInterpreter, VR.PN, obs.getRoiInterpreter());
			obsSq.addDicomObject(obsItem);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(RtSeriesModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		int number = module.getSeriesNumber();
		dcm.putString(Tag.SeriesNumber, VR.IS,
			(number > 0) ? Integer.toString(number) : "");
		String date = module.getSeriesDate();
		if ((date != null) && !date.isEmpty())
		{
			dcm.putString(Tag.SeriesDate, VR.DA, date);
		}
		String time = module.getSeriesTime();
		if ((time != null) && !time.isEmpty())
		{
			dcm.putString(Tag.SeriesTime, VR.TM, time);
		}
		String desc = module.getSeriesDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.SeriesDescription, VR.LO, desc);
		}
		dcm.putString(Tag.OperatorsName, VR.PN, module.getOperatorsName());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SegmentationImageModule module, DicomObject dcm)
	{
		pack((GeneralImageModule) module, dcm);
		dcm.putStrings(Tag.ImageType, VR.CS, new String[] {"DERIVED","PRIMARY"});
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		dcm.putString(Tag.ContentLabel, VR.CS, module.getContentLabel());
		dcm.putString(Tag.ContentDescription, VR.LO, module.getContentDescription());
		dcm.putString(Tag.ContentCreatorName, VR.PN, module.getContentCreatorsName());
		dcm.putInt(Tag.SamplesPerPixel, VR.US, module.getSamplesPerPixel());
		dcm.putString(Tag.PhotometricInterpretation, VR.CS,
			module.getPhotometricInterpretation());
		dcm.putInt(Tag.PixelRepresentation, VR.US, module.getPixelRepresentation());
		dcm.putInt(Tag.BitsAllocated, VR.US, module.getBitsAllocated());
		dcm.putInt(Tag.BitsStored, VR.US, module.getBitsStored());
		dcm.putInt(Tag.HighBit, VR.US, module.getHighBit());
		dcm.putString(Tag.LossyImageCompression, VR.CS,
			module.getLossyImageCompression() ? "01" : "00");
		String segType = module.getSegmentationType();
		dcm.putString(Tag.SegmentationType, VR.CS, segType);
		if (Constants.Fractional.equals(segType))
		{
			dcm.putString(Tag.SegmentationFractionalType, VR.CS,
				module.getSegmentationFractionalType());
			dcm.putInt(Tag.MaximumFractionalValue, VR.US,
				module.getMaximumFractionalValue());
		}
		dcm.putSequence(Tag.SegmentSequence);
		DicomElement segSq = dcm.get(Tag.SegmentSequence);
		for (Segment segment : module.getSegmentList())
		{
			packSegment(segment, segSq);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SegmentationSeriesModule module, DicomObject dcm)
	{
		pack((GeneralSeriesModule) module, dcm);
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putInt(Tag.SeriesNumber, VR.IS, module.getSeriesNumber());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SopCommonModule module, DicomObject dcm)
	{
		dcm.putString(Tag.SOPClassUID, VR.UI, module.getSopClassUid());
		dcm.putString(Tag.SOPInstanceUID, VR.UI, module.getSopInstanceUid());
		int number = module.getInstanceNumber();
		if (number > 0)
		{
			dcm.putInt(Tag.InstanceNumber, VR.IS, number);
		}
		putIfExists(module.getLongitudinalTemporalInformationModified(),
			dcm, Tag.LongitudinalTemporalInformationModified, VR.CS);
		putIfExists(module.getContentQualification(), dcm,
			Tag.ContentQualification, VR.CS);
		List<CodingSchemeIdentifier> csiList =
			module.getCodingSchemeIdentifierList();
		if (!csiList.isEmpty())
		{
			dcm.putSequence(Tag.CodingSchemeIdentificationSequence);
			DicomElement csiSq = dcm.get(Tag.CodingSchemeIdentificationSequence);
			for (CodingSchemeIdentifier csi : csiList)
			{
				DicomObject csiDcm = new BasicDicomObject();
				csiDcm.putString(Tag.CodingSchemeDesignator, VR.SH,
					csi.getCodingSchemeDesignator());
				putIfExists(csi.getCodingSchemeName(), csiDcm, Tag.CodingSchemeName,
					VR.ST);
				putIfExists(csi.getCodingSchemeVersion(), csiDcm,
					Tag.CodingSchemeVersion, VR.SH);
				putIfExists(csi.getCodingSchemeResponsibleOrganisation(), csiDcm, 
					Tag.CodingSchemeResponsibleOrganization, VR.ST);
				csiSq.addDicomObject(csiDcm);
			}
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentContentModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		ContentItem.ContainerItem rootItem =  module.getRootContentItem();
		packContentItem((ContentItem) rootItem, dcm);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentGeneralModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		dcm.putString(Tag.InstanceNumber, VR.IS,
			Integer.toString(module.getInstanceNumber()));
		dcm.putString(Tag.CompletionFlag, VR.CS, module.getCompletionFlag());
		dcm.putString(Tag.VerificationFlag, VR.CS, module.getVerificationFlag());
		dcm.putString(Tag.ContentDate, VR.DA, module.getContentDate());
		dcm.putString(Tag.ContentTime, VR.TM, module.getContentTime());
		dcm.putSequence(Tag.PerformedProcedureCodeSequence);
		DicomElement perfProcSq = dcm.get(Tag.PerformedProcedureCodeSequence);
		for (Code ppCode : module.getPerformedProcedureCodeList())
		{
			packCode(ppCode, perfProcSq);
		}
		List<HeirarchicalSopInstanceReference> crpeList = 
			module.getCurrentRequestedProcedureEvidenceList();
		if (!crpeList.isEmpty())
		{
			dcm.putSequence(Tag.CurrentRequestedProcedureEvidenceSequence);
			DicomElement crpeSq = dcm.get(
				Tag.CurrentRequestedProcedureEvidenceSequence);
			for (HeirarchicalSopInstanceReference ref : crpeList)
			{
				DicomObject refDcm = new BasicDicomObject();
				packHeirSopInstRef(ref, refDcm);
				crpeSq.addDicomObject(refDcm);
			}
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentSeriesModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		dcm.putString(Tag.SeriesNumber, VR.IS,
			Integer.toString(module.getSeriesNumber()));
		String date = module.getSeriesDate();
		if ((date != null) && !date.isEmpty())
		{
			dcm.putString(Tag.SeriesDate, VR.DA, date);
		}
		String time = module.getSeriesTime();
		if ((time != null) && !time.isEmpty())
		{
			dcm.putString(Tag.SeriesTime, VR.TM, time);
		}
		String desc = module.getSeriesDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.SeriesDescription, VR.LO, desc);
		}
		dcm.putSequence(Tag.ReferencedPerformedProcedureStepSequence);
		DicomElement rppsSq = dcm.get(Tag.ReferencedPerformedProcedureStepSequence);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if structure set is invalid
	 */
	public static void pack(StructureSetModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		dcm.putString(Tag.StructureSetLabel, VR.SH, module.getStructureSetLabel());
		String name = module.getStructureSetName();
		if ((name != null) && !name.isEmpty())
		{
			dcm.putString(Tag.StructureSetName, VR.LO, name);
		}
		String desc = module.getStructureSetDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.StructureSetDescription, VR.ST, desc);
		}
		int number = module.getInstanceNumber();
		if (number > 0)
		{
			dcm.putInt(Tag.InstanceNumber, VR.IS, number);
		}
		dcm.putString(Tag.StructureSetDate, VR.DA, module.getStructureSetDate());
		dcm.putString(Tag.StructureSetTime, VR.TM, module.getStructureSetTime());
		List<ReferencedFrameOfReference> refFoRList =
			module.getReferencedFrameOfReferenceList();
		if (!refFoRList.isEmpty())
		{
			packReferencedFramesOfReference(refFoRList, dcm);
		}
		List<StructureSetRoi> ssRoiList = module.getStructureSetRoiList();
		if (ssRoiList.isEmpty())
		{
			throw new IllegalArgumentException("No structure set ROIs present");
		}
		packStructureSetRois(ssRoiList, dcm);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SynchronisationModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
	}

	/**
	 * Returns a new PatientModule.
	 * @return the new module
	 */
	public static PatientModule patientModule()
	{
		return new DefaultPatientModule();
	}

	/**
	 * Returns a new PatientStudyModule.
	 * @return the new module
	 */
	public static PatientStudyModule patientStudyModule()
	{
		return new DefaultPatientStudyModule();
	}

	/**
	 * Returns a new RoiContourModule.
	 * @return the new module
	 */
	public static RoiContourModule roiContourModule()
	{
		return new DefaultRoiContourModule();
	}

	/**
	 * Returns a new RtRoiObservationsModule.
	 * @return the new module
	 */
	public static RtRoiObservationsModule rtRoiObservationsModule()
	{
		return new DefaultRtRoiObservationsModule();
	}

	/**
	 * Returns a new RtSeriesModule.
	 * @return the new module
	 */
	public static RtSeriesModule rtSeriesModule()
	{
		return new DefaultRtSeriesModule();
	}

	/**
	 *
	 * @param classUid
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static SopCommonModule sopCommonModule(String classUid)
		throws IllegalArgumentException
	{
		if ((classUid == null) || classUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopClassUid must not be null or empty");
		}
		return new DefaultSopCommonModule(classUid);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialSeriesModule module) throws IllegalArgumentException
	{
		String name = dcm.getString(Tag.ClinicalTrialCoordinatingCenterName,
			VR.LO, null);
		if (name == null)
		{
			module.setClinicalTrialCoordinatingCenterName(null);
			module.setClinicalTrialSeriesId(null);
			module.setClinicalTrialSeriesDescription(null);
			return;
		}
		module.setClinicalTrialCoordinatingCenterName(name);
		module.setClinicalTrialSeriesId(
			dcm.getString(Tag.ClinicalTrialSeriesID, VR.LO, null));
		module.setClinicalTrialSeriesDescription(
			dcm.getString(Tag.ClinicalTrialSeriesDescription, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialStudyModule module) throws IllegalArgumentException
	{
		String timePointId = dcm.getString(Tag.ClinicalTrialTimePointID, VR.LO, null);
		if (timePointId == null)
		{
			module.setClinicalTrialTimePointId(null);
			module.setClinicalTrialTimePointDescription(null);
			return;
		}
		module.setClinicalTrialTimePointId(timePointId);
		module.setClinicalTrialTimePointDescription(
			dcm.getString(Tag.ClinicalTrialTimePointDescription, VR.ST, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialSubjectModule module) throws IllegalArgumentException
	{
		String sponsor = dcm.getString(Tag.ClinicalTrialSponsorName, VR.LO, null);
		String protoId = dcm.getString(Tag.ClinicalTrialProtocolID, VR.LO, null);
		if ((sponsor == null) || sponsor.isEmpty() ||
			 (protoId == null) || protoId.isEmpty())
		{
			module.setClinicalTrialSponsorName(null);
			module.setClinicalTrialProtocolId(null);
			module.setClinicalTrialProtocolName(null);
			module.setClinicalTrialSiteId(null);
			module.setClinicalTrialSiteName(null);
			return;
		}
		String subjId = dcm.getString(Tag.ClinicalTrialSubjectID, VR.LO, null);
		if ((subjId != null) && !subjId.isEmpty())
		{
			module.setClinicalTrialSubjectId(subjId);
		}
		else
		{
			String readId = dcm.getString(Tag.ClinicalTrialSubjectReadingID, VR.LO,
				null);
			if ((readId == null) || readId.isEmpty())
			{
				throw new IllegalArgumentException(
					"Subject ID and subject reading ID cannot both be null");
			}
			module.setClinicalTrialSubjectReadingId(readId);
		}
		module.setClinicalTrialSponsorName(sponsor);
		module.setClinicalTrialProtocolId(protoId);
		module.setClinicalTrialProtocolName(
			dcm.getString(Tag.ClinicalTrialProtocolName, VR.LO, null));
		module.setClinicalTrialSiteId(
			dcm.getString(Tag.ClinicalTrialSiteID, VR.LO, null));
		module.setClinicalTrialSiteName(
			dcm.getString(Tag.ClinicalTrialSiteName, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		CommonInstanceReferenceModule module) throws IllegalArgumentException
	{
		DicomElement refSerSq = dcm.get(Tag.ReferencedSeriesSequence);
		// Sequence is 1C so may be null.
		if (refSerSq == null)
		{
			return;
		}
		if (refSerSq.countItems() < 1)
		{
			throw new IllegalArgumentException("ReferencedSeriesSequence empty");
		}
		for (int i=0; i<refSerSq.countItems(); i++)
		{
			module.addReferencedSeries(
				unpackReferencedSeries(refSerSq.getDicomObject(i)));
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		EnhancedGeneralEquipmentModule module) throws IllegalArgumentException
	{
		unpack(dcm, (GeneralEquipmentModule) module);
		module.setManufacturer(dcm.getString(Tag.Manufacturer));
		module.setManufacturersModelName(dcm.getString(Tag.ManufacturerModelName));
		module.setDeviceSerialNumber(dcm.getString(Tag.DeviceSerialNumber));
		module.setSoftwareVersion(dcm.getString(Tag.SoftwareVersions));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, FrameOfReferenceModule module)
		throws IllegalArgumentException
	{
		String forUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		module.setFrameOfReferenceUid(forUid);
		if (forUid == null)
		{
			return;
		}
		module.setPositionReferenceIndicator(
			dcm.getString(Tag.PositionReferenceIndicator, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, GeneralEquipmentModule module)
	{
		module.setManufacturer(dcm.getString(Tag.Manufacturer));
		String modelName = dcm.getString(Tag.ManufacturerModelName);
		if (modelName != null)
		{
			module.setManufacturersModelName(modelName);
		}
		String serial = dcm.getString(Tag.DeviceSerialNumber);
		if (serial != null)
		{
			module.setDeviceSerialNumber(serial);
		}
		String software = dcm.getString(Tag.SoftwareVersions);
		if (software != null)
		{
			module.setSoftwareVersion(software);
		}
		module.setSpatialResolution(
			dcm.getDouble(Tag.SpatialResolution, VR.DS, Double.NaN));
		module.setPixelPaddingValue(
			dcm.getInt(Tag.PixelPaddingValue, VR.US, Integer.MIN_VALUE));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralImageModule module)
		throws IllegalArgumentException
	{
		int instNum = dcm.getInt(Tag.InstanceNumber, -1);
		if (instNum == -1)
		{
			throw new IllegalArgumentException("InstanceNumber must exist");
		}
		module.setInstanceNumber(instNum);
		module.setPatientOrientation(
			dcm.getStrings(Tag.PatientOrientation, VR.CS, null));
		module.setContentDate(dcm.getString(Tag.ContentDate, VR.DA, ""));
		module.setContentTime(dcm.getString(Tag.ContentTime, VR.TM, ""));
		module.setImageType(dcm.getString(Tag.ImageType, VR.CS, null));
		module.setAcquisitionDate(dcm.getString(Tag.AcquisitionDate, VR.DA, null));
		module.setAcquisitionTime(dcm.getString(Tag.AcquisitionTime, VR.TM, null));
		module.setImageComments(dcm.getString(Tag.ImageComments, VR.LT, null));
		module.setBurnedInAnnotation(
			dcm.getString(Tag.BurnedInAnnotation, VR.CS, null));
		String lossyCompression = dcm.getString(Tag.LossyImageCompression, VR.CS,
			"");
		switch (lossyCompression)
		{
			case "00":
				module.setLossyImageCompression(false);
				break;
			case "01":
				module.setLossyImageCompression(true);
				break;
			case "":
				break;
			default:
				throw new IllegalArgumentException(
					"LossyImageCompression invalid: "+lossyCompression);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralReferenceModule module)
		throws IllegalArgumentException
	{
		DicomElement refImageSq = dcm.get(Tag.ReferencedImageSequence);
		if ((refImageSq != null) && !refImageSq.isEmpty())
		{
			int nItems = refImageSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = refImageSq.getDicomObject(i);
				module.addReferencedImage(unpackReferencedInstance(item));
			}
		}
		module.setDerivationDescription(
			dcm.getString(Tag.DerivationDescription, VR.ST, null));
		DicomElement derivCodeSq = dcm.get(Tag.DerivationCodeSequence);
		if ((derivCodeSq != null) && !derivCodeSq.isEmpty())
		{
			int nItems = derivCodeSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = derivCodeSq.getDicomObject(i);
				module.addDerivationCode(unpackCode(item));
			}
		}	
		DicomElement srcImageSq = dcm.get(Tag.SourceImageSequence);
		if ((srcImageSq != null) && !srcImageSq.isEmpty())
		{
			int nItems = srcImageSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = srcImageSq.getDicomObject(i);
				module.addSourceImage(unpackSourceImage(item, false));
			}
		}	
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralSeriesModule module)
		throws IllegalArgumentException
	{
		module.setModality(dcm.getString(Tag.Modality));
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		module.setLaterality(dcm.getString(Tag.Laterality, VR.CS, null));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		module.setOperatorsName(dcm.getString(Tag.OperatorsName));
		module.setProtocolName(dcm.getString(Tag.ProtocolName));
		module.setPatientPosition(dcm.getString(Tag.PatientPosition));
		module.setSmallestPixelValueInSeries(
			dcm.getInt(Tag.SmallestPixelValueInSeries, VR.US, -1));
		module.setLargestPixelValueInSeries(
			dcm.getInt(Tag.LargestPixelValueInSeries, VR.US, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralStudyModule module)
		throws IllegalArgumentException
	{
		module.setStudyInstanceUid(dcm.getString(Tag.StudyInstanceUID));
		module.setStudyDate(dcm.getString(Tag.StudyDate));
		module.setStudyTime(dcm.getString(Tag.StudyTime));
		module.setReferringPhysician(dcm.getString(Tag.ReferringPhysicianName));
		module.setStudyId(dcm.getString(Tag.StudyID));
		module.setAccessionNumber(dcm.getString(Tag.AccessionNumber));
		module.setStudyDescription(dcm.getString(Tag.StudyDescription, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, ImagePixelModule module)
		throws IllegalArgumentException
	{
		module.setPixelData(dcm.getBytes(Tag.PixelData));
		module.setSamplesPerPixel(dcm.getInt(Tag.SamplesPerPixel, VR.US));
		module.setPhotometricInterpretation(
			dcm.getString(Tag.PhotometricInterpretation, VR.CS, null));
		module.setRowCount(dcm.getInt(Tag.Rows, VR.US, -1));
		module.setColumnCount(dcm.getInt(Tag.Columns, VR.US, -1));
		module.setBitsAllocated(dcm.getInt(Tag.BitsAllocated, VR.US, -1));
		module.setBitsStored(dcm.getInt(Tag.BitsStored, VR.US, -1));
		int highBit = dcm.getInt(Tag.HighBit, VR.US, -1);
		if (highBit != module.getBitsStored()-1)
		{
			throw new IllegalArgumentException("HighBit must be (BitsStored-1)");
		}
		module.setHighBit(highBit);
		module.setPixelRepresentation(
			dcm.getInt(Tag.PixelRepresentation, VR.US, -1));
		if (module.getSamplesPerPixel() > 1)
		{
			module.setPlanarConfiguration(
				dcm.getInt(Tag.PlanarConfiguration, VR.US, -1));
		}
		module.setSmallestPixelValue(
			dcm.getInt(Tag.SmallestImagePixelValue, VR.US, -1));
		module.setLargestPixelValue(
			dcm.getInt(Tag.LargestImagePixelValue, VR.US, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, MultiframeDimensionModule module)
		throws IllegalArgumentException
	{
		DicomElement dimOrgSq = dcm.get(Tag.DimensionOrganizationSequence);
		if ((dimOrgSq == null) || (dimOrgSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"DimensionOrganizationSequence missing or empty");
		}
		for (int i=0; i<dimOrgSq.countItems(); i++)
		{
			DicomObject dimOrgDcm = dimOrgSq.getDicomObject(i);
			module.addDimensionOrganisationUid(
				dimOrgDcm.getString(Tag.DimensionOrganizationUID, VR.UI, null));
		}
		module.setDimensionOrganisationType(
			dcm.getString(Tag.DimensionOrganizationType, VR.CS, null));

		DicomElement dimIdxSq = dcm.get(Tag.DimensionIndexSequence);
		if ((dimIdxSq == null) || (dimIdxSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"DimensionIndexSequence missing or empty");
		}
		for (int i=0; i<dimIdxSq.countItems(); i++)
		{
			module.addDimensionIndex(
				unpackDimensionIndex(dimIdxSq.getDicomObject(i)));
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module
	 * @throws IllegalArgumentException
	 */
	public static void unpack(DicomObject dcm,
		MultiframeFunctionalGroupsModule module) throws IllegalArgumentException
	{
		String sopClassUid = dcm.getString(Tag.SOPClassUID, VR.UI, "");
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				unpackSegmentationStorage(dcm, module);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID");
		}
		module.setInstanceNumber(dcm.getInt(Tag.InstanceNumber, VR.IS, -1));
		module.setContentDate(dcm.getString(Tag.ContentDate, VR.DA, null));
		module.setContentTime(dcm.getString(Tag.ContentTime, VR.TM, null));
		module.setNumberOfFrames(dcm.getInt(Tag.NumberOfFrames, VR.IS, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, PatientModule module)
	{
		module.setPatientName(dcm.getString(Tag.PatientName));
		module.setPatientId(dcm.getString(Tag.PatientID));
		module.setPatientBirthDate(dcm.getString(Tag.PatientBirthDate));
		String sex = dcm.getString(Tag.PatientSex);
		if (sex != null)
		{
			module.setPatientSex(sex);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, PatientStudyModule module)
	{
		module.setPatientAge(dcm.getString(Tag.PatientAge, VR.AS, null));
		module.setPatientSize(dcm.getDouble(Tag.PatientSize, VR.DS, Double.NaN));
		module.setPatientWeight(
			dcm.getDouble(Tag.PatientWeight, VR.DS, Double.NaN));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, RoiContourModule module)
		throws IllegalArgumentException
	{
		DicomElement roiContourSq = dcm.get(Tag.ROIContourSequence);
		if ((roiContourSq == null) || roiContourSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"RoiContourSequence must not be null or empty");
		}
		int nItems = roiContourSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject item = roiContourSq.getDicomObject(i);
			RoiContour rc = new DefaultRoiContour();
			try
			{
				rc.setReferencedRoiNumber(item.getInt(Tag.ReferencedROINumber));
				int[] rgb = item.getInts(Tag.ROIDisplayColor);
				if ((rgb != null) && (rgb.length == 3))
				{
					rc.setRoiDisplayColour(rgb);
				}
			}
			catch (NumberFormatException ex)
			{
				logger.debug(
					"Invalid integer (IS) in RoiContour", ex);
			}
			module.addRoiContour(rc);
			unpackRoiContour(item.get(Tag.ContourSequence), rc);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		RtRoiObservationsModule module) throws IllegalArgumentException
	{
		DicomElement rtRoiObsSq = dcm.get(Tag.RTROIObservationsSequence);
		if ((rtRoiObsSq == null) || rtRoiObsSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"RtRoiObservationsSequence must not be null or empty");
		}
		int nItems = rtRoiObsSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject item = rtRoiObsSq.getDicomObject(i);
			RtRoiObservation rrObs = new DefaultRtRoiObservation();
			try
			{
				rrObs.setObservationNumber(item.getInt(Tag.ObservationNumber));
			}
			catch (NumberFormatException ex)
			{
				throw new IllegalArgumentException(
					"Invalid observation number (IS) in RT ROI observation", ex);
			}
			try
			{
				rrObs.setReferencedRoiNumber(item.getInt(Tag.ReferencedROINumber));
			}
			catch (NumberFormatException ex)
			{
				throw new IllegalArgumentException(
					"Invalid referenced ROI number (IS) in RT ROI observation", ex);
			}
			rrObs.setRoiInterpreter(item.getString(Tag.ROIInterpreter));
			rrObs.setRtRoiInterpretedType(item.getString(Tag.RTROIInterpretedType));
			module.addRtRoiObservation(rrObs);
			rrObs.setRoiObservationDescription(
				item.getString(Tag.ROIObservationDescription));
			rrObs.setRoiObservationLabel(item.getString(Tag.ROIObservationLabel));
		}		
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, RtSeriesModule module)
		throws IllegalArgumentException
	{
		module.setModality(dcm.getString(Tag.Modality));
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		module.setOperatorsName(dcm.getString(Tag.OperatorsName));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SegmentationImageModule module)
		throws IllegalArgumentException
	{
		Modules.unpack(dcm, (GeneralImageModule) module);
		module.setContentLabel(dcm.getString(Tag.ContentLabel, VR.CS, null));
		module.setContentDescription(
			dcm.getString(Tag.ContentDescription, VR.LO, null));
		module.setContentCreatorsName(
			dcm.getString(Tag.ContentCreatorName, VR.PN, null));
		module.setBitsAllocated(dcm.getInt(Tag.BitsAllocated, VR.US, -1));
		module.setBitsStored(dcm.getInt(Tag.BitsStored, VR.US, -1));
		module.setHighBit(dcm.getInt(Tag.HighBit, VR.US, -1));
		String lic = dcm.getString(Tag.LossyImageCompression, VR.CS, null);
		if ("00".equals(lic) || "01".equals(lic))
		{
			module.setLossyImageCompression("01".equals(lic));
		}
		else
		{
			throw new IllegalArgumentException("Invalid LossyImageCompression: "+
				lic);
		}
		module.setSegmentationType(
			dcm.getString(Tag.SegmentationType, VR.CS, null));
		if (module.getSegmentationType().equals(Constants.Fractional))
		{
			module.setSegmentationFractionalType(
				dcm.getString(Tag.SegmentationFractionalType, VR.CS, null));
			module.setMaximumFractionalValue(
				dcm.getInt(Tag.MaximumFractionalValue, VR.US, -1));
		}
		DicomElement segSq = dcm.get(Tag.SegmentSequence);
		if ((segSq == null) || (segSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"SegmentSequence missing or invalid size");
		}
		int nSeg = segSq.countItems();
		for (int i=0; i<nSeg; i++)
		{
			DicomObject dcmSeg = segSq.getDicomObject(i);
			unpackSegment(dcmSeg, module);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SegmentationSeriesModule module)
		throws IllegalArgumentException
	{
		unpack(dcm, (GeneralSeriesModule) module);
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SopCommonModule module)
		throws IllegalArgumentException
	{
		// SOP class UID will already be set in module constructor
		module.setSopInstanceUid(dcm.getString(Tag.SOPInstanceUID));
		String number = dcm.getString(Tag.InstanceNumber);
		if (number != null)
		{
			try
			{
				module.setInstanceNumber(Integer.parseInt(number));
			}
			catch (NumberFormatException ex)
			{
				logger.debug(
					"Invalid instance number (IS) in SopCommonModule: {}", number);
			}
		}
		module.setLongitudinalTemporalInformationModified(
			dcm.getString(Tag.LongitudinalTemporalInformationModified, VR.CS, null));
		module.setContentQualification(dcm.getString(Tag.ContentQualification,
			VR.CS, null));
		unpackCodingSchemeIds(dcm.get(Tag.CodingSchemeIdentificationSequence),
			module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentContentModule module) throws IllegalArgumentException
	{
		ContentItem ci = unpackContentItem(dcm);
		if (!(ci instanceof ContentItem.ContainerItem))
		{
			throw new IllegalArgumentException("Root content item must be a container");
		}
		module.setRootContentItem((ContentItem.ContainerItem) ci);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentGeneralModule module) throws IllegalArgumentException
	{
		module.setInstanceNumber(dcm.getInt(Tag.InstanceNumber));
		module.setCompletionFlag(dcm.getString(Tag.CompletionFlag));
		module.setVerificationFlag(dcm.getString(Tag.VerificationFlag));
		module.setContentDate(dcm.getString(Tag.ContentDate));
		module.setContentTime(dcm.getString(Tag.ContentTime));
		DicomElement perfProcSq = dcm.get(Tag.PerformedProcedureCodeSequence);
		if (perfProcSq == null)
		{
			throw new IllegalArgumentException(
				"Performed procedure code sequence missing");
		}
		int nPerfProc = perfProcSq.countItems();
		for (int i=0; i<nPerfProc; i++)
		{
			DicomObject ppDcm = perfProcSq.getDicomObject(i);
			module.addPerformedProcedureCode(unpackCode(ppDcm));
		}
		unpackCurrReqProcEvidence(dcm, module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentSeriesModule module) throws IllegalArgumentException
	{
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		DicomElement rppsSq = dcm.get(Tag.ReferencedPerformedProcedureStepSequence);
		if (rppsSq == null)
		{
			throw new IllegalArgumentException(
				"ReferencedPerformedProcedureStepSequence missing");
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, StructureSetModule module)
		throws IllegalArgumentException
	{
		module.setStructureSetLabel(dcm.getString(Tag.StructureSetLabel));
		String name = dcm.getString(Tag.StructureSetName);
		if (name != null)
		{
			module.setStructureSetName(name);
		}
		String desc = dcm.getString(Tag.StructureSetDescription);
		if (desc != null)
		{
			module.setStructureSetDescription(desc);
		}
		String number = dcm.getString(Tag.InstanceNumber);
		if (number != null)
		{
			try
			{
				module.setInstanceNumber(Integer.parseInt(number));
			}
			catch (NumberFormatException ex)
			{
				logger.debug(
					"Invalid instance number (IS) in StructureSetModule: {}", number);
			}
		}
		module.setStructureSetDate(dcm.getString(Tag.StructureSetDate));
		module.setStructureSetTime(dcm.getString(Tag.StructureSetTime));
		unpackReferencedFrameOfReference(
			dcm.get(Tag.ReferencedFrameOfReferenceSequence), module);
		unpackStructureSetRoi(dcm.get(Tag.StructureSetROISequence), module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SynchronisationModule module) throws IllegalArgumentException
	{
	}

	private static void packCode(Code code, DicomElement codeSq)
	{
		packCode(code, codeSq, 0);
	}

	private static void packCode(Code code, DicomElement codeSq, int modifierTag)
	{
		DicomObject dcm = new BasicDicomObject();
		String value = code.getCodeValue();
		if (value == null)
		{
			value = code.getLongCodeValue();
			if (value != null)
			{
				dcm.putString(0x00080119, VR.UT, value);
			}
		}
		else
		{
			dcm.putString(Tag.CodeValue, VR.SH, value);
		}
		if (value != null)
		{
			dcm.putString(Tag.CodingSchemeDesignator, VR.SH,
				code.getCodingSchemeDesignator());
		}
		putIfExists(code.getCodingSchemeVersion(), dcm, Tag.CodingSchemeVersion,
			VR.SH);
		dcm.putString(Tag.CodeMeaning, VR.LO, code.getCodeMeaning());
		codeSq.addDicomObject(dcm);

		List<Code> mods = code.getModifiers();
		if ((modifierTag == 0) || mods.isEmpty())
		{
			return;
		}
		dcm.putSequence(modifierTag);
		DicomElement modSq = dcm.get(modifierTag);
		for (Code mod : mods)
		{
			packCode(mod, modSq);
		}
	}

	private static void packCodeItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.CodeItem))
		{
			throw new IllegalArgumentException("Content item not a code");
		}
		ContentItem.CodeItem item = (ContentItem.CodeItem) ci;
		Code code = item.getCode();
		if (code != null)
		{
			dcm.putSequence(Tag.ConceptCodeSequence);
			DicomElement codeSq = dcm.get(Tag.ConceptCodeSequence);
			packCode(code, codeSq);
		}
		packContentItemUniversal(ci, dcm);
	}

	private static void packContainerContentItem(ContentItem ci, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (!(ci instanceof ContentItem.ContainerItem))
		{
			throw new IllegalArgumentException("Content item not a container");
		}
		ContentItem.ContainerItem container = (ContentItem.ContainerItem) ci;
		dcm.putString(Tag.ContinuityOfContent, VR.CS,
			container.getContinuityOfContent());
		ContentTemplate template = container.getContentTemplate();
		if (template != null)
		{
			dcm.putSequence(Tag.ContentTemplateSequence);
			DicomElement ctSq = dcm.get(Tag.ContentTemplateSequence);
			DicomObject ctDcm = new BasicDicomObject();
			ctSq.addDicomObject(ctDcm);
			packContentTemplate(template, ctDcm);
		}
		packContentItemUniversal(ci, dcm);
	}

	private static void packContentItem(ContentItem ci, DicomObject dcm)
	{
		String type = ci.getValueType();
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Content item has null or empty value type");
		}
		switch (type)
		{
			case Constants.Container:
				packContainerContentItem(ci, dcm);
				break;
			case Constants.Code:
				packCodeItem(ci, dcm);
				break;
			case Constants.PersonName:
				packPersonNameItem(ci, dcm);
				break;
			case Constants.Text:
				packTextItem(ci, dcm);
				break;
			case Constants.Image:
				packImageItem(ci, dcm);
				break;
			case Constants.UidReference:
				packUidRefItem(ci, dcm);
				break;
			case Constants.Numeric:
				packNumericItem(ci, dcm);
				break;
			case Constants.SpatialCoord:
				packSCoordItem(ci, dcm);
				break;
			case Constants.Date:
			case Constants.Time:
			case Constants.DateTime:
			case Constants.Composite:
			case Constants.Waveform:
			case Constants.SpatialCoord3D:
			case Constants.TemporalCoord:
				packContentItemUniversal(ci, dcm);
				break;
			default:
				throw new IllegalArgumentException("Invalid value type: "+type);
		}
	}

	private static void packContentItemUniversal(ContentItem ci, DicomObject dcm)
	{
		dcm.putString(Tag.ValueType, VR.CS, ci.getValueType());
		Code conceptNameCode = ci.getConceptNameCode();
		if (conceptNameCode != null)
		{
			dcm.putSequence(Tag.ConceptNameCodeSequence);
			DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
			packCode(conceptNameCode, conceptNameSq);
		}
		List<ContentRelationship> crList = ci.getContentRelationshipList();
		if (!crList.isEmpty())
		{
			dcm.putSequence(Tag.ContentSequence);
			DicomElement contentSq = dcm.get(Tag.ContentSequence);
			packContentRelationships(crList, contentSq);
		}
	}

	private static void packContentRelationships(List<ContentRelationship> crList,
		DicomElement contentSq)
	{
		for (ContentRelationship cr : crList)
		{
			DicomObject dcm = new BasicDicomObject();
			dcm.putString(Tag.RelationshipType, VR.CS, cr.getRelationshipType());
			ContentItem item = cr.getRelatedContentItem();
			if (item != null)
			{
				packContentItem(item, dcm);
			}
			else
			{
				int[] refIds = cr.getReferencedContentItemIdentifier();
				if ((refIds != null) && (refIds.length > 0))
				{
					dcm.putInts(Tag.ReferencedContentItemIdentifier, VR.UL, refIds);
				}
			}
			contentSq.addDicomObject(dcm);
		}
	}

	private static void packContentTemplate(ContentTemplate template,
		DicomObject dcm)
	{
		dcm.putString(Tag.MappingResource, VR.CS, template.getMappingResource());
		dcm.putString(Tag.TemplateIdentifier, VR.CS,
			template.getTemplateIdentifier());
	}

	private static void packContourImages(List<ContourImage> ciList,
		DicomObject conItem)
	{
		conItem.putSequence(Tag.ContourImageSequence);
		DicomElement ciSq = conItem.get(Tag.ContourImageSequence);
		for (ContourImage ci : ciList)
		{
			DicomObject ciItem = new BasicDicomObject();
			ciItem.putString(Tag.ReferencedSOPClassUID, VR.UI,
				ci.getReferencedSopClassUid());
			ciItem.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
				ci.getReferencedSopInstanceUid());
			int frame = ci.getReferencedFrameNumber();
			if (frame > 0)
			{
				ciItem.putInt(Tag.ReferencedFrameNumber, VR.IS, frame);
			}
			ciSq.addDicomObject(ciItem);
		}
	}

	private static void packContours(List<Contour> conList, DicomObject rcItem)
		throws IllegalArgumentException
	{
		rcItem.putSequence(Tag.ContourSequence);
		DicomElement cSq = rcItem.get(Tag.ContourSequence);
		for (Contour contour : conList)
		{
			DicomObject conItem = new BasicDicomObject();
			conItem.putInt(Tag.ContourNumber, VR.IS, contour.getContourNumber());
			conItem.putString(Tag.ContourGeometricType, VR.CS,
				contour.getContourGeometricType());
			List<Coordinate3D> coordList = contour.getContourData();
			int nCoords = coordList.size();
			if (nCoords < 1)
			{
				throw new IllegalArgumentException("No contour coordinates found");
			}
			conItem.putInt(Tag.NumberOfContourPoints, VR.IS, nCoords);
			double[] coords = new double[3*nCoords];
			int idx = 0;
			for (Coordinate3D coord : coordList)
			{
				coords[idx++] = coord.x;
				coords[idx++] = coord.y;
				coords[idx++] = coord.z;
			}
			conItem.putDoubles(Tag.ContourData, VR.DS, coords);
			cSq.addDicomObject(conItem);
			List<ContourImage> ciList = contour.getContourImageList();
			if (ciList.isEmpty())
			{
				continue;
			}
			packContourImages(ciList, conItem);
		}
	}

	private static void packDerivationImage(DerivationImage image,
		DicomElement derivSq)
	{
		DicomObject dcm = new BasicDicomObject();
		List<SourceImage> sources = image.getSourceImageList();
		if (!sources.isEmpty())
		{
			dcm.putSequence(Tag.SourceImageSequence);
			DicomElement sourceImageSq = dcm.get(Tag.SourceImageSequence);
			for (SourceImage source : sources)
			{
				packSourceImage(source, sourceImageSq);
			}
		}

		List<Code> codes = image.getDerivationCodeList();
		if (!codes.isEmpty())
		{
			dcm.putSequence(Tag.DerivationCodeSequence);
			DicomElement codeSq = dcm.get(Tag.DerivationCodeSequence);
			for (Code code : codes)
			{
				packCode(code, codeSq);
			}
		}
		if (!dcm.isEmpty())
		{
			derivSq.addDicomObject(dcm);
		}
	}

	private static void packDimensionIndex(DimensionIndex idx,
		DicomElement dimIdxSq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putString(Tag.DimensionOrganizationUID, VR.UI,
			idx.getDimensionOrganisationUid());
		dcm.putInt(Tag.DimensionIndexPointer, VR.AT, idx.getIndexPointer());
		int funcPtr = idx.getFunctionalGroupPointer();
		if (funcPtr != 0)
		{
			dcm.putInt(Tag.FunctionalGroupPointer, VR.AT, funcPtr);
		}
		putIfExists(idx.getDescriptionLabel(), dcm, Tag.DimensionDescriptionLabel,
			VR.LO);
		dimIdxSq.addDicomObject(dcm);
	}

	private static void packFrameContent(FrameContent fc, DicomObject parentDcm)
	{
		DicomObject dcm = new BasicDicomObject();
		int[] dimIdx = fc.getDimensionIndexValues();
		if (dimIdx.length != 0)
		{
			dcm.putInts(Tag.DimensionIndexValues, VR.UL, dimIdx);
		}
		String stackId = fc.getStackId();
		if ((stackId != null) && !stackId.isEmpty())
		{
			dcm.putString(Tag.StackID, VR.SH, stackId);
			dcm.putInt(Tag.InStackPositionNumber, VR.UL,
				fc.getInStackPositionNumber());
		}
		if (!dcm.isEmpty())
		{
			parentDcm.putSequence(Tag.FrameContentSequence);
			DicomElement sq = parentDcm.get(Tag.FrameContentSequence);
			sq.addDicomObject(dcm);
		}
	}

	private static void packHeirSeriesRef(HeirarchicalSeriesReference hsr,
		DicomObject dcm)
	{
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, hsr.getSeriesInstanceUid());
		dcm.putSequence(Tag.ReferencedSOPSequence);
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		for (ReferencedInstance inst : hsr.getReferencedInstanceList())
		{
			packReferencedInstance(inst, refInstSq);
		}
	}

	private static void packHeirSopInstRef(HeirarchicalSopInstanceReference hsir,
		DicomObject dcm)
	{
		dcm.putString(Tag.StudyInstanceUID, VR.UI, hsir.getStudyInstanceUid());
		dcm.putSequence(Tag.ReferencedSeriesSequence);
		DicomElement serSq = dcm.get(Tag.ReferencedSeriesSequence);
		for (HeirarchicalSeriesReference ref : hsir.getSeriesReferenceList())
		{
			DicomObject refDcm = new BasicDicomObject();
			packHeirSeriesRef(ref, refDcm);
			serSq.addDicomObject(refDcm);
		}
	}

	private static void packImageItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.ImageItem))
		{
			throw new IllegalArgumentException("Content item not an image");
		}
		ContentItem.ImageItem item = (ContentItem.ImageItem) ci;
		ReferencedInstance inst = item.getReferencedInstance();
		if (inst != null)
		{
			dcm.putSequence(Tag.ReferencedSOPSequence);
			DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
			packReferencedInstance(inst, refInstSq);
		}
		packContentItemUniversal(ci, dcm);
	}

	private static void packNumericItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.NumericItem))
		{
			throw new IllegalArgumentException("Content item not a numeric value");
		}
		ContentItem.NumericItem item = (ContentItem.NumericItem) ci;
		dcm.putSequence(Tag.MeasuredValueSequence);
		String numVal = item.getNumericValue();
		if (!StringUtils.isNullOrEmpty(numVal))
		{
			DicomElement measValSq = dcm.get(Tag.MeasuredValueSequence);
			DicomObject valDcm = new BasicDicomObject();
			valDcm.putString(Tag.NumericValue, VR.DS, numVal);
			double floatValue = item.getFloatingPointValue();
			if (!Double.isNaN(floatValue))
			{
				valDcm.putDouble(0x0040a161, VR.FD, floatValue);
			}
			int numerator = item.getRationalNumeratorValue();
			int denominator = item.getRationalDenominatorValue();
			if ((numerator != 0) && (denominator != 0))
			{
				valDcm.putInt(0x0040a162, VR.SL, numerator);
				valDcm.putInt(0x0040a163, VR.UL, denominator);
			}
			valDcm.putSequence(Tag.MeasurementUnitsCodeSequence);
			DicomElement unitsSq = valDcm.get(Tag.MeasurementUnitsCodeSequence);
			packCode(item.getMeasurementUnits(), unitsSq);
			measValSq.addDicomObject(valDcm);
		}
		packContentItemUniversal(ci, dcm);
	}

	private static void packPersonNameItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.PersonNameItem))
		{
			throw new IllegalArgumentException("Content item not a person name");
		}
		ContentItem.PersonNameItem item = (ContentItem.PersonNameItem) ci;
		dcm.putString(Tag.PersonName, VR.PN, item.getPersonName());
		packContentItemUniversal(ci, dcm);
	}

	private static void packPixelMeasures(PixelMeasures pixelMeasures,
		DicomObject parentDcm, boolean hasFrameOfRefUid)
	{
		if (pixelMeasures == null)
		{
			return;
		}
		DicomObject dcm = null;
		double[] pixSpacing = pixelMeasures.getPixelSpacing();
		double thickness = pixelMeasures.getSliceThickness();
		double sliceSpacing = pixelMeasures.getSpacingBetweenSlices();
		if (hasFrameOfRefUid)
		{
			// Fields are required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.PixelSpacing, VR.DS, pixSpacing);
			dcm.putDouble(Tag.SliceThickness, VR.DS, thickness);
			if (MathUtils.isFinite(sliceSpacing))
			{
				dcm.putDouble(Tag.SpacingBetweenSlices, VR.DS, sliceSpacing);
			}
		}
		else
		{
			if ((pixSpacing.length == 2) && MathUtils.isFinite(thickness))
			{
				// Fields are optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.PixelSpacing, VR.DS, pixSpacing);
				dcm.putDouble(Tag.SliceThickness, VR.DS, thickness);
				if (MathUtils.isFinite(sliceSpacing))
				{
					dcm.putDouble(Tag.SpacingBetweenSlices, VR.DS, sliceSpacing);
				}
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PixelMeasuresSequence);
			DicomElement sq = parentDcm.get(Tag.PixelMeasuresSequence);
			sq.addDicomObject(dcm);
		}
	}

	private static void packPlaneOrientation(
		SharedFunctionalGroups sharedGroups, DicomObject parentDcm,
		boolean hasFrameOfRefUid)
	{
		DicomObject dcm = null;
		double[] ori = sharedGroups.getImageOrientationPatient();
		if (hasFrameOfRefUid)
		{
			// Field is required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.ImageOrientationPatient, VR.DS, ori);
		}
		else
		{
			if (ori.length == 6)
			{
				// Field is optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.ImageOrientationPatient, VR.DS, ori);
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PlaneOrientationSequence);
			DicomElement sq = parentDcm.get(Tag.PlaneOrientationSequence);
			sq.addDicomObject(dcm);
		}
	}

	private static void packPlanePosition(double[] pos, DicomObject parentDcm,
		boolean hasFrameOfRefUid)
	{
		DicomObject dcm = null;
		if (hasFrameOfRefUid)
		{
			// Field is required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.ImagePositionPatient, VR.DS, pos);
		}
		else
		{
			if (pos.length == 3)
			{
				// Field is optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.ImagePositionPatient, VR.DS, pos);
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PlanePositionSequence);
			DicomElement sq = parentDcm.get(Tag.PlanePositionSequence);
			sq.addDicomObject(dcm);
		}
	}

	private static void packReferencedFramesOfReference(
		List<ReferencedFrameOfReference> refFoRList, DicomObject dcm)
		 throws IllegalArgumentException
	{
		dcm.putSequence(Tag.ReferencedFrameOfReferenceSequence);
		DicomElement refFoRSq = dcm.get(Tag.ReferencedFrameOfReferenceSequence);
		for (ReferencedFrameOfReference refFoR : refFoRList)
		{
			DicomObject refFoRItem = new BasicDicomObject();
			refFoRItem.putString(Tag.FrameOfReferenceUID, VR.UI,
				refFoR.getFrameOfReferenceUid());
			refFoRSq.addDicomObject(refFoRItem);
			List<RtReferencedStudy> studyList = refFoR.getRtReferencedStudyList();
			if (!studyList.isEmpty())
			{
				packRtReferencedStudies(studyList, refFoRItem);
			}
		}
	}

	private static void packReferencedInstance(ReferencedInstance inst,
		DicomElement riSq)
	{
		DicomObject dcm = new BasicDicomObject();
		String sopClassUid = inst.getReferencedSopClassUid();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			inst.getReferencedSopInstanceUid());
		if (DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			dcm.putInt(Tag.ReferencedFrameNumber, VR.IS,
				inst.getReferencedFrameNumber());
		}
		riSq.addDicomObject(dcm);
	}

	private static void packReferencedSeries(ReferencedSeries refSeries,
		DicomObject rsItem)
	{
		rsItem.putString(Tag.SeriesInstanceUID, VR.UI,
			refSeries.getSeriesInstanceUid());
		rsItem.putSequence(Tag.ReferencedInstanceSequence);
		DicomElement refInstSq = rsItem.get(Tag.ReferencedInstanceSequence);
		for (ReferencedInstance inst : refSeries.getReferencedInstanceList())
		{
			packReferencedInstance(inst, refInstSq);
		}
	}

	private static void packRtReferencedSeries(
		List<RtReferencedSeries> seriesList, DicomObject studyItem)
	{
		studyItem.putSequence(Tag.RTReferencedSeriesSequence);
		DicomElement rtRefSeriesSq = studyItem.get(Tag.RTReferencedSeriesSequence);
		for (RtReferencedSeries series : seriesList)
		{
			DicomObject seriesItem = new BasicDicomObject();
			seriesItem.putString(Tag.SeriesInstanceUID, VR.UI,
				series.getSeriesInstanceUid());
			rtRefSeriesSq.addDicomObject(seriesItem);
			List<ContourImage> ciList = series.getContourImageList();
			if (ciList.isEmpty())
			{
				throw new IllegalArgumentException("No referenced images found");
			}
			packContourImages(ciList, seriesItem);
		}
	}

	private static void packRtReferencedStudies(List<RtReferencedStudy> studyList,
		DicomObject refFoRItem) throws IllegalArgumentException
	{
		refFoRItem.putSequence(Tag.RTReferencedStudySequence);
		DicomElement rtRefStudySq = refFoRItem.get(Tag.RTReferencedStudySequence);
		for (RtReferencedStudy study : studyList)
		{
			DicomObject studyItem = new BasicDicomObject();
			studyItem.putString(Tag.ReferencedSOPClassUID, VR.UI,
				study.getReferencedSopClassUid());
			studyItem.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
				study.getReferencedSopInstanceUid());
			rtRefStudySq.addDicomObject(studyItem);
			List<RtReferencedSeries> seriesList = study.getRtReferencedSeriesList();
			if (seriesList.isEmpty())
			{
				throw new IllegalArgumentException("No referenced series found");
			}
			packRtReferencedSeries(seriesList, studyItem);
		}
	}

	private static void packSCoordItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.SpatialCoordItem))
		{
			throw new IllegalArgumentException("Content item not a spatial coordinate");
		}
		ContentItem.SpatialCoordItem item = (ContentItem.SpatialCoordItem) ci;
		String gfxType = item.getGraphicType();
		if (!StringUtils.isNullOrEmpty(gfxType))
		{
			dcm.putString(Tag.GraphicType, VR.CS, gfxType);
			switch (gfxType)
			{
				case Constants.Point:
				case Constants.MultiPoint:
				case Constants.Polyline:
				case Constants.Circle:
				case Constants.Ellipse:
					List<Coordinate2D> coordList = item.getGraphicData();
					int nCoords = coordList.size();
					float[] gfxData = new float[2*nCoords];
					for (int i=0; i<nCoords; i++)
					{
						Coordinate2D coord = coordList.get(i);
						gfxData[2*i] = (float) coord.column;
						gfxData[2*i+1] = (float) coord.row;
					}
					dcm.putFloats(Tag.GraphicData, VR.FL, gfxData);
					break;
				default:
			}
		}
		packContentItemUniversal(ci, dcm);
	}

	private static void packSegment(Segment segment, DicomElement segSq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putInt(Tag.SegmentNumber, VR.US, segment.getSegmentNumber());
		dcm.putString(Tag.SegmentLabel, VR.LO, segment.getSegmentLabel());
		putIfExists(segment.getSegmentDescription(), dcm, Tag.SegmentDescription,
			VR.ST);
		dcm.putString(Tag.SegmentAlgorithmType, VR.CS,
			segment.getSegmentAlgorithmType());
		if (!Constants.Manual.equals(segment.getSegmentAlgorithmType()))
		{
			dcm.putString(Tag.SegmentAlgorithmName, VR.LO,
				segment.getSegmentAlgorithmName());
		}
		int[] cieValue = segment.getRecommendedDisplayCIELabValue();
		if ((cieValue != null) && (cieValue.length == 3))
		{
			dcm.putInts(Tag.RecommendedDisplayCIELabValue, VR.US, cieValue);
		}

		List<Code> anatomicRegCodes = segment.getAnatomicRegionCodeList();
		if (!anatomicRegCodes.isEmpty())
		{
			dcm.putSequence(Tag.AnatomicRegionSequence);
			DicomElement anatomicRegSq = dcm.get(Tag.AnatomicRegionSequence);
			for (Code code : anatomicRegCodes)
			{
				packCode(code, anatomicRegSq, Tag.AnatomicRegionModifierSequence);
			}
		}
		dcm.putSequence(Tag.SegmentedPropertyCategoryCodeSequence);
		DicomElement catSq = dcm.get(Tag.SegmentedPropertyCategoryCodeSequence);
		packCode(segment.getSegmentedPropertyCategoryCode(), catSq);
		dcm.putSequence(Tag.SegmentedPropertyTypeCodeSequence);
		DicomElement typeSq = dcm.get(Tag.SegmentedPropertyTypeCodeSequence);
		packCode(segment.getSegmentedPropertyTypeCode(), typeSq);
		segSq.addDicomObject(dcm);
	}

	private static void packSegmentIdentification(int number,
		DicomObject parentDcm)
	{
		parentDcm.putSequence(Tag.SegmentIdentificationSequence);
		DicomElement sq = parentDcm.get(Tag.SegmentIdentificationSequence);
		DicomObject dcm = new BasicDicomObject();
		dcm.putInt(Tag.ReferencedSegmentNumber, VR.US, number);
		sq.addDicomObject(dcm);
	}

	private static void packSegmentationFrame(
		SegmentationFunctionalGroupsFrame frame, DicomElement perFrameSq,
		boolean hasFrameOfRefUid)
	{
		DicomObject dcm = new BasicDicomObject();
		packFrameContent(frame.getFrameContent(), dcm);
		List<DerivationImage> derivImages = frame.getDerivationImages();
		if (!derivImages.isEmpty())
		{
			dcm.putSequence(Tag.DerivationImageSequence);
			DicomElement derivSq = dcm.get(Tag.DerivationImageSequence);
			for (DerivationImage image : derivImages)
			{
				packDerivationImage(image, derivSq);
			}
		}
		packPlanePosition(frame.getImagePositionPatient(), dcm, hasFrameOfRefUid);
		packSegmentIdentification(frame.getReferencedSegmentNumber(), dcm);
		perFrameSq.addDicomObject(dcm);
	}

	private static void packSegmentationStorage(
		SegmentationMultiframeFunctionalGroupsModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		boolean hasFrameOfRefUid = (module.getFrameOfReferenceUid() == null);

		dcm.putSequence(Tag.SharedFunctionalGroupsSequence);
		DicomElement sharedSq = dcm.get(Tag.SharedFunctionalGroupsSequence);
		DicomObject sharedDcm = new BasicDicomObject();
		sharedSq.addDicomObject(sharedDcm);
		SegmentationSharedFunctionalGroups sharedGroups =
			module.getSharedFunctionalGroups();
		PixelMeasures pixMeasures = sharedGroups.getPixelMeasures();
		packPixelMeasures(pixMeasures, sharedDcm, hasFrameOfRefUid);
		packPlaneOrientation(sharedGroups, sharedDcm, hasFrameOfRefUid);

		dcm.putSequence(Tag.PerFrameFunctionalGroupsSequence);
		DicomElement perFrameSq = dcm.get(Tag.PerFrameFunctionalGroupsSequence);
		SegmentationPerFrameFunctionalGroups perFrameGroups =
			module.getPerFrameFunctionalGroups();
		for (FunctionalGroupsFrame frame : perFrameGroups.getFrameList())
		{
			packSegmentationFrame((SegmentationFunctionalGroupsFrame) frame,
				perFrameSq, hasFrameOfRefUid);
		}
	}

	private static void packSourceImage(SourceImage image, DicomElement imageSq)
	{
		DicomObject dcm = new BasicDicomObject();
		String sopClassUid = image.getReferencedSopClassUid();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			image.getReferencedSopInstanceUid());
		int number = image.getReferencedFrameNumber();
		if (DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			dcm.putInt(Tag.ReferencedFrameNumber, VR.IS, number);
		}
		putIfExists(image.getSpatialLocationsPreserved(), dcm,
			Tag.SpatialLocationsPreserved, VR.CS);
		Code code = image.getPurposeOfReferenceCode();
		if (code != null)
		{
			dcm.putSequence(Tag.PurposeOfReferenceCodeSequence);
			packCode(code, dcm.get(Tag.PurposeOfReferenceCodeSequence));
		}
		if (!dcm.isEmpty())
		{
			imageSq.addDicomObject(dcm);
		}
	}

	private static void packStructureSetRois(List<StructureSetRoi> ssRoiList,
		DicomObject dcm)
	{
		dcm.putSequence(Tag.StructureSetROISequence);
		DicomElement ssRoiSq = dcm.get(Tag.StructureSetROISequence);
		for (StructureSetRoi ssRoi : ssRoiList)
		{
			DicomObject roiItem = new BasicDicomObject();
			ssRoiSq.addDicomObject(roiItem);
			roiItem.putInt(Tag.ROINumber, VR.IS, ssRoi.getRoiNumber());
			roiItem.putString(Tag.ReferencedFrameOfReferenceUID, VR.UI,
				ssRoi.getReferencedFrameOfReferenceUid());
			roiItem.putString(Tag.ROIName, VR.LO, ssRoi.getRoiName());
			String desc = ssRoi.getRoiDescription();
			if ((desc != null) && !desc.isEmpty())
			{
				roiItem.putString(Tag.ROIDescription, VR.ST, desc);
			}
			roiItem.putString(Tag.ROIGenerationAlgorithm, VR.CS,
				ssRoi.getRoiGenerationAlgorithm());
		}
	}

	private static void packTextItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.TextItem))
		{
			throw new IllegalArgumentException("Content item not text");
		}
		ContentItem.TextItem item = (ContentItem.TextItem) ci;
		dcm.putString(Tag.TextValue, VR.UT, item.getText());
		packContentItemUniversal(ci, dcm);
	}

	private static void packUidRefItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.UidRefItem))
		{
			throw new IllegalArgumentException("Content item not a UID reference");
		}
		ContentItem.UidRefItem item = (ContentItem.UidRefItem) ci;
		dcm.putString(Tag.UID, VR.UI, item.getUidReference());
		packContentItemUniversal(ci, dcm);
	}

	private static void putIfExists(String value, DicomObject dcm, int tag, VR vr)
	{
		if ((value != null) && !value.isEmpty())
		{
			dcm.putString(tag, vr, value);
		}
	}

	private static void putIfExists(String[] value, DicomObject dcm, int tag, VR vr)
	{
		if ((value != null) && (value.length > 0))
		{
			dcm.putStrings(tag, vr, value);
		}
	}

	private static void putIfFinite(double value, DicomObject dcm, int tag, VR vr)
	{
		if (MathUtils.isFinite(value))
		{
			dcm.putDouble(tag, vr, value);
		}
	}

	private static Code unpackCode(DicomObject dcm)
		throws IllegalArgumentException
	{
		return unpackCode(dcm, 0);
	}

	private static Code unpackCode(DicomObject dcm, int modifierTag)
		throws IllegalArgumentException
	{
		Code code = new DefaultCode();
		String value = dcm.getString(Tag.CodeValue, VR.SH, null);
		if (value != null)
		{
			code.setCodeValue(value);
		}
		else
		{
			value = dcm.getString(0x00080119, VR.UT, null);
			if (value != null)
			{
				code.setLongCodeValue(value);
			}
		}
		if (value != null)
		{
			code.setCodingSchemeDesignator(
				dcm.getString(Tag.CodingSchemeDesignator, VR.SH, null));
		}
		code.setCodingSchemeVersion(
			dcm.getString(Tag.CodingSchemeVersion, VR.SH, null));
		code.setCodeMeaning(dcm.getString(Tag.CodeMeaning, VR.LO, null));

		// Fetch the modifiers
		DicomElement sq = dcm.get(modifierTag);
		if (sq == null)
		{
			return code;
		}
		for (int i=0; i<sq.countItems(); i++)
		{
			DicomObject modDcm = sq.getDicomObject(i);
			Code modifier = unpackCode(modDcm, 0);
			code.addModifier(modifier);
		}
	
		return code;
	}

	private static ContentItem.CodeItem unpackCodeItem(DicomObject dcm)
	{
		ContentItem.CodeItem item = new DefaultCodeItem();
		item.setValueType(Constants.Code);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		DicomElement codeSq = dcm.get(Tag.ConceptCodeSequence);
		if ((codeSq == null) || (codeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"ConceptCodeSeqeunce must be present and contain a single item");
		}
		item.setCode(unpackCode(codeSq.getDicomObject()));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static void unpackCodingSchemeIds(DicomElement csiSq,
		SopCommonModule module)
	{
		if ((csiSq == null) || csiSq.isEmpty())
		{
			return;
		}
		for (int i=0; i<csiSq.countItems(); i++)
		{
			DicomObject dcm = csiSq.getDicomObject(i);
			CodingSchemeIdentifier csi = new DefaultCodingSchemeIdentifier();
			csi.setCodingSchemeDesignator(dcm.getString(Tag.CodingSchemeDesignator,
				VR.SH, null));
			csi.setCodingSchemeName(dcm.getString(Tag.CodingSchemeName,
				VR.ST, null));
			csi.setCodingSchemeVersion(dcm.getString(Tag.CodingSchemeVersion,
				VR.SH, null));
			csi.setCodingSchemeResposibleOrganisation(
				dcm.getString(Tag.CodingSchemeResponsibleOrganization, VR.ST, null));
			module.addCodingSchemeIdentifier(csi);
		}
	}

	private static Code unpackConceptNameCode(DicomObject dcm)
	{
		return unpackConceptNameCode(dcm, true);
	}

	private static Code unpackConceptNameCode(DicomObject dcm, boolean required)
	{
		DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
		if ((conceptNameSq == null) && !required)
		{
			return null;
		}
		if ((conceptNameSq == null) || (conceptNameSq.countItems() != 1))
		{
				throw new IllegalArgumentException(
					"ConceptNameSequence must be present and contain a single item");
		}
		return unpackCode(conceptNameSq.getDicomObject(0));
	}

	private static ContentItem.ContainerItem unpackContainerContentItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.ContainerItem container = new DefaultContainerItem();
		container.setValueType(Constants.Container);
		DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
		if (conceptNameSq == null)
		{
			return container;
		}
		DicomObject conceptNameDcm = conceptNameSq.getDicomObject(0);
		if (conceptNameDcm == null)
		{
			throw new IllegalArgumentException(
				"ConceptNameCodeSequence must not be empty");
		}
		container.setConceptNameCode(unpackCode(conceptNameDcm));
		container.setContinuityOfContent(dcm.getString(Tag.ContinuityOfContent));
		DicomElement ctSq = dcm.get(Tag.ContentTemplateSequence);
		if (ctSq != null)
		{
			unpackContentTemplate(ctSq, container);
		}
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(container, contentSq);
		}

		return container;
	}

	private static ContentItem unpackContentItem(DicomObject dcm)
	{
		ContentItem ci;
		String type = dcm.getString(Tag.ValueType);
		switch (type)
		{
			case Constants.Container:
				ci = unpackContainerContentItem(dcm);
				break;
			case Constants.Code:
				ci = unpackCodeItem(dcm);
				break;
			case Constants.PersonName:
				ci = unpackPersonNameItem(dcm);
				break;
			case Constants.Text:
				ci = unpackTextItem(dcm);
				break;
			case Constants.Image:
				ci = unpackImageItem(dcm);
				break;
			case Constants.UidReference:
				ci = unpackUidRefItem(dcm);
				break;
			case Constants.Numeric:
				ci = unpackNumericItem(dcm);
				break;
			case Constants.SpatialCoord:
				ci = unpackSCoordItem(dcm);
				break;
			default:
				ci = new GenericContentItem();
				ci.setValueType(type);
		}
		return ci;
	}

	private static void unpackContentRelationships(ContentItem parent,
		DicomElement contentSq) throws IllegalArgumentException
	{
		int nItems = contentSq.countItems();
		if (nItems < 1)
		{
			throw new IllegalArgumentException("ContentSequence must not be empty");
		}
		for (int i=0; i<nItems; i++)
		{
			ContentRelationship cr = new DefaultContentRelationship();
			DicomObject contentDcm = contentSq.getDicomObject(i);
			cr.setRelationshipType(contentDcm.getString(Tag.RelationshipType));
			cr.setReferencedContentItemIdentifier(
				contentDcm.getInts(Tag.ReferencedContentItemIdentifier, VR.UL, null));
			cr.setRelatedContentItem(unpackContentItem(contentDcm));
			parent.addContentRelationship(cr);
		}
	}

	private static void unpackContentTemplate(DicomElement ctSq,
		ContentItem.ContainerItem container) throws IllegalArgumentException
	{
		if (ctSq.countItems() != 1)
		{
			throw new IllegalArgumentException(
				"ContentTemplateSequence must contain a single item");
		}
		ContentTemplate template = new DefaultContentTemplate();
		DicomObject dcm = ctSq.getDicomObject(0);
		template.setMappingResource(dcm.getString(Tag.MappingResource));
		template.setTemplateIdentifier(dcm.getString(Tag.TemplateIdentifier));
		container.setContentTemplate(template);
	}

	private static void unpackContourImages(DicomElement ciSq,
		ContourImageContainer container) throws IllegalArgumentException
	{
		if (ciSq == null)
		{
			return;
		}
		int nItems = ciSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = ciSq.getDicomObject(i);
			ContourImage ci = new DefaultContourImage();
			ci.setReferencedSopClassUid(
				dcm.getString(Tag.ReferencedSOPClassUID));
			ci.setReferencedSopInstanceUid(
				dcm.getString(Tag.ReferencedSOPInstanceUID));
			container.addContourImage(ci);
			String number = dcm.getString(Tag.ReferencedFrameNumber);
			if (number != null)
			{
				try
				{
					ci.setReferencedFrameNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug(
						"Invalid instance number (IS) in ContourImage: {}", number);
				}
			}
		}
	}

	private static void unpackCoordinates(DicomObject dcm, Contour contour)
		throws IllegalArgumentException
	{
		String[] data = dcm.getStrings(Tag.ContourData);
		if ((data == null) || (data.length < 3))
		{
			throw new IllegalArgumentException(
				"Contour data must be at least one triplet");
		}
		int nCoords = data.length/3;
		if (3*nCoords != data.length)
		{
			throw new IllegalArgumentException(
				"Contour data must be in triplets, found "+data.length+
					" individual points");
		}
		for (int i=0; i<data.length; i+=3)
		{
			try
			{
				Coordinate3D coord = new Coordinate3D(Double.parseDouble(data[i]),
					Double.parseDouble(data[i+1]), Double.parseDouble(data[i+2]));
				contour.addCoordinate(coord);
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Invalid point (DS) in ContourData", ex);
			}
		}
	}

	private static void unpackCurrReqProcEvidence(DicomObject dcm,
		SrDocumentGeneralModule module)
	{
		DicomElement currReqSq = dcm.get(
			Tag.CurrentRequestedProcedureEvidenceSequence);
		if (currReqSq == null)
		{
			return;
		}
		int nCurrReq = currReqSq.countItems();
		for (int i=0; i<nCurrReq; i++)
		{
			DicomObject crDcm = currReqSq.getDicomObject(i);
			module.addCurrentRequestedProcedureEvidence(unpackHeirSopInstRef(crDcm));
		}
	}

	private static DerivationImage unpackDerivationImage(DicomObject dcm)
	{
		DerivationImage image = new DefaultDerivationImage();

		DicomElement sourceImageSq = dcm.get(Tag.SourceImageSequence);
		if ((sourceImageSq != null) && (sourceImageSq.countItems() > 0))
		{
			for (int i=0; i<sourceImageSq.countItems(); i++)
			{
				image.addSourceImage(
					unpackSourceImage(sourceImageSq.getDicomObject(i), true));
			}
		}

		DicomElement derivCodeSq = dcm.get(Tag.DerivationCodeSequence);
		if (derivCodeSq != null)
		{
			for (int i=0; i<derivCodeSq.countItems(); i++)
			{
				image.addDerivationCode(unpackCode(derivCodeSq.getDicomObject(i)));
			}
		}

		return image;
	}

	private static DimensionIndex unpackDimensionIndex(DicomObject dcm)
		throws IllegalArgumentException
	{
		DimensionIndex dimIdx = new DefaultDimensionIndex();
		dimIdx.setDimensionOrganisationUid(
			dcm.getString(Tag.DimensionOrganizationUID, VR.UI, null));
		dimIdx.setIndexPointer(dcm.getInt(Tag.DimensionIndexPointer, VR.AT, 0));
		dimIdx.setFunctionalGroupPointer(
			dcm.getInt(Tag.FunctionalGroupPointer, VR.AT, 0));
		dimIdx.setDescriptionLabel(
			dcm.getString(Tag.DimensionDescriptionLabel, VR.LO, null));

		return dimIdx;
	}

	private static FrameContent unpackFrameContent(DicomObject dcm)
	{
		FrameContent fc = new DefaultFrameContent();
		int[] dimIdx = dcm.getInts(Tag.DimensionIndexValues, VR.UL, null);
		if (dimIdx != null)
		{
			fc.setDimensionIndexValues(dimIdx);
		}
		String stackId = dcm.getString(Tag.StackID, VR.SH, null);
		if (stackId != null)
		{
			fc.setStackId(stackId);
			fc.setInStackPositionNumber(
				dcm.getInt(Tag.InStackPositionNumber, VR.UL, -1));
		}

		return fc;
	}

	private static HeirarchicalSeriesReference unpackHeirSeriesRef(DicomObject dcm)
	{
		HeirarchicalSeriesReference refSeries =
			new DefaultHeirarchicalSeriesReference();
		refSeries.setSeriesInstanceUid(
			dcm.getString(Tag.SeriesInstanceUID, VR.UI, null));
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		if ((refInstSq == null) || (refInstSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"Referenced SOP sequence missing or empty");
		}
		for (int i=0; i<refInstSq.countItems(); i++)
		{
			refSeries.addReferencedInstance(
				unpackReferencedInstance(refInstSq.getDicomObject(i)));
		}

		return refSeries;
	}

	private static HeirarchicalSopInstanceReference unpackHeirSopInstRef(
		DicomObject dcm) throws IllegalArgumentException
	{
		HeirarchicalSopInstanceReference hsir =
			new DefaultHeirarchicalSopInstanceReference();
		hsir.setStudyInstanceUid(dcm.getString(Tag.StudyInstanceUID, VR.UI, null));
		DicomElement refSeriesSq = dcm.get(Tag.ReferencedSeriesSequence);
		if ((refSeriesSq == null) || (refSeriesSq.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Referenced series sequence missing or empty");
		}
		for (int i=0; i<refSeriesSq.countItems(); i++)
		{
			DicomObject serDcm = refSeriesSq.getDicomObject(i);
			HeirarchicalSeriesReference series = unpackHeirSeriesRef(serDcm);
			hsir.addSeriesReference(series);
		}
		
		return hsir;
	}

	private static ContentItem.ImageItem unpackImageItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.ImageItem item = new DefaultImageItem();
		item.setValueType(Constants.Image);
		Code cnc = unpackConceptNameCode(dcm, false);
		if (cnc != null)
		{
			item.setConceptNameCode(cnc);
		}
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		if (refInstSq == null)
		{
			throw new IllegalArgumentException("Referenced SOP sequence missing");
		}
		if (refInstSq.countItems() != 1)
		{
			throw new IllegalArgumentException(
				"Referenced SOP sequence must have a single item");
		}
		item.setReferencedInstance(
			unpackReferencedInstance(refInstSq.getDicomObject()));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static ContentItem.NumericItem unpackNumericItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.NumericItem item = new DefaultNumericItem();
		item.setValueType(Constants.Numeric);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		DicomElement measValSq = dcm.get(Tag.MeasuredValueSequence);
		if (measValSq == null)
		{
			throw new IllegalArgumentException("MeasureValueSequence missing");
		}
		int nObj = measValSq.countItems();
		if (!((nObj == 0) || (nObj == 1)))
		{
			throw new IllegalArgumentException(
				"MeasurementValueSequence must contain 0 or 1 items");
		}
		if (nObj == 1)
		{
			DicomObject valDcm = measValSq.getDicomObject();
			item.setNumericValue(valDcm.getString(Tag.NumericValue, VR.DS, null));
			double floatValue = valDcm.getDouble(0x0040a161, VR.FD, Double.NaN);
			if (!Double.isNaN(floatValue))
			{
				item.setFloatingPointValue(floatValue);
			}
			int numerator = valDcm.getInt(0x0040a162, VR.SL, 0);
			int denominator = valDcm.getInt(0x0040a163, VR.UL, 0);
			if ((numerator != 0) && (denominator != 0))
			{
				item.setRationalNumeratorValue(numerator);
				item.setRationalDenominatorValue(denominator);
			}
			DicomElement unitsSq = valDcm.get(Tag.MeasurementUnitsCodeSequence);
			if (unitsSq == null)
			{
				throw new IllegalArgumentException(
					"Measurement units code sequence missing");
			}
			if (unitsSq.countItems() != 1)
			{
				throw new IllegalArgumentException(
					"Measurement units code sequence must have a single item");
			}
			item.setMeasurementUnits(unpackCode(unitsSq.getDicomObject()));
		}
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static ContentItem.PersonNameItem unpackPersonNameItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.PersonNameItem item = new DefaultPersonNameItem();
		item.setValueType(Constants.PersonName);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setPersonName(dcm.getString(Tag.PersonName, VR.PN, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static PixelMeasures unpackPixelMeasures(DicomObject dcm,
		boolean required) throws IllegalArgumentException
	{
		double[] pixelSpacing = dcm.getDoubles(Tag.PixelSpacing, VR.DS, null);
		double sliceThickness = dcm.getDouble(Tag.SliceThickness, VR.DS, Double.NaN);
		if (required &&
			 ((pixelSpacing == null) || !MathUtils.isFinite(sliceThickness)))
		{
			throw new IllegalArgumentException(
				"Required pixel measure fields missing");
		}
		PixelMeasures pixelMeasures = new DefaultPixelMeasures();
		pixelMeasures.setPixelSpacing(pixelSpacing);
		pixelMeasures.setSliceThickness(sliceThickness);
		pixelMeasures.setSpacingBetweenSlices(
			dcm.getDouble(Tag.SpacingBetweenSlices, VR.DS, Double.NaN));

		return pixelMeasures;
	}

	private static double[] unpackPlaneOrientation(DicomObject dcm,
		boolean required) throws IllegalArgumentException
	{
		double[] ori = dcm.getDoubles(Tag.ImageOrientationPatient, VR.DS, null);
		if (required && (ori == null))
		{
			throw new IllegalArgumentException("ImageOrientationPatient missing");
		}

		return ori;
	}

	private static double[] unpackPlanePosition(DicomObject dcm,
		boolean required) throws IllegalArgumentException
	{
		double[] pos = dcm.getDoubles(Tag.ImagePositionPatient, VR.DS, null);
		if (required && (pos == null))
		{
			throw new IllegalArgumentException("ImagePositionPatient missing");
		}

		return pos;
	}

	private static void unpackReferencedFrameOfReference(DicomElement refFoRSq,
		StructureSetModule ss) throws IllegalArgumentException
	{
		if (refFoRSq == null)
		{
			return;
		}
		int nItems = refFoRSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = refFoRSq.getDicomObject(i);
			ReferencedFrameOfReference refFoR =
				new DefaultReferencedFrameOfReference();
			refFoR.setFrameOfReferenceUid(dcm.getString(Tag.FrameOfReferenceUID));
			ss.addReferencedFrameOfReference(refFoR);
			unpackRtReferencedStudy(dcm.get(Tag.RTReferencedStudySequence), refFoR);
		}
	}

	private static ReferencedInstance unpackReferencedInstance(DicomObject dcm)
		throws IllegalArgumentException
	{
		ReferencedInstance inst = new DefaultReferencedInstance();
		inst.setReferencedSopClassUid(
			dcm.getString(Tag.ReferencedSOPClassUID, VR.UI, null));
		inst.setReferencedSopInstanceUid(
			dcm.getString(Tag.ReferencedSOPInstanceUID, VR.UI, null));
		int number = dcm.getInt(Tag.ReferencedFrameNumber, VR.IS, -1);
		if (number > 0)
		{
			inst.setReferencedFrameNumber(number);
		}

		return inst;
	}

	private static ReferencedSeries unpackReferencedSeries(DicomObject dcm)
		throws IllegalArgumentException
	{
		ReferencedSeries refSeries = new DefaultReferencedSeries();
		refSeries.setSeriesInstanceUid(
			dcm.getString(Tag.SeriesInstanceUID, VR.UI, null));
		DicomElement refInstSq = dcm.get(Tag.ReferencedInstanceSequence);
		if ((refInstSq == null) || (refInstSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"ReferencedInstanceSequence missing or empty");
		}
		for (int i=0; i<refInstSq.countItems(); i++)
		{
			refSeries.addReferencedInstance(
				unpackReferencedInstance(refInstSq.getDicomObject(i)));
		}

		return refSeries;
	}

	private static void unpackRoiContour(DicomElement contourSq, RoiContour rc)
		throws IllegalArgumentException
	{
		if (contourSq == null)
		{
			return;
		}
		int nItems = contourSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject item = contourSq.getDicomObject(i);
			Contour contour = new DefaultContour();
			String number = item.getString(Tag.ContourNumber);
			if (number != null)
			{
				try
				{
					contour.setContourNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug(
						"Invalid instance number (IS) in Contour: {}", number);	
					// See else branch
					contour.setContourNumber(i+1);
				}
			}
			else
			{
				// Standard is a pain here. Contour number is optional but required
				// to be unique. RoiContour stores Contours in a map using contour
				// number so a workaround is to enforce a unique number.
				contour.setContourNumber(i+1);
			}
			contour.setContourGeometricType(item.getString(Tag.ContourGeometricType));
			unpackCoordinates(item, contour);
			rc.addContour(contour);
			unpackContourImages(item.get(Tag.ContourImageSequence), contour);
		}
	}

	private static void unpackRtReferencedSeries(DicomElement refSeriesSq,
		RtReferencedStudy refStudy) throws IllegalArgumentException
	{
		if (refSeriesSq == null)
		{
			return;
		}
		int nItems = refSeriesSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = refSeriesSq.getDicomObject(i);
			RtReferencedSeries refSeries = new DefaultRtReferencedSeries();
			refSeries.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
			refStudy.addRtReferencedSeries(refSeries);
			unpackContourImages(dcm.get(Tag.ContourImageSequence), refSeries);
		}
	}

	private static void unpackRtReferencedStudy(DicomElement refStudySq,
		ReferencedFrameOfReference refFoR) throws IllegalArgumentException
	{
		if (refStudySq == null)
		{
			return;
		}
		int nItems = refStudySq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = refStudySq.getDicomObject(i);
			RtReferencedStudy refStudy = new DefaultRtReferencedStudy();
			refStudy.setReferencedSopClassUid(
				dcm.getString(Tag.ReferencedSOPClassUID));
			refStudy.setReferencedSopInstanceUid(
				dcm.getString(Tag.ReferencedSOPInstanceUID));
			refFoR.addRtReferencedStudy(refStudy);
			unpackRtReferencedSeries(dcm.get(Tag.RTReferencedSeriesSequence),
				refStudy);
		}
	}

	private static ContentItem.SpatialCoordItem unpackSCoordItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.SpatialCoordItem item = new DefaultSpatialCoordItem();
		item.setValueType(Constants.SpatialCoord);
		item.setConceptNameCode(unpackConceptNameCode(dcm, false));
		String gfxType = dcm.getString(Tag.GraphicType);
		item.setGraphicType(gfxType);
		float[] gfxData = dcm.getFloats(Tag.GraphicData, VR.FL, new float[] {});
		if (gfxData.length == 0)
		{
			throw new IllegalArgumentException("Graphics data missing or empty");
		}
		switch (gfxType)
		{
			case Constants.Point:
				if (gfxData.length != 2)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly one column/row pair");
				}
				break;
			case Constants.MultiPoint:
			case Constants.Polyline:
				if ((gfxData.length % 2) != 0)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires multiple one column/row pairs");
				}
				break;
			case Constants.Circle:
				if (gfxData.length != 4)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly two column/row pairs");
				}
				break;
			case Constants.Ellipse:
				if (gfxData.length != 8)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly four column/row pairs");
				}
				break;
			default:
				throw new IllegalArgumentException("Unknown graphic type: "+gfxType);
		}
		for (int i=0; i<gfxData.length; i+=2)
		{
			item.addCoordinate(gfxData[i], gfxData[i+1]);
		}

		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static void unpackSegment(DicomObject dcm,
		SegmentationImageModule module) throws IllegalArgumentException
	{
		Segment segment = new DefaultSegment();
		segment.setSegmentNumber(dcm.getInt(Tag.SegmentNumber, VR.US, -1));
		segment.setSegmentLabel(dcm.getString(Tag.SegmentLabel, VR.LO, null));
		segment.setSegmentDescription(
			dcm.getString(Tag.SegmentDescription, VR.ST, null));
		segment.setSegmentAlgorithmType(
			dcm.getString(Tag.SegmentAlgorithmType, VR.CS, null));
		if (!segment.getSegmentAlgorithmType().equals(Constants.Manual))
		{
			segment.setSegmentAlgorithmName(
				dcm.getString(Tag.SegmentAlgorithmName, VR.LO, null));
		}
		int[] cieValue = dcm.getInts(Tag.RecommendedDisplayCIELabValue, VR.US, null);
		if ((cieValue != null) && (cieValue.length == 3))
		{
			segment.setRecommendedDisplayCIELabValue(cieValue);
		}

		DicomElement anatomicRegSq = dcm.get(Tag.AnatomicRegionSequence);
		if (anatomicRegSq != null)
		{
			for (int i=0; i<anatomicRegSq.countItems(); i++)
			{
				Code anatomicRegCode = unpackCode(anatomicRegSq.getDicomObject(i),
					Tag.AnatomicRegionModifierSequence);
				segment.addAnatomicRegionCode(anatomicRegCode);
			}
		}
		DicomElement propCategorySq =
			dcm.get(Tag.SegmentedPropertyCategoryCodeSequence);
		if ((propCategorySq == null) || (propCategorySq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyCategoryCodeSequence missing or invalid size");
		}
		segment.setSegmentedPropertyCategoryCode(
			unpackCode(propCategorySq.getDicomObject(0)));
		DicomElement propTypeSq = dcm.get(Tag.SegmentedPropertyTypeCodeSequence);
		if ((propTypeSq == null) || (propTypeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyTypeCodeSequence missing or invalid size");
		}
		segment.setSegmentedPropertyTypeCode(
			unpackCode(propTypeSq.getDicomObject(0), 0x00620011));

		module.addSegment(segment);
	}

	private static FunctionalGroupsFrame unpackSegmentationFrame(DicomObject dcm,
		String frameOfRefUid) throws IllegalArgumentException
	{
		SegmentationFunctionalGroupsFrame frame =
			new DefaultSegmentationFunctionalGroupsFrame();

		DicomElement frameContentSq = dcm.get(Tag.FrameContentSequence);
		if (!((frameContentSq != null) && (frameContentSq.countItems() == 1)))
		{
			throw new IllegalArgumentException("FrameContent missing");
		}
		frame.setFrameContent(
			unpackFrameContent(frameContentSq.getDicomObject()));

		DicomElement derivImageSq = dcm.get(Tag.DerivationImageSequence);
		if ((derivImageSq != null) && (derivImageSq.countItems() > 0))
		{
			for (int i=0; i<derivImageSq.countItems(); i++)
			{
				frame.addDerivationImage(
					unpackDerivationImage(derivImageSq.getDicomObject(i)));
			}
		}

		DicomElement planePosSq = dcm.get(Tag.PlanePositionSequence);
		if ((planePosSq != null) && (planePosSq.countItems() == 1))
		{
			frame.setImagePositionPatient(
				unpackPlanePosition(planePosSq.getDicomObject(),
					(frameOfRefUid != null)));
		}

		DicomElement segIdSq = dcm.get(Tag.SegmentIdentificationSequence);
		if (!((segIdSq != null) && (segIdSq.countItems() == 1)))
		{
			throw new IllegalArgumentException("SegmentIdentification missing");
		}
		int refSeg = segIdSq.getDicomObject().getInt(
			Tag.ReferencedSegmentNumber, VR.US, -1);
		frame.setReferencedSegmentNumber(refSeg);


		return frame;
	}

	private static void unpackSegmentationStorage(DicomObject dcm,
		MultiframeFunctionalGroupsModule module) throws IllegalArgumentException
	{
		DicomElement sharedSq = dcm.get(Tag.SharedFunctionalGroupsSequence);
		if ((sharedSq == null) || (sharedSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SharedFunctionalGroups missing or empty");
		}
		DicomElement perFrameSq = dcm.get(Tag.PerFrameFunctionalGroupsSequence);
		int nFrames = dcm.getInt(Tag.NumberOfFrames, -1);
		if ((perFrameSq == null) || (perFrameSq.countItems() != nFrames))
		{
			throw new IllegalArgumentException(
				"PerFrameFunctionalGroups missing or invalid length");
		}

		DefaultSegmentationSharedFunctionalGroups sharedGroups =
			(DefaultSegmentationSharedFunctionalGroups) module.getSharedFunctionalGroups();
		String frameOfRefUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		DicomElement pixMeasSq =
			sharedSq.getDicomObject().get(Tag.PixelMeasuresSequence);
		if ((pixMeasSq != null) && (pixMeasSq.countItems() == 1))
		{
			sharedGroups.setPixelMeasures(
				unpackPixelMeasures(pixMeasSq.getDicomObject(),
					(frameOfRefUid != null)));
		}
		DicomElement planeOriSq =
			sharedSq.getDicomObject().get(Tag.PlaneOrientationSequence);
		if ((planeOriSq != null) && (planeOriSq.countItems() == 1))
		{
			sharedGroups.setImageOrientationPatient(
				unpackPlaneOrientation(planeOriSq.getDicomObject(),
					(frameOfRefUid != null)));
		}

		DefaultSegmentationPerFrameFunctionalGroups perFrameGroups =
			(DefaultSegmentationPerFrameFunctionalGroups) module.getPerFrameFunctionalGroups();
		for (int i=0; i<nFrames; i++)
		{
			FunctionalGroupsFrame frame = unpackSegmentationFrame(
				perFrameSq.getDicomObject(i), frameOfRefUid);
			perFrameGroups.addFrame(frame);
		}
	}

	private static SourceImage unpackSourceImage(DicomObject dcm, boolean required)
		throws IllegalArgumentException
	{
		SourceImage image = new DefaultSourceImage(required);
		image.setReferencedSopClassUid(
			dcm.getString(Tag.ReferencedSOPClassUID, VR.UI, null));
		image.setReferencedSopInstanceUid(
			dcm.getString(Tag.ReferencedSOPInstanceUID, VR.UI, null));
		int number = dcm.getInt(Tag.ReferencedFrameNumber, VR.IS, -1);
		if (number > 0)
		{
			image.setReferencedFrameNumber(number);
		}
		image.setSpatialLocationsPreserved(
			dcm.getString(Tag.SpatialLocationsPreserved, VR.CS, null));
		DicomElement codeSq = dcm.get(Tag.PurposeOfReferenceCodeSequence);
		if (required)
		{
			if (!((codeSq != null) && (codeSq.countItems() == 1)))
			{
				throw new IllegalArgumentException(
					"PurposeOfReferenceCodeSequence missing");
			}
		}
		if (codeSq != null)
		{
			image.setPurposeOfReferenceCode(unpackCode(codeSq.getDicomObject()));
		}

		return image;
	}

	private static void unpackStructureSetRoi(DicomElement roiSq,
		StructureSetModule ss) throws IllegalArgumentException
	{
		if (roiSq == null)
		{
			throw new IllegalArgumentException(
				"StructureSetROISequence must not be null");
		}
		int nItems = roiSq.countItems();
		if (nItems < 1)
		{
			throw new IllegalArgumentException(
				"StructureSetROISequence must not be empty");
		}
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = roiSq.getDicomObject(i);
			StructureSetRoi ssRoi = new DefaultStructureSetRoi();
			String number = dcm.getString(Tag.ROINumber);
			if (number != null)
			{
				try
				{
					ssRoi.setRoiNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug(
						"Invalid instance number (IS) in StructureSetRoi: {}", number);
				}
			}
			ssRoi.setReferencedFrameOfReferenceUid(
				dcm.getString(Tag.ReferencedFrameOfReferenceUID));
			ss.addStructureSetRoi(ssRoi);
			ssRoi.setRoiName(dcm.getString(Tag.ROIName));
			ssRoi.setRoiDescription(dcm.getString(Tag.ROIDescription));
			ssRoi.setRoiGenerationAlgorithm(
				dcm.getString(Tag.ROIGenerationAlgorithm));
		}
	}

	private static ContentItem.TextItem unpackTextItem(DicomObject dcm)
	{
		ContentItem.TextItem item = new DefaultTextItem();
		item.setValueType(Constants.Text);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setText(dcm.getString(Tag.TextValue, VR.UT, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private static ContentItem.UidRefItem unpackUidRefItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.UidRefItem item = new DefaultUidRefItem();
		item.setValueType(Constants.UidReference);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setUidReference(dcm.getString(Tag.UID, VR.UI, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}

		return item;
	}

	private Modules()
	{}
}
