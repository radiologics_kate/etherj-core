/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;

/**
 * RtSeries module in DICOM IOD.
 * @author jamesd
 */
public interface RtSeriesModule extends Displayable, Validatable
{
	/**
	 * Returns the modality, type 1. Enumerated values: RTDOSE, RTIMAGE,
	 * RTPLAN, RTRECORD, RTSTRUCT.
	 * @return
	 */
	String getModality();

	/**
	 * Returns the operators name, type 3.
	 * @return
	 */
	String getOperatorsName();

	/**
	 *	Returns the series date, type 3.
	 * @return
	 */
	String getSeriesDate();

	/**
	 * Returns the series description, type 3.
	 * @return
	 */
	String getSeriesDescription();

	/**
	 * Returns the series instance UID, type 1.
	 * @return
	 */
	String getSeriesInstanceUid();

	/**
	 * Returns the series number, type 2. 
	 * @return
	 */
	int getSeriesNumber();

	/**
	 * Returns the series time, type 3.
	 * @return
	 */
	String getSeriesTime();

	/**
	 *	Sets the modality, type 1. Enumerated values: RTDOSE, RTIMAGE, RTPLAN,
	 * RTRECORD, RTSTRUCT.
	 * @param modality
	 * @throws IllegalArgumentException
	 */
	void setModality(String modality) throws IllegalArgumentException;

	/**
	 * Sets the operators name, type 3.
	 * @param name
	 */
	void setOperatorsName(String name);

	/**
	 * Sets the series date, type 3.
	 * @param date
	 */
	void setSeriesDate(String date);

	/**
	 * Sets the series description, type 3.
	 * @param description
	 */
	void setSeriesDescription(String description);

	/**
	 * Sets the series instance UID, type 1.
	 * @param uid
	 * @throws IllegalArgumentException
	 */
	void setSeriesInstanceUid(String uid) throws IllegalArgumentException;

	/**
	 * Sets the series number, type 2.
	 * @param number
	 */
	void setSeriesNumber(int number);

	/**
	 * Sets the series time, type 3.
	 * @param time
	 */
	void setSeriesTime(String time);

}
