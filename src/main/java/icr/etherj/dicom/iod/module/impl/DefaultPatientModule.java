/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.module.PatientModule;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public final class DefaultPatientModule extends AbstractDisplayable
	implements PatientModule
{
	private String birthDate = "";
	private String id = "";
	private String name = "";
	private String sex;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"PatientName: "+name);
		ps.println(pad+"PatientId: "+id);
		ps.println(pad+"PatientBirthDate: "+birthDate);
		if (!StringUtils.isNullOrEmpty(sex))
		{
			ps.println(pad+"PatientSex: "+sex);
		}
	}

	/**
	 * @return the birthDate
	 */
	@Override
	public String getPatientBirthDate()
	{
		return birthDate;
	}

	/**
	 * @return the id
	 */
	@Override
	public String getPatientId()
	{
		return id;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getPatientName()
	{
		return name;
	}

	/**
	 * @return the sex
	 */
	@Override
	public String getPatientSex()
	{
		return sex;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	@Override
	public void setPatientBirthDate(String birthDate)
	{
		this.birthDate = (birthDate != null) ? birthDate : "";
			
	}

	/**
	 * @param id the id to set
	 */
	@Override
	public void setPatientId(String id)
	{
		this.id = (id != null) ? id : "";
	}

	/**
	 * @param name the name to set
	 */
	@Override
	public void setPatientName(String name)
	{
		this.name = (name != null) ? name : "";
	}

	/**
	 * Set the sex, must be M, F, O as enumerated by the DICOM standard
	 * @param sex the sex to set
	 */
	@Override
	public void setPatientSex(String sex)
	{
		if (sex == null)
		{
			this.sex = sex;
			return;
		}
		switch (sex)
		{
			case "M":
			case "F":
			case "O":
				this.sex = sex;
				break;
			default:
				this.sex = "O";
		}
	}

	@Override
	public boolean validate()
	{
		String clazz = "PatientModule";
		boolean dobOk = Validation.type2Date(clazz, birthDate, Tag.PatientBirthDate);
		boolean idOk = Validation.type2(clazz, id, Tag.PatientID);
		boolean nameOk = Validation.type2(clazz, name, Tag.PatientName);
		boolean sexOk = Validation.type3(clazz, sex, new String[] {"M", "F", "O" },
			Tag.PatientSex);
		return dobOk && idOk && nameOk && sexOk;
	}
	
}
