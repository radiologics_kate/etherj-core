/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface GeneralSeriesModule extends Displayable
{

	/**
	 * Returns largest pixel value in series, type 3.
	 * @return
	 */
	int getLargestPixelValueInSeries();

	/**
	 * Returns the laterality, type 2C.
	 * @return
	 */
	String getLaterality();

	/**
	 * Returns the modality, type 1.
	 * @return
	 */
	String getModality();

	/**
	 * Returns the operators name, type 3.
	 * @return
	 */
	String getOperatorsName();

	/**
	 * Returns the patient position, type 2C. Enumerated values:
	 * {@link icr.etherj.dicom.iod.Constants#HFP},
	 * {@link icr.etherj.dicom.iod.Constants#HFS},
	 * {@link icr.etherj.dicom.iod.Constants#HFDR},
	 * {@link icr.etherj.dicom.iod.Constants#HFDL},
	 * {@link icr.etherj.dicom.iod.Constants#FFDR},
	 * {@link icr.etherj.dicom.iod.Constants#FFDL},
	 * {@link icr.etherj.dicom.iod.Constants#FFP},
	 * {@link icr.etherj.dicom.iod.Constants#FFS},
	 * {@link icr.etherj.dicom.iod.Constants#LFP},
	 * {@link icr.etherj.dicom.iod.Constants#LFS},
	 * {@link icr.etherj.dicom.iod.Constants#RFP},
	 * {@link icr.etherj.dicom.iod.Constants#RFS},
	 * {@link icr.etherj.dicom.iod.Constants#AFDR},
	 * {@link icr.etherj.dicom.iod.Constants#AFDL},
	 * {@link icr.etherj.dicom.iod.Constants#PFDR},
	 * {@link icr.etherj.dicom.iod.Constants#PFDL}.
	 * @return
	 */
	String getPatientPosition();

	/**
	 * Returns the protocol name, type 3.
	 * @return
	 */
	String getProtocolName();

	/**
	 *	Returns the series date, type 3.
	 * @return
	 */
	String getSeriesDate();

	/**
	 * Returns the series description, type 3.
	 * @return
	 */
	String getSeriesDescription();

	/**
	 * Returns the series instance UID, type 1.
	 * @return
	 */
	String getSeriesInstanceUid();

	/**
	 * Returns the series number, type 2. 
	 * @return
	 */
	int getSeriesNumber();

	/**
	 * Returns the series time, type 3.
	 * @return
	 */
	String getSeriesTime();

	/**
	 * Returns smallest pixel value in series, type 3.
	 * @return
	 */
	int getSmallestPixelValueInSeries();

	/**
	 * Sets largest pixel value in series, type 3.
	 * @param value
	 */
	void setLargestPixelValueInSeries(int value);

	/**
	 * Sets the laterality, type 2C. Enumerated values: L, R.
	 * @param laterality
	 * @throws IllegalArgumentException
	 */
	void setLaterality(String laterality) throws IllegalArgumentException;

	/**
	 *	Sets the modality, type 1.
	 * @param modality
	 * @throws IllegalArgumentException
	 */
	void setModality(String modality) throws IllegalArgumentException;

	/**
	 * Sets the operators name, type 3.
	 * @param name
	 */
	void setOperatorsName(String name);

	/**
	 * Sets the patient position, type 2C. Enumerated values:
	 * {@link icr.etherj.dicom.iod.Constants#HFP},
	 * {@link icr.etherj.dicom.iod.Constants#HFS},
	 * {@link icr.etherj.dicom.iod.Constants#HFDR},
	 * {@link icr.etherj.dicom.iod.Constants#HFDL},
	 * {@link icr.etherj.dicom.iod.Constants#FFDR},
	 * {@link icr.etherj.dicom.iod.Constants#FFDL},
	 * {@link icr.etherj.dicom.iod.Constants#FFP},
	 * {@link icr.etherj.dicom.iod.Constants#FFS},
	 * {@link icr.etherj.dicom.iod.Constants#LFP},
	 * {@link icr.etherj.dicom.iod.Constants#LFS},
	 * {@link icr.etherj.dicom.iod.Constants#RFP},
	 * {@link icr.etherj.dicom.iod.Constants#RFS},
	 * {@link icr.etherj.dicom.iod.Constants#AFDR},
	 * {@link icr.etherj.dicom.iod.Constants#AFDL},
	 * {@link icr.etherj.dicom.iod.Constants#PFDR},
	 * {@link icr.etherj.dicom.iod.Constants#PFDL}.
	 * @param position
	 */
	void setPatientPosition(String position);

	/**
	 * Sets the protocol name, type 3.
	 * @param name
	 */
	void setProtocolName(String name);

	/**
	 * Sets the series date, type 3.
	 * @param date
	 */
	void setSeriesDate(String date);

	/**
	 * Sets the series description, type 3.
	 * @param description
	 */
	void setSeriesDescription(String description);

	/**
	 * Sets the series instance UID, type 1.
	 * @param uid
	 * @throws IllegalArgumentException
	 */
	void setSeriesInstanceUid(String uid) throws IllegalArgumentException;

	/**
	 * Sets the series number, type 2.
	 * @param number
	 */
	void setSeriesNumber(int number);

	/**
	 * Sets the series time, type 3.
	 * @param time
	 */
	void setSeriesTime(String time);
	
	/**
	 * Sets smallest pixel value in series, type 3.
	 * @param value
	 */
	void setSmallestPixelValueInSeries(int value);

}
