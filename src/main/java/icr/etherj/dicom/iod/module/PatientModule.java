/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;

/**
 * Patient module in DICOM IOD.
 * @author jamesd
 */
public interface PatientModule extends Displayable, Validatable
{
	/**
	 * Returns the patient birth date, type 2.
	 * @return the birthDate
	 */
	String getPatientBirthDate();

	/**
	 * Returns the patient ID, type 2.
	 * @return the id
	 */
	String getPatientId();

	/**
	 * Returns the patient name, type 2.
	 * @return the name
	 */
	String getPatientName();

	/**
	 * Returns the patient sex, type 3.
	 * @return the sex
	 */
	String getPatientSex();

	/**
	 * Sets the patient birth date, type 2.
	 * @param birthDate the birthDate to set
	 */
	void setPatientBirthDate(String birthDate);

	/**
	 * Sets the patient ID, type 2.
	 * @param id the id to set
	 */
	void setPatientId(String id);

	/**
	 * Sets the patient name, type 2.
	 * @param name the name to set
	 */
	void setPatientName(String name);

	/**
	 * Set the patient sex, must be M, F, O as enumerated by the DICOM standard,
	 * type 3.
	 * @param sex the sex to set
	 */
	void setPatientSex(String sex);
}
