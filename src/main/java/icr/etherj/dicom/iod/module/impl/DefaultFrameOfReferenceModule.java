/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultFrameOfReferenceModule extends AbstractDisplayable
	implements FrameOfReferenceModule
{
	private String indicator = "";
	private String uid = null;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (uid == null)
		{
			return;
		}
		ps.println(pad+"FrameOfReferenceUid: "+uid);
		if (!indicator.isEmpty())
		{
			ps.println(pad+"PositionReferenceIndicator: "+indicator);
		}
	}

	@Override
	public String getFrameOfReferenceUid()
	{
		return uid;
	}

	@Override
	public String getPositionReferenceIndicator()
	{
		return indicator;
	}

	@Override
	public void setFrameOfReferenceUid(String uid) throws IllegalArgumentException
	{
		if (uid == null)
		{
			this.uid = null;
			return;
		}
		if (uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"FrameOfReferenceUid must not be empty");
		}
		this.uid = uid;
	}

	@Override
	public void setPositionReferenceIndicator(String indicator)
	{
		this.indicator = (indicator != null) ? indicator : "";
	}
	
}
