/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Validatable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.module.RtRoiObservationsModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public class DefaultRtRoiObservationsModule extends AbstractDisplayable
	implements RtRoiObservationsModule
{
	private final List<RtRoiObservation> rtRoiObsList = new ArrayList<>();

	@Override
	public boolean addRtRoiObservation(RtRoiObservation observation)
	{
		return (observation != null) ? rtRoiObsList.add(observation) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nObs = rtRoiObsList.size();
		ps.println(pad+"RtRoiObservations: "+nObs+" RT ROI observation"+
			((nObs != 1) ? "s" : ""));
		if (recurse)
		{
			for (RtRoiObservation rro : rtRoiObsList)
			{
				rro.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<RtRoiObservation> getRtRoiObservationList()
	{
		return ImmutableList.copyOf(rtRoiObsList);
	}

	@Override
	public boolean validate()
	{
		String clazz = "RtRoiObservationsModule";
		List<Validatable> list = new ArrayList<>();
		list.addAll(rtRoiObsList);
		return Validation.type1(clazz, list, 1, Tag.RTROIObservationsSequence);
	}
	
}
