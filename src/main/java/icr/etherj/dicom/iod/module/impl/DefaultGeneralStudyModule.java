/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public final class DefaultGeneralStudyModule extends AbstractDisplayable
	implements GeneralStudyModule
{
	private String accession = "";
	private String date = "";
	private String desc = null;
	private String id = "";
	private String refPhys = "";
	private String time = "";
	private String uid = Uids.generateDicomUid();
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"StudyInstanceUid: "+uid);
		ps.println(pad+"StudyDate: "+date);
		ps.println(pad+"StudyTime: "+time);
		ps.println(pad+"ReferringPhysicianName: "+refPhys);
		ps.println(pad+"StudyID: "+id);
		ps.println(pad+"AccessionNumber: "+accession);
		if ((desc != null) && !desc.isEmpty())
		{
			ps.println(pad+"StudyDescription: "+desc);
		}
	}

	@Override
	public String getAccessionNumber()
	{
		return accession;
	}

	@Override
	public String getStudyDate()
	{
		return date;
	}

	@Override
	public String getStudyDescription()
	{
		return desc;
	}

	@Override
	public String getStudyId()
	{
		return id;
	}

	@Override
	public String getStudyInstanceUid()
	{
		return uid;
	}

	@Override
	public String getReferringPhysicianName()
	{
		return refPhys;
	}

	@Override
	public String getStudyTime()
	{
		return time;
	}

	@Override
	public void setAccessionNumber(String accessionNumber)
	{
		accession = (accessionNumber != null) ? accessionNumber : "";
	}

	@Override
	public void setStudyDate(String date)
	{
		this.date = (date != null) ? date : "";
	}

	@Override
	public void setStudyDescription(String description)
	{
		desc = description;
	}

	@Override
	public void setStudyId(String id)
	{
		this.id = (id != null) ? id : "";
	}

	@Override
	public void setStudyInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"StudyInstanceUid must not be null or empty");
		}
		this.uid = uid;
	}

	@Override
	public void setReferringPhysician(String name)
	{
		refPhys = (name != null) ? name : "";
	}

	@Override
	public void setStudyTime(String time)
	{
		this.time = (time != null) ? time : "";
	}

	@Override
	public boolean validate()
	{
		String clazz = "GeneralStudyModule";
		boolean uidOk = Validation.type1(clazz, uid, Tag.StudyInstanceUID);
		boolean dateOk = Validation.type2Date(clazz, date, Tag.StudyDate);
		boolean timeOk = Validation.type2Time(clazz, time, Tag.StudyTime);
		boolean physOk = Validation.type2(clazz, refPhys, Tag.ReferringPhysicianName);
		boolean idOk = Validation.type2(clazz, id, Tag.StudyID);
		boolean accOk = Validation.type2(clazz, accession, Tag.AccessionNumber);

		return uidOk && dateOk && timeOk && physOk && idOk && accOk;
	}
	
}
