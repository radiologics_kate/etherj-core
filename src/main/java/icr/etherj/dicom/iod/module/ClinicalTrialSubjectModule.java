/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface ClinicalTrialSubjectModule extends Displayable
{

	/**
	 * Returns the clinical trial protocol ID, type 1.
	 * @return
	 */
	String getClinicalTrialProtocolId();

	/**
	 * Returns the clinical trial protocol name, type 2.
	 * @return
	 */
	String getClinicalTrialProtocolName();

	/**
	 * Returns the clinical trial site ID, type 2.
	 * @return
	 */
	String getClinicalTrialSiteId();

	/**
	 *	Returns the clinical trial site name, type 2.
	 * @return
	 */
	String getClinicalTrialSiteName();

	/**
	 * Returns the clinical trial sponsor name, type 1.
	 * @return
	 */
	String getClinicalTrialSponsorName();

	/**
	 * Returns the clinical trial subject ID, type 1C. Shall be present if
	 * clinical trial subject reading ID is absent.
	 * @return
	 */
	String getClinicalTrialSubjectId();

	/**
	 * Returns the clinical trial subject reading ID, type 1C. Shall be present
	 * if clinical trial subject ID is absent.
	 * @return
	 */
	String getClinicalTrialSubjectReadingId();

	/**
	 * Sets the clinical trial protocol ID, type 1. Setting this to null
	 * disables the module.
	 * @param id
	 */
	void setClinicalTrialProtocolId(String id);

	/**
	 * Sets the clinical trial protocol name, type 2.
	 * @param name
	 */
	void setClinicalTrialProtocolName(String name);

	/**
	 * Sets the clinical trial site ID, type 2.
	 * @param id
	 */
	void setClinicalTrialSiteId(String id);

	/**
	 * Sets the clinical trial site name, type 2.
	 * @param name
	 */
	void setClinicalTrialSiteName(String name);

	/**
	 * Sets the clinical trial sponsor name, type 1. Setting this to null
	 * disables the module.
	 * @param name
	 */
	void setClinicalTrialSponsorName(String name);

	/**
	 * Sets the clinical trial subject ID, type 1C. Shall be present
	 * if clinical trial subject reading ID is absent.
	 * @param id
	 * @throws IllegalArgumentException
	 */
	void setClinicalTrialSubjectId(String id) throws IllegalArgumentException;

	/**
	 * Sets the clinical trial subject reading ID, type 1C. Shall be present
	 * if clinical trial subject ID is absent.
	 * @param id
	 * @throws IllegalArgumentException
	 */
	void setClinicalTrialSubjectReadingId(String id)
		throws IllegalArgumentException;
}
