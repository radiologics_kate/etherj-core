/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class SegmentationImagePixelModule extends AbstractDisplayable
	implements ImagePixelModule
{
	private int maxPix = -1;
	private int minPix = -1;
	private int nCols = 0;
	private int nRows = 0;
	private byte[] pixelData = new byte[] {};
	private final SegmentationMultiModuleCore segMultiModCore;

	public SegmentationImagePixelModule(SegmentationMultiModuleCore segMultiModCore)
		throws IllegalArgumentException
	{
		if (segMultiModCore == null)
		{
			throw new IllegalArgumentException(
				"Segmentation multimodule core must not be null");
		}
		this.segMultiModCore = segMultiModCore;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SamplesPerPixel: "+getSamplesPerPixel());
		ps.println(pad+"PhotometricInterpretation: "+
			getPhotometricInterpretation());
		ps.println(pad+"Rows: "+nRows);
		ps.println(pad+"Columns: "+nCols);
		ps.println(pad+"BitsAllocated: "+getBitsAllocated());
		ps.println(pad+"BitsStored: "+getBitsStored());
		ps.println(pad+"HighBit: "+getHighBit());
		ps.println(pad+"PixelRepresentation: "+getPixelRepresentation());
		if (minPix >= 0)
		{
			ps.println(pad+"SmallestPixelValue: "+minPix);
		}
		if (maxPix >= 0)
		{
			ps.println(pad+"LargestPixelValue: "+maxPix);
		}
	}

	@Override
	public int getBitsAllocated()
	{
		return segMultiModCore.getBitsAllocated();
	}

	@Override
	public int getBitsStored()
	{
		return segMultiModCore.getBitsStored();
	}

	@Override
	public int getColumnCount()
	{
		return nCols;
	}

	@Override
	public int getHighBit()
	{
		return segMultiModCore.getHighBit();
	}

	@Override
	public int getLargestPixelValue()
	{
		return maxPix;
	}

	@Override
	public String getPhotometricInterpretation()
	{
		return segMultiModCore.getPhotometricInterpretation();
	}

	@Override
	public byte[] getPixelData()
	{
		return pixelData;
	}

	@Override
	public int getPixelRepresentation()
	{
		return segMultiModCore.getPixelRepresentation();
	}

	@Override
	public int getPlanarConfiguration()
	{
		throw new UnsupportedOperationException("Not valid for segmentations");
	}

	@Override
	public int getRowCount()
	{
		return nRows;
	}

	@Override
	public int getSamplesPerPixel()
	{
		return segMultiModCore.getSamplesPerPixel();
	}

	@Override
	public int getSmallestPixelValue()
	{
		return minPix;
	}

	@Override
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		segMultiModCore.setBitsAllocated(bits);
	}

	@Override
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		segMultiModCore.setBitsStored(bits);
	}

	@Override
	public void setColumnCount(int columns) throws IllegalArgumentException
	{
		if (columns < 1)
		{
			throw new IllegalArgumentException("Invalid column count: "+columns);
		}
		nCols = columns;
	}

	@Override
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		segMultiModCore.setHighBit(bit);
	}

	@Override
	public void setLargestPixelValue(int value)
	{
		maxPix = (value >= 0) ? value : -1;
	}

	@Override
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		if (!Constants.Monochrome2.equals(interpretation))
		{
			throw new IllegalArgumentException(
				"Photometric interpretation must be "+Constants.Monochrome2);
		}
	}

	@Override
	public void setPixelData(byte[] pixels) throws IllegalArgumentException
	{
		if (pixels == null)
		{
			throw new IllegalArgumentException("PixelData must not be null");
		}
		this.pixelData = Arrays.copyOf(pixels, pixels.length);
	}

	@Override
	public void setPixelRepresentation(int representation)
		throws IllegalArgumentException
	{
		if (representation != 0)
		{
			throw new IllegalArgumentException("Pixel representation must be 0");
		}
	}

	@Override
	public void setPlanarConfiguration(int configuration) throws IllegalArgumentException
	{
		throw new UnsupportedOperationException("Not valid for segmentations");
	}

	@Override
	public void setRowCount(int rows) throws IllegalArgumentException
	{
		if (rows < 1)
		{
			throw new IllegalArgumentException("Invalid row count: "+rows);
		}
		nRows = rows;
	}

	@Override
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		if (samples != 1)
		{
			throw new IllegalArgumentException("Samples per pixel must be 1");
		}
	}

	@Override
	public void setSmallestPixelValue(int value)
	{
		minPix = (value >= 0) ? value : -1;
	}
	
}
