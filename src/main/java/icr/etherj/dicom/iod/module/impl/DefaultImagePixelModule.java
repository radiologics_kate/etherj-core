/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import java.io.PrintStream;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultImagePixelModule extends AbstractDisplayable
	implements ImagePixelModule
{
	private final static Logger logger = LoggerFactory.getLogger(
		DefaultImagePixelModule.class);

	private int bitsAlloc = 16;
	private int bitsStored = 12;
	private int highBit = 11;
	private int maxPix = -1;
	private int minPix = -1;
	private int nCols = 0;
	private int nRows = 0;
	private int nSamples = 1;
	private String photoInterp = Constants.Monochrome2;
	private byte[] pixelData = new byte[] {};
	private int pixelRep = Constants.UnsignedInteger;
	private int planarConf = Constants.Samplewise;
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SamplesPerPixel: "+nSamples);
		ps.println(pad+"PhotometricInterpretation: "+photoInterp);
		ps.println(pad+"Rows: "+nRows);
		ps.println(pad+"Columns: "+nCols);
		ps.println(pad+"BitsAllocated: "+bitsAlloc);
		ps.println(pad+"BitsStored: "+bitsStored);
		ps.println(pad+"HighBit: "+highBit);
		ps.println(pad+"PixelRepresentation: "+pixelRep);
		if (nSamples > 1)
		{
			ps.println("PlanarConfiguration: "+planarConf);
		}
		if (minPix >= 0)
		{
			ps.println(pad+"SmallestPixelValue: "+minPix);
		}
		if (maxPix >= 0)
		{
			ps.println(pad+"LargestPixelValue: "+maxPix);
		}
	}

	@Override
	public int getBitsAllocated()
	{
		return bitsAlloc;
	}

	@Override
	public int getBitsStored()
	{
		return bitsStored;
	}

	@Override
	public int getColumnCount()
	{
		return nCols;
	}

	@Override
	public int getHighBit()
	{
		return highBit;
	}

	@Override
	public int getLargestPixelValue()
	{
		return maxPix;
	}

	@Override
	public String getPhotometricInterpretation()
	{
		return photoInterp;
	}

	@Override
	public byte[] getPixelData()
	{
		return pixelData;
	}

	@Override
	public int getPixelRepresentation()
	{
		return pixelRep;
	}

	@Override
	public int getPlanarConfiguration()
	{
		return planarConf;
	}

	@Override
	public int getRowCount()
	{
		return nRows;
	}

	@Override
	public int getSamplesPerPixel()
	{
		return nSamples;
	}

	@Override
	public int getSmallestPixelValue()
	{
		return minPix;
	}

	@Override
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		if ((bits == 1) || (bits % 8 == 0))
		{
			bitsAlloc = bits;
			return;
		}
		throw new IllegalArgumentException(
			"Invalid number of bits allocated: "+bits);
	}

	@Override
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		if (bits < 1)
		{
			throw new IllegalArgumentException(
				"Invalid number of bits stored: "+bits);
		}
		bitsStored = bits;
	}

	@Override
	public void setColumnCount(int columns) throws IllegalArgumentException
	{
		if (columns < 1)
		{
			throw new IllegalArgumentException("Invalid column count: "+columns);
		}
		nCols = columns;
	}

	@Override
	public void setHighBit(int bit)
	{
		if (bit < 0)
		{
			throw new IllegalArgumentException("Invalid high bit: "+bit);
		}
		highBit = bit;
	}

	@Override
	public void setLargestPixelValue(int value)
	{
		maxPix = (value >= 0) ? value : -1;
	}

	@Override
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		if ((interpretation == null) || interpretation.isEmpty())
		{
			throw new IllegalArgumentException(
				"PhotometricInterpretation must not be null or empty");
		}
		switch (interpretation)
		{
			case Constants.Monochrome1:
			case Constants.Monochrome2:
			case Constants.PaletteColour:
			case Constants.Rgb:
			case Constants.YbrFull:
			case Constants.YbrFull422:
			case Constants.YbrPartial420:
			case Constants.YbrIct:
			case Constants.YbrRct:
				break;
			default:
				logger.warn("Unknown PhotometricInterpretation: {}", interpretation);
		}
		this.photoInterp = interpretation;
	}

	@Override
	public void setPixelData(byte[] pixels) throws IllegalArgumentException
	{
		if (pixels == null)
		{
			throw new IllegalArgumentException("PixelData must not be null");
		}
		this.pixelData = Arrays.copyOf(pixels, pixels.length);
	}

	@Override
	public void setPixelRepresentation(int representation) throws IllegalArgumentException
	{
		if (!((representation == 0) || (representation == 1)))
		{
			throw new IllegalArgumentException("PixelRepresentation must be 0 or 1");
		}
		this.pixelRep = representation;
	}

	@Override
	public void setPlanarConfiguration(int configuration) throws IllegalArgumentException
	{
		switch (configuration)
		{
			case 0:
			case 1:
				planarConf = configuration;
				break;
			default:
				logger.warn("Unknown planar configuration: "+configuration);
		}
	}

	@Override
	public void setRowCount(int rows) throws IllegalArgumentException
	{
		if (rows < 1)
		{
			throw new IllegalArgumentException("Invalid row count: "+rows);
		}
		nRows = rows;
	}

	@Override
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		if ((samples == 1) || (samples == 3))
		{
			nSamples = samples;
			return;
		}
		throw new IllegalArgumentException(
			"Invalid number of samples per pixel: "+samples);
	}

	@Override
	public void setSmallestPixelValue(int value)
	{
		minPix = (value >= 0) ? value : -1;
	}

}
