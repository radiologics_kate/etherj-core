/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Constants;
import org.dcm4che2.data.UID;

/**
 * Internal class that contains all DICOM fields that are specified in multiple
 * modules. Uses strictest requirements of all modules that specify each field.
 * @author jamesd
 */
public class SegmentationMultiModuleCore
{
	private int bitsAlloc = 8;
	private int bitsStored = 8;
	private int highBit = 7;
	private String contentDate = "00000000";
	private String contentTime = "000000.00";
	private int instNum = 0;
	private String frameOfRefUid = null;
	private String sopInstUid = Uids.generateDicomUid();

	/**
	 * Returns bits allocated: ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getBitsAllocated()
	{
		return bitsAlloc;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getBitsStored()
	{
		return bitsStored;
	}

	/**
	 * Returns , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @return 
	 */
	public String getContentDate()
	{
		return contentDate;
	}

	/**
	 * Returns , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @return 
	 */
	public String getContentTime()
	{
		return contentTime;
	}

	/**
	 * 
	 * @return 
	 */
	public String getFrameOfReferenceUid()
	{
		return frameOfRefUid;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getHighBit()
	{
		return highBit;
	}

	/**
	 * Returns instance number, type 1: SopCommonModule, GeneralImageModule,
	 * SegmentationImageModule, MultiFrameFunctionalGroupsModule.
	 * @return 
	 */
	public int getInstanceNumber()
	{
		return instNum;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public String getPhotometricInterpretation()
	{
		return Constants.Monochrome2;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getPixelRepresentation()
	{
		return 0;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getSamplesPerPixel()
	{
		return 1;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopClassUid()
	{
		return UID.SegmentationStorage;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopInstanceUid()
	{
		return sopInstUid;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param bits 
	 */
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		if (!((bits == 1) || (bits == 8)))
		{
			throw new IllegalArgumentException("Bits allocated must be 1 or 8");
		}
		bitsAlloc = bits;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param bits 
	 */
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		if (!((bits == 1) || (bits == 8)))
		{
			throw new IllegalArgumentException("Bits stored must be 1 or 8");
		}
		bitsStored = bits;
	}

	/**
	 * Sets , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @param date
	 * @throws IllegalArgumentException 
	 */
	public void setContentDate(String date) throws IllegalArgumentException
	{
		if (DicomUtils.parseDate(date) == null)
		{
			throw new IllegalArgumentException("Invalid date: "+date);
		}
		contentDate = date;
	}

	/**
	 * Sets , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @param time
	 * @throws IllegalArgumentException 
	 */
	public void setContentTime(String time) throws IllegalArgumentException
	{
		if (DicomUtils.parseTime(time) == null)
		{
			throw new IllegalArgumentException("Invalid time: "+time);
		}
		contentTime = time;
	}

	/**
	 *
	 * @param uid
	 * @throws IllegalArgumentException
	 */
	public void setFrameOfReferenceUid(String uid) throws IllegalArgumentException
	{
		if ((uid != null) && uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"Frame of reference UID must not be empty");
		}
		frameOfRefUid = uid;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param bit 
	 */
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		if (!((bit == 0) || (bit == 7)))
		{
			throw new IllegalArgumentException("High bit must be 0 or 7");
		}
		highBit = bit;
	}

	/**
	 * Sets instance number, type 1: SopCommonModule, GeneralImageModule,
	 * SegmentationImageModule, MultiFrameFunctionalGroupsModule.
	 * @param number 
	 */
	public void setInstanceNumber(int number)
	{
		if (number < 1)
		{
			throw new IllegalArgumentException(
				"InstanceNumber must be greater than zero");
		}
		instNum = number;
	}
	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param interpretation
	 */
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		if (!Constants.Monochrome2.equals(interpretation))
		{
			throw new IllegalArgumentException(
				"Photometric interpretation must be "+Constants.Monochrome2);
		}
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param representation
	 */
	public void setPixelRepresentation(int representation)
		throws IllegalArgumentException
	{
		if (representation != 0)
		{
			throw new IllegalArgumentException("Pixel representation must be 0");
		}
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param samples
	 */
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		if (samples != 1)
		{
			throw new IllegalArgumentException("Samples per pixel must be 1");
		}
	}

	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		sopInstUid = uid;
	}
	
}
