/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.DimensionIndex;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface MultiframeDimensionModule extends Displayable
{

	/**
	 * Adds a dimension index, type 1.
	 * @param index
	 * @return
	 */
	boolean addDimensionIndex(DimensionIndex index);

	/**
	 * Adds a dimension organisation UID, type 1.
	 * @param uid
	 * @return
	 * @throws IllegalArgumentException
	 */
	boolean addDimensionOrganisationUid(String uid)
		throws IllegalArgumentException;

	/**
	 * Returns the dimension index list, requires at least one item to be valid.
	 * @return
	 */
	List<DimensionIndex> getDimensionIndexList();

	/**
	 * Returns the dimension organisation type, type 3. Enumerated values: 3D,
	 * 3D_TEMPORAL.
	 * @return
	 */
	String getDimensionOrganisationType();

	/**
	 * Returns the dimension organisation list, requires at least one item to be
	 * valid.
	 * @return
	 */
	List<String> getDimensionOrganisationUidList();

	/**
	 * Removes a dimension index.
	 * @param index
	 * @return
	 */
	boolean removeDimensionIndex(DimensionIndex index);

	/**
	 * Removes a dimension organisation UID.
	 * @param uid
	 * @return
	 */
	boolean removeDimensionOrganisationUid(String uid);

	/**
	 * Sets the dimension organisation type, type 3. Enumerated values: 3D,
	 * 3D_TEMPORAL.
	 * @param type
	 */
	void setDimensionOrganisationType(String type)
		throws IllegalArgumentException;

}
