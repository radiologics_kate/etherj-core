/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultContour;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultEnhancedSr;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedStudy;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultRtStruct;
import icr.etherj.dicom.iod.impl.DefaultSegmentation;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.Modules;
import java.util.List;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;

/**
 * Utility and factory class for DICOM IOD concept classes.
 * @author jamesd
 */
public class Iods
{

	/**
	 * Returns a new Code.
	 * @return
	 */
	public static Code code()
	{
		return new DefaultCode();
	}

	/**
	 * Returns a new Contour.
	 * @return
	 */
	public static Contour contour()
	{
		return new DefaultContour();
	}

	/**
	 * Returns a new ContourImage.
	 * @return
	 */
	public static ContourImage contourImage()
	{
		return new DefaultContourImage();
	}

	/**
	 * Returns a new EnhancedSr.
	 * @return
	 */
	public static EnhancedSr enhancedSr()
	{
		return new DefaultEnhancedSr();
	}

	/**
	 * Returns a new EnhancedSr.
	 * @return
	 */
	public static EnhancedSr enhancedSr(DicomObject dcm)
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.EnhancedSRStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of EnhancedSRStorage, found: "+sopClassUid);
		}
		EnhancedSr sr = new DefaultEnhancedSr();
		Modules.unpack(dcm, sr.getPatientModule());
		Modules.unpack(dcm, sr.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, sr.getGeneralStudyModule());
		Modules.unpack(dcm, sr.getPatientStudyModule());
		Modules.unpack(dcm, sr.getClinicalTrialStudyModule());
		Modules.unpack(dcm, sr.getSrDocumentSeriesModule());
		Modules.unpack(dcm, sr.getClinicalTrialSeriesModule());
		Modules.unpack(dcm, sr.getSynchronisationModule());
		Modules.unpack(dcm, sr.getGeneralEquipmentModule());
		Modules.unpack(dcm, sr.getSrDocumentGeneralModule());
		Modules.unpack(dcm, sr.getSrDocumentContentModule());
		Modules.unpack(dcm, sr.getSopCommonModule());

		return sr;
	}

	/**
	 * Returns a unique key for the instance.
	 * @param instance
	 * @return
	 */
	public static String makeKey(ReferencedInstance instance)
	{
		return (instance != null)
			? instance.getReferencedSopInstanceUid()+"."+instance.getReferencedFrameNumber()
			: null;
	}

	/**
	 * Packs the RtStruct into the DicomObject.
	 * @param enhSr the source RtStruct
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(EnhancedSr enhSr, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (enhSr == null)
		{
			throw new IllegalArgumentException("EnhancedSR must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.EnhancedSRStorage,
			enhSr.getSopInstanceUid(), UID.ExplicitVRLittleEndian);
		Modules.pack(enhSr.getPatientModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(enhSr.getGeneralStudyModule(), dcm);
		Modules.pack(enhSr.getPatientStudyModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialStudyModule(), dcm);
		Modules.pack(enhSr.getSrDocumentSeriesModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialSeriesModule(), dcm);
		Modules.pack(enhSr.getGeneralEquipmentModule(), dcm);
		Modules.pack(enhSr.getSynchronisationModule(), dcm);
		Modules.pack(enhSr.getSrDocumentGeneralModule(), dcm);
		Modules.pack(enhSr.getSrDocumentContentModule(), dcm);
		Modules.pack(enhSr.getSopCommonModule(), dcm);
	}

	/**
	 * Packs the RtStruct into the DicomObject.
	 * @param rtStruct the source RtStruct
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(RtStruct rtStruct, DicomObject dcm)
	{
		if (rtStruct == null)
		{
			throw new IllegalArgumentException("RTSTRUCT must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.RTStructureSetStorage,
			rtStruct.getSopInstanceUid(), UID.ExplicitVRLittleEndian);
		Modules.pack(rtStruct.getPatientModule(), dcm);
		Modules.pack(rtStruct.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(rtStruct.getGeneralStudyModule(), dcm);
		Modules.pack(rtStruct.getPatientStudyModule(), dcm);
		Modules.pack(rtStruct.getClinicalTrialStudyModule(), dcm);
		Modules.pack(rtStruct.getRtSeriesModule(), dcm);
		Modules.pack(rtStruct.getGeneralEquipmentModule(), dcm);
		Modules.pack(rtStruct.getFrameOfReferenceModule(), dcm);
		Modules.pack(rtStruct.getStructureSetModule(), dcm);
		Modules.pack(rtStruct.getRoiContourModule(), dcm);
		Modules.pack(rtStruct.getRtRoiObservationsModule(), dcm);
		Modules.pack(rtStruct.getGeneralReferenceModule(), dcm);
		Modules.pack(rtStruct.getSopCommonModule(), dcm);
		Modules.pack(rtStruct.getCommonInstanceReferenceModule(), dcm);
	}

	/**
	 * Packs the RtStruct into the DicomObject.
	 * @param segmentation
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(Segmentation segmentation, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (segmentation == null)
		{
			throw new IllegalArgumentException("Segmentation must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.SegmentationStorage,
			segmentation.getSopInstanceUid(), UID.ExplicitVRLittleEndian);
		Modules.pack(segmentation.getSopCommonModule(), dcm);
		Modules.pack(segmentation.getPatientModule(), dcm);
		Modules.pack(segmentation.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(segmentation.getGeneralStudyModule(), dcm);
		Modules.pack(segmentation.getPatientStudyModule(), dcm);
		Modules.pack(segmentation.getClinicalTrialStudyModule(), dcm);
		Modules.pack(segmentation.getSegmentationSeriesModule(), dcm);
		FrameOfReferenceModule forModule = segmentation.getFrameOfReferenceModule();
		if (forModule.getFrameOfReferenceUid() != null)
		{
			Modules.pack(forModule, dcm);
		}
		Modules.pack(segmentation.getFrameOfReferenceModule(), dcm);
		Modules.pack(segmentation.getEnhancedEquipmentModule(), dcm);
		Modules.pack(segmentation.getGeneralReferenceModule(), dcm);
		Modules.pack(segmentation.getImagePixelModule(), dcm);
		Modules.pack(segmentation.getSegmentationImageModule(), dcm);
		Modules.pack(segmentation.getMultiframeFunctionalGroupsModule(), dcm);
		Modules.pack(segmentation.getMultiframeDimensionModule(), dcm);
		List<FunctionalGroupsFrame> frames = 
			segmentation.getMultiframeFunctionalGroupsModule()
				.getPerFrameFunctionalGroups().getFrameList();
		if (!frames.isEmpty())
		{
			SegmentationFunctionalGroupsFrame frame =
				(SegmentationFunctionalGroupsFrame) frames.get(0);
			if (!frame.getDerivationImages().isEmpty())
			{
				Modules.pack(segmentation.getCommonInstanceReferenceModule(), dcm);
			}
		}
	}

	/**
	 * Returns a new ReferencedFrameOfReference.
	 * @return
	 */
	public static ReferencedFrameOfReference referencedFrameOfReference()
	{
		return new DefaultReferencedFrameOfReference();
	}

	/**
	 * Returns a new RoiContour.
	 * @return
	 */
	public static RoiContour roiContour()
	{
		return new DefaultRoiContour();
	}

	/**
	 * Returns a new RtReferencedSeries.
	 * @return
	 */
	public static RtReferencedSeries rtReferencedSeries()
	{
		return new DefaultRtReferencedSeries();
	}

	/**
	 * Returns a new RtReferencedStudy.
	 * @return
	 */
	public static RtReferencedStudy rtReferencedStudy()
	{
		return new DefaultRtReferencedStudy();
	}

	/**
	 * Returns a new RtRoiObservation.
	 * @return
	 */
	public static RtRoiObservation rtRoiObservation()
	{
		return new DefaultRtRoiObservation();
	}

	/**
	 * Returns a new RtStruct.
	 * @return the RtStruct
	 */
	public static RtStruct rtStruct()
	{
		return new DefaultRtStruct();
	}

	/**
	 * Returns a new RtStruct created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the RtStruct
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid RtStruct
	 */
	public static RtStruct rtStruct(DicomObject dcm)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.RTStructureSetStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of RTStructureSetStorage, found: "+sopClassUid);
		}
		RtStruct rtStruct = new DefaultRtStruct();
		Modules.unpack(dcm, rtStruct.getPatientModule());
		Modules.unpack(dcm, rtStruct.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, rtStruct.getGeneralStudyModule());
		Modules.unpack(dcm, rtStruct.getPatientStudyModule());
		Modules.unpack(dcm, rtStruct.getClinicalTrialStudyModule());
		Modules.unpack(dcm, rtStruct.getRtSeriesModule());
		Modules.unpack(dcm, rtStruct.getGeneralEquipmentModule());
		Modules.unpack(dcm, rtStruct.getFrameOfReferenceModule());
		Modules.unpack(dcm, rtStruct.getStructureSetModule());
		Modules.unpack(dcm, rtStruct.getRoiContourModule());
		Modules.unpack(dcm, rtStruct.getRtRoiObservationsModule());
		Modules.unpack(dcm, rtStruct.getGeneralReferenceModule());
		Modules.unpack(dcm, rtStruct.getSopCommonModule());
		Modules.unpack(dcm, rtStruct.getCommonInstanceReferenceModule());

		return rtStruct;
	}

	/**
	 * Returns a new Segmentation.
	 * @return the Segmentation
	 */
	public static Segmentation segmentation()
	{
		return new DefaultSegmentation();
	}

	/**
	 * Returns a new Segmentation created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the Segmentation
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid Segmentation
	 */
	public static Segmentation segmentation(DicomObject dcm)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.SegmentationStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of SegmentationStorage, found: "+sopClassUid);
		}
		Segmentation segmentation = new DefaultSegmentation();
		Modules.unpack(dcm, segmentation.getPatientModule());
		Modules.unpack(dcm, segmentation.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, segmentation.getGeneralStudyModule());
		Modules.unpack(dcm, segmentation.getPatientStudyModule());
		Modules.unpack(dcm, segmentation.getClinicalTrialStudyModule());
		Modules.unpack(dcm, segmentation.getSegmentationSeriesModule());
		String forUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		if (forUid != null)
		{
			Modules.unpack(dcm, segmentation.getFrameOfReferenceModule());
		}
		Modules.unpack(dcm, segmentation.getEnhancedEquipmentModule());
		Modules.unpack(dcm, segmentation.getGeneralReferenceModule());
		Modules.unpack(dcm, segmentation.getImagePixelModule());
		Modules.unpack(dcm, segmentation.getSegmentationImageModule());
		Modules.unpack(dcm, segmentation.getMultiframeFunctionalGroupsModule());
		Modules.unpack(dcm, segmentation.getMultiframeDimensionModule());
		List<FunctionalGroupsFrame> frames = 
			segmentation.getMultiframeFunctionalGroupsModule()
				.getPerFrameFunctionalGroups().getFrameList();
		if (!frames.isEmpty())
		{
			SegmentationFunctionalGroupsFrame frame =
				(SegmentationFunctionalGroupsFrame) frames.get(0);
			if (!frame.getDerivationImages().isEmpty())
			{
				Modules.unpack(dcm, segmentation.getCommonInstanceReferenceModule());
			}
		}
		Modules.unpack(dcm, segmentation.getSopCommonModule());

		return segmentation;
	}

	/**
	 * Returns a new StructureSetRoi.
	 * @return
	 */
	public static StructureSetRoi structureSetRoi()
	{
		return new DefaultStructureSetRoi();
	}

	//	Prevent instantiation.
	private Iods()
	{}
}
