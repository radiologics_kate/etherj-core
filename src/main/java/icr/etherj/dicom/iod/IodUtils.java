/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.StringUtils;
import icr.etherj.dicom.DicomUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class IodUtils
{
	private static final Logger logger = LoggerFactory.getLogger(IodUtils.class);

	private static String defDate = "19000101";
	private static String defTime = "000000";

	/**
	 *
	 * @param rtStruct
	 * @return
	 */
	public static String getDate(RtStruct rtStruct)
	{
		return IodUtils.getDate(rtStruct, "yyyyMMdd");
	}

	/**
	 *
	 * @param rtStruct
	 * @param format
	 * @return
	 */
	public static String getDate(RtStruct rtStruct, String format)
	{
		// Find the date from the most specific but optional to the least specific
		// but mandatory
		String date = rtStruct.getStructureSetModule().getStructureSetDate();
		if (date.isEmpty())
		{
			date = rtStruct.getRtSeriesModule().getSeriesDate();
			if ((date == null) || date.isEmpty())
			{
				date = rtStruct.getStudyDate();
			}
		}
		Date dt = DicomUtils.parseDate(date);
		return formatDate(dt, format);
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getDate(Segmentation seg)
	{
		return getDate(seg, "yyyyMMdd");
	}

	/**
	 *
	 * @param seg
	 * @param format
	 * @return
	 */
	public static String getDate(Segmentation seg, String format)
	{
		String date = seg.getMultiframeFunctionalGroupsModule().getContentDate();
		if (StringUtils.isNullOrEmpty(date))
		{
			return defDate;
		}
		Date dt = DicomUtils.parseDate(date);
		return formatDate(dt, format);
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getName(Segmentation seg)
	{
		String desc = seg.getGeneralSeriesModule().getSeriesDescription();
		if (!StringUtils.isNullOrEmpty(desc))
		{
			return desc;	
		}
		return "Segmentation "+seg.getSegmentationSeriesModule().getSeriesNumber();
	}

	/**
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationDate(Segmentation seg)
	{
		return getDate(seg);
	}

	/**
	 * @param seg
	 * @param format
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationDate(Segmentation seg, String format)
	{
		return getDate(seg, format);
	}

	/**
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationName(Segmentation seg)
	{
		return getName(seg);
	}

	/**
	 *
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationTime(Segmentation seg)
	{
		return IodUtils.getTime(seg);
	}

	/**
	 *
	 * @param seg
	 * @param format
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationTime(Segmentation seg, String format)
	{
		return IodUtils.getTime(seg, format);
	}

	/**
	 *
	 * @param rtStruct
	 * @return
	 */
	public static String getTime(RtStruct rtStruct)
	{
		return IodUtils.getTime(rtStruct, "HHmmss");
	}

	/**
	 *
	 * @param rtStruct
	 * @param format
	 * @return
	 */
	public static String getTime(RtStruct rtStruct, String format)
	{
		// Find the date from the most specific but optional to the least specific
		// but mandatory
		String time = rtStruct.getStructureSetModule().getStructureSetTime();
		if (time.isEmpty())
		{
			time = rtStruct.getRtSeriesModule().getSeriesTime();
			if ((time == null) || time.isEmpty())
			{
				time = rtStruct.getStudyTime();
			}
		}
		Date dt = DicomUtils.parseTime(time);
		return formatTime(dt, format);
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getTime(Segmentation seg)
	{
		return IodUtils.getTime(seg, "HHmmss");
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getTime(Segmentation seg, String format)
	{
		String time = seg.getMultiframeFunctionalGroupsModule().getContentTime();
		if (StringUtils.isNullOrEmpty(time))
		{
			return defTime;
		}
		Date dt = DicomUtils.parseTime(time);
		return formatTime(dt, format);
	}

	/**
	 *
	 * @param date
	 * @throws IllegalArgumentException
	 */
	public static void setDefaultDate(String date)
		throws IllegalArgumentException
	{
		if (DicomUtils.parseDate(date) == null)
		{
			throw new IllegalArgumentException("Invalid DICOM date: "+date);
		}
		defDate = date;
	}

	/**
	 *
	 * @param time
	 * @throws IllegalArgumentException
	 */
	public static void setDefaultTime(String time)
		throws IllegalArgumentException
	{
		if (DicomUtils.parseTime(time) == null)
		{
			throw new IllegalArgumentException("Invalid DICOM time: "+time);
		}
		defTime = time;
	}

	private static String formatDate(Date dt, String format)
	{
		String date = defDate;
		try
		{
			if (dt != null)
			{
				DateFormat df = new SimpleDateFormat(format);
				date = df.format(dt);
			}
		}
		catch (NullPointerException | IllegalArgumentException ex)
		{
			logger.warn("Date format error", ex);
		}
		return date;
	}

	private static String formatTime(Date dt, String format)
	{
		String time = defTime;
		try
		{
			if (dt != null)
			{
				DateFormat df = new SimpleDateFormat(format);
				time = df.format(dt);
			}
		}
		catch (NullPointerException | IllegalArgumentException ex)
		{
			logger.warn("Time format error", ex);
		}
		return time;
	}

	private IodUtils()
	{}
}
