/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.Coordinate2D;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface ContentItem extends Displayable
{
	boolean addContentRelationship(ContentRelationship relationship);

	/**
	 *	Returns the concept name code, type 1C.
	 * @return
	 */
	Code getConceptNameCode();

	List<ContentRelationship> getContentRelationshipList();

	/**
	 * Returns the value type, type 1. Enumerated values: CONTAINER, TEXT, NUM,
	 * CODE, DATE, TIME, DATETIME, UIDREF, PNAME, COMPOSITE, IMAGE, WAVEFORM,
	 * SCOORD, SCOORD3D, TCOORD.
	 * @return
	 */
	String getValueType();

	/**
	 *
	 * @param conceptName
	 */
	void setConceptNameCode(Code conceptName);

	/**
	 * Sets the value type, type 1. Enumerated values: CONTAINER, TEXT, NUM,
	 * CODE, DATE, TIME, DATETIME, UIDREF, PNAME, COMPOSITE, IMAGE, WAVEFORM,
	 * SCOORD, SCOORD3D, TCOORD.
	 * @param type
	 * @throws IllegalArgumentException
	 */
	void setValueType(String type) throws IllegalArgumentException;

	/**
	 * Code content item type.
	 */
	interface CodeItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		Code getCode();

		/**
		 *
		 * @param code
		 * @throws IllegalArgumentException
		 */
		void setCode(Code code) throws IllegalArgumentException;
	}

	/**
	 * Container content item type.
	 */
	interface ContainerItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		ContentTemplate getContentTemplate();

		/**
		 *
		 * @return
		 */
		String getContinuityOfContent();

		/**
		 *
		 * @param template
		 */
		void setContentTemplate(ContentTemplate template);

		/**
		 *
		 * @param continuity
		 * @throws IllegalArgumentException
		 */
		void setContinuityOfContent(String continuity) throws IllegalArgumentException;
	}

	/**
	 * Image content item type.
	 */
	interface ImageItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		ReferencedInstance getReferencedInstance();

		/**
		 *
		 * @param instance
		 * @throws IllegalArgumentException
		 */
		void setReferencedInstance(ReferencedInstance instance)
			throws IllegalArgumentException;
	}

	/**
	 * Number content item type.
	 */
	interface NumericItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		double getFloatingPointValue();

		/**
		 *
		 * @return
		 */
		Code getMeasurementUnits();

		/**
		 *
		 * @return
		 */
		String getNumericValue();

		/**
		 *
		 * @return
		 */
		int getRationalDenominatorValue();

		/**
		 *
		 * @return
		 */
		int getRationalNumeratorValue();

		/**
		 *
		 * @param value
		 */
		void setFloatingPointValue(double value);

		/**
		 *
		 * @param units
		 * @throws IllegalArgumentException
		 */
		void setMeasurementUnits(Code units) throws IllegalArgumentException;

		/**
		 *
		 * @param value
		 * @throws IllegalArgumentException
		 */
		void setNumericValue(String value) throws IllegalArgumentException;

		/**
		 *
		 * @param value
		 */
		void setRationalNumeratorValue(int value);

		/**
		 *
		 * @param value
		 */
		void setRationalDenominatorValue(int value);
	}

	/**
	 * Person name content item type.
	 */
	interface PersonNameItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		String getPersonName();

		/**
		 *
		 * @param name
		 * @throws IllegalArgumentException
		 */
		void setPersonName(String name) throws IllegalArgumentException;
	}

	/**
	 * Spatial coordinate content item type.
	 */
	interface SpatialCoordItem extends ContentItem
	{
		/**
		 *
		 * @param coord
		 * @return
		 */
		boolean addCoordinate(Coordinate2D coord);

		/**
		 *
		 * @param column
		 * @param row
		 * @return
		 */
		boolean addCoordinate(double column, double row);

		/**
		 *
		 * @return
		 */
		List<Coordinate2D> getGraphicData();

		/**
		 *
		 * @return
		 */
		String getGraphicType();

		/**
		 * Sets the graphic type, type 1. Enumerated values: POINT, MULTIPOINT,
		 * POLYLINE, CIRCLE, ELLIPSE.
		 * @param type
		 * @throws IllegalArgumentException
		 */
		void setGraphicType(String type) throws IllegalArgumentException;
	}

	/**
	 * Text content item type.
	 */
	interface TextItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		String getText();

		/**
		 *
		 * @param text
		 * @throws IllegalArgumentException
		 */
		void setText(String text) throws IllegalArgumentException;
	}

	/**
	 * Image content item type.
	 */
	interface UidRefItem extends ContentItem
	{

		/**
		 *
		 * @return
		 */
		String getUidReference();

		/**
		 *
		 * @param uid
		 * @throws IllegalArgumentException
		 */
		void setUidReference(String uid) throws IllegalArgumentException;
	}

}
