/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface Segment extends Displayable
{

	/**
	 *
	 * @param code
	 * @return
	 */
	boolean addAnatomicRegionCode(Code code);

	/**
	 *
	 * @return
	 */
	List<Code> getAnatomicRegionCodeList();

	/**
	 *
	 * @return
	 */
	int[] getRecommendedDisplayCIELabValue();

	/**
	 *
	 * @return
	 */
	String getSegmentAlgorithmName();

	/**
	 * Returns the segment algorithm type, type 1. Enumerated values:
	 * AUTOMATIC, SEMIAUTOMATIC, MANUAL.
	 * @return
	 */
	String getSegmentAlgorithmType();

	/**
	 * Returns the segment description, type 3.
	 * @return
	 */
	String getSegmentDescription();

	/**
	 * Returns the segment label, type 1.
	 * @return
	 */
	String getSegmentLabel();

	/**
	 * Returns the segment number, type 1.
	 * @return
	 */
	int getSegmentNumber();

	/**
	 * Returns the segmented property category code, type 1.
	 * @return
	 */
	Code getSegmentedPropertyCategoryCode();

	/**
	 * Returns the segmented property type code, type 1.
	 * @return
	 */
	Code getSegmentedPropertyTypeCode();

	/**
	 *
	 * @return
	 */
	String getTrackingId();

	/**
	 *
	 * @return
	 */
	String getTrackingUid();

	/**
	 *
	 * @param code
	 * @return
	 */
	boolean removeAnatomicRegionCode(Code code);
	/**
	 *
	 * @param value
	 */
	void setRecommendedDisplayCIELabValue(int[] value)
		throws IllegalArgumentException;

	/**
	 *
	 * @param name
	 */
	void setSegmentAlgorithmName(String name) throws IllegalArgumentException;

	/**
	 *
	 * @param type
	 */
	void setSegmentAlgorithmType(String type) throws IllegalArgumentException;

	/**
	 *
	 * @param description
	 */
	void setSegmentDescription(String description);

	/**
	 *
	 * @param label
	 */
	void setSegmentLabel(String label) throws IllegalArgumentException;

	/**
	 *
	 * @param number
	 */
	void setSegmentNumber(int number) throws IllegalArgumentException;

	/**
	 *
	 * @param code
	 */
	void setSegmentedPropertyCategoryCode(Code code)
		throws IllegalArgumentException;

	/**
	 *
	 * @param code
	 */
	void setSegmentedPropertyTypeCode(Code code)
		throws IllegalArgumentException;

	/**
	 *
	 * @param id
	 */
	void setTrackingId(String id);

	/**
	 *
	 * @param uid
	 */
	void setTrackingUid(String uid);
}
