/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.ContentRelationship;
import icr.etherj.dicom.iod.ContentTemplate;
import java.io.PrintStream;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultContainerItem extends AbstractContentItem
	implements ContentItem.ContainerItem
{
	private String continuity = Constants.Separate;
	private ContentTemplate template;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ValueType: "+getValueType());
		ps.println(pad+"ConceptNameCode:");
		Code cnc = getConceptNameCode();
		if (cnc != null)
		{
			cnc.display(ps, indent+"  ", recurse);
		}
		ps.println(pad+"ContinuityOfContent: "+continuity);
		if (template != null)
		{
			ps.println(pad+"ContentTemplate:");
			template.display(ps, indent+"  ", recurse);
		}
		List<ContentRelationship> relationships = getContentRelationshipList();
		if (relationships.size() > 0)
		{
			ps.println(pad+"Relationships:");
			for (ContentRelationship r : relationships)
			{
				r.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public ContentTemplate getContentTemplate()
	{
		return template;
	}

	@Override
	public String getContinuityOfContent()
	{
		return continuity;
	}

	@Override
	public void setContentTemplate(ContentTemplate template)
	{
		this.template = template;
	}

	@Override
	public void setContinuityOfContent(String continuity) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(continuity))
		{
			throw new IllegalArgumentException(
				"Continuity of content must not be null or empty");
		}
		switch (continuity)
		{
			case Constants.Separate:
			case Constants.Continuous:
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid continuity of content: "+continuity);
		}
		this.continuity = continuity;
	}

	@Override
	public void setValueType(String type) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Value type must not be null or empty");
		}
		if (!Constants.Container.equals(type))
		{
			throw new IllegalArgumentException(
				"Value type must be "+Constants.Container);
		}
		super.setValueType(type);
	}
	
	
}
