/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
// * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationSharedFunctionalGroups
	extends AbstractDisplayable
	implements SegmentationSharedFunctionalGroups
{
	private PixelMeasures pixelMeasures = null;
	private double[] ori = new double[] {};

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (ori.length == 6)
		{
			ps.println(pad+"ImageOrientationPatient: "+
				ori[0]+"\\"+ori[1]+"\\"+ori[2]+"\\"+ori[3]+"\\"+ori[4]+"\\"+ori[5]);
		}
		if (pixelMeasures != null)
		{
			ps.println(pad+"PixelMeasures:");
			pixelMeasures.display(ps, indent+"  ");
		}
	}

	@Override
	public double[] getImageOrientationPatient()
	{
		return Arrays.copyOf(ori, ori.length);
	}

	@Override
	public PixelMeasures getPixelMeasures()
	{
		return pixelMeasures;
	}

	@Override
	public void setImageOrientationPatient(double[] orientation)
	{
		if ((orientation == null) || (orientation.length != 6))
		{
			ori = new double[] {};
			return;
		}
		ori = Arrays.copyOf(orientation, orientation.length);
	}

	@Override
	public void setPixelMeasures(PixelMeasures pixelMeasures)
	{
		this.pixelMeasures = pixelMeasures;
	}

}
