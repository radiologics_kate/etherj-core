/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.FrameContent;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationFunctionalGroupsFrame extends AbstractDisplayable
	implements SegmentationFunctionalGroupsFrame
{
	private final List<DerivationImage> derivationImages = new ArrayList<>();
	private FrameContent frameContent = new DefaultFrameContent();
	private double[] pos = new double[] {};
	private int refSegNumber = 0;

	@Override
	public boolean addDerivationImage(DerivationImage image)
	{
		return (image != null) ? derivationImages.add(image) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nItems = derivationImages.size();
		ps.println(pad+"DerivationImageList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		for (DerivationImage image : derivationImages)
		{
			image.display(ps, indent+"  ");
		}
		frameContent.display(ps, indent+"  ");
		if (pos.length == 3)
		{
			ps.println(pad+"ImagePositionPatient: "+
				pos[0]+"\\"+pos[1]+"\\"+pos[2]);
		}
		ps.println(pad+"ReferencedSegmentNumber: "+refSegNumber);
	}

	@Override
	public List<DerivationImage> getDerivationImages()
	{
		return ImmutableList.copyOf(derivationImages);
	}

	@Override
	public FrameContent getFrameContent()
	{
		return frameContent;
	}

	@Override
	public double[] getImagePositionPatient()
	{
		return Arrays.copyOf(pos, pos.length);
	}

	@Override
	public int getReferencedSegmentNumber()
	{
		return refSegNumber;
	}

	@Override
	public boolean removeDerivationImage(DerivationImage image)
	{
		return derivationImages.remove(image);
	}

	@Override
	public void setFrameContent(FrameContent frameContent)
	{
		if (frameContent == null)
		{
			throw new IllegalArgumentException("FrameContent must not be null");
		}
		this.frameContent = frameContent;
	}

	@Override
	public void setImagePositionPatient(double[] position)
	{
		if ((position == null) || (position.length != 3))
		{
			pos = new double[] {};
			return;
		}
		pos = Arrays.copyOf(position, position.length);
	}
	
	@Override
	public void setReferencedSegmentNumber(int number)
		throws IllegalArgumentException
	{
		if (number < 1)
		{
			throw new IllegalArgumentException(
				"ReferencedSegmentNumber must be > 0");
		}
		refSegNumber = number;
	}

}
