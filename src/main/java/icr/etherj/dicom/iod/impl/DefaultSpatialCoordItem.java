/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.StringUtils;
import icr.etherj.dicom.Coordinate2D;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContentItem.SpatialCoordItem;
import icr.etherj.dicom.iod.ContentRelationship;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultSpatialCoordItem extends AbstractContentItem
	implements SpatialCoordItem
{
	private final List<Coordinate2D> graphicData = new ArrayList<>();
	private String graphicType = null;

	@Override
	public boolean addCoordinate(Coordinate2D coord)
	{
		return graphicData.add(coord);
	}

	@Override
	public boolean addCoordinate(double column, double row)
	{
		return graphicData.add(new Coordinate2D(column, row));
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ValueType: "+getValueType());
		Code cnc = getConceptNameCode();
		if (cnc != null)
		{
			ps.println(pad+"ConceptNameCode:");
			cnc.display(ps, indent+"  ", recurse);
		}

		ps.println(pad+"GraphicType: "+graphicType);
		ps.println(pad+"GraphicData:");
		for (Coordinate2D coord : graphicData)
		{
			coord.display(ps, indent+"  ", recurse);
		}

		List<ContentRelationship> relationships = getContentRelationshipList();
		if (relationships.size() > 0)
		{
			ps.println(pad+"Relationships:");
			for (ContentRelationship r : relationships)
			{
				r.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<Coordinate2D> getGraphicData()
	{
		return ImmutableList.copyOf(graphicData);
	}

	@Override
	public String getGraphicType()
	{
		return graphicType;
	}

	@Override
	public void setGraphicType(String type) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Graphic type must not be null or empty");
		}
		switch (type)
		{
			case Constants.Point:
			case Constants.MultiPoint:
			case Constants.Polyline:
			case Constants.Circle:
			case Constants.Ellipse:
				break;
			default:
				throw new IllegalArgumentException("Invalid graphic type: "+type);
		}
		graphicType = type;
	}

	@Override
	public void setValueType(String type) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Value type must not be null or empty");
		}
		if (!Constants.SpatialCoord.equals(type))
		{
			throw new IllegalArgumentException(
				"Value type must be "+Constants.SpatialCoord);
		}
		super.setValueType(type);
	}

}