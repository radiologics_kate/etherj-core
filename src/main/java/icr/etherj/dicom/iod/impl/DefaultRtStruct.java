/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.dicom.iod.module.impl.RtStructSopCommonModule;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtRoiObservationsModule;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.impl.DefaultCommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultFrameOfReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultRoiContourModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtRoiObservationsModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultStructureSetModule;
import icr.etherj.dicom.iod.module.impl.RtStructMultiModuleCore;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public final class DefaultRtStruct extends AbstractDisplayable implements RtStruct
{
	private final ClinicalTrialStudyModule clinTrialStudy;
	private final ClinicalTrialSubjectModule clinTrialSubj;
	private final CommonInstanceReferenceModule commonInstRef;
	private final FrameOfReferenceModule frameOfRef;
	private final GeneralEquipmentModule genEquip;
	private final GeneralReferenceModule genRef;
	private final GeneralStudyModule genStudy;
	private final PatientModule patient;
	private final PatientStudyModule patientStudy;
	private final RoiContourModule roiContour ;
	private final RtRoiObservationsModule rtRoiObservations;
	private final RtSeriesModule rtSeries;
	private final RtStructMultiModuleCore rtMultiModCore;
	private final SopCommonModule sopCommon;
	private final StructureSetModule structureSet;

	public DefaultRtStruct()
	{
		// Interdependent modules
		rtMultiModCore = new RtStructMultiModuleCore();
		sopCommon = new RtStructSopCommonModule(rtMultiModCore);
		structureSet = new DefaultStructureSetModule(rtMultiModCore);

		// Standalone modules
		clinTrialSubj = new DefaultClinicalTrialSubjectModule();
		clinTrialStudy = new DefaultClinicalTrialStudyModule();
		commonInstRef = new DefaultCommonInstanceReferenceModule();
		frameOfRef = new DefaultFrameOfReferenceModule();
		genEquip = new DefaultGeneralEquipmentModule();
		genRef = new DefaultGeneralReferenceModule();
		genStudy = new DefaultGeneralStudyModule();
		patient = new DefaultPatientModule();
		patientStudy = new DefaultPatientStudyModule();
		roiContour = new DefaultRoiContourModule();
		rtRoiObservations = new DefaultRtRoiObservationsModule();
		rtSeries = new DefaultRtSeriesModule();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		patient.display(ps, indent+"  ", recurse);
		genStudy.display(ps, indent+"  ", recurse);
		rtSeries.display(ps, indent+"  ", recurse);
		genEquip.display(ps, indent+"  ", recurse);
		frameOfRef.display(ps, indent+"  ", recurse);
		structureSet.display(ps, indent+"  ", recurse);
		roiContour.display(ps, indent+"  ", recurse);
		rtRoiObservations.display(ps, indent+"  ", recurse);
		sopCommon.display(ps, indent+"  ", recurse);
	}

	@Override
	public ClinicalTrialStudyModule getClinicalTrialStudyModule()
	{
		return clinTrialStudy;
	}

	@Override
	public ClinicalTrialSubjectModule getClinicalTrialSubjectModule()
	{
		return clinTrialSubj;
	}

	@Override
	public CommonInstanceReferenceModule getCommonInstanceReferenceModule()
	{
		return commonInstRef;
	}

	@Override
	public FrameOfReferenceModule getFrameOfReferenceModule()
	{
		return frameOfRef;
	}

	@Override
	public GeneralEquipmentModule getGeneralEquipmentModule()
	{
		return genEquip;
	}

	@Override
	public GeneralReferenceModule getGeneralReferenceModule()
	{
		return genRef;
	}

	@Override
	public GeneralStudyModule getGeneralStudyModule()
	{
		return genStudy;
	}

	@Override
	public PatientModule getPatientModule()
	{
		return patient;
	}

	@Override
	public PatientStudyModule getPatientStudyModule()
	{
		return patientStudy;
	}

	@Override
	public RoiContourModule getRoiContourModule()
	{
		return roiContour;
	}

	@Override
	public RtRoiObservationsModule getRtRoiObservationsModule()
	{
		return rtRoiObservations;
	}

	@Override
	public RtSeriesModule getRtSeriesModule()
	{
		return rtSeries;
	}

	@Override
	public SopCommonModule getSopCommonModule()
	{
		return sopCommon;
	}

	@Override
	public String getSopInstanceUid()
	{
		return sopCommon.getSopInstanceUid();
	}

	@Override
	public StructureSetModule getStructureSetModule()
	{
		return structureSet;
	}

	@Override
	public String getStudyDate()
	{
		return genStudy.getStudyDate();
	}

	@Override
	public String getStudyTime()
	{
		return genStudy.getStudyTime();
	}

	@Override
	public boolean validate()
	{
		boolean patientOk = patient.validate();
		boolean genStudyOk = genStudy.validate();
		boolean rtSeriesOk = rtSeries.validate();
		boolean genEquipOk = genEquip.validate();
		boolean ssOk = structureSet.validate();
		boolean rcOk = roiContour.validate();
		boolean rtRoiObsOk = rtRoiObservations.validate();

		return patientOk && genStudyOk && rtSeriesOk && genEquipOk && ssOk &&
			rcOk && rtRoiObsOk;
	}

}
