/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.dicom.iod.HeirarchicalSeriesReference;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class DefaultHeirarchicalSopInstanceReference extends AbstractDisplayable
	implements HeirarchicalSopInstanceReference
{
	private String studyUid = Uids.generateDicomUid();
	private Map<String,HeirarchicalSeriesReference> refSeries =
		new LinkedHashMap<>();

	@Override
	public HeirarchicalSeriesReference addSeriesReference(HeirarchicalSeriesReference ref)
	{
		return (ref != null)
			? refSeries.put(ref.getSeriesInstanceUid(), ref)
			: null;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"StudyInstanceUid: "+studyUid);
		int nItems = refSeries.size();
		ps.println(pad+"HeirarchicalSeriesReferenceList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		if (recurse)
		{
			for (HeirarchicalSeriesReference series : refSeries.values())
			{
				series.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public HeirarchicalSeriesReference getSeriesReference(String uid)
	{
		return refSeries.get(uid);
	}

	@Override
	public List<HeirarchicalSeriesReference> getSeriesReferenceList()
	{
		return ImmutableList.copyOf(refSeries.values());
	}

	@Override
	public String getStudyInstanceUid()
	{
		return studyUid;
	}

	@Override
	public void merge(HeirarchicalSopInstanceReference other)
		throws IllegalArgumentException
	{
		if (!studyUid.equals(other.getStudyInstanceUid()))
		{
			throw new IllegalArgumentException("Study instance UID mismatch");
		}
		for (HeirarchicalSeriesReference series : other.getSeriesReferenceList())
		{
			String seriesUid = series.getSeriesInstanceUid();
			HeirarchicalSeriesReference existing = refSeries.get(seriesUid);
			if (existing == null)
			{
				refSeries.put(seriesUid, series);
				continue;
			}
			existing.merge(series);
		}
	}

	@Override
	public HeirarchicalSeriesReference removeSeriesReference(
		HeirarchicalSeriesReference ref)
	{
		return (ref != null)
			? refSeries.remove(ref.getSeriesInstanceUid())
			: null;
	}

	@Override
	public HeirarchicalSeriesReference removeSeriesReference(String uid)
	{
		return refSeries.remove(uid);
	}

	@Override
	public void setStudyInstanceUid(String uid) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(uid))
		{
			throw new IllegalArgumentException(
				"Study instance UID must not be null or empty");
		}
		studyUid = uid;
	}
	
}
