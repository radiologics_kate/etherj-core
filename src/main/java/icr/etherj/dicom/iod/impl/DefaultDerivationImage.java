/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.SourceImage;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultDerivationImage extends AbstractDisplayable
	implements DerivationImage
{
	private final List<Code> codes = new ArrayList<>();
	private final List<SourceImage> sourceImages = new ArrayList<>();

	@Override
	public boolean addDerivationCode(Code code)
	{
		return (code != null) ? codes.add(code) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nItems = sourceImages.size();
		ps.println(pad+"SourceImageList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		for (SourceImage image : sourceImages)
		{
			image.display(ps, indent+"  ");
		}
		nItems = codes.size();
		ps.println(pad+"DerivationCodeList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		for (Code code : codes)
		{
			code.display(ps, indent+"  ");
		}
	}

	@Override
	public boolean addSourceImage(SourceImage image)
	{
		return (image != null) ? sourceImages.add(image) : false;
	}

	@Override
	public List<Code> getDerivationCodeList()
	{
		return ImmutableList.copyOf(codes);
	}

	@Override
	public List<SourceImage> getSourceImageList()
	{
		return ImmutableList.copyOf(sourceImages);
	}

	@Override
	public boolean removeDerivationCode(Code code)
	{
		return codes.remove(code);
	}

	@Override
	public boolean removeSourceImage(SourceImage image)
	{
		return sourceImages.remove(image);
	}
	
}
