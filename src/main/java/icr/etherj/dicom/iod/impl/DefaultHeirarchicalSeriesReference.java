/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import icr.etherj.dicom.iod.HeirarchicalSeriesReference;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class DefaultHeirarchicalSeriesReference extends AbstractDisplayable
	implements HeirarchicalSeriesReference
{
	private final Map<String,ReferencedInstance> refInstances = new LinkedHashMap<>();
	private String seriesUid = Uids.generateDicomUid();

	@Override
	public ReferencedInstance addReferencedInstance(ReferencedInstance instance)
	{
		return (instance != null)
			? refInstances.put(Iods.makeKey(instance), instance)
			: null;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SeriesInstanceUid: "+seriesUid);
		int nItems = refInstances.size();
		ps.println(pad+"ReferencedInstanceList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		if (recurse)
		{
			for (ReferencedInstance inst : refInstances.values())
			{
				inst.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public ReferencedInstance getReferencedInstance(String key)
	{
		return refInstances.get(key);
	}

	@Override
	public List<ReferencedInstance> getReferencedInstanceList()
	{
		return ImmutableList.copyOf(refInstances.values());
	}

	@Override
	public String getSeriesInstanceUid()
	{
		return seriesUid;
	}

	@Override
	public void merge(ReferencedSeries other)
	{
		if (!seriesUid.equals(other.getSeriesInstanceUid()))
		{
			throw new IllegalArgumentException("Series instance UID mismatch");
		}
		for (ReferencedInstance inst : other.getReferencedInstanceList())
		{
			refInstances.put(Iods.makeKey(inst), inst);
		}
	}

	@Override
	public void merge(HeirarchicalSeriesReference other)
	{
		if (!seriesUid.equals(other.getSeriesInstanceUid()))
		{
			throw new IllegalArgumentException("Series instance UID mismatch");
		}
		for (ReferencedInstance inst : other.getReferencedInstanceList())
		{
			refInstances.put(Iods.makeKey(inst), inst);
		}
	}

	@Override
	public ReferencedInstance removeReferencedInstance(ReferencedInstance instance)
	{
		return refInstances.remove(Iods.makeKey(instance));
	}

	@Override
	public ReferencedInstance removeReferencedInstance(String key)
	{
		return refInstances.remove(key);
	}

	@Override
	public void setSeriesInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SeriesInstanceUid must not be null or empty");
		}
		this.seriesUid = uid;
	}

}
