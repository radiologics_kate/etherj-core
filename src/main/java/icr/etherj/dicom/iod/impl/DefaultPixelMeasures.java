/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.MathUtils;
import icr.etherj.dicom.iod.PixelMeasures;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class DefaultPixelMeasures extends AbstractDisplayable
	implements PixelMeasures
{
	private double[] pixelSpacing = new double[] {};
	private double sliceThickness = Double.NaN;
	private double sliceSpacing = Double.NaN;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (pixelSpacing.length == 2)
		{
			ps.println(pad+"PixelSpacing: "+
				pixelSpacing[0]+"\\"+pixelSpacing[1]);
		}
		if (MathUtils.isFinite(sliceThickness))
		{
			ps.println(pad+"SliceThickness: "+sliceThickness);
		}
		if (MathUtils.isFinite(sliceSpacing))
		{
			ps.println(pad+"SpacingBetweenSlices: "+sliceSpacing);
		}
	}
	
	@Override
	public double[] getPixelSpacing()
	{
		return Arrays.copyOf(pixelSpacing, pixelSpacing.length);
	}

	@Override
	public double getSliceThickness()
	{
		return sliceThickness;
	}

	@Override
	public double getSpacingBetweenSlices()
	{
		return sliceSpacing;
	}

	@Override
	public void setPixelSpacing(double[] pixelSpacing)
	{
		if ((pixelSpacing == null) || (pixelSpacing.length != 2))
		{
			this.pixelSpacing = new double[] {};
			return;
		}
		this.pixelSpacing = Arrays.copyOf(pixelSpacing, pixelSpacing.length);
	}

	@Override
	public void setSliceThickness(double thickness)
	{
		if (!MathUtils.isFinite(thickness))
		{
			sliceThickness = Double.NaN;
			return;
		}
		sliceThickness = (thickness <= 0.0) ? Double.NaN : thickness;
	}

	@Override
	public void setSpacingBetweenSlices(double spacing)
	{
		if (!MathUtils.isFinite(spacing))
		{
			sliceSpacing = Double.NaN;
			return;
		}
		sliceSpacing = (spacing <= 0.0) ? Double.NaN : spacing;
	}
	
}
