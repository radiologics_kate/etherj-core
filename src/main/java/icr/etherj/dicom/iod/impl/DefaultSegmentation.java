/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralImageModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.Modules;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.dicom.iod.module.SegmentationSeriesModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.impl.DefaultCommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultMultiframeDimensionModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultSegmentationImageModule;
import icr.etherj.dicom.iod.module.impl.DefaultSegmentationSeriesModule;
import icr.etherj.dicom.iod.module.impl.SegmentationFrameOfReferenceModule;
import icr.etherj.dicom.iod.module.impl.SegmentationImagePixelModule;
import icr.etherj.dicom.iod.module.impl.SegmentationMultiModuleCore;
import icr.etherj.dicom.iod.module.impl.DefaultSegmentationMultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.impl.SegmentationSopCommonModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public final class DefaultSegmentation extends AbstractDisplayable
	implements Segmentation
{
	private final ClinicalTrialStudyModule clinTrialStudy;
	private final ClinicalTrialSubjectModule clinTrialSubj;
	private final CommonInstanceReferenceModule commonInstRef;
	private final EnhancedGeneralEquipmentModule enhGenEquip;
	private final FrameOfReferenceModule frameOfRef;
	private final GeneralReferenceModule genRef;
	private final GeneralStudyModule genStudy;
	private final ImagePixelModule imagePixel;
	private final MultiframeFunctionalGroupsModule funcGroups;
	private final MultiframeDimensionModule dimension;
	private final PatientModule patient;
	private final PatientStudyModule patientStudy;
	private final SegmentationImageModule segImage;
	private final SegmentationMultiModuleCore segMultiModCore;
	private final SegmentationSeriesModule segSeries;
	private final SopCommonModule sopCommon;

	public DefaultSegmentation()
	{
		// Interdependent modules
		segMultiModCore = new SegmentationMultiModuleCore();
		frameOfRef = new SegmentationFrameOfReferenceModule(segMultiModCore);
		segImage = new DefaultSegmentationImageModule(segMultiModCore);
		imagePixel = new SegmentationImagePixelModule(segMultiModCore);
		DefaultSegmentationSharedFunctionalGroups sharedGroups =
			new DefaultSegmentationSharedFunctionalGroups();
		SegmentationPerFrameFunctionalGroups perFrameGroups =
			new DefaultSegmentationPerFrameFunctionalGroups();
		funcGroups = new DefaultSegmentationMultiframeFunctionalGroupsModule(
			segMultiModCore, sharedGroups, perFrameGroups);
		sopCommon = new SegmentationSopCommonModule(segMultiModCore);

		// Inheriting modules
		enhGenEquip = Modules.enhancedGeneralEquipmentModule();
		segSeries = new DefaultSegmentationSeriesModule();

		// Standalone modules
		clinTrialSubj = new DefaultClinicalTrialSubjectModule();
		clinTrialStudy = new DefaultClinicalTrialStudyModule();
		commonInstRef = new DefaultCommonInstanceReferenceModule();
		genStudy = new DefaultGeneralStudyModule();
		genRef = new DefaultGeneralReferenceModule();
		dimension = new DefaultMultiframeDimensionModule();
		patient = new DefaultPatientModule();
		patientStudy = new DefaultPatientStudyModule();
	}
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		patient.display(ps, indent+"  ", recurse);
		clinTrialSubj.display(ps, indent+"  ", recurse);
		genStudy.display(ps, indent+"  ", recurse);
		patientStudy.display(ps, indent+"  ", recurse);
		clinTrialStudy.display(ps, indent+"  ", recurse);
		segSeries.display(ps, indent+"  ", recurse);
		frameOfRef.display(ps, indent+"  ", recurse);
		enhGenEquip.display(ps, indent+"  ", recurse);
		genRef.display(ps, indent+"  ", recurse);
		imagePixel.display(ps, indent+"  ", recurse);
		segImage.display(ps, indent+"  ", recurse);
		funcGroups.display(ps, indent+"  ", recurse);
		dimension.display(ps, indent+"  ", recurse);
		commonInstRef.display(ps, indent+"  ", recurse);
		sopCommon.display(ps, indent+"  ", recurse);
	}

	@Override
	public ClinicalTrialStudyModule getClinicalTrialStudyModule()
	{
		return clinTrialStudy;
	}

	@Override
	public ClinicalTrialSubjectModule getClinicalTrialSubjectModule()
	{
		return clinTrialSubj;
	}

	@Override
	public CommonInstanceReferenceModule getCommonInstanceReferenceModule()
	{
		return commonInstRef;
	}

	@Override
	public EnhancedGeneralEquipmentModule getEnhancedEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public FrameOfReferenceModule getFrameOfReferenceModule()
	{
		return frameOfRef;
	}

	@Override
	public GeneralEquipmentModule getGeneralEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public GeneralImageModule getGeneralImageModule()
	{
		return segImage;
	}

	@Override
	public GeneralReferenceModule getGeneralReferenceModule()
	{
		return genRef;
	}

	@Override
	public GeneralSeriesModule getGeneralSeriesModule()
	{
		return segSeries;
	}

	@Override
	public GeneralStudyModule getGeneralStudyModule()
	{
		return genStudy;
	}

	@Override
	public ImagePixelModule getImagePixelModule()
	{
		return imagePixel;
	}

	@Override
	public MultiframeFunctionalGroupsModule getMultiframeFunctionalGroupsModule()
	{
		return funcGroups;
	}

	@Override
	public MultiframeDimensionModule getMultiframeDimensionModule()
	{
		return dimension;
	}

	@Override
	public PatientModule getPatientModule()
	{
		return patient;
	}

	@Override
	public PatientStudyModule getPatientStudyModule()
	{
		return patientStudy;
	}

	@Override
	public SegmentationImageModule getSegmentationImageModule()
	{
		return segImage;
	}

	@Override
	public SegmentationSeriesModule getSegmentationSeriesModule()
	{
		return segSeries;
	}

	@Override
	public SopCommonModule getSopCommonModule()
	{
		return sopCommon;
	}

	@Override
	public String getSopInstanceUid()
	{
		return sopCommon.getSopInstanceUid();
	}

	@Override
	public String getStudyDate()
	{
		return genStudy.getStudyDate();
	}

	@Override
	public String getStudyTime()
	{
		return genStudy.getStudyTime();
	}

}
