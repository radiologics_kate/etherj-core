/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.FrameContent;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class DefaultFrameContent extends AbstractDisplayable
	implements FrameContent
{
	private int[] dimIdx = new int[] {};
	private int inStackPos = 0;
	private String stackId = "";

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (!stackId.isEmpty())
		{
			ps.println(pad+"StackId: "+stackId);
			ps.println(pad+"InStackPositionNumber: "+inStackPos);
		}
		if (dimIdx.length > 0)
		{
			StringBuilder sb = new StringBuilder().append(dimIdx[0]);
			for (int i=1; i<dimIdx.length; i++)
			{
				sb.append(",").append(dimIdx[i]);
			}
			ps.println(pad+"DimensionIndexValues: "+sb.toString());
		}
	}

	@Override
	public int[] getDimensionIndexValues()
	{
		return Arrays.copyOf(dimIdx, dimIdx.length);
	}

	@Override
	public int getInStackPositionNumber()
	{
		return inStackPos;
	}

	@Override
	public String getStackId()
	{
		return stackId;
	}

	@Override
	public void setDimensionIndexValues(int[] values)
		throws IllegalArgumentException
	{
		if (values == null)
		{
			throw new IllegalArgumentException("DimensionIndexValues must not be null");
		}
		this.dimIdx = Arrays.copyOf(values, values.length);
	}

	@Override
	public void setInStackPositionNumber(int number)
		throws IllegalArgumentException
	{
		if (number < 1)
		{
			throw new IllegalArgumentException("InStackPositionNumber must be > 0");
		}
		inStackPos = number;
	}

	@Override
	public void setStackId(String id) throws IllegalArgumentException
	{
		if ((id == null) || id.isEmpty())
		{
			throw new IllegalArgumentException("StackID must not be null or empty.");
		}
		stackId = id;
	}
	
}
