/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.StructureSetRoi;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public class DefaultStructureSetRoi extends AbstractDisplayable
	implements StructureSetRoi
{
	private String desc;
	private String genAlgo = "";
	private String name = "";
	private int number = 1;
	private String refFoRUid = Uids.generateDicomUid();

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"RoiNumber: "+number);
		ps.println(pad+"ReferencedFrameOfReferenceUid: "+refFoRUid);
		if (!name.isEmpty())
		{
			ps.println(pad+"RoiName: "+name);
		}
		if ((desc != null) && !desc.isEmpty())
		{
			ps.println(pad+"RoiDescription: "+desc);
		}
		if (!genAlgo.isEmpty())
		{
			ps.println(pad+"RoiGenerationAlgorithm: "+genAlgo);
		}
	}

	@Override
	public String getReferencedFrameOfReferenceUid()
	{
		return refFoRUid;
	}

	@Override
	public String getRoiDescription()
	{
		return desc;
	}

	@Override
	public String getRoiGenerationAlgorithm()
	{
		return genAlgo;
	}

	@Override
	public String getRoiName()
	{
		return name;
	}

	@Override
	public int getRoiNumber()
	{
		return number;
	}

	@Override
	public void setReferencedFrameOfReferenceUid(String uid)
		throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"ReferencedFrameOfReferenceUid must not be null or empty");
		}
		this.refFoRUid = uid;
	}

	@Override
	public void setRoiDescription(String description)
	{
		this.desc = description;
	}

	@Override
	public void setRoiGenerationAlgorithm(String algorithm)
		throws IllegalArgumentException
	{
		if (algorithm == null)
		{
			genAlgo = "";
			return;
		}
		switch (algorithm)
		{
			case Automatic:
			case SemiAutomatic:
			case Manual:
			case "":
				genAlgo = algorithm;
				break;
			default:
				throw new IllegalArgumentException(
					"ROI generation algorithm must be AUTOMATIC, SEMIAUTOMATIC, "+
					"MANUAL or empty. Found: "+algorithm);
		}
	}

	@Override
	public void setRoiName(String name)
	{
		this.name = (name != null) ? name : "";
	}

	@Override
	public void setRoiNumber(int number)
	{
		this.number = number;
	}

	@Override
	public boolean validate()
	{
		String clazz = "StructureSetRoi";
		boolean refFoROk = Validation.type1(clazz, refFoRUid,
			Tag.ReferencedFrameOfReferenceUID);
		String[] allowable = new String[] {Automatic, SemiAutomatic, Manual};
		boolean genAlgoOk = Validation.type2(clazz, genAlgo, allowable,
			Tag.ROIGenerationAlgorithm);
		boolean nameOk = Validation.type2(clazz, name, Tag.ROIName);
		// ToDo: DerivationCodeSequence

		return refFoROk && genAlgoOk && nameOk;
	}

}
