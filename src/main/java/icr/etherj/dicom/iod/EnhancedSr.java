/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.module.ClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.SrDocumentContentModule;
import icr.etherj.dicom.iod.module.SrDocumentGeneralModule;
import icr.etherj.dicom.iod.module.SrDocumentSeriesModule;
import icr.etherj.dicom.iod.module.SynchronisationModule;

/**
 * Enhanced SR from DICOM IOD.
 * @author jamesd
 */
public interface EnhancedSr extends Displayable
{
	/**
	 * Returns the clinical trial series module.
	 * @return the module
	 */
	ClinicalTrialSeriesModule getClinicalTrialSeriesModule();

	/**
	 * Returns the clinical trial study module.
	 * @return the module
	 */
	ClinicalTrialStudyModule getClinicalTrialStudyModule();

	/**
	 * Returns the clinical trial subject module.
	 * @return the module
	 */
	ClinicalTrialSubjectModule getClinicalTrialSubjectModule();

	/**
	 * Returns the general equipment module.
	 * @return the module
	 */
	GeneralEquipmentModule getGeneralEquipmentModule();

	/**
	 * Returns the general study module.
	 * @return the module
	 */
	GeneralStudyModule getGeneralStudyModule();

	/**
	 * Returns the patient module.
	 * @return the module
	 */
	PatientModule getPatientModule();
	
	/**
	 * Returns the patient study module.
	 * @return the module
	 */
	PatientStudyModule getPatientStudyModule();

	/**
	 * Returns the SOP common module.
	 * @return the module
	 */
	SopCommonModule getSopCommonModule();

	/**
	 * Returns the SOP instance UID.
	 * @return
	 */
	String getSopInstanceUid();

	/**
	 * Returns the SR document content module.
	 * @return the module
	 */
	SrDocumentContentModule getSrDocumentContentModule();

	/**
	 * Returns the SR document general module.
	 * @return the module
	 */
	SrDocumentGeneralModule getSrDocumentGeneralModule();

	/**
	 * Returns the SR document series module.
	 * @return the module
	 */
	SrDocumentSeriesModule getSrDocumentSeriesModule();

	/**
	 * Returns the study date.
	 * @return the date
	 */
	String getStudyDate();

	/**
	 * Returns the study time.
	 * @return the time
	 */
	String getStudyTime();

	/**
	 * Returns the synchronisation module.
	 * @return the module
	 */
	SynchronisationModule getSynchronisationModule();

}
