/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import java.util.List;

/**
 * Coded entry with a value and meaning as defined in section 8 of DICOM IOD.
 * @author jamesd
 */
public interface Code extends Displayable
{

	/**
	 * Add a modifier code.
	 * @param code the code
	 * @return true if modifier added
	 */
	boolean addModifier(Code code);

	/**
	 * Returns the code meaning, type 1.
	 * @return the meaning
	 */
	String getCodeMeaning();

	/**
	 * Returns the code value if the value is 16 characters or less, type 1C.
	 * @return the value
	 */
	String getCodeValue();

	/**
	 * Returns the coding scheme designator, type 1C.
	 * @return the designator
	 */
	String getCodingSchemeDesignator();

	/**
	 * Returns the coding scheme version, type 1C.
	 * @return the designator
	 */
	String getCodingSchemeVersion();

	/**
	 * Returns the code value if the value is more than 16 characters, type 1C.
	 * @return the value
	 */
	String getLongCodeValue();

	/**
	 * Returns a list of modifier codes.
	 * @return the list
	 */
	List<Code> getModifiers();

	/**
	 * Removes a modifier code.
	 * @param code the code
	 * @return true if modifier removed
	 */
	boolean removeModifier(Code code);

	/**
	 * Sets the code meaning, type 1.
	 * @param meaning the meaning
	 * @throws IllegalArgumentException if meaning is null or empty
	 */
	void setCodeMeaning(String meaning) throws IllegalArgumentException;

	/**
	 * Sets the code value if the value is 16 characters or less, type 1C.
	 * @param value the value
	 * @throws IllegalArgumentException if value is null, empty or more than
	 * 16 characters
	 */
	void setCodeValue(String value) throws IllegalArgumentException;

	/**
	 * Sets the coding scheme designator, type 1C.
	 * @param designator the designator
	 * @throws IllegalArgumentException if designator is null or empty
	 */
	void setCodingSchemeDesignator(String designator)
		throws IllegalArgumentException;

	/**
	 * Sets the coding scheme version, type 1C.
	 * @param version the version
	 * @throws IllegalArgumentException if designator is null or empty
	 */
	void setCodingSchemeVersion(String version)
		throws IllegalArgumentException;

	/**
	 * Sets the code value if the value is more than 16 characters, type 1C.
	 * @param value
	 * @throws IllegalArgumentException if value is null, empty or less than 17
	 * characters
	 */
	void setLongCodeValue(String value) throws IllegalArgumentException;

}
