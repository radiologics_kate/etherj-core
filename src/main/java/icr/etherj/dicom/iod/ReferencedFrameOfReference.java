/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;
import java.util.List;

/**
 * ReferencedFrameOfReference from StructureSet module in DICOM IOD.
 * @author jamesd
 */
public interface ReferencedFrameOfReference extends Displayable, Validatable
{

	/**
	 * Adds a referenced study to the frame of reference.
	 * @param refStudy the study
	 * @return true if a non-null study was added
	 */
	boolean addRtReferencedStudy(RtReferencedStudy refStudy);

	/**
	 * Returns the frame of reference UID, type 1.
	 * @return the UID
	 */
	String getFrameOfReferenceUid();

	/**
	 * Returns the list of RT referenced studies, type 3.
	 * @return the list of studies
	 */
	List<RtReferencedStudy> getRtReferencedStudyList();

	/**
	 * Sets the frame of reference UID, type 1.
	 * @param uid the UID to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	void setFrameOfReferenceUid(String uid) throws IllegalArgumentException;
}
