/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.MR;
import icr.etherj.dicom.MrSopInstance;
import icr.etherj.dicom.Tags;
import java.io.File;
import java.io.PrintStream;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultMrSopInstance extends DefaultSopInstance
	implements MrSopInstance
{
	private final static Logger logger = LoggerFactory.getLogger(
		DefaultMrSopInstance.class);

	//	tetra is length of each element of tetrahedral vector [1,1,1], [1,-1,-1],
	//	[-1,1,-1], [-1,-1,1] as a unit vector
	private static final double tetra = 1.0/Math.sqrt(3.0);

	private double bValue = 0.0;
	private double[] bDirection = {0.0,0.0,0.0};
	private String bDirectionality = MR.NONE;
	private double[] bMatrix = {0.0,0.0,0.0,0.0,0.0,0.0};
	private double flipAngle;
	private double te;
	private double ti = Double.NaN;
	private double tr;
	
	DefaultMrSopInstance(File file)
	{
		super(file);
	}

	DefaultMrSopInstance(File file, DicomObject dcm)
		throws IllegalArgumentException
	{
		this(file, dcm, false);
	}

	DefaultMrSopInstance(File file, DicomObject dcm, boolean discard)
		throws IllegalArgumentException
	{
		super(file, dcm, false);
		flipAngle = dcm.getDouble(Tag.FlipAngle, Double.NaN);
		te = dcm.getDouble(Tag.EchoTime, Double.NaN);
		ti = dcm.getDouble(Tag.InversionTime, Double.NaN);
		tr = dcm.getDouble(Tag.RepetitionTime, Double.NaN);
		parseDiffusionInfo(dcm);
		if (discard)
		{
			compact();
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		super.display(ps, indent, recurse);
		String pad = indent+"  * ";
		ps.println(pad+"FlipAngle: "+flipAngle);
		ps.println(pad+"EchoTime: "+te);
		if (!Double.isNaN(ti) && !Double.isInfinite(ti))
		{
			ps.println(pad+"InversionTime: "+ti);
		}
		ps.println(pad+"RepetitionTime: "+tr);
		if (bValue != 0.0)
		{
			ps.println(pad+"DiffusionBValue: "+bValue);
			ps.println(pad+"DiffusionBDirectionality: "+bDirectionality);
			ps.println(pad+"DiffusionBDirection: "+
				String.format("%f\\%f\\%f",
					bDirection[0], bDirection[1], bDirection[2]));
			if (MR.BMATRIX.equals(bDirectionality))
			{
				ps.println(pad+"DiffusionBMatrix: "+
					String.format("%f\\%f\\%f\\%f\\%f\\%f",
						bMatrix[0], bMatrix[1], bMatrix[2], bMatrix[3], bMatrix[4],
						bMatrix[5]));
			}
		}
	}

	@Override
	public double[] getDiffusionBMatrix()
	{
		return bMatrix;
	}

	@Override
	public double getDiffusionBValue()
	{
		return bValue;
	}

	@Override
	public double[] getDiffusionDirection()
	{
		return bDirection;
	}

	@Override
	public String getDiffusionDirectionality()
	{
		return bDirectionality;
	}

	@Override
	public double getEchoTime()
	{
		return te;
	}

	@Override
	public double getFlipAngle()
	{
		return flipAngle;
	}

	@Override
	public double getInversionTime()
	{
		return ti;
	}

	@Override
	public double getRepetitionTime()
	{
		return tr;
	}

	@Override
	public void setDiffusionBMatrix(double[] matrix)
	{
		if ((matrix == null) || (matrix.length != 6))
		{
			throw new IllegalArgumentException("Matrix must be six elements");
		}
		System.arraycopy(matrix, 0, bMatrix, 0, 6);
	}

	@Override
	public void setDiffusionBValue(double bValue)
	{
		this.bValue = bValue;
	}

	@Override
	public void setDiffusionDirection(double[] dir)
	{
		if ((dir == null) || (dir.length != 3))
		{
			throw new IllegalArgumentException("Direction must be three elements");
		}
		System.arraycopy(dir, 0, bDirection, 0, 3);
	}

	@Override
	public void setDiffusionDirectionality(String dir)
	{
		switch (dir)
		{
			case MR.NONE:
			case MR.ISOTROPIC:
			case MR.DIRECTIONAL:
			case MR.BMATRIX:
				this.bDirectionality = dir;
				break;
			default:
				throw new IllegalArgumentException("Invalid directionality: "+dir);
		}
	}

	@Override
	public void setEchoTime(double te)
	{
		this.te = te;
	}

	@Override
	public void setFlipAngle(double angle)
	{
		flipAngle = angle;
	}

	@Override
	public void setInversionTime(double ti)
	{
		this.ti = ti;
	}

	@Override
	public void setRepetitionTime(double tr)
	{
		this.tr = tr;
	}

	private boolean isBruker(String manufacturer)
	{
		return manufacturer.toUpperCase().contains("BRUKER");
	}

	private boolean isGe(String manufacturer, String model)
	{
		return (manufacturer.contains("GE MEDICAL SYSTEMS") ||
			model.toUpperCase().contains("SIGNA"));
	}

	private boolean isPhilips(String manufacturer)
	{
		return manufacturer.toUpperCase().contains("PHILIPS MEDICAL SYSTEMS");
	}

	private boolean isSiemens(String manufacturer)
	{
		return manufacturer.contains("SIEMENS");
	}

	private void parseDiffusionInfo(DicomObject dcm)
	{
		String manufacturer = dcm.getString(Tag.Manufacturer, VR.LO, "").trim();
		String model = dcm.getString(Tag.ManufacturerModelName);
		model = (model == null) ? "" : model.trim();
		try
		{
			if (isSiemens(manufacturer))
			{
				parseSiemensDiffusion(dcm);
				return;
			}
			if (isGe(manufacturer, model))
			{
				parseGEDiffusion(dcm);
				return;
			}
			if (isBruker(manufacturer))
			{
				parseSiemensDiffusion(dcm);
				return;
			}
			if (isPhilips(manufacturer))
			{
				parsePhilipsDiffusion(dcm);
			}
		}
		// Clumsy but uncertain what could get thrown while parsing these private
		// tags
		catch (Exception ex)
		{
			logger.warn("Error in diffusion parsing", ex);
			resetDiffusion();
		}
	}

	private void parseGEDiffusion(DicomObject dcm)
	{
		double value = -1.0;
		if (dcm.contains(Tags.GeBValue))
		{
			// b-Value is 1st of 4 element array. Can have 100000000 added to it
			// for no obvious reason.
			String strValue = dcm.getString(Tags.GeBValue);
			if ((strValue == null) || strValue.isEmpty())
			{
				return;
			}
			String[] tokens = strValue.trim().split("\\\\");
			try
			{
				value = Double.parseDouble(tokens[0]);
			}
			catch (NumberFormatException ex)
			{
				return;
			}
			if (value > 50000)
			{
				value -= 1000000000;
			}
		}
		bValue = value;

		// DicomObject has getShorts() but not getShort()
		short[] dirArr = { -1 };
		if (dcm.contains(Tags.GeBDirection))
		{
			DicomElement element = dcm.get(Tags.GeBDirection);
			if (!element.vr().equals(VR.UN))
			{
				dirArr = dcm.getShorts(Tags.GeBDirection);
			}
			else
			{
				// Implicit VR LE => UN - do conversion manually
				byte[] bytes = element.getBytes();
				dirArr[0] = (short) ((bytes[0] & 0x00ff) | (bytes[1] << 8));
			}
		}
		//	Assume that all images are transaxial for now
		switch (dirArr[0])
		{
			// R/L
			case MR.GE_M:
				setDiffusionDirInfo("M");
				break;
	
			// A/P
			case MR.GE_P:
				setDiffusionDirInfo("P");
				break;
	
			// S/I
			case MR.GE_S:
				setDiffusionDirInfo("S");
				break;
	
			// Combined to trace
			case MR.GE_TRACE:
				bDirectionality = "ISOTROPIC";
				break;
	
			// TETRA Dir1
			case MR.GE_TETRA_DIR1:
				bDirectionality = "DIRECTIONAL";
				setDiffusionDirection(new double[] {tetra,tetra,tetra});
				break;
	
			// TETRA Dir2
			case MR.GE_TETRA_DIR2:
				bDirectionality = "DIRECTIONAL";
				setDiffusionDirection(new double[] {tetra,-tetra,-tetra});
				break;
	
			// TETRA Dir3
			case MR.GE_TETRA_DIR3:
				bDirectionality = "DIRECTIONAL";
				setDiffusionDirection(new double[] {-tetra,tetra,-tetra});
				break;
	
			// TETRA Dir4
			case MR.GE_TETRA_DIR4:
				bDirectionality = "DIRECTIONAL";
				setDiffusionDirection(new double[] {-tetra,-tetra,tetra});
				break;
	
			default:
		}
	}

	private void parsePhilipsBDirection(DicomObject dcm)
	{
		String dir = "";
		if (dcm.contains(Tags.PhilipsBDirection))
		{
			dir = dcm.getString(Tags.PhilipsBDirection);
		}
		else
		{
			if (dcm.contains(Tags.PhilipsLegacyBDirection))
			{
				dir = dcm.getString(Tags.PhilipsLegacyBDirection);
			}
		}
		if (dir == null)
		{
			dir = "";
		}
		setDiffusionDirInfo(dir);
	}

	// Return true if non-zero b-value found
	private boolean parsePhilipsBValue(DicomObject dcm)
	{
		double value = -1.0;
		if (dcm.contains(Tags.PhilipsBValue))
		{
			value = dcm.getFloat(Tags.PhilipsBValue);
		}
		else
		{
			if (dcm.contains(Tags.PhilipsLegacyBValue))
			{
				String strValue = dcm.getString(Tags.PhilipsLegacyBValue);
				try
				{
					value = Double.parseDouble(strValue);
				}
				catch (NumberFormatException ex)
				{ /* Deliberate no-op */ }
			}
		}
		if (value >= 0.0)
		{
			bValue = value;
			return true;
		}
		return false;
	}

	private void parsePhilipsDiffusion(DicomObject dcm)
	{
		if (parsePhilipsPublicFields(dcm))
		{
			return;
		}
		if (!parsePhilipsBValue(dcm))
		{
			return;
		}
		parsePhilipsBDirection(dcm);
	}

	private boolean parsePhilipsPublicFields(DicomObject dcm)
	{
		double value;
		double[] direction;
		if (dcm.contains(Tag.DiffusionBValue))
		{
			value = dcm.getDouble(Tag.DiffusionBValue);
			if (value <= 0.0)
			{
				return true;
			}
			bValue = value;
			// Public field used so probably Achieva or later, try further public
			// field
			if (dcm.contains(Tag.DiffusionGradientOrientation))
			{
				direction = dcm.getDoubles(Tag.DiffusionGradientOrientation);
				if (direction.length != 3)
				{
					return true;
				}
				System.arraycopy(direction, 0, bDirection, 0, 3);
				bDirectionality =  (Arrays.equals(direction, MR.BDIR_NONE))
					? MR.ISOTROPIC
					: MR.DIRECTIONAL;
			}
			return true;
		}
		return false;
	}

	private void parseSiemensBDirection(DicomObject dcm)
	{
		System.arraycopy(MR.BDIR_NONE, 0, bDirection, 0, 3);
		bDirectionality = MR.NONE;
		// Type maybe be NONE, ISOTROPIC, DIRECTIONAL or BMATRIX
		String type = MR.NONE;
		if (!dcm.contains(Tags.SiemensBDirectionality))
		{
			return;
		}
		type = dcm.getString(Tags.SiemensBDirectionality);
		if (type != null)
		{
			type = type.trim();
		}
		double[] direction;
		switch (type)
		{
			case MR.DIRECTIONAL:
			case MR.ISOTROPIC:
			case MR.BMATRIX:
				if (dcm.contains(Tags.SiemensBDirection))
				{
					VR vr = dcm.vrOf(Tags.SiemensBDirection);
					switch (vr.toString())
					{
						case "UN":
						case "UK":
							// Byte array, probably. Normally FD
							ByteOrder order = dcm.bigEndian()
								? ByteOrder.BIG_ENDIAN
								: ByteOrder.LITTLE_ENDIAN;
							direction = DicomUtils.bytesToDoubles(
								dcm.getBytes(Tags.SiemensBDirection), order);
							break;
						default:
							direction = dcm.getDoubles(Tags.SiemensBDirection);
					}
					if (direction.length == 3)
					{
						System.arraycopy(direction, 0, bDirection, 0, 3);
						bDirectionality = type;
					}
				}
				break;

			default:
		}
	}

	private void parseSiemensBMatrix(DicomObject dcm)
	{
		if (!dcm.contains(Tags.SiemensBMatrix))
		{
			return;
		}
		// 6x FD, upper right triangle of symmetric b matrix
		double[] matrix = Arrays.copyOf(MR.BMATRIX_EMPTY, 6);
		VR vr = dcm.vrOf(Tags.SiemensBMatrix);
		switch (vr.toString())
		{
			case "UN":
			case "UK":
				// Byte array, probably. Normally FD
				ByteOrder order = dcm.bigEndian()
					? ByteOrder.BIG_ENDIAN
					: ByteOrder.LITTLE_ENDIAN;
				matrix = DicomUtils.bytesToDoubles(dcm.getBytes(Tags.SiemensBMatrix),
					order);
				break;
			default:
				matrix = dcm.getDoubles(Tags.SiemensBMatrix);
		}
		if (matrix.length == 6)
		{
			System.arraycopy(matrix, 0, bMatrix, 0, 6);
		}
	}

	private boolean parseSiemensBValue(DicomObject dcm)
	{
		if (!dcm.contains(Tags.SiemensBValue))
		{
			return false;
		}
		double value = 0.0;
		String strValue = dcm.getString(Tags.SiemensBValue);
		try
		{
			value = Double.parseDouble(strValue);
		}
		catch (NumberFormatException ex)
		{
			return false;
		}
		bValue = value;
		return true;
	}

	private void parseSiemensDiffusion(DicomObject dcm)
	{
		// Pillage private fields. More reliable but also subject to change.
		if (!parseSiemensBValue(dcm))
		{
			// Fall back on regex. Horrible method but mostly works
			parseSiemensDiffusionRegex(dcm);
			return;
		}
		if (!(bValue > 0))
		{
			return;
		}
		parseSiemensBMatrix(dcm);
		parseSiemensBDirection(dcm);
	}

	private void parseSiemensDiffusionRegex(DicomObject dcm)
	{
		//	Kludgy and unreliable method using regex's of the sequence name
		String diffStr = dcm.getString(Tag.SequenceName);
		if ((diffStr == null) || diffStr.isEmpty())
		{
			setDiffusionDirInfo("");
			bValue = 0.0;
			return;
		}
		diffStr = diffStr.trim();
		int diffLen = diffStr.length();
		String prefix = "ep_b";
		int prefixPos = diffStr.indexOf(prefix);
		if (prefixPos < 0)
		{
			return;
		}
		int prefixLen = prefix.length();

		Pattern pattern = Pattern.compile("^(\\*?)ep_b([0-9]{1,4})(p|r|s|t)$");
		Matcher matcher = pattern.matcher(diffStr);
		if (matcher.find())
		{
			String dir = diffStr.substring(diffLen-1);
			if (dir.equals("r"))
			{
				dir = "m";
			}
			double value = -1.0;
			try
			{
				value = Double.parseDouble(
					diffStr.substring(prefixPos+prefixLen, diffLen-1));
			}
			catch (NumberFormatException ex)
			{ /* Deliberate no-op */ }
			if (value >= 0.0)
			{
				setDiffusionDirInfo(dir);
				bValue = value;
				return;
			}
		}

		pattern = Pattern.compile("^(\\*?)ep_b([0-9]{1,4})$");
		matcher = pattern.matcher(diffStr);
		if (matcher.find())
		{
			double value = -1.0;
			try
			{
				value = Double.parseDouble(
					diffStr.substring(prefixPos+prefixLen));
			}
			catch (NumberFormatException ex)
			{ /* Deliberate no-op */ }
			if (value >= 0.0)
			{
				setDiffusionDirInfo("");
				bValue = value;
				return;
			}
		}

		pattern = Pattern.compile("^(\\*?)ep_b([0-9]{1,4})(p|r|s|t)(#[0-9]{1})$");
		matcher = pattern.matcher(diffStr);
		if (matcher.find())
		{
			String dir = diffStr.substring(diffLen-3, diffLen-2);
			if (dir.equals("r"))
			{
				dir = "m";
			}
			double value = -1.0;
			try
			{
				value = Double.parseDouble(
					diffStr.substring(prefixPos+prefixLen, diffLen-3));
			}
			catch (NumberFormatException ex)
			{ /* Deliberate no-op */ }
			if (value >= 0.0)
			{
				setDiffusionDirInfo(dir);
				bValue = value;
				return;
			}
		}
	}

	private void resetDiffusion()
	{
		bValue = 0.0;
		System.arraycopy(MR.BDIR_NONE, 0, bDirection, 0, 3);
		bMatrix = Arrays.copyOf(MR.BMATRIX_EMPTY, 6);
		setDiffusionDirectionality(MR.NONE);
	}

	private void setDiffusionDirInfo(String bDir)
	{
		switch (bDir.toLowerCase())
		{
			case "m":
				System.arraycopy(MR.BDIR_M, 0, bDirection, 0, 3);
				setDiffusionDirectionality(MR.DIRECTIONAL);
				break;

			case "p":
				System.arraycopy(MR.BDIR_P, 0, bDirection, 0, 3);
				setDiffusionDirectionality(MR.DIRECTIONAL);
				break;

			case "s":
				System.arraycopy(MR.BDIR_S, 0, bDirection, 0, 3);
				setDiffusionDirectionality(MR.DIRECTIONAL);
				break;

			case "i":
			case "t":
				System.arraycopy(MR.BDIR_NONE, 0, bDirection, 0, 3);
				setDiffusionDirectionality(MR.ISOTROPIC);
				break;

			default:
				System.arraycopy(MR.BDIR_NONE, 0, bDirection, 0, 3);
				setDiffusionDirectionality(MR.NONE);
		}
	}

}
