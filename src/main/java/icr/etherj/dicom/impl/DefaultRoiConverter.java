/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.Uids;
import icr.etherj.aim.AimToolkit;
import icr.etherj.aim.AimUtils;
import icr.etherj.aim.Code;
import icr.etherj.aim.DicomImageReference;
import icr.etherj.aim.Equipment;
import icr.etherj.aim.Image;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.ImageReference;
import icr.etherj.aim.ImageSeries;
import icr.etherj.aim.ImageStudy;
import icr.etherj.aim.Markup;
import icr.etherj.aim.Person;
import icr.etherj.aim.TwoDimensionCircle;
import icr.etherj.aim.TwoDimensionCoordinate;
import icr.etherj.aim.TwoDimensionEllipse;
import icr.etherj.aim.TwoDimensionGeometricShape;
import icr.etherj.aim.TwoDimensionMultiPoint;
import icr.etherj.aim.TwoDimensionPoint;
import icr.etherj.aim.TwoDimensionPolyline;
import icr.etherj.aim.User;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.RoiConverter;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.Modality;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultContour;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedStudy;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import icr.etherj.nifti.Nifti;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
final class DefaultRoiConverter implements RoiConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(DefaultRoiConverter.class);

	@Override
	public ImageAnnotationCollection toIac(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		ImageAnnotationCollection iac = AimToolkit.getToolkit().createIac();
		try
		{
			processRtStruct(iac, rtStruct, dcmMap);
		}
		catch (IllegalArgumentException ex)
		{
			throw new ConversionException(ex);
		}
		return iac;
	}

	@Override
	public Nifti toNifti(Segmentation seg, Map<String, DicomObject> dcmMap)
		throws ConversionException
	{
		SegToNiftiRoiConverter converter = new SegToNiftiRoiConverter();
		return converter.convert(seg, dcmMap);
	}

	@Override
	public RtStruct toRtStruct(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		RtStruct rtStruct = DicomToolkit.getToolkit().createRtStruct();
		processIac(rtStruct, iac);
		List<DicomImageReference> dcmRefs = gatherDicomImageReferences(iac);
		processIas(rtStruct, iac, dcmRefs, dcmMap);

		return rtStruct;
	}
	
	private String aimContourType(TwoDimensionGeometricShape shape)
	{
		if ((shape instanceof TwoDimensionPolyline) ||
			 (shape instanceof TwoDimensionCircle) ||
			 (shape instanceof TwoDimensionEllipse))
		{
			return ClosedPlanar;
		}
		if ((shape instanceof TwoDimensionPoint) ||
			 (shape instanceof TwoDimensionMultiPoint))
		{
			return Point;
		}
		return null;
	}

	private List<Coordinate3D> aimToDicom(List<TwoDimensionCoordinate> coords2D,
		DicomObject refDcm, int frame) throws ConversionException
	{
		boolean isMultiframe = DicomUtils.isMultiframeImageSopClass(
			refDcm.getString(Tag.SOPClassUID));
		double[] pos = getImagePositionPatient(refDcm, isMultiframe, frame);
		double[] ori = getImageOrientationPatient(refDcm, isMultiframe, frame);
		double[] row = Arrays.copyOfRange(ori, 0, 3);
		double[] col = Arrays.copyOfRange(ori, 3, 6);
		double[] pixDims = getPixelSpacing(refDcm, isMultiframe, frame);
		List<Coordinate3D> coords3D = new ArrayList<>(coords2D.size());
		for (TwoDimensionCoordinate coord2D : coords2D)
		{
			// AIM uses TLHC of pixel not centre
			coords3D.add(DicomUtils.imageCoordToPatientCoord3D(pos, row, col,
				pixDims, coord2D.getX()+0.5, coord2D.getY()+0.5));
		}
		return coords3D;
	}

	private void buildStudySeriesModules(RtStruct rtStruct,
		ImageAnnotationCollection iac) throws ConversionException
	{
		Map<String,ImageStudy> studyMap = new HashMap<>();
		Set<String> seriesSet = new HashSet<>();
		for (ImageAnnotation ia : iac.getAnnotationList())
		{
			List<ImageReference> refList = ia.getReferenceList();
			List<DicomImageReference> dcmImageRefs = new ArrayList<>();
			for (ImageReference imageRef : refList)
			{
				if (!(imageRef instanceof DicomImageReference))
				{
					continue;
				}
				DicomImageReference dcmRef = (DicomImageReference) imageRef;
				dcmImageRefs.add(dcmRef);
				processDicomImageReference(dcmRef, studyMap, seriesSet,
					ia.getUid());
			}
			if (dcmImageRefs.isEmpty())
			{
				throw new ConversionException(
					"No DicomImageReferences found in ImageAnnotation, UID: "+
						ia.getUid());
			}
		}
		if ((studyMap.size() != 1) || (seriesSet.size() != 1))
		{
			throw new ConversionException(
				"Study and Series UIDs not unique in ImageAnnotationCollection, UID:"+
					iac.getUid());
		}
		logger.debug("Building GeneralStudyModule");
		ImageStudy aimStudy = studyMap.values().iterator().next();
		GeneralStudyModule gsm = rtStruct.getGeneralStudyModule();
		gsm.setStudyInstanceUid(aimStudy.getInstanceUid());
		gsm.setStudyDate(AimUtils.getDate(aimStudy.getStartDate()));
		gsm.setStudyTime(AimUtils.getTime(aimStudy.getStartTime()));
		logger.debug("Building RtSeriesModule");
		RtSeriesModule rsm = rtStruct.getRtSeriesModule();
		rsm.setModality(Modality.RTSTRUCT);
		rsm.setSeriesInstanceUid(Uids.generateDicomUid());
	}

	/*
	 *	Create a ContourImage for the RoiContour or 
	 * ReferencedFrameOfReference in the StructureSetModule
	 */
	private ContourImage createContourImage(DicomObject dcm, int frame)
	{
		ContourImage ci = new DefaultContourImage();
		ci.setReferencedSopClassUid(dcm.getString(Tag.SOPClassUID));
		ci.setReferencedSopInstanceUid(dcm.getString(Tag.SOPInstanceUID));
		if (frame > 0)
		{
			ci.setReferencedFrameNumber(frame);
		}
		return ci;
	}

	/*
	 *	Create an item for the RT ROI Observations Module from Information Object
	 *	Definitions section C.8.8.8 (2015)
	 */
	private RtRoiObservation createRtRoiObservation(int iaIdx)
	{
		RtRoiObservation obs = new DefaultRtRoiObservation();
		obs.setObservationNumber(iaIdx);
		obs.setReferencedRoiNumber(iaIdx);
		return obs;
	}

	private StructureSetRoi createStructureSetRoi(ImageAnnotation ia, int iaIdx,
		String refUid)
	{
		StructureSetRoi ssRoi = new DefaultStructureSetRoi();
		ssRoi.setRoiNumber(iaIdx);
		ssRoi.setReferencedFrameOfReferenceUid(refUid);
		ssRoi.setRoiName(ia.getName());
		String value = ia.getComment();
		if (!value.isEmpty())
		{
			ssRoi.setRoiDescription(value);
		}
		return ssRoi;
	}

	private List<DicomImageReference> gatherDicomImageReferences(
		ImageAnnotationCollection iac)
	{
		logger.debug("Gathering DicomImageReferences");
		List<DicomImageReference> dcmImageRefs = new ArrayList<>();
		for (ImageAnnotation ia : iac.getAnnotationList())
		{
			List<ImageReference> refList = ia.getReferenceList();
			for (ImageReference imageRef : refList)
			{
				if (imageRef instanceof DicomImageReference)
				{
					dcmImageRefs.add((DicomImageReference) imageRef);
				}
			}
		}
		return dcmImageRefs;
	}

	private double[] getImageOrientationPatient(DicomObject refDcm,
		boolean isMultiframe, int frame) throws ConversionException
	{
		double[] ori;
		if (!isMultiframe)
		{
			ori = refDcm.getDoubles(Tag.ImageOrientationPatient);
		}
		else
		{
			ori = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PlaneOrientationSequence, 0,
				Tag.ImageOrientationPatient});
			if ((ori == null) || (ori.length != 6))
			{
				ori = refDcm.getDoubles(new int[] {
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PlaneOrientationSequence, 0,
					Tag.ImageOrientationPatient});
			}
		}
		if ((ori == null) || (ori.length != 6))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"ImageOrientationPatient missing or invalid"+extra);
		}
		return ori;
	}

	private double[] getImagePositionPatient(DicomObject refDcm,
		boolean isMultiframe, int frame) throws ConversionException
	{
		double[] pos;
		if (!isMultiframe)
		{
			pos = refDcm.getDoubles(Tag.ImagePositionPatient);
		}
		else
		{
			pos = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PlanePositionSequence, 0,
				Tag.ImagePositionPatient});
		}
		if ((pos == null) || (pos.length != 3))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"ImagePositionPatient missing or invalid"+extra);
		}
		return pos;
	}

	private double[] getPixelSpacing(DicomObject refDcm, boolean isMultiframe,
		int frame) throws ConversionException
	{
		double[] spacing;
		if (!isMultiframe)
		{
			spacing = refDcm.getDoubles(Tag.PixelSpacing);
		}
		else
		{
			spacing = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PixelMeasuresSequence, 0,
				Tag.PixelSpacing});
			if ((spacing == null) || (spacing.length != 2))
			{
				spacing = refDcm.getDoubles(new int[] {
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PixelMeasuresSequence, 0,
					Tag.PixelSpacing});
			}
		}
		if ((spacing == null) || (spacing.length != 2))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"PixelSpacing missing or invalid"+extra);
		}
		return spacing;
	}

	private String getStructureSetDateTime(StructureSetModule ssm)
	{
		String ssDate = ssm.getStructureSetDate();
		String ssTime = ssm.getStructureSetTime();
		if (ssDate.isEmpty() || ssTime.isEmpty())
		{
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			return df.format(new Date());
		}
		return ssDate+ssTime;
	}

	private ImageAnnotation iaForRoi(Map<Integer, ImageAnnotation> roiToIa,
		RoiContour rc) throws ConversionException
	{
		ImageAnnotation ia = roiToIa.get(rc.getReferencedRoiNumber());
		if (ia == null)
		{
			throw new ConversionException(
				"Referenced ROI not found for ROI number: "+
					rc.getReferencedRoiNumber());
		}
		return ia;
	}

	private DicomImageReference mergeReferences(List<DicomImageReference> dcmRefs)
	{
		DicomImageReference mergedRef = new DicomImageReference();
		ImageStudy mergedStudy = new ImageStudy();
		mergedRef.setStudy(mergedStudy);
		ImageStudy study = dcmRefs.get(0).getStudy();
		mergedStudy.setInstanceUid(study.getInstanceUid());
		mergedStudy.setStartDate(study.getStartDate());
		mergedStudy.setStartTime(study.getStartTime());
		ImageSeries mergedSeries = new ImageSeries();
		mergedStudy.setSeries(mergedSeries);
		ImageSeries series = study.getSeries();
		mergedSeries.setInstanceUid(series.getInstanceUid());
		mergedSeries.setModality(series.getModality());
		for (DicomImageReference ref : dcmRefs)
		{
			for (Image image : ref.getStudy().getSeries().getImageList())
			{
				mergedSeries.addImage(new Image(image));
			}
		}

		return mergedRef;
	}

	private void processDicomImageReference(DicomImageReference dcmRef,
		Map<String,ImageStudy> studyMap, Set<String> seriesSet, String iaUid)
		throws ConversionException
	{
		ImageStudy study = dcmRef.getStudy();
		if (study == null)
		{
			throw new ConversionException(
				"DicomImageReference with null ImageStudy found in ImageAnnotation, UID: "+
					iaUid);
		}
		String studyUid = study.getInstanceUid();
		if ((studyUid == null) || studyUid.isEmpty())
		{
			throw new ConversionException(
				"ImageStudy has null or empty UID found in ImageAnnotation, UID: "+
					iaUid);
		}
		studyMap.put(studyUid, study);
		ImageSeries series = study.getSeries();
		if (series == null)
		{
			throw new ConversionException(
				"DicomImageReference has null ImageStudy in ImageAnnotation, UID: "+
					iaUid);
		}
		String seriesUid = series.getInstanceUid();
		if ((seriesUid == null) || seriesUid.isEmpty())
		{
			throw new ConversionException(
				"ImageSeries has null or empty UID found in ImageAnnotation, UID: "+
					iaUid);
		}
		seriesSet.add(seriesUid);
	}

	private void processEquipment(ImageAnnotationCollection iac, RtStruct rtStruct)
	{
		Equipment equip = new Equipment();
		GeneralEquipmentModule gem = rtStruct.getGeneralEquipmentModule();
		equip.setManufacturerName(gem.getManufacturer());
		equip.setManufacturerModelName(gem.getManufacturersModelName());
		equip.setDeviceSerialNumber(gem.getDeviceSerialNumber());
		equip.setSoftwareVersion(gem.getSoftwareVersion());
		iac.setEquipment(equip);
	}

	private void processIaReferences(ImageAnnotation ia,
		Map<String,DicomObject> dcmMap, Map<String,String> seriesToStudy,
		Map<String,String> instanceToSeries)
		throws ConversionException
	{
		Map<String,DicomImageReference> studyToRef = new HashMap<>();
		for (ImageReference imageRef : ia.getReferenceList())
		{
			if (imageRef instanceof DicomImageReference)
			{
				DicomImageReference dcmRef = (DicomImageReference) imageRef;
				studyToRef.put(dcmRef.getStudy().getInstanceUid(), dcmRef);
			}
		}
		for (Markup markup : ia.getMarkupList())
		{
			if (!(markup instanceof TwoDimensionPolyline))
			{
				continue;
			}
			TwoDimensionPolyline polyline = (TwoDimensionPolyline) markup;
			String instUid = polyline.getImageReferenceUid();
			String seriesUid = instanceToSeries.get(instUid);
			if (seriesUid == null)
			{
				throw new ConversionException(
					"No series found for instance UID: "+instUid);
			}
			String studyUid = seriesToStudy.get(seriesUid);
			if (studyUid == null)
			{
				throw new ConversionException(
					"No study found for series UID: "+seriesUid);
			}
			DicomObject refDcm = dcmMap.get(instUid);
			DicomImageReference dcmRef = studyToRef.get(studyUid);
			if (dcmRef == null)
			{
				dcmRef = new DicomImageReference();
				ImageStudy study = new ImageStudy(studyUid);
				dcmRef.setStudy(study);
				study.setStartDate(refDcm.getString(Tag.StudyDate, ""));
				study.setStartTime(refDcm.getString(Tag.StudyTime, ""));
				studyToRef.put(studyUid, dcmRef);
				ia.addReference(dcmRef);
			}
			ImageStudy study = dcmRef.getStudy();
			ImageSeries series = study.getSeries();
			if (series == null)
			{
				series = new ImageSeries(seriesUid);
				Code modality = new Code();
				modality.setCode(refDcm.getString(Tag.Modality));
				series.setModality(modality);
				study.setSeries(series);
			}
			Image image = series.getImage(instUid);
			if (image == null)
			{
				image = new Image(refDcm.getString(Tag.SOPClassUID), instUid);
				series.addImage(image);
			}
		}
	}

	private void processIac(RtStruct rtStruct, ImageAnnotationCollection iac)
		throws ConversionException
	{
		logger.debug("Building StructureSetModule");
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		ssm.setStructureSetLabel(iac.getDescription());
		Date now = new Date();
		DateFormat dateF = new SimpleDateFormat("yyyyMMdd");
		ssm.setStructureSetDate(dateF.format(now));
		DateFormat timeF = new SimpleDateFormat("HHmmss");
		ssm.setStructureSetTime(timeF.format(now));

		buildStudySeriesModules(rtStruct, iac);
		Person person = iac.getPerson();
		if (person != null)
		{
			logger.debug("Building PatientModule");
			PatientModule pm = rtStruct.getPatientModule();
			pm.setPatientName(person.getName());
			pm.setPatientId(person.getId());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date date = AimUtils.parseDateTime(person.getBirthDate());
			if (date != null)
			{
				pm.setPatientBirthDate(sdf.format(date));
			}
			String sex = person.getSex();
			if (!sex.isEmpty())
			{
				pm.setPatientSex(sex);
			}
		}
		Equipment aimEquipment = iac.getEquipment();
		if (aimEquipment != null)
		{
			logger.debug("Building GeneralEquipmentModule");
			GeneralEquipmentModule gem = rtStruct.getGeneralEquipmentModule();
			gem.setManufacturer(aimEquipment.getManufacturerName());
			String value = aimEquipment.getManufacturerModelName();
			if (!value.isEmpty())
			{
				gem.setManufacturersModelName(value);
			}
			value = aimEquipment.getDeviceSerialNumber();
			if (!value.isEmpty())
			{
				gem.setDeviceSerialNumber(value);
			}
			value = aimEquipment.getSoftwareVersion();
			if (!value.isEmpty())
			{
				gem.setSoftwareVersion(value);
			}
		}
	}

	/*
	 *	Process IA's Markups into a single StructureSetRoi, RoiContour and
	 * RtRoiObservation for each IA.
	 */
	private void processIaMarkups(RtStruct rtStruct, ImageAnnotation ia,
		int iaIdx, Map<String,DicomObject> dcmMap) throws ConversionException
	{
		logger.debug("Building StructureSetRoi");
		String refUid = rtStruct.getFrameOfReferenceModule().getFrameOfReferenceUid();
		StructureSetRoi ssRoi = createStructureSetRoi(ia, iaIdx, refUid);
		RoiContour roiContour = new DefaultRoiContour();
		roiContour.setReferencedRoiNumber(iaIdx);
		RtRoiObservation rtRoiObs = createRtRoiObservation(iaIdx);
		rtStruct.getStructureSetModule().addStructureSetRoi(ssRoi);
		rtStruct.getRoiContourModule().addRoiContour(roiContour);
		rtStruct.getRtRoiObservationsModule().addRtRoiObservation(rtRoiObs);

		logger.debug("Building RoiContour");
		int idx = 1;
		List<Markup> markups = ia.getMarkupList();
		for (Markup markup : markups)
		{
			if (!(markup instanceof TwoDimensionGeometricShape))
			{
				continue;
			}
			TwoDimensionGeometricShape shape = (TwoDimensionGeometricShape) markup;
			processMarkup(roiContour, shape, idx, dcmMap);
			idx++;
		}
	}

	private void processIas(RtStruct rtStruct, ImageAnnotationCollection iac,
		List<DicomImageReference> dcmRefs, Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		processImageReferences(rtStruct, dcmRefs, dcmMap);
		List<ImageAnnotation> iaList = iac.getAnnotationList();
		int iaIdx = 1;
		for (ImageAnnotation ia : iaList)
		{
			processIaMarkups(rtStruct, ia, iaIdx, dcmMap);
			iaIdx++;
		}
	}

	/*
	 *	Process the DicomImageReferences to build the
	 * ReferencedFrameOfReferenceSequence
	 */
	private void processImageReferences(RtStruct rtStruct,
		List<DicomImageReference> dcmRefs, Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		if (dcmMap.isEmpty())
		{
			throw new ConversionException(
				"No referenced DicomObjects - cannot locate FrameOfReferenceUID");
		}
		DicomObject dcm = dcmMap.values().iterator().next();
		String refFoRUid = dcm.getString(Tag.FrameOfReferenceUID);
		rtStruct.getFrameOfReferenceModule().setFrameOfReferenceUid(refFoRUid);
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		logger.debug("Building ReferencedFrameOfReference");
		ReferencedFrameOfReference refFoR = new DefaultReferencedFrameOfReference();
		refFoR.setFrameOfReferenceUid(refFoRUid);
		ssm.addReferencedFrameOfReference(refFoR);

		DicomImageReference dcmRef = mergeReferences(dcmRefs);
		ImageStudy aimStudy = dcmRef.getStudy();
		RtReferencedStudy rtStudy = new DefaultRtReferencedStudy();
		// Retired UID allowed see Information Object Definitions,
		// Section C.8.8.5.4 (2015)
		rtStudy.setReferencedSopClassUid(
			UID.DetachedStudyManagementSOPClassRetired);
		rtStudy.setReferencedSopInstanceUid(aimStudy.getInstanceUid());
		refFoR.addRtReferencedStudy(rtStudy);
		ImageSeries aimSeries = aimStudy.getSeries();
		RtReferencedSeries rtSeries = new DefaultRtReferencedSeries();
		rtSeries.setSeriesInstanceUid(aimSeries.getInstanceUid());
		rtStudy.addRtReferencedSeries(rtSeries);
		for (Image aimImage : aimSeries.getImageList())
		{
			ContourImage ci = new DefaultContourImage();
			ci.setReferencedSopClassUid(aimImage.getSopClassUid());
			ci.setReferencedSopInstanceUid(aimImage.getSopInstanceUid());
			rtSeries.addContourImage(ci);
		}
	}

	private void processMarkup(RoiContour roiContour,
		TwoDimensionGeometricShape shape, int idx,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		String contourType = aimContourType(shape);
		if (contourType == null)
		{
			logger.warn(
				"Unknown AIM TwoDimensionGeometricShape: {} in Markup. UID: {}",
				shape.getClass().getName(), shape.getUid());
			return;
		}
		List<TwoDimensionCoordinate> coords2D = shape.getCoordinateList();
		if (coords2D.isEmpty())
		{
			logger.warn("Zero coordinates found in Markup. UID: {}",
				shape.getUid());
			return;
		}
		if (shape instanceof TwoDimensionMultiPoint)
		{
			logger.info("TwoDimensionMultiPoint not yet supported.");
			return;
		}
		if (shape instanceof TwoDimensionCircle)
		{
			logger.info("TwoDimensionCircle not yet supported.");
			return;
		}
		if (shape instanceof TwoDimensionEllipse)
		{
			logger.info("TwoDimensionEllipse not yet supported.");
			return;
		}
		String imageRefUid = shape.getImageReferenceUid();
		DicomObject refDcm = dcmMap.get(imageRefUid);
		if (imageRefUid.isEmpty() || (refDcm == null))
		{
			throw new ConversionException("Referenced DicomObject cannot be located");
		}
		Contour contour = new DefaultContour();
		contour.setContourNumber(idx);
		roiContour.addContour(contour);
		contour.setContourGeometricType(contourType);
		contour.addContourImage(createContourImage(refDcm,
			shape.getReferencedFrameNumber()));
		contour.setContourGeometricType(contourType);
		List<Coordinate3D> coords3D = aimToDicom(coords2D, refDcm,
			shape.getReferencedFrameNumber());
		for (Coordinate3D coord : coords3D)
		{
			contour.addCoordinate(coord);
		}
	}

	private void processPerson(ImageAnnotationCollection iac, RtStruct rtStruct)
	{
		Person person = new Person();
		PatientModule pm = rtStruct.getPatientModule();
		person.setName(pm.getPatientName());
		person.setId(pm.getPatientId());
		person.setBirthDate(pm.getPatientBirthDate());
		person.setSex(pm.getPatientSex());
		iac.setPerson(person);
	}

	private void processRefFrameOfRef(StructureSetModule ssm,
		Map<String,String> seriesToStudy, Map<String,String> instanceToSeries)
	{
		for (ReferencedFrameOfReference refFoR : ssm.getReferencedFrameOfReferenceList())
		{
			for (RtReferencedStudy refStudy : refFoR.getRtReferencedStudyList())
			{
				String studyUid = refStudy.getReferencedSopInstanceUid();
				for (RtReferencedSeries refSeries : refStudy.getRtReferencedSeriesList())
				{
					String seriesUid = refSeries.getSeriesInstanceUid();
					seriesToStudy.putIfAbsent(seriesUid, studyUid);
					for (ContourImage ci : refSeries.getContourImageList())
					{
						instanceToSeries.putIfAbsent(ci.getReferencedSopInstanceUid(),
							seriesUid);
					}
				}
			}
		}
	}

	private void processRoiContours(ImageAnnotationCollection iac,
		RtStruct rtStruct, Map<String,DicomObject> dcmMap,
		Map<Integer,ImageAnnotation> roiToIa, Map<String,String> seriesToStudy,
		Map<String,String> instanceToSeries) throws ConversionException
	{
		RoiContourModule rcm = rtStruct.getRoiContourModule();
		for (RoiContour rc : rcm.getRoiContourList())
		{
			ImageAnnotation ia = iaForRoi(roiToIa, rc);
			int shapeId = 0;
			for (Contour contour : rc.getContourList())
			{
				TwoDimensionGeometricShape markup = null;
				switch (contour.getContourGeometricType())
				{
					case Contour.ClosedPlanar:
						markup = new TwoDimensionPolyline();
						break;
					case Contour.Point:
						markup = new TwoDimensionPoint();
						break;
					default:
						throw new ConversionException(
							"Unsupported contour geometric type: "+
								contour.getContourGeometricType());
				}
				List<ContourImage> ciList = contour.getContourImageList();
				if (ciList.size() != 1)
				{
					throw new ConversionException(
						"AIM requires exactly one referenced image per contour.");
				}
				ContourImage ci = ciList.get(0);
				String refSopInstUid = ci.getReferencedSopInstanceUid();
				DicomObject refDcm = dcmMap.get(refSopInstUid);
				if (refDcm == null)
				{
					throw new ConversionException(
						"Referenced image not found for UID: "+
							ci.getReferencedSopInstanceUid());
				}
				if (!ci.getReferencedSopClassUid().equals(
						refDcm.getString(Tag.SOPClassUID)))
				{
					throw new ConversionException(
						String.format("SOP class UID mismatch. Expected: %s Found: %s",
							ci.getReferencedSopClassUid(),
							refDcm.getString(Tag.SOPClassUID)));
				}
				int refFrame = ci.getReferencedFrameNumber();
				boolean isMultiframe = DicomUtils.isMultiframeImageSopClass(
					refDcm.getString(Tag.SOPClassUID));
				double[] pos = getImagePositionPatient(refDcm, isMultiframe,
					refFrame);
				double[] ori = getImageOrientationPatient(refDcm, isMultiframe,
					refFrame);
				double[] row = Arrays.copyOfRange(ori, 0, 3);
				double[] col = Arrays.copyOfRange(ori, 3, 6);
				double[] pixDims = getPixelSpacing(refDcm, isMultiframe, refFrame);
				double[] patCoord = new double[3];
				
				markup.setIncludeFlag(true);
				markup.setShapeId(shapeId++);
				markup.setImageReferenceUid(refSopInstUid);
				markup.setReferencedFrameNumber(refFrame);
				int i = 0;
				for (Coordinate3D coord3D : contour.getContourData())
				{
					patCoord[0] = coord3D.x;
					patCoord[1] = coord3D.y;
					patCoord[2] = coord3D.z;
					double[] coord2D = DicomUtils.patientCoordToImageCoord(patCoord,
						pos, row, col, pixDims);
					// AIM uses TLHC of pixel instead of centre
					markup.addCoordinate(
						new TwoDimensionCoordinate(i++, coord2D[0]-0.5, coord2D[1]-0.5));
				}
				ia.addMarkup(markup);
			}
			Code code = new Code();
			code.setCode("AnyClosedShape");
			ia.addTypeCode(code);
			processIaReferences(ia, dcmMap, seriesToStudy, instanceToSeries);
		}
	}

	private void processRtStruct(ImageAnnotationCollection iac, RtStruct rtStruct,
		Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		iac.setDescription(ssm.getStructureSetLabel());
		iac.setDateTime(getStructureSetDateTime(ssm));
		processPerson(iac, rtStruct);
		processUser(iac);
		processEquipment(iac, rtStruct);
		processStructureSetRois(iac, rtStruct, dcmMap);
	}

	private void processStructureSetRois(ImageAnnotationCollection iac,
		RtStruct rtStruct, Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		Map<String,String> seriesToStudy = new HashMap<>();
		Map<String,String> instanceToSeries = new HashMap<>();
		processRefFrameOfRef(ssm, seriesToStudy, instanceToSeries);
		Map<Integer,ImageAnnotation> roiToIa = new HashMap<>();
		List<StructureSetRoi> roiList = ssm.getStructureSetRoiList();
		for (StructureSetRoi roi : roiList)
		{
			ImageAnnotation ia = new ImageAnnotation();
			ia.setName(roi.getRoiName());
			ia.setDateTime(getStructureSetDateTime(ssm));
			ia.setComment(roi.getRoiDescription());
			roiToIa.put(roi.getRoiNumber(), ia);
			iac.addAnnotation(ia);
		}
		processRoiContours(iac, rtStruct, dcmMap, roiToIa, seriesToStudy,
			instanceToSeries);
	}

	private void processUser(ImageAnnotationCollection iac)
	{
		User user = new User();
		user.setLoginName(System.getProperty("user.name"));
		iac.setUser(user);
	}

}
