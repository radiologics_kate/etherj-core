/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.nifti.Nifti;
import icr.etherj.nifti.NiftiHeader;
import icr.etherj.nifti.NiftiToolkit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
final class SegToNiftiRoiConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(SegToNiftiRoiConverter.class);

	private boolean sourceMultiFrame;
	private final Lock lock = new ReentrantLock();
	private int nCols;
	private int nFrames;
	private int nRows;
	// LinkedHashMap to ensure iteration in insertion order
	private final Map<String,RefFrame> refFrames = new LinkedHashMap<>();

	// Lock the single public method to allow use of instance variables
	public Nifti convert(Segmentation seg, Map<String, DicomObject> dcmMap)
		throws ConversionException
	{
		try
		{
			lock.lock();
			return convertImpl(seg, dcmMap);
		}
		catch (IllegalArgumentException ex)
		{
			throw new ConversionException(ex);
		}
		finally
		{
			lock.unlock();
		}
	}

	private void buildFrameMapping(Set<String> refInstUids,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		logger.debug("Building frame mapping");
		if (refInstUids.isEmpty())
		{
			throw new ConversionException(
				"Segmentation contains no references to source images");
		}
		if (!dcmMap.keySet().containsAll(refInstUids))
		{
			throw new ConversionException(
				"Segmentation references one or more images not contained in the reference map");
		}
		DicomObject refDcm = dcmMap.get(refInstUids.iterator().next());
		String sopClassUid = refDcm.getString(Tag.SOPClassUID);
		// Multiframe image not yet supported
		sourceMultiFrame = DicomUtils.isMultiframeImageSopClass(sopClassUid);
		if (sourceMultiFrame)
		{
			throw new ConversionException(
				"Referenced multiframe image has invalid frame count");
		}
		// Single frame images - find all images with matching series UID
		String seriesUid = refDcm.getString(Tag.SeriesInstanceUID, VR.UI, null);
		if (StringUtils.isNullOrEmpty(seriesUid))
		{
			throw new ConversionException(
				"Referenced image has invalid series instance UID");
		}
		List<RefFrame> refFrameList = new ArrayList<>();
		for (DicomObject dcm : dcmMap.values())
		{
			if (seriesUid.equals(dcm.getString(Tag.SeriesInstanceUID, VR.UI, null)))
			{
				refFrameList.add(new RefFrame(dcm));
			}
		}
		Collections.sort(refFrameList, new RefFrameComparator());
		for (int i=0; i<refFrameList.size(); i++)
		{
			RefFrame refFrame = refFrameList.get(i);
			refFrame.setIndex(i);
			refFrames.put(refFrame.getSopInstUid(), refFrame);
		}
		nFrames = refFrames.size();
	}

	private void checkCompatible(Segmentation seg) throws ConversionException
	{
		SegmentationImageModule segImageMod = seg.getSegmentationImageModule();
		if ((segImageMod.getSamplesPerPixel() != 1) ||
			 (segImageMod.getBitsStored() != 1) ||
			 !Constants.Binary.equals(segImageMod.getSegmentationType()))
		{
			throw new ConversionException(
				"Non-binary segmentations cannot be converted to NIFTI");
		}
	}

	private static double[] cross(double[] row, double[] col)
	{
		double[] product = new double[3];
		product[0] = row[1]*col[2]-row[2]*col[1];
		product[1] = row[2]*col[0]-row[0]*col[2];
		product[2] = row[0]*col[1]-row[1]*col[0];
		return product;
	}

	private double[] computeQuaternionBcd(double[] row, double[] col,
		double[] normalDcm)
	{
		int idx = findIndexOfMax(normalDcm);
		double sign = Math.signum(normalDcm[idx]);
		double[] normal = new double[3];
		for (int i=0; i<3; i++)
		{
			normal[i] = normalDcm[i]*sign;
		}
		// Calcs lifted from nifti_io.c
		double r11 = row[0];
		double r12 = col[0];
		double r13 = normal[0];
		double r21 = row[1];
		double r22 = col[1];
		double r23 = normal[1];
		double r31 = row[2];
		double r32 = col[2];
		double r33 = normal[2];
		double a, b, c, d;
		double diag = r11+r22+r33+1.0;
		if (diag > 0.5)
		{
			// Simplest case
			a = 0.5*Math.sqrt(diag) ;
			b = 0.25*(r32-r23)/a;
			c = 0.25*(r13-r31)/a;
			d = 0.25*(r21-r12)/a ;
		}
		else 
		{
			// Trickier case
			double xd = 1.0+r11-(r22+r33);  // 4*b*b
			double yd = 1.0+r22-(r11+r33);  // 4*c*c
			double zd = 1.0+r33-(r11+r22);  // 4*d*d
			if ( xd > 1.0 )
			{
				b = 0.5*Math.sqrt(xd);
				c = 0.25*(r12+r21)/b;
				d = 0.25*(r13+r31)/b;
				a = 0.25*(r32-r23)/b ;
			}
			else if (yd > 1.0)
			{
				c = 0.5*Math.sqrt(yd);
				b = 0.25*(r12+r21)/c;
				d = 0.25*(r23+r32)/c;
				a = 0.25*(r13-r31)/c;
			}
			else
			{
				d = 0.5*Math.sqrt(zd);
				b = 0.25*(r13+r31)/d;
				c = 0.25*(r23+r32)/d;
				a = 0.25*(r21-r12)/d;
			}
			if (a < 0.0)
			{
				b=-b;
				c=-c;
				d=-d;
			}
		}

		return new double[] {b,c,d};
	}

	private void computeTransforms(NiftiHeader header)
	{
		// Position according to DICOM
		RefFrame firstFrame = refFrames.get(refFrames.keySet().iterator().next());
		double[] pos = firstFrame.getImagePosition();
		double[] ori = firstFrame.getImageOrientation();
		double[] row = Arrays.copyOfRange(ori, 0, 3);
		double[] col = Arrays.copyOfRange(ori, 3, 6);
		double[] normal = cross(row, col);

		// DICOM has x,y dimensions inverted relative to NIfTI
		for (int i=0; i<2; i++)
		{
			row[i] = (row[i] != 0) ? -row[i] : 0;
			col[i] = (col[i] != 0) ? -col[i] : 0;
			pos[i] = (pos[i] != 0) ? -pos[i] : 0;
		}

		// Affine xform
		double[] pixDims = header.getPixelSpacing();
		double xSize = pixDims[1];
		double ySize = pixDims[2];
		double zSize = pixDims[3];
		double[] affineX = new double[4];
		double[] affineY = new double[4];
		double[] affineZ = new double[4];
		for (int i=0; i<3; i++)
		{
			affineX[i] = xSize*row[i];
			affineY[i] = ySize*col[i];
			affineZ[i] = zSize*normal[i];
		}
		affineX[3] = pos[0];
		affineY[3] = pos[1];
		affineZ[3] = pos[2];
		header.setSFormCode(Nifti.TransformScanner);
		double qFac = determinant(row, col, normal);
		if (qFac != pixDims[0])
		{
			pixDims[0] = qFac;
			header.setPixelSpacing(pixDims);
		}
		header.setAffineX(affineX);
		header.setAffineY(affineY);
		header.setAffineZ(affineZ);

		header.setQFormCode(Nifti.TransformAligned);
		header.setQuaternionXShift(pos[0]);
		header.setQuaternionYShift(pos[1]);
		header.setQuaternionZShift(pos[2]);
		double[] bcd = computeQuaternionBcd(row, col, normal);
		header.setQuaternionB(bcd[0]);
		header.setQuaternionC(bcd[1]);
		header.setQuaternionD(bcd[2]);
	}

	private Nifti convertImpl(Segmentation seg, Map<String, DicomObject> dcmMap)
		throws ConversionException
	{
		checkCompatible(seg);
		reset();
		Set<String> refInstUids = gatherReferencedSopInstanceUids(seg);
		buildFrameMapping(refInstUids, dcmMap);

		NiftiToolkit nifTk = NiftiToolkit.getToolkit();
		NiftiHeader header = nifTk.createNiftiHeader();
		populateHeader(seg, header);
		Nifti nifti = nifTk.createNifti(header);
		populateData(nifti, seg);

		return nifti;
	}

	private double determinant(double[] row, double[] col, double[] normal)
	{
		double r11 = row[0];
		double r12 = col[0];
		double r13 = normal[0];
		double r21 = row[1];
		double r22 = col[1];
		double r23 = normal[1];
		double r31 = row[2];
		double r32 = col[2];
		double r33 = normal[2];
		double determ = r11*r22*r33-r11*r32*r23-r21*r12*r33
       +r21*r32*r13+r31*r12*r23-r31*r22*r13;

		return (determ > 0.0) ? 1.0 : -1.0;
	}

	private int findIndexOfMax(double[] array)
	{
		if ((array == null) || (array.length == 0))
		{
			return -1;
		}
		if (array.length == 1)
		{
			return 0;
		}
		int idx = 0;
		double max = Math.abs(array[0]);
		for (int i=1; i<array.length; i++)
		{
			double test = Math.abs(array[i]);
			if (test > max)
			{
				idx = i;
				max = test;
			}
		}
		return idx;
	}

	private Set<String> gatherReferencedSopInstanceUids(
		Segmentation seg)
	{
		logger.debug("Gathering referenced UIDs");
		Set<String> refInstUids = new HashSet<>();

		GeneralReferenceModule genRef = seg.getGeneralReferenceModule();
		for (ReferencedInstance refInst : genRef.getReferencedImageList())
		{
			refInstUids.add(refInst.getReferencedSopInstanceUid());
		}
		for (SourceImage source : genRef.getSourceImageList())
		{
			refInstUids.add(source.getReferencedSopInstanceUid());
		}

		for (FunctionalGroupsFrame frame :
			seg.getMultiframeFunctionalGroupsModule().getPerFrameFunctionalGroups().getFrameList())
		{
			SegmentationFunctionalGroupsFrame segFrame =
				(SegmentationFunctionalGroupsFrame) frame;
			for (DerivationImage derivImage : segFrame.getDerivationImages())
			{
				for (SourceImage source : derivImage.getSourceImageList())
				{
					refInstUids.add(source.getReferencedSopInstanceUid());
				}
			}
		}

		return refInstUids;
	}

	private int getReferencedPlaneIndex(SegmentationFunctionalGroupsFrame segFrame,
		int frameNumber) throws ConversionException
	{
		// Should only be one referenced SOP inst
		List<DerivationImage> derivImages = segFrame.getDerivationImages();
		if (derivImages.isEmpty())
		{
			throw new ConversionException("Frame "+frameNumber+
				" does not contain derivation images");
		}
		List<SourceImage> sourceImages = derivImages.get(0).getSourceImageList();
		if (sourceImages.isEmpty())
		{
			throw new ConversionException("Frame "+frameNumber+
				" does not contain source images");
		}
		String refUid = sourceImages.get(0).getReferencedSopInstanceUid();
		return refFrames.get(refUid).getIndex();
	}

	private void populateData(Nifti nifti, Segmentation seg) throws ConversionException
	{
		logger.debug("Unpacking segmentation frames");
		int nElements = nifti.getDataLength();
		short[] data = new short[nElements];
		long[] dims = nifti.getHeader().getDimensions();
		int nfRowStride = (int) dims[1];
		int nfPlaneStride = (int) (dims[1]*dims[2]);

		ImagePixelModule imagePixel = seg.getImagePixelModule();
		int rowStride = nCols/8; // Account for bit packing
		int frameStride = nRows*rowStride;
		byte[] allFrames = imagePixel.getPixelData();
		List<FunctionalGroupsFrame> frames =
			seg.getMultiframeFunctionalGroupsModule().getPerFrameFunctionalGroups().getFrameList();
		for (int i=0; i<frames.size(); i++)
		{
			SegmentationFunctionalGroupsFrame segFrame =
				(SegmentationFunctionalGroupsFrame) frames.get(i);
			int segNumber = segFrame.getReferencedSegmentNumber();
			int frameOffset = i*frameStride;
			byte[] frameBytes = Arrays.copyOfRange(allFrames, frameOffset,
				frameOffset+frameStride);
			int nfPlaneIdx = getReferencedPlaneIndex(segFrame, i+1);
			int nfPlaneOffset = nfPlaneStride*nfPlaneIdx;
			for (int j=0; j<nRows; j++)
			{
				for (int k=0; k<rowStride; k++)
				{
					byte b = frameBytes[j*rowStride+k];
					byte mask = (byte) 1;
					for (int m=0; m<8; m++)
					{
						if ((b & mask) == mask)
						{
							int idx = nfPlaneOffset+j*nfRowStride+k*8+m;
							data[idx] = (short) segNumber;
						}
						mask <<= 1;
					}
				}
			}
		}

		nifti.setShortData(data);
	}

	private void populateHeader(Segmentation seg, NiftiHeader header)
	{
		logger.debug("Populating header for v1.1");
		header.setLength(NiftiHeader.Length_v1_1);
		header.setVoxelOffset(352);
		header.setMagic(NiftiHeader.Magic_v1_1);

		// Dimensions
		long[] dims = new long[8];
		dims[0] = 3;
		Arrays.fill(dims, 1, 8, 1);
		ImagePixelModule imagePixelMod = seg.getImagePixelModule();
		nRows = imagePixelMod.getRowCount();
		nCols = imagePixelMod.getColumnCount();
		dims[1] = nCols;
		dims[2] = nRows;
		dims[3] = nFrames;
		header.setDimensions(dims);

		// Pixel spacing
		double[] pixSpacing = new double[8];
		pixSpacing[0] = 1;
		MultiframeFunctionalGroupsModule mfFuncGroups =
			seg.getMultiframeFunctionalGroupsModule();
		SharedFunctionalGroups sharedGroups =
			mfFuncGroups.getSharedFunctionalGroups();
		PixelMeasures pixMeasures = sharedGroups.getPixelMeasures();
		double[] pixSize = pixMeasures.getPixelSpacing();
		pixSpacing[1] = pixSize[0];
		pixSpacing[2] = pixSize[1];
		pixSpacing[3] = pixMeasures.getSliceThickness();
		header.setPixelSpacing(pixSpacing);

		header.setDataType(Nifti.Uint16);
		header.setXyztUnits(Nifti.UnitsMillimeters);

		computeTransforms(header);

		String desc = seg.getSegmentationImageModule().getContentDescription();
		if (StringUtils.isNullOrEmpty(desc))
		{
			desc = seg.getGeneralSeriesModule().getSeriesDescription();
			desc = StringUtils.isNullOrEmpty(desc) ? "Segmentation" : desc;
		}
		header.setDescription(desc);
	}

	private void reset()
	{
		sourceMultiFrame = false;
		refFrames.clear();
	}

	private class RefFrame
	{
		private final DicomObject dcm;
		private int index = -1;
		private final double[] ori;
		private final double[] pos;
		private final double sliceLocation;
		private final String sopInstUid;

		public RefFrame(DicomObject dcm)
			throws ConversionException, IllegalArgumentException
		{
			this.dcm = dcm;
			sopInstUid = dcm.getString(Tag.SOPInstanceUID, VR.UI, null);
			if (StringUtils.isNullOrEmpty(sopInstUid))
			{
				throw new ConversionException(
					"Invalid referenced image: no SOP instance UID");
			}
			pos = dcm.getDoubles(Tag.ImagePositionPatient, new double[0]);
			ori = dcm.getDoubles(Tag.ImageOrientationPatient, new double[0]);
			sliceLocation = DicomUtils.sliceLocation(pos, ori);
		}

		/**
		 * @return the DICOM object
		 */
		public DicomObject getDicomObject()
		{
			return dcm;
		}

		/**
		 * @return the image position
		 */
		public double[] getImagePosition()
		{
			return Arrays.copyOf(pos, pos.length);
		}

		/**
		 * @return the image position
		 */
		public double[] getImageOrientation()
		{
			return Arrays.copyOf(ori, ori.length);
		}

		/**
		 * @return the index
		 */
		public int getIndex()
		{
			return index;
		}

		/**
		 * @return the sliceLocation
		 */
		public double getSliceLocation()
		{
			return sliceLocation;
		}

		/**
		 * @return the sopInstUid
		 */
		public String getSopInstUid()
		{
			return sopInstUid;
		}

		/**
		 * @param index the index to set
		 */
		public void setIndex(int index)
		{
			this.index = index;
		}
	}

	private class RefFrameComparator implements Comparator<RefFrame>
	{
		@Override
		public int compare(RefFrame a, RefFrame b)
		{
			double delta = a.getSliceLocation()-b.getSliceLocation();
			if (Math.abs(delta) < 0.05)
			{
				return 0;
			}
			return (delta < 0.0) ? -1 : 1;
		}
		
	}

}
