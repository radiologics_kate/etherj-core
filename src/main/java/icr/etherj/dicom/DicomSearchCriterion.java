/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

import icr.etherj.search.AbstractSearchCriterion;
import icr.etherj.search.SearchCriterion;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dcm4che2.data.Tag;

/**
 * <code>SearchCriterion</code> for DICOM types.
 * @author jamesd
 */
public class DicomSearchCriterion extends AbstractSearchCriterion
{
	/** Type for instance searches. */
	public static final int Instance = 1001;
	/** Type for series searches. */
	public static final int Series = 1002;
	/** Type for study searches. */
	public static final int Study = 1003;

	private static final Map<Integer,Set<Integer>> types = new HashMap<>();
	private int combinator;
	private final int comparator;
	private final int tag;
	private final String value;
	private int type = Unspecified;

	static
	{
		Set<Integer> instTags = new HashSet<>();
		instTags.add(Tag.Modality);
		instTags.add(Tag.SeriesInstanceUID);
		instTags.add(Tag.StudyInstanceUID);
		types.put(Instance, instTags);
		Set<Integer> seTags = new HashSet<>();
		seTags.add(Tag.Modality);
		seTags.add(Tag.SeriesInstanceUID);
		types.put(Series, seTags);
		Set<Integer> stTags = new HashSet<>();
		stTags.add(Tag.Modality);
		stTags.add(Tag.StudyInstanceUID);
		types.put(Study, stTags);
	}

	/**
	 * Constructs a new <code>DicomSearchCriterion</code> with tag, comparator and
	 * value using the default {@link SearchCriterion#And} combinator.
	 * @param tag the tag to compare
	 * @param comparator the comparator
	 * @param value the value to compare
	 */
	public DicomSearchCriterion(int tag, int comparator, String value)
	{
		this(tag, comparator, value, And);
	}

	/**
	 * Constructs a new <code>DicomSearchCriterion</code> with tag, comparator,
	 * value and combinator.
	 * @param tag the tag to compare
	 * @param comparator the comparator
	 * @param value the value to compare
	 * @param combinator how to combine sibling criteria
	 */
	public DicomSearchCriterion(int tag, int comparator, String value,
		int combinator)
	{
		this.tag = tag;
		this.value = value;
		this.comparator = comparator;
		this.combinator = combinator;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Combinator: "+createCombinatorSql());
		ps.println(pad+buildString(new StringBuilder(), this).toString());
	}

	@Override
	public int getCombinator()
	{
		return combinator;
	}

	@Override
	public int getComparator()
	{
		return comparator;
	}

	@Override
	public List<SearchCriterion> getCriteria()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getTag()
	{
		return tag;
	}

	@Override
	public String getTagName()
	{
		return DicomUtils.tagName(tag);
	}

	@Override
	public int getType()
	{
		return type;
	}

	@Override
	public String getTypeString()
	{
		String typeStr = "";
		switch (type)
		{
//			case Person:
//				typeStr = "Instance";
//				break;

			default:
		}
		return typeStr;
	}

	@Override
	public String getValue()
	{
		return value;
	}

	@Override
	public boolean hasCriteria()
	{
		return false;
	}

	@Override
	public void setCombinator(int combinator)
	{
		this.combinator = combinator;
	}

	@Override
	public void setType(int type)
	{
		if (type == Unspecified)
		{
			this.type = type;
			return;
		}
		Set<Integer> tags = types.get(type);
		if (tags == null)
		{
			throw new IllegalArgumentException("Invalid DICOM type: "+type);
		}
		if (!tags.contains(tag))
		{
			throw new IllegalArgumentException("DICOM type "+type+
				" not compatible with Tag "+DicomUtils.tagName(tag));
		}
		this.type = type;
	}

}
