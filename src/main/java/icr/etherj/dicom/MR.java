/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

/**
 * Constants for DICOM's MR modality.
 * @author jamesd
 */
public final class MR
{
	public static final String BMATRIX = "BMATRIX";
	public static final String DIRECTIONAL = "DIRECTIONAL";
	public static final String ISOTROPIC = "ISOTROPIC";
	public static final String NONE = "NONE";

	public static final double[] BDIR_M = {1.0,0.0,0.0};
	public static final double[] BDIR_P = {0.0,1.0,0.0};
	public static final double[] BDIR_S = {0.0,0.0,1.0};
	public static final double[] BDIR_NONE = {0.0,0.0,0.0};
	public static final double[] BMATRIX_EMPTY = {0.0,0.0,0.0,0.0,0.0,0.0};

	public static final short GE_M = 3;
	public static final short GE_P = 4;
	public static final short GE_S = 5;
	public static final short GE_TRACE = 15;
	public static final short GE_TETRA_DIR1 = 43;
	public static final short GE_TETRA_DIR2 = 44;
	public static final short GE_TETRA_DIR3 = 45;
	public static final short GE_TETRA_DIR4 = 46;

	private MR()
	{}
}
