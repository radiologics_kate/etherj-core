/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

/**
 * An <code>ImageSopInstance</code> for the MR Image Storage SOP class.
 * @author jamesd
 */
public interface MrSopInstance extends ImageSopInstance
{
	/**
	 * Returns the full matrix of diffusion sensitisation gradients.
	 * @return the b-matrix
	 */
	public double[] getDiffusionBMatrix();

	/**
	 * Returns the diffusion b-value.
	 * @return the b-value
	 */
	public double getDiffusionBValue();

	/**
	 * Returns the vector of the diffusion gradient direction.
	 * @return the direction
	 */
	public double[] getDiffusionDirection();

	/**
	 * Returns the diffusion directionality, defined terms: {#MR.NONE},
	 * {#MR.ISOTROPIC}, {#MR.DIRECTIONAL}, {#MR.BMATRIX}.
	 * @return the directionality
	 */
	public String getDiffusionDirectionality();

	/**
	 * Returns the echo time.
	 * @return the echo time
	 */
	public double getEchoTime();

	/**
	 * Returns the flip angle.
	 * @return the flip angle
	 */
	public double getFlipAngle();

	/**
	 * Returns the inversion time.
	 * @return the inversion time
	 */
	public double getInversionTime();

	/**
	 * Returns the repetition time.
	 * @return the repetition time
	 */
	public double getRepetitionTime();

	/**
	 * Sets the matrix of diffusion sensitisation gradients.
	 * @param matrix the b-matrix
	 */
	public void setDiffusionBMatrix(double[] matrix);

	/**
	 * Sets the diffusion b-value.
	 * @param bValue the b-value
	 */
	public void setDiffusionBValue(double bValue);

	/**
	 * Sets the direction vector of the diffusion gradients.
	 * @param dir the vector
	 */
	public void setDiffusionDirection(double[] dir);

	/**
	 * Sets the diffusion directionality, defined terms: {#MR.NONE},
	 * {#MR.ISOTROPIC}, {#MR.DIRECTIONAL}, {#MR.BMATRIX}.
	 * @param dir the directionality
	 */
	public void setDiffusionDirectionality(String dir);

	/**
	 * Sets the echo time.
	 * @param te the echo time
	 */
	public void setEchoTime(double te);

	/**
	 * Sets the flip angle.
	 * @param angle the flip angle
	 */
	public void setFlipAngle(double angle);

	/**
	 * Sets the inversion time.
	 * @param ti the inversion time
	 */
	public void setInversionTime(double ti);

	/**
	 * Sets the repetition time.
	 * @param tr the repetition time
	 */
	public void setRepetitionTime(double tr);
}
