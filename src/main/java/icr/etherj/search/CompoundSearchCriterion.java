/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.search;

import com.google.common.collect.ImmutableList;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A search criterion that is a collection of child criteria.
 * @author jamesd
 */
public class CompoundSearchCriterion extends AbstractSearchCriterion
{
	private int combinator;
	private final List<SearchCriterion> criteria = new ArrayList<>();

	/**
	 * Constructs a new <code>CompoundSearchCriterion</code> from two criteria
	 * and the default {@link SearchCriterion#And} combinator.
	 * @param a the first search criterion
	 * @param b the second search criterion
	 * @throws java.lang.IllegalArgumentException if either search criterion is
	 * null
	 */
	public CompoundSearchCriterion(SearchCriterion a, SearchCriterion b)
	{
		this(a, b, SearchCriterion.And);
	}

	/**
	 * Constructs a new <code>CompoundSearchCriterion</code> from two criteria
	 * and a combinator.
	 * @param a the first search criterion
	 * @param b the second search criterion
	 * @param combinator the combinator
	 * @throws java.lang.IllegalArgumentException if either search criterion is
	 * null
	 */
	public CompoundSearchCriterion(SearchCriterion a, SearchCriterion b,
		int combinator)
	{
		if ((a == null) || (b == null))
		{
			throw new IllegalArgumentException("Null criteria not permitted");
		}
		criteria.add(a);
		criteria.add(b);
		this.combinator = combinator;
	}

	/**
	 * Constructs a new <code>CompoundSearchCriterion</code> from a list of
	 * criteria and the default {@link SearchCriterion#And} combinator.
	 * @param criteria the list of criteria
	 * @throws java.lang.IllegalArgumentException if the list contains fewer than
	 * two criteria
	 */
	public CompoundSearchCriterion(List<SearchCriterion> criteria)
	{
		this(criteria, SearchCriterion.And);
	}

	/**
	 * Constructs a new <code>CompoundSearchCriterion</code> from a list of
	 * criteria and a combinator.
	 * @param criteria the list of criteria
	 * @param combinator the combinator
	 * @throws java.lang.IllegalArgumentException if the list contains fewer than
	 * two criteria
	 */
	public CompoundSearchCriterion(List<SearchCriterion> criteria, int combinator)
	{
		if (criteria.size() < 2)
		{
			throw new IllegalArgumentException(
				"Criteria list must have at least two elements");
		}
		this.criteria.addAll(criteria);
		this.combinator = combinator;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Combinator: "+createCombinatorSql());
		StringBuilder sb = new StringBuilder(pad);
		sb = buildString(sb, this);
		ps.println(sb.toString());
	}

	@Override
	public int getCombinator()
	{
		return combinator;
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public int getComparator()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public int getTag()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public String getTagName()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public int getType()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public String getTypeString()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @return nothing
	 */
	@Override
	public String getValue()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public List<SearchCriterion> getCriteria()
	{
		return ImmutableList.copyOf(criteria);
	}

	@Override
	public boolean hasCriteria()
	{
		return criteria.size() > 0;
	}

	@Override
	public void setCombinator(int combinator)
	{
		this.combinator = combinator;
	}

	/**
	 * Throws an <code>UnsupportedOperationException</code>.
	 * @param type this is ignored
	 */
	@Override
	public void setType(int type)
	{
		throw new UnsupportedOperationException();
	}

}
