/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.search;

import icr.etherj.Displayable;
import java.util.List;

/**
 * Interface allowing specification of search parameters for simple databases. A
 * <code>SearchCriterion</code> has either a) a tag, a value, a type, a
 * comparator and a combinator or b) a collection of child criteria.
 * <p>The tag specifies the field to compare and the set of supported tags should
 * be defined by the implementation. The value is what to compare the field to.
 * The type specifies the table to search and the set of supported types should
 * be defined by the implementation. The comparator specifies the comparison
 * e.g. &gt;, ==, &lt;, LIKE. The combinator specifies how sibling criteria are
 * combined e.g. AND, OR.</p>
 * @author jamesd
 */
public interface SearchCriterion extends Displayable
{

	/**
	 * Null comparator.
	 */
	public static final int Null = 0;

	/**
	 * Comparator for &lt;.
	 */
	public static final int LessThan = 1;

	/**
	 * Comparator for &lt;=.
	 */
	public static final int LessThanOrEqual = 2;

	/**
	 * Comparator for ==.
	 */
	public static final int Equal = 3;

	/**
	 * Comparator for &gt;=.
	 */
	public static final int GreaterThanOrEqual = 4;

	/**
	 * Comparator for &gt;.
	 */
	public static final int GreaterThan = 5;

	/**
	 * Comparator for !=.
	 */
	public static final int NotEqual = 6;

	/**
	 * Comparator for SQL LIKE.
	 */
	public static final int Like = 7;

	/**
	 * Combinator for AND.
	 */
	public static final int And = 100;

	/**
	 * Combinator for OR.
	 */
	public static final int Or = 101;

	/**
	 * No search table specified.
	 */
	public static final int Unspecified = 1000;

	/**
	 * Returns the SQL for the combinator.
	 * @return the SQL for the combinator
	 */
	public String createCombinatorSql();

	/**
	 * Returns the SQL for the comparator.
	 * @return the SQL for the comparator
	 */
	public String createComparatorSql();

	/**
	 * Returns the combinator. 
	 * @return the combinator
	 */
	public int getCombinator();

	/**
	 * Returns the comparator.
	 * @return the comparator
	 */
	public int getComparator();

	/**
	 * Returns the list of child criteria.
	 * @return the list
	 */
	public List<SearchCriterion> getCriteria();

	/**
	 * Returns the tag.
	 * @return the tag
	 */
	public int getTag();

	/**
	 * Returns the human readable name corresponding to the integer tag.
	 * @return the name of the tag
	 */
	public String getTagName();

	/**
	 * Returns the type.
	 * @return the type
	 */
	public int getType();

	/**
	 * Returns the human readable name corresponding to the integer type.
	 * @return the name of the type
	 */
	public String getTypeString();

	/**
	 * Returns the value to compare the field to.
	 * @return the value
	 */
	public String getValue();

	/**
	 * Returns whether there are child criteria.
	 * @return the presence of child criteria
	 */
	public boolean hasCriteria();

	/**
	 * Sets the combinator.
	 * @param combinator the combinator
	 */
	public void setCombinator(int combinator);

	/**
	 * Sets the type.
	 * @param type the type
	 */
	public void setType(int type);
}
