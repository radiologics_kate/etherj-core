/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui;

import icr.etherj.aim.AimFileInstance;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.Markup;
import icr.etherj.aim.ui.impl.DefaultAimUiFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory and factory locator class for <code>etherj.aim.ui</code> package.
 * @author jamesd
 */
public class AimUiToolkit
{
	/** Key for the default toolkit. */
	public static final String Default = "default";
	/** Key for error icon overlay. */
	public static final String ErrorOverlay = "ErrorOverlay";
	/** Key for warning icon overlay. */
	public static final String WarningOverlay = "WarningOverlay";

	private static final Logger logger = LoggerFactory.getLogger(
		AimUiToolkit.class);
	private static final Map<String,AimUiToolkit> toolkitMap = new HashMap<>();

	private final AimUiToolkit.AimUiFactory factory =
		new DefaultAimUiFactory();

	static
	{
		toolkitMap.put(Default, new AimUiToolkit());
	}

	/**
	 * Returns the default toolkit.
	 * @return the toolkit
	 */
	public static AimUiToolkit getToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 *	Returns the toolkit associated with the key.
	 * @param key the key
	 * @return the toolkit
	 */
	public static AimUiToolkit getToolkit(String key)
	{
		return toolkitMap.get(key);
	}

	/**
	 * Associate the supplied toolkit with the key.
	 * @param key the key
	 * @param toolkit the toolkit
	 * @return the toolkit previously associated with the key or null.
	 */
	public static AimUiToolkit setToolkit(String key, AimUiToolkit toolkit)
	{
		AimUiToolkit tk = toolkitMap.put(key, toolkit);
		logger.info(toolkit.getClass().getName()+" set with key '"+key+"'");
		return tk;
	}

	/**
	 *	Creates a new node from the list of <code>AimFileInstance</code>s.
	 * @param afiList the list
	 * @return the node
	 */
	public AimTreeNode createNode(List<AimFileInstance> afiList)
	{
		return factory.createNode(afiList);
	}

	/**
	 *	Creates a new node from the <code>AimFileInstance</code>.
	 * @param afi file instance
	 * @return the node
	 */
	public AimTreeNode createNode(AimFileInstance afi)
	{
		return factory.createNode(afi);
	}

	/**
	 * Creates a new node from the <code>ImageAnnotation</code>.
	 * @param ia the annotation
	 * @return the node
	 */
	public AimTreeNode createNode(ImageAnnotation ia)
	{
		return factory.createNode(ia);
	}

	/**
	 * Creates a new node from the <code>Markup</code>.
	 * @param markup the markup
	 * @return the node
	 */
	public AimTreeNode createNode(Markup markup)
	{
		return factory.createNode(markup);
	}

	/**
	 * Creates a new cell renderer.
	 * @return the renderer
	 */
	public AimTreeCellRenderer createTreeCellRenderer()
	{
		return factory.createTreeCellRenderer();
	}

	/**
	 * Returns the icon overlay associated with the key.
	 * @param key the key
	 * @return the overlay
	 */
	public Icon getIconOverlay(String key)
	{
		return factory.getIconOverlay(key);
	}

	/*
	 *	Private constructor to prevent direct instantiation
	 */
	private AimUiToolkit()
	{}

	public interface AimUiFactory
	{
		public AimTreeNode createNode(List<AimFileInstance> afiList);

		public AimTreeNode createNode(AimFileInstance afi);

		public AimTreeNode createNode(ImageAnnotation ia);

		public AimTreeNode createNode(Markup markup);

		public AimTreeCellRenderer createTreeCellRenderer();

		public Icon getIconOverlay(String key);
	}
}
