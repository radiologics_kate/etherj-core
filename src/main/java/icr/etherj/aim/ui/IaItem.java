/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui;

import icr.etherj.Displayable;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author jamesd
 */
public interface IaItem extends Displayable
{

	/**
	 * @return the annotation
	 */
	ImageAnnotation getAnnotation();

	int getLesionNumber();

	/**
	 * @return the collection containing the annotation
	 */
	ImageAnnotationCollection getParentCollection();

	String getPersonId();

	String getPersonName();

	/**
	 * @return the roiNumber
	 */
	int getRoiNumber();

	/**
	 * @return the scan
	 */
	String getScan();
	
	public static class Comparator implements java.util.Comparator<IaItem>
	{
		@Override
		public int compare(IaItem a, IaItem b)
		{
			int result = a.getPersonName().compareTo(b.getPersonName());
			if (result != 0)
			{
				return result;
			}
			result = a.getPersonId().compareTo(b.getPersonId());
			if (result != 0)
			{
				return result;
			}
			result = (int) Math.signum(a.getLesionNumber()-b.getLesionNumber());
			if (result != 0)
			{
				return result;
			}
			result = a.getScan().compareTo(b.getScan());
			if (result != 0)
			{
				return result;
			}
			return (int) Math.signum(a.getRoiNumber()-b.getRoiNumber());
		}
	}

	public static class CellRenderer extends DefaultTableCellRenderer
	{
		private final Color odd = new Color(233, 239, 248);

		public CellRenderer()
		{
			super();
		}

		@Override
		public Component getTableCellRendererComponent(JTable table,
			Object object, boolean isSelected, boolean hasFocus, int row, int col)
		{
			Color colour;
			if (isSelected)
			{
				colour = table.getSelectionBackground();
			}
			else if ((row % 2) == 1)
			{
				colour = odd;
			}
			else
			{
				colour = table.getBackground();
			}
			setBackground(colour);
			setText(object.toString());
			setHorizontalAlignment((col > 0) ? SwingConstants.RIGHT
				: SwingConstants.LEFT);
			return this;
 		}
		
	}

	public static class HeaderRenderer implements TableCellRenderer
	{
		DefaultTableCellRenderer renderer;

		public HeaderRenderer(JTable table)
		{
			this.renderer = (DefaultTableCellRenderer) table.getTableHeader()
				.getDefaultRenderer();
		}

		@Override
		public Component getTableCellRendererComponent(JTable jtable,
			Object value, boolean isSelected, boolean hasFocus, int row, int col)
		{
			renderer.setHorizontalAlignment((col > 0) ? SwingConstants.RIGHT
				: SwingConstants.LEFT);
			return renderer.getTableCellRendererComponent(jtable, value, hasFocus,
				hasFocus, row, col);
		}
		
	}
	
}
