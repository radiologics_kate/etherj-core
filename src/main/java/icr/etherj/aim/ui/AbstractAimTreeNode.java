/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui;

import icr.etherj.aim.Entity;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.tree.TreeNode;

/**
 * Abstract base class providing a default implementation of
 * <code>AimTreeNode</code>.
 * @author jamesd
 */
public abstract class AbstractAimTreeNode implements AimTreeNode
{
	private boolean allowsChildren = true;
	private Icon baseIcon;
	private Icon baseOpenIcon;
	private final List<AimTreeNode> childList = new ArrayList<>();
	private final SortedMap<String,AimTreeNode> orderedChildren = new TreeMap<>();
	private final Map<String,AimTreeNode> children = new HashMap<>();
	private String displayName = null;
	private Icon icon;
	private Icon iconOverlay = null;
	private boolean isInitialised = false;
	private boolean isLeaf = false;
	private final Lock lock = new ReentrantLock();
	private Icon openIcon;
	private AimTreeNode parent;

	@Override
	public Enumeration children()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean getAllowsChildren()
	{
		return allowsChildren;
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		if (!allowsChildren)
		{
			return null;
		}
		TreeNode node = null;
		lock.lock();
		try
		{
			node = childList.get(childIndex);
		}
		finally
		{
			lock.unlock();
		}
		return node;
	}

	@Override
	public int getChildCount()
	{
		if (!allowsChildren)
		{
			return 0;
		}
		int nNodes = 0;
		lock.lock();
		try
		{
			nNodes = orderedChildren.size();
		}
		finally
		{
			lock.unlock();
		}
		return nNodes;
	}

	@Override
	public void setAllowsChildren(boolean allowsChildren)
	{
		lock.lock();
		try
		{
			this.allowsChildren = allowsChildren;
			if (!allowsChildren)
			{
				orderedChildren.clear();
				childList.clear();
			}
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public String getDisplayName()
	{
		return displayName;
	}

	@Override
	public Icon getIcon()
	{
		return icon;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return childList.indexOf(node);
	}

	@Override
	public Icon getOpenIcon()
	{
		return openIcon;
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public boolean isInitialised()
	{
		return isInitialised;
	}

	@Override
	public boolean isLeaf()
	{
		return isLeaf;
	}

	@Override
	public void setDisplayName(String name)
	{
		displayName = name;
	}

	@Override
	public void setIcon(Icon icon)
	{
		baseIcon = icon;
		this.icon = addOverlay(baseIcon);
	}

	@Override
	public void setIconOverlay(Icon iconOverlay)
	{
		this.iconOverlay = iconOverlay;
		icon = addOverlay(baseIcon);
		openIcon = addOverlay(baseOpenIcon);
	}

	@Override
	public void setOpenIcon(Icon icon)
	{
		baseOpenIcon = icon;
		openIcon = addOverlay(baseOpenIcon);
	}

	@Override
	public void setParent(AimTreeNode parent)
	{
		this.parent = parent;
	}

	/**
	 * Constructs a new instance which allows children, called by sub-classes.
	 */
	protected AbstractAimTreeNode()
	{
		this(true);
	}

	/**
	 * Constructs a new instance which optionally allows children, called by
	 * sub-classes.
	 * @param allowsChildren whether the node allows children
	 */
	protected AbstractAimTreeNode(boolean allowsChildren)
	{
		icon = new ImageIcon(getClass().getResource(
			"/icr/etherj/ui/resources/GreenDiamond16.png"));
		openIcon = new ImageIcon(getClass().getResource(
			"/icr/etherj/ui/resources/GreenDiamond16.png"));
		isInitialised = !allowsChildren;
		isLeaf = !allowsChildren;
	}

	/**
	 * Adds a child node.
	 * @param node the child
	 * @return true if child successfully added
	 */
	protected boolean addChild(AimTreeNode node)
	{
		AimTreeNode inserted = null;
		lock.lock();
		try
		{
			inserted = orderedChildren.put(node.getKey(), node);
			children.put(node.getAimEntity().getUid(), node);
			node.setParent(this);
			childList.clear();
			childList.addAll(orderedChildren.values());
			isInitialised = true;
		}
		finally
		{
			lock.unlock();
		}
		return (inserted != null);
	}

	/**
	 *	Returns the child associated with the entity or null.
	 * @param entity the entity
	 * @return the child or null
	 */
	protected AimTreeNode getChild(Entity entity)
	{
		return children.get(entity.getUid());
	}

	/**
	 * Removes the child node.
	 * @param node the child
	 * @return true if child successfully removed
	 */
	protected boolean removeChild(AimTreeNode node)
	{
		AimTreeNode removed = null;
		lock.lock();
		try
		{
			removed = orderedChildren.remove(node.getKey());
			children.remove(node.getAimEntity().getUid());
			if (removed != null)
			{
				removed.setParent(null);
			}
			childList.clear();
			childList.addAll(orderedChildren.values());
		}
		finally
		{
			lock.unlock();
		}
		return (removed != null);
	}

	/**
	 * Returns true if the node is a leaf.
	 * @param isLeaf true if a leaf
	 */
	protected void setIsLeaf(boolean isLeaf)
	{
		this.isLeaf = isLeaf;
		if (isLeaf)
		{
			isInitialised = true;
		}
	}

	/**
	 * Sets the initialised flag.
	 * @param isInitialised the flag
	 */
	protected void setInitialised(boolean isInitialised)
	{
		this.isInitialised = isInitialised;
	}

	private Icon addOverlay(Icon icon)
	{
		if (iconOverlay == null)
		{
			return icon;
		}
		BufferedImage image = new BufferedImage(icon.getIconWidth(),
			icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();
		g2d.drawImage(getIconImage(icon), 0, 0, null);
		g2d.drawImage(getIconImage(iconOverlay), 0, 0, null);
		g2d.dispose();
		return new ImageIcon(image);
	}

	private Image getIconImage(Icon icon)
	{
		if (icon instanceof ImageIcon)
		{
			return ((ImageIcon)icon).getImage();
		} 
		else
		{
			GraphicsEnvironment ge = 
			  GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gd.getDefaultConfiguration();
			BufferedImage image = gc.createCompatibleImage(icon.getIconWidth(),
				icon.getIconHeight());
			Graphics2D g2d = image.createGraphics();
			icon.paintIcon(new JPanel(), g2d, 0, 0);
			g2d.dispose();
			return image;
		}
	}

}
