/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui.impl;

import icr.etherj.aim.AimFileInstance;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.Markup;
import icr.etherj.aim.TwoDimensionGeometricShape;
import icr.etherj.aim.ui.AimTreeCellRenderer;
import icr.etherj.aim.ui.AimTreeNode;
import icr.etherj.aim.ui.AimUiToolkit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author jamesd
 */
public class DefaultAimUiFactory implements AimUiToolkit.AimUiFactory
{
	private final static Icon defaultIcon;
	private final static Map<String,Icon> icons = new HashMap<>();
	private final static Map<String,Icon> iconOverlays = new HashMap<>();
	private final static String IAC = "IAC";
	private final static String IA = "IA";
	private final static String Markup = "Markup";
	private final static String TwoDGeometricShape = "TwoDGeometricShape";

	static
	{
		// Icons
		defaultIcon = 
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Question16.png"));
		icons.put(IAC,
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/CrosshairGroup16.png")));
		icons.put(IA,
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/Crosshair16.png")));
		icons.put(TwoDGeometricShape,
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/BlueRoi16.png")));

		// Icon overlay
		iconOverlays.put(AimUiToolkit.ErrorOverlay,
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/ErrorOverlay16.png")));
		iconOverlays.put(AimUiToolkit.WarningOverlay,
			new ImageIcon(DefaultAimUiFactory.class.getResource(
				"/icr/etherj/ui/resources/WarningOverlay16.png")));
	}

	@Override
	public AimTreeNode createNode(List<AimFileInstance> afiList)
	{
		AimTreeNode node = new AfiListNode(afiList);
		return node;
	}

	@Override
	public AimTreeNode createNode(AimFileInstance afi)
	{
		AimTreeNode node = new AfiNode(afi);
		Icon icon = getIcon(IAC);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		return node;
	}

	@Override
	public AimTreeNode createNode(ImageAnnotation ia)
	{
		AimTreeNode node = new IaNode(ia);
		Icon icon = getIcon(IA);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		return node;
	}

	@Override
	public AimTreeNode createNode(Markup markup)
	{
		AimTreeNode node;
		if (markup instanceof TwoDimensionGeometricShape)
		{
			TwoDimensionGeometricShape shape2D =
				(TwoDimensionGeometricShape) markup;
			node = new TwoDGeometricShapeNode(shape2D);
			Icon icon = getIcon(TwoDGeometricShape);
			node.setIcon(icon);
			node.setOpenIcon(icon);
			return node;
		}
		node = new MarkupNode(markup);
		Icon icon = getIcon(Markup);
		node.setIcon(icon);
		node.setOpenIcon(icon);
		return node;
	}

	@Override
	public AimTreeCellRenderer createTreeCellRenderer()
	{
		AimTreeCellRenderer atcr = new DefaultAimTreeCellRenderer();
		return atcr;
	}

	@Override
	public Icon getIconOverlay(String key)
	{
		return iconOverlays.getOrDefault(key, null);
	}

	private Icon getIcon(String key)
	{
		return icons.getOrDefault(key, defaultIcon);
	}

}
