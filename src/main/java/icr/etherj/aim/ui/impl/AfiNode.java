/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui.impl;

import icr.etherj.aim.AimFileInstance;
import icr.etherj.aim.AimUnique;
import icr.etherj.aim.AimUtils;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.ui.AbstractAimTreeNode;
import icr.etherj.aim.ui.AimTreeNode;
import icr.etherj.aim.ui.AimUiToolkit;
import java.util.List;

/**
 *
 * @author jamesd
 */
final class AfiNode extends AbstractAimTreeNode
{
	private final AimFileInstance afi;

	public AfiNode(AimFileInstance afi)
	{
		super(true);
		this.afi = afi;
		ImageAnnotationCollection iac = afi.getImageAnnotationCollection();
		String dateTime = iac.getDateTime();
		String desc = iac.getDescription();
		if ((desc == null) || desc.isEmpty())
		{
			setDisplayName("AIM_"+AimUtils.getDate(dateTime)+
				"_"+AimUtils.getTime(dateTime));
		}
		else
		{
			setDisplayName(desc+" ("+AimUtils.getDate(dateTime)+
				" "+AimUtils.getTime(dateTime)+")");
		}
		List<ImageAnnotation> annotations = iac.getAnnotationList();
		for (ImageAnnotation annotation : annotations)
		{
			AimTreeNode node = AimUiToolkit.getToolkit().createNode(annotation);
			addChild(node);	
		}
	}

	@Override
	public AimUnique getAimEntity()
	{
		return afi.getImageAnnotationCollection();
	}

	@Override
	public String getKey()
	{
		ImageAnnotationCollection iac = afi.getImageAnnotationCollection();
		return "00_"+iac.getDateTime()+"_"+iac.getDescription();
	}

}
