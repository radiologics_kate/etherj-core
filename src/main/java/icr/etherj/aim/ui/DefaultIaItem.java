/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui;

import icr.etherj.AbstractDisplayable;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultIaItem extends AbstractDisplayable implements IaItem
{
	private final ImageAnnotation ia;
	private final ImageAnnotationCollection iac;
	private int lesionNumber;
	private int roiNumber;
	private String scan;

	public DefaultIaItem(ImageAnnotation ia, ImageAnnotationCollection iac)
	{
		this.ia = ia;
		this.iac = iac;
		String annoName = ia.getName();
		String[] tokens = annoName.split("-");
		if (!parseTokens(tokens))
		{
			lesionNumber = -1;
			scan = "ERROR: "+annoName;
			roiNumber = -1;
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		iac.display(ps, indent+"  ");
		ia.display(ps, indent+"  ");
	}

	/**
	 * @return the annotation
	 */
	@Override
	public ImageAnnotation getAnnotation()
	{
		return ia;
	}

	/**
	 * @return the collection containing the annotation
	 */
	@Override
	public ImageAnnotationCollection getParentCollection()
	{
		return iac;
	}

	@Override
	public String getPersonName()
	{
		return iac.getPerson().getName();
	}

	@Override
	public String getPersonId()
	{
		return iac.getPerson().getId();
	}

	@Override
	public int getLesionNumber()
	{
		return lesionNumber;
	}

	/**
	 * @return the roiNumber
	 */
	@Override
	public int getRoiNumber()
	{
		return roiNumber;
	}

	/**
	 * @return the scan
	 */
	@Override
	public String getScan()
	{
		return scan;
	}

	private boolean parseTokens(String[] tokens)
	{
		if (tokens.length < 3)
		{
			return false;
		}
		if (tokens[1].endsWith("a") || tokens[1].endsWith("A"))
		{
			scan = "A";
		}
		else if (tokens[1].endsWith("b") || tokens[1].endsWith("B"))
		{
			scan = "B";
		}
		else
		{
			return false;
		}
		try
		{
			lesionNumber = Integer.parseInt(
				tokens[1].substring(0, tokens[1].length()-1));
			int endIdx = 0;
			while (Character.isDigit(tokens[2].charAt(endIdx)))
			{
				endIdx++;
			}
			roiNumber = Integer.parseInt(tokens[2].substring(0, endIdx));
		}
		catch (NumberFormatException ex)
		{
			return false;
		}
		return true;
	}

}
