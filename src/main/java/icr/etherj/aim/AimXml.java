/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

/**
 *
 * @author jamesd
 */
public class AimXml
{
	public static final String AIM_V4_0 = "AIMv4_0";
	public static final String ATTR_AIM_VERSION = "aimVersion";
	public static final String ATTR_CODE = "code";
	public static final String ATTR_CODE_SYSTEM = "codeSystem";
	public static final String ATTR_CODE_SYSTEM_NAME = "codeSystemName";
	public static final String ATTR_CODE_SYSTEM_VERSION = "codeSystemVersion";
	public static final String ATTR_ROOT = "root";
	public static final String ATTR_TYPE = "type";
	public static final String ATTR_VALUE = "value";
	public static final String ATTR_XMLNS = "xmlns";
	public static final String ATTR_XMLNS_RDF = "xmlns:rdf";
	public static final String ATTR_XMLNS_XSI = "xmlns:xsi";
	public static final String ATTR_XSI_SCHEMA_LOCATION = "xsi:schemaLocation";
	public static final String ATTR_XSI_TYPE = "xsi:type";
	public static final String DICOM_IMAGE_REFERENCE_ENTITY =
		"DicomImageReferenceEntity";
	public static final String NODE_ALGORITHM = "algorithm";
	public static final String NODE_ANNOTATOR_CONFIDENCE = "annotatorConfidence";
	public static final String NODE_ANNOTATIONS = "imageAnnotations";
	public static final String NODE_ANNOTATION_COLLECTION =
		"ImageAnnotationCollection";
	public static final String NODE_ANNOTATION_ROLE_COLLECTION =
		"annotationRoleCollection";
	public static final String NODE_CALCULATION_COLLECTION =
		"calculationEntityCollection";
	public static final String NODE_CALCULATION_DATA_COLLECTION =
		"calculationDataCollection";
	public static final String NODE_CALCULATION_RESULT_COLLECTION =
		"calculationResultCollection";
	public static final String NODE_COMMENT = "comment";
	public static final String NODE_COORDINATE_COLLECTION =
		"coordinateCollection";
	public static final String NODE_COORDINATE_INDEX = "coordinateIndex";
	public static final String NODE_DATATYPE = "dataType";
	public static final String NODE_DATE_TIME = "dateTime";
	public static final String NODE_DESCRIPTION = "description";
	public static final String NODE_DIMENSION_COLLECTION = "dimensionCollection";
	public static final String NODE_DIMENSION_INDEX = "dimensionIndex";
	public static final String NODE_EQUIPMENT = "equipment";
	public static final String NODE_EQUIPMENT_MANUFACTURER_NAME =
		"manufacturerName";
	public static final String NODE_EQUIPMENT_MANUFACTURER_MODEL_NAME =
		"manufacturerModelName";
	public static final String NODE_EQUIPMENT_DEVICE_SERIAL_NUMBER =
		"deviceSerialNumber";
	public static final String NODE_EQUIPMENT_SOFTWARE_VERSION =
		"softwareVersion";
	public static final String NODE_IMAGE = "Image";
	public static final String NODE_IMAGE_ANNOTATION = "ImageAnnotation";
	public static final String NODE_IMAGE_COLLECTION = "imageCollection";
	public static final String NODE_IMAGE_EVIDENCE = "imageEvidence";
	public static final String NODE_IMAGE_REFERENCE_UID = "imageReferenceUid";
	public static final String NODE_IMAGE_SERIES = "imageSeries";
	public static final String NODE_IMAGE_STUDY = "imageStudy";
	public static final String NODE_IMAGE_REFERENCE = "ImageReferenceEntity";
	public static final String NODE_IMAGE_REFERENCE_COLLECTION =
		"imageReferenceEntityCollection";
	public static final String NODE_IMAGING_OBSERVATION_COLLECTION =
		"imagingObservationEntityCollection";
	public static final String NODE_IMAGING_PHYSICAL_COLLECTION =
		"imagingPhysicalEntityCollection";
	public static final String NODE_INCLUDE_FLAG = "includeFlag";
	public static final String NODE_INDEX = "index";
	public static final String NODE_INFERENCE_COLLECTION =
		"inferenceEntityCollection";
	public static final String NODE_INSTANCE_UID = "instanceUid";
	public static final String NODE_IS_PRESENT = "isPresent";
	public static final String NODE_LABEL = "label";
	public static final String NODE_LINE_COLOUR = "lineColour";
	public static final String NODE_LINE_OPACITY = "lineOpacity";
	public static final String NODE_LINE_STYLE = "lineStyle";
	public static final String NODE_LINE_THICKNESS = "lineThickness";
	public static final String NODE_MARKUP = "MarkupEntity";
	public static final String NODE_MARKUP_COLLECTION = "markupEntityCollection";
	public static final String NODE_MODALITY = "modality";
	public static final String NODE_NAME = "name";
	public static final String NODE_PARAMETER_COLLECTION =
		"parameterCollection";
	public static final String NODE_PERSON = "person";
	public static final String NODE_PERSON_BIRTHDATE = "birthDate";
	public static final String NODE_PERSON_ETHNIC_GROUP = "ethnicGroup";
	public static final String NODE_PERSON_ID = "id";
	public static final String NODE_PERSON_NAME = "name";
	public static final String NODE_PERSON_SEX = "sex";
	public static final String NODE_POSITION = "position";
	public static final String NODE_QUESTION_INDEX = "questionIndex";
	public static final String NODE_QUESTION_TYPECODE = "questionTypeCode";
	public static final String NODE_REFERENCED_FRAME_NUMBER =
		"referencedFrameNumber";
	public static final String NODE_ROLECODE = "roleCode";
	public static final String NODE_ROLE_SEQUENCE_NUMBER = "roleSequenceNumber";
	public static final String NODE_SHAPE_ID = "shapeIdentifier";
	public static final String NODE_SIZE = "size";
	public static final String NODE_SOP_CLASS_UID = "sopClassUid";
	public static final String NODE_SOP_INSTANCE_UID = "sopInstanceUid";
	public static final String NODE_START_DATE = "startDate";
	public static final String NODE_START_TIME = "startTime";
	public static final String NODE_TASK_CONTEXT_COLLECTION =
		"taskContextCollection";
	public static final String NODE_TEXT = "#text";
	public static final String NODE_TWO_DIMENSION_COORDINATE =
		"TwoDimensionSpatialCoordinate";
	public static final String NODE_TWO_DIMENSION_COORDINATE_COLLECTION =
		"twoDimensionSpatialCoordinateCollection";
	public static final String NODE_TYPE = "type";
	public static final String NODE_TYPECODE = "typeCode";
	public static final String NODE_UID = "uniqueIdentifier";
	public static final String NODE_UNIT_OF_MEASURE = "unitOfMeasure";
	public static final String NODE_USER = "user";
	public static final String NODE_USER_LOGIN_NAME = "loginName";
	public static final String NODE_USER_NAME = "name";
	public static final String NODE_USER_ROLE = "roleInTrial";
	public static final String NODE_USER_NUMBER_IN_ROLE =
		"numberWithinRoleOfClinicalTrial";
	public static final String NODE_VALUE = "value";
	public static final String NODE_VERSION = "version";
	public static final String NODE_WORKLIST_SUBTASK_CLOSED_DT =
		"worklistSubtaskClosedDateTime";
	public static final String NODE_WORKLIST_SUBTASK_NAME =
		"worklistSubtaskName";
	public static final String NODE_WORKLIST_SUBTASK_START_DT =
		"worklistSubtaskStartDateTime";
	public static final String NODE_WORKLIST_SUBTASK_UID = "worklistSubtaskUid";
	public static final String NODE_WORKLIST_TASK_CATEGORY =
		"worklistTaskCategory";
	public static final String NODE_WORKLIST_TASK_DESCRIPTION =
		"worklistTaskDescription";
	public static final String NODE_WORKLIST_TASK_LEVEL = "worklistTaskLevel";
	public static final String NODE_WORKLIST_TASK_NAME = "worklistTaskName";
	public static final String NODE_WORKLIST_TASK_REPEAT_TYPE =
		"worklistTaskRepeatType";
	public static final String NODE_WORKLIST_TASK_TYPE = "worklistTaskType";
	public static final String NODE_WORKLIST_TASK_UID = "worklistTaskUid";
	public static final String NODE_WORKLIST_TASK_VARIABILITY_TYPE =
		"worklistTaskVariabilityType";
	public static final String NODE_X = "x";
	public static final String NODE_Y = "y";
	public static final String TWO_DIMENSION_CIRCLE = "TwoDimensionCircle";
	public static final String TWO_DIMENSION_POLYLINE = "TwoDimensionPolyline";

	private AimXml()
	{}
}
