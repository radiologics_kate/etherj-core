/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import com.google.common.collect.ImmutableSortedMap;
import icr.etherj.Displayable;
import icr.etherj.Uids;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 *
 * @author jamesd
 */
public abstract class Entity implements AimUnique, Displayable
{
	protected String uid = Uids.generateDicomUid();

	@Override
	public void display()
	{
		display("", false);
	}

	@Override
	public void display(boolean recurse)
	{
		display("", recurse);
	}

	@Override
	public void display(String indent)
	{
		display(indent, false);
	}

	@Override
	public void display(String indent, boolean recurse)
	{
		display(System.out, indent, recurse);
	}

	@Override
	public void display(PrintStream ps)
	{
		display(ps, "", false);
	}

	@Override
	public void display(PrintStream ps, boolean recurse)
	{
		display(ps, "", recurse);
	}

	@Override
	public void display(PrintStream ps, String indent)
	{
		display(ps, indent, false);
	}

	@Override
	public String getUid()
	{
		return uid;
	}

	@Override
	public void setUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.uid = uid;
	}

	/**
	 *
	 * @param c
	 * @param map
	 * @return
	 */
	protected Code addCode(Code c, SortedMap<String,Code> map)
	{
		if (c == null)
		{
			return null;
		}
		return map.put(c.getCode(), c);
	}

	/**
	 *
	 * @param <T>
	 * @param t
	 * @param map
	 * @return
	 */
	protected <T extends Entity> T addEntity(T t, Map<String,T> map)
	{
		if (t == null)
		{
			return null;
		}
		return map.put(t.getUid(), t);
	}

	/**
	 *
	 * @param code
	 * @param map
	 * @return
	 */
	protected Code getCode(String code, SortedMap<String,Code> map)
	{
		return map.get(code);
	}

	/**
	 *
	 * @param map
	 * @return
	 */
	protected SortedMap<String,Code> getCodeMap(SortedMap<String,Code> map)
	{
		return ImmutableSortedMap.copyOf(map);
	}

	/**
	 *
	 * @param map
	 * @return
	 */
	protected List<Code> getCodeList(SortedMap<String,Code> map)
	{
		List<Code> list = new ArrayList<>();
		Set<Map.Entry<String,Code>> entries = map.entrySet();
		Iterator<Map.Entry<String,Code>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Map.Entry<String,Code> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	/**
	 *
	 * @param code
	 * @param map
	 * @return
	 */
	protected Code removeCode(String code, SortedMap<String,Code> map)
	{
		return map.remove(code);
	}

	/**
	 *
	 * @param <T>
	 * @param uid
	 * @param map
	 * @return
	 */
	protected <T extends Entity> T getEntity(String uid, Map<String,T> map)
	{
		return map.get(uid);
	}

	/**
	 *
	 * @param <T>
	 * @param map
	 * @return
	 */
	protected <T extends Entity> List<T> getEntityList(Map<String,T> map)
	{
		List<T> list = new ArrayList<>();
		Set<Map.Entry<String,T>> entries = map.entrySet();
		Iterator<Map.Entry<String,T>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Map.Entry<String,T> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	/**
	 *
	 * @param <T>
	 * @param uid
	 * @param map
	 * @return
	 */
	protected <T extends Entity> T removeEntity(String uid, Map<String,T> map)
	{
		return map.remove(uid);
	}

}
