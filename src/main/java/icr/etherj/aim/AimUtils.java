/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility methods for <code>ether.aim</code> package.
 * @author jamesd
 */
public class AimUtils
{
	private static final Logger logger = LoggerFactory.getLogger(AimUtils.class);
	private static final List<DateFormat> dateFormats = new ArrayList<>();

	static
	{
		dateFormats.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		dateFormats.add(new SimpleDateFormat("yyyyMMddHHmmss"));
		dateFormats.add(new SimpleDateFormat("yyyyMMdd"));
		dateFormats.add(new SimpleDateFormat("HH:mm:ss"));
		dateFormats.add(new SimpleDateFormat("HHmmss"));
	}

	/**
	 *
	 * @param dateTime
	 * @return
	 */
	public static long dateTimeToLong(String dateTime)
	{
		Date date = parseDateTime(dateTime);
		if (date == null)
		{
			return 0;
		}
		return date.getTime();
	}

	/**
	 *
	 * @param dateTime
	 * @return
	 */
	public static String getDate(String dateTime)
	{
		if ((dateTime == null) || dateTime.isEmpty())
		{
			return "";
		}
		Date dt = AimUtils.parseDateTime(dateTime);
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(dt);
	}

	/**
	 *
	 * @param dateTime
	 * @return
	 */
	public static String getTime(String dateTime)
	{
		if ((dateTime == null) || dateTime.isEmpty())
		{
			return "";
		}
		Date dt = AimUtils.parseDateTime(dateTime);
		DateFormat format = new SimpleDateFormat("HHmmss");
		return format.format(dt);
	}

	/**
	 *
	 * @param dateTime
	 * @return
	 */
	public static Date parseDateTime(String dateTime)
	{
		if ((dateTime == null) || dateTime.isEmpty())
		{
			return null;
		}
		Date date = null;
		for (DateFormat df : dateFormats)
		{
			date = parseDateTime(dateTime, df);
			if (date != null)
			{
				return date;
			}
		}
		return date;
	}

	/**
	 *
	 * @param tag
	 * @return
	 */
	public static String tagName(int tag)
	{
		for (Field field : AimTag.class.getDeclaredFields())
		{
			try
			{
				if (field.getInt(null) == tag)
				{
					return field.getName();
				}
			}
			catch (IllegalArgumentException | IllegalAccessException ignore)
			{}
		}
		return null;
	}

	/**
	 *
	 * @param date
	 * @return
	 */
	public static String toDateTime(Date date)
	{
		DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format.format(date);
	}

	/**
	 *
	 * @param iac
	 * @return
	 */
	public static String toXml(ImageAnnotationCollection iac)
	{
		XmlWriter xmlWriter = new DefaultXmlWriter();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String xml = "";
		try
		{
			xmlWriter.write(iac, baos);
			xml = baos.toString();
		}
		catch (IOException ex)
		{
			logger.warn("XML conversion failed", ex);
		}
		return xml;
	}

	private static Date parseDateTime(String dateTime, DateFormat df)
	{
		Date date = null;
		try
		{
			date = df.parse(dateTime);
		}
		catch (ParseException ex)
		{ /* Deliberate no-op */ }
		return date;
	}

}
