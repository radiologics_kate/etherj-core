/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class CalculationData extends AbstractDisplayable
{
	private final List<Coordinate> coords = new ArrayList<>();
	private String value;

	public boolean addCoordinate(Coordinate coord)
	{
		return coords.add(coord);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Value: "+value);
		int nCodes = coords.size();
		if (nCodes < 1)
		{
			ps.println(pad+"ERROR. CalculationData requires 1 or more coordinates.");
			return;
		}
		int nCoords = coords.size();
		ps.println(pad+"CoordinateList: "+nCoords+" coordinate"+
			(nCoords != 1 ? "s" : ""));
		if (recurse)
		{
			if (!coords.isEmpty())
			{
				ps.println(pad+"Coordinates:");
			}
			for (Coordinate coord : coords)
			{
				coord.display(ps, indent+"  ", recurse);
			}
		}
	}

	/**
	 * @return the coordinate list
	 */
	public List<Coordinate> getCoordinateList()
	{
		return ImmutableList.copyOf(coords);
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	public boolean removeCoordinate(Coordinate coord)
	{
		return coords.remove(coord);
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

}
