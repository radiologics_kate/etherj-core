/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class ImageSeries extends AbstractDisplayable
{
	private String instanceUid;
	private Code modality = new Code();
	private final Map<String,Image> images = new HashMap<>();

	/**
	 *
	 */
	public ImageSeries()
	{
		instanceUid = Uids.generateDicomUid();
	}

	/**
	 *
	 * @param uid
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public ImageSeries(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.instanceUid = uid;
	}

	/**
	 *
	 * @param image
	 * @return
	 */
	public Image addImage(Image image)
	{
		return images.put(image.getSopInstanceUid(), image);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceUid: "+instanceUid);
		modality.display(ps, indent+"  ");
		int nImages = images.size();
		ps.println(pad+"ImagesList: "+nImages+" image"+
			((nImages != 1) ? "s" : ""));
		if (recurse)
		{
			for (Image image : getImageList())
			{
				image.display(ps, indent+"  ");
			}
		}
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public Image getImage(String uid)
	{
		return images.get(uid);
	}

	/**
	 *
	 * @return
	 */
	public List<Image> getImageList()
	{
		List<Image> list = new ArrayList<>();
		Set<Map.Entry<String,Image>> entries = images.entrySet();
		Iterator<Map.Entry<String,Image>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Map.Entry<String,Image> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	/**
	 * @return the instanceUid
	 */
	public String getInstanceUid()
	{
		return instanceUid;
	}

	/**
	 * @return the modality
	 */
	public Code getModality()
	{
		return modality;
	}

	public Image removeImage(String uid)
	{
		return images.remove(uid);
	}

	/**
	 * @param instanceUid the instanceUid to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public void setInstanceUid(String instanceUid) throws IllegalArgumentException
	{
		if ((instanceUid == null) || instanceUid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.instanceUid = instanceUid;
	}

	/**
	 * @param modality the modality to set
	 */
	public void setModality(Code modality)
	{
		if (modality == null)
		{
			this.modality = new Code();
			return;
		}
		this.modality = modality;
	}

}
