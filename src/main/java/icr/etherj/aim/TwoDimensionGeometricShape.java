/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public abstract class TwoDimensionGeometricShape extends GeometricShape
{
	private String imageReferenceUid = "";
	private int referencedFrameNumber = 1;
	protected final SortedMap<Integer,TwoDimensionCoordinate> coords = new TreeMap<>();

	/**
	 *
	 */
	public TwoDimensionGeometricShape()
	{
		dimensionCount = 2;
	}

	/**
	 *
	 * @param coord
	 * @return
	 */
	public TwoDimensionCoordinate addCoordinate(TwoDimensionCoordinate coord)
	{
		return coords.put(coord.getIndex(), coord);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+uid);
		ps.println(pad+"IncludeFlag: "+(includeFlag ? "true" : "false"));
		ps.println(pad+"ShapeId: "+shapeId);
		ps.println(pad+"ImageReferenceUid: "+imageReferenceUid);
		ps.println(pad+"ReferencedFrameNumber: "+referencedFrameNumber);
		int nCoords = coords.size();
		ps.println(pad+"CoordinateList: "+nCoords+" coordinate"+
			((nCoords != 1) ? "s" : ""));
		if (recurse)
		{
			for (TwoDimensionCoordinate coord : getCoordinateList())
			{
				coord.display(ps, indent+"  ");
			}
		}
	}

	/**
	 *
	 * @param index
	 * @return
	 */
	public TwoDimensionCoordinate getCoordinate(int index)
	{
		return coords.get(index);
	}

	/**
	 *
	 * @return
	 */
	public List<TwoDimensionCoordinate> getCoordinateList()
	{
		List<TwoDimensionCoordinate> list = new ArrayList<>();
		Set<Map.Entry<Integer,TwoDimensionCoordinate>> entries = coords.entrySet();
		Iterator<Map.Entry<Integer,TwoDimensionCoordinate>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Map.Entry<Integer,TwoDimensionCoordinate> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	/**
	 * @return the imageReferenceUid
	 */
	public String getImageReferenceUid()
	{
		return imageReferenceUid;
	}

	/**
	 * @return the referencedFrameNumber
	 */
	public int getReferencedFrameNumber()
	{
		return referencedFrameNumber;
	}

	public TwoDimensionCoordinate removeCoordinate(int index)
	{
		return coords.remove(index);
	}

	/**
	 * @param imageReferenceUid the imageReferenceUid to set
	 */
	public void setImageReferenceUid(String imageReferenceUid)
	{
		this.imageReferenceUid = (imageReferenceUid == null)
			? "" : imageReferenceUid;
	}

	/**
	 * @param referencedFrameNumber the referencedFrameNumber to set
	 */
	public void setReferencedFrameNumber(int referencedFrameNumber)
	{
		this.referencedFrameNumber = referencedFrameNumber;
	}
}
