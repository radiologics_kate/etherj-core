/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class Algorithm extends AbstractDisplayable implements TypeCoded
{
	private String description = "";
	private String name = null;
	private final List<Parameter> params = new ArrayList<>();
	private final SortedMap<String,Code> typeCodes = new TreeMap<>();
	private String uid = "";
	private String version = "";

	public boolean addParameter(Parameter param)
	{
		return params.add(param);
	}

	@Override
	public Code addTypeCode(Code type)
	{
		return typeCodes.put(type.getCode(), type);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Name: "+name);
		if (!uid.isEmpty())
		{
			ps.println(pad+"UID: "+uid);
		}
		if (!version.isEmpty())
		{
			ps.println(pad+"Version: "+version);
		}
		if (!description.isEmpty())
		{
			ps.println(pad+"Description: "+description);
		}
		int nTypes = typeCodes.size();
		if (nTypes < 1)
		{
			ps.println(pad+"ERROR. Algorithm requires 1 or more types.");
			return;
		}
		ps.println(pad+"TypeList: "+nTypes+" type"+
			(nTypes != 1 ? "s" : ""));
		if (recurse)
		{
			if (!typeCodes.isEmpty())
			{
				ps.println(pad+"Types:");
			}
			for (String key : typeCodes.keySet())
			{
				typeCodes.get(key).display(ps, indent+"  ", recurse);
			}
		}
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	public List<Parameter> getParameterList()
	{
		return ImmutableList.copyOf(params);
	}

	@Override
	public Code getTypeCode(String code)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 * @return the types
	 */
	@Override
	public SortedMap<String,Code> getTypeCodeMap()
	{
		return ImmutableSortedMap.copyOf(typeCodes);
	}

	/**
	 * @return the uid
	 */
	public String getUid()
	{
		return uid;
	}

	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	public boolean removeParameter(Parameter param)
	{
		return params.remove(param);
	}

	@Override
	public Code removeTypeCode(String code)
	{
		return typeCodes.remove(code);
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid)
	{
		this.uid = uid;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version)
	{
		this.version = version;
	}

}
