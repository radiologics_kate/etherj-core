/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Annotation from AIM v4r48.
 * @author jamesd
 */
public abstract class Annotation extends Entity implements TypeCoded
{
	private final Map<String,Calculation> calculations = new HashMap<>();
	private String comment = "";
	private String dateTime = "";
	private final Map<String,ImagingObservation> imagingObservations =
		new HashMap<>();
	private final Map<String,ImagingPhysical> imagingPhysicals = new HashMap<>();
	private final Map<String,Inference> inferences = new HashMap<>();
	private String name = "";
	private final Map<String,AnnotationRole> roles = new HashMap<>();
	private final Map<String,TaskContext> taskContexts = new HashMap<>();
	private final SortedMap<String,Code> typeCodes = new TreeMap<>();

	/**
	 *
	 * @param role
	 * @return
	 */
	public AnnotationRole addAnnotationRole(AnnotationRole role)
	{
		return addEntity(role, roles);
	}

	/**
	 *
	 * @param c
	 * @return
	 */
	public Calculation addCalculation(Calculation c)
	{
		return addEntity(c, calculations);
	}

	/**
	 *
	 * @param io
	 * @return
	 */
	public ImagingObservation addImagingObservation(ImagingObservation io)
	{
		return addEntity(io, imagingObservations);
	}

	/**
	 *
	 * @param ip
	 * @return
	 */
	public ImagingPhysical addImagingPhysical(ImagingPhysical ip)
	{
		return addEntity(ip, imagingPhysicals);
	}

	/**
	 *
	 * @param inference
	 * @return
	 */
	public Inference addInference(Inference inference)
	{
		return addEntity(inference, inferences);
	}

	/**
	 *
	 * @param taskContext
	 * @return
	 */
	public TaskContext addTaskContext(TaskContext taskContext)
	{
		return addEntity(taskContext, taskContexts);
	}

	@Override
	public Code addTypeCode(Code code)
	{
		return addCode(code, typeCodes);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public AnnotationRole getAnnotationRole(String uid)
	{
		return getEntity(uid, roles);
	}

	/**
	 *
	 * @return
	 */
	public int getAnnotationRoleCount()
	{
		return roles.size();
	}

	/**
	 *
	 * @return
	 */
	public List<AnnotationRole> getAnnotationRoleList()
	{
		return getEntityList(roles);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public Calculation getCalculation(String uid)
	{
		return getEntity(uid, calculations);
	}

	/**
	 *
	 * @return
	 */
	public int getCalculationCount()
	{
		return calculations.size();
	}

	/**
	 *
	 * @return
	 */
	public List<Calculation> getCalculationList()
	{
		return getEntityList(calculations);
	}

	/**
	 *
	 * @return
	 */
	public String getComment()
	{
		return comment;
	}

	/**
	 *
	 * @return
	 */
	public String getDateTime()
	{
		return dateTime;
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public ImagingObservation getImagingObservation(String uid)
	{
		return getEntity(uid, imagingObservations);
	}

	/**
	 *
	 * @return
	 */
	public int getImagingObservationCount()
	{
		return imagingObservations.size();
	}

	/**
	 *
	 * @return
	 */
	public List<ImagingObservation> getImagingObservationList()
	{
		return getEntityList(imagingObservations);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public ImagingPhysical getImagingPhysical(String uid)
	{
		return getEntity(uid, imagingPhysicals);
	}

	/**
	 *
	 * @return
	 */
	public int getImagingPhysicalCount()
	{
		return imagingPhysicals.size();
	}

	/**
	 *
	 * @return
	 */
	public List<ImagingPhysical> getImagingPhysicalList()
	{
		return getEntityList(imagingPhysicals);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public Inference getInference(String uid)
	{
		return inferences.get(uid);
	}

	/**
	 *
	 * @return
	 */
	public int getInferenceCount()
	{
		return inferences.size();
	}

	/**
	 *
	 * @return
	 */
	public List<Inference> getInferenceList()
	{
		return getEntityList(inferences);
	}

	/**
	 *
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public TaskContext getTaskContext(String uid)
	{
		return getEntity(uid, taskContexts);
	}

	/**
	 *
	 * @return
	 */
	public int getTaskContextCount()
	{
		return taskContexts.size();
	}

	/**
	 *
	 * @return
	 */
	public List<TaskContext> getTaskContextList()
	{
		return getEntityList(taskContexts);
	}

	@Override
	public Code getTypeCode(String code)
	{
		return getCode(code, typeCodes);
	}

	@Override
	public SortedMap<String,Code> getTypeCodeMap()
	{
		return getCodeMap(typeCodes);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public AnnotationRole removeAnnotationRole(String uid)
	{
		return removeEntity(uid, roles);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public Calculation removeCalculation(String uid)
	{
		return removeEntity(uid, calculations);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public ImagingObservation removeImagingObservation(String uid)
	{
		return removeEntity(uid, imagingObservations);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public ImagingPhysical removeImagingPhysical(String uid)
	{
		return removeEntity(uid, imagingPhysicals);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public Inference removeInference(String uid)
	{
		return removeEntity(uid, inferences);
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public TaskContext removeTaskContext(String uid)
	{
		return removeEntity(uid, taskContexts);
	}

	@Override
	public Code removeTypeCode(String code)
	{
		return removeCode(code, typeCodes);
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime)
	{
		this.dateTime = dateTime;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

}
