/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

/**
 *
 * @author jamesd
 */
public abstract class GeometricShape extends Markup
{
	protected String description = "";
	protected String label = "";
	protected String lineColour = "";
	protected String lineOpacity = "";
	protected String lineStyle = "";
	protected String lineThickness = "";
	protected boolean includeFlag = false;
	protected int shapeId = 0;

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 *
	 * @return
	 */
	public boolean getIncludeFlag()
	{
		return includeFlag;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return the lineColour
	 */
	public String getLineColour()
	{
		return lineColour;
	}


	/**
	 * @return the lineOpacity
	 */
	public String getLineOpacity()
	{
		return lineOpacity;
	}

	/**
	 * @return the lineStyle
	 */
	public String getLineStyle()
	{
		return lineStyle;
	}

	/**
	 * @return the lineThickness
	 */
	public String getLineThickness()
	{
		return lineThickness;
	}

	/**
	 *
	 * @return
	 */
	public int getShapeId()
	{
		return shapeId;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = (description != null) ? description : "";
	}

	/**
	 * @param includeFlag the includeFlag to set
	 */
	public void setIncludeFlag(boolean includeFlag)
	{
		this.includeFlag = includeFlag;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label)
	{
		this.label = (label != null) ? label : "";
	}

	/**
	 * @param lineColour the lineColour to set
	 */
	public void setLineColour(String lineColour)
	{
		this.lineColour = (lineColour != null) ? lineColour : "";
	}

	/**
	 * @param lineOpacity the lineOpacity to set
	 */
	public void setLineOpacity(String lineOpacity)
	{
		this.lineOpacity = (lineOpacity != null) ? lineOpacity : "";
	}

	/**
	 * @param lineStyle the lineStyle to set
	 */
	public void setLineStyle(String lineStyle)
	{
		this.lineStyle = (lineStyle != null) ? lineStyle : "";
	}

	/**
	 * @param lineThickness the lineThickness to set
	 */
	public void setLineThickness(String lineThickness)
	{
		this.lineThickness = (lineThickness != null) ? lineThickness : "";
	}

	/**
	 * @param shapeId the shapeId to set
	 */
	public void setShapeId(int shapeId)
	{
		this.shapeId = shapeId;
	}

}
