/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class TaskContext extends Entity
{
	private Map<String,TaskContext> taskContextMap = new HashMap<>();
	private String worklistSubtaskClosedDateTime = "";
	private String worklistSubtaskName = "";
	private String worklistSubtaskStartDateTime = "";
	private String worklistSubtaskUid = "";
	private Code worklistTaskCategory = new Code();
	private String worklistTaskDescription = "";
	private Code worklistTaskLevel = new Code();
	private String worklistTaskName = "";
	private Code worklistTaskRepeatType = null;
	private Code worklistTaskType = new Code();
	private String worklistTaskUid = "";
	private Code worklistTaskVariabilityType = null;
	private String worklistTaskVersion = "";

	/**
	 *
	 * @param tc
	 * @return
	 */
	public TaskContext addTaskContext(TaskContext tc)
	{
		return addEntity(tc, taskContextMap);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+getUid());
		ps.println(pad+"WorklistTaskUID: "+worklistTaskUid);
		ps.println(pad+"WorklistTaskName: "+worklistTaskName );
		ps.println(pad+"WorklistTaskDescription: "+
			worklistTaskDescription);
		ps.println(pad+"WorklistTaskCategory:");
		worklistTaskCategory.display(ps, indent+"  ");
		ps.println(pad+"WorklistTaskLevel:");
		worklistTaskLevel.display(ps, indent+"  ");
		ps.println(pad+"WorklistTaskType:");
		worklistTaskType.display(ps, indent+"  ");
		if (worklistTaskRepeatType != null)
		{
			ps.println(pad+"WorklistTaskLevel:");
			worklistTaskRepeatType.display(ps, indent+"  ");
		}
		if (worklistTaskVariabilityType != null)
		{
			ps.println(pad+"WorklistTaskLevel:");
			worklistTaskVariabilityType.display(ps, indent+"  ");
		}
		ps.println(pad+"WorklistTaskVersion: "+worklistTaskVersion);
		ps.println(pad+"WorklistSubtaskUID: "+worklistSubtaskUid);
		ps.println(pad+"WorklistSubtaskName: "+worklistSubtaskName);
		ps.println(pad+"WorklistSubtaskStartDateTime: "+
			worklistSubtaskStartDateTime);
		ps.println(pad+"WorklistSubtaskClosedDateTime: "+
			worklistSubtaskClosedDateTime);
		List<TaskContext> tcList = getTaskContextList();
		int nTC = tcList.size();
		ps.println(pad+"TaskContextList: "+nTC+" taskcontext"+
			(nTC != 1 ? "s" : ""));
		if (recurse)
		{
			ps.println(pad+"TaskContexts:");
			for (TaskContext tc : tcList)
			{
				tc.display(ps, indent+"  ", true);
			}
		}
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public TaskContext getTaskContext(String uid)
	{
		return getEntity(uid, taskContextMap);
	}

	/**
	 *
	 * @return
	 */
	public List<TaskContext> getTaskContextList()
	{
		return getEntityList(taskContextMap);
	}

	/**
	 * @return the worklistSubtaskClosedDateTime
	 */
	public String getWorklistSubtaskClosedDateTime()
	{
		return worklistSubtaskClosedDateTime;
	}

	/**
	 * @return the worklistSubtaskName
	 */
	public String getWorklistSubtaskName()
	{
		return worklistSubtaskName;
	}

	/**
	 * @return the worklistSubtaskStartDateTime
	 */
	public String getWorklistSubtaskStartDateTime()
	{
		return worklistSubtaskStartDateTime;
	}

	/**
	 * @return the worklistSubtaskUid
	 */
	public String getWorklistSubtaskUid()
	{
		return worklistSubtaskUid;
	}

	/**
	 * @return the worklistTaskCategory
	 */
	public Code getWorklistTaskCategory()
	{
		return worklistTaskCategory;
	}

	/**
	 * @return the worklistTaskDescription
	 */
	public String getWorklistTaskDescription()
	{
		return worklistTaskDescription;
	}

	/**
	 * @return the worklistTaskLevel
	 */
	public Code getWorklistTaskLevel()
	{
		return worklistTaskLevel;
	}

	/**
	 * @return the worklistTaskName
	 */
	public String getWorklistTaskName()
	{
		return worklistTaskName;
	}

	/**
	 * @return the worklistTaskRepeatType
	 */
	public Code getWorklistTaskRepeatType()
	{
		return worklistTaskRepeatType;
	}

	/**
	 * @return the worklistTaskType
	 */
	public Code getWorklistTaskType()
	{
		return worklistTaskType;
	}

	/**
	 * @return the worklistTaskUid
	 */
	public String getWorklistTaskUid()
	{
		return worklistTaskUid;
	}

	/**
	 * @return the worklistTaskVariabilityType
	 */
	public Code getWorklistTaskVariabilityType()
	{
		return worklistTaskVariabilityType;
	}

	/**
	 * @return the worklistTaskVersion
	 */
	public String getWorklistTaskVersion()
	{
		return worklistTaskVersion;
	}

	public TaskContext removeTaskContext(String uid)
	{
		return removeEntity(uid, taskContextMap);
	}

	/**
	 * @param worklistSubtaskClosedDateTime the worklistSubtaskClosedDateTime to set
	 */
	public void setWorklistSubtaskClosedDateTime(String worklistSubtaskClosedDateTime)
	{
		this.worklistSubtaskClosedDateTime = worklistSubtaskClosedDateTime;
	}

	/**
	 * @param worklistSubtaskName the worklistSubtaskName to set
	 */
	public void setWorklistSubtaskName(String worklistSubtaskName)
	{
		this.worklistSubtaskName = worklistSubtaskName;
	}

	/**
	 * @param worklistSubtaskStartDateTime the worklistSubtaskStartDateTime to set
	 */
	public void setWorklistSubtaskStartDateTime(String worklistSubtaskStartDateTime)
	{
		this.worklistSubtaskStartDateTime = worklistSubtaskStartDateTime;
	}

	/**
	 * @param worklistSubtaskUid the worklistSubtaskUid to set
	 */
	public void setWorklistSubtaskUid(String worklistSubtaskUid)
	{
		this.worklistSubtaskUid = worklistSubtaskUid;
	}

	/**
	 * @param worklistTaskCategory the worklistTaskCategory to set
	 */
	public void setWorklistTaskCategory(Code worklistTaskCategory)
	{
		this.worklistTaskCategory = worklistTaskCategory;
	}

	/**
	 * @param worklistTaskDescription the worklistTaskDescription to set
	 */
	public void setWorklistTaskDescription(String worklistTaskDescription)
	{
		this.worklistTaskDescription = worklistTaskDescription;
	}

	/**
	 * @param worklistTaskLevel the worklistTaskLevel to set
	 */
	public void setWorklistTaskLevel(Code worklistTaskLevel)
	{
		this.worklistTaskLevel = worklistTaskLevel;
	}

	/**
	 * @param worklistTaskName the worklistTaskName to set
	 */
	public void setWorklistTaskName(String worklistTaskName)
	{
		this.worklistTaskName = worklistTaskName;
	}

	/**
	 * @param worklistTaskRepeatType the worklistTaskRepeatType to set
	 */
	public void setWorklistTaskRepeatType(Code worklistTaskRepeatType)
	{
		this.worklistTaskRepeatType = worklistTaskRepeatType;
	}

	/**
	 * @param worklistTaskType the worklistTaskType to set
	 */
	public void setWorklistTaskType(Code worklistTaskType)
	{
		this.worklistTaskType = worklistTaskType;
	}

	/**
	 * @param worklistTaskUid the worklistTaskUid to set
	 */
	public void setWorklistTaskUid(String worklistTaskUid)
	{
		this.worklistTaskUid = worklistTaskUid;
	}

	/**
	 * @param worklistTaskVariabilityType the worklistTaskVariabilityType to set
	 */
	public void setWorklistTaskVariabilityType(Code worklistTaskVariabilityType)
	{
		this.worklistTaskVariabilityType = worklistTaskVariabilityType;
	}

	/**
	 * @param worklistTaskVersion the worklistTaskVersion to set
	 */
	public void setWorklistTaskVersion(String worklistTaskVersion)
	{
		this.worklistTaskVersion = worklistTaskVersion;
	}
	
}
