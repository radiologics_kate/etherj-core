/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DicomSegmentation extends Segmentation
{
	private String referencedSopInstanceUid = "";
	private int segmentNumber = -1;
	private String sopClassUid = "";
	private String sopInstanceUid = "";

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+getUid());
		ps.println(pad+"SopInstanceUID: "+sopInstanceUid);
		ps.println(pad+"SopClassUID: "+sopClassUid);
		ps.println(pad+"ReferencedSopInstanceUID: "+
			referencedSopInstanceUid);
		ps.println(pad+"SegmentNumber: "+segmentNumber);
	}

	/**
	 * @return the referencedSopInstanceUid
	 */
	public String getReferencedSopInstanceUid()
	{
		return referencedSopInstanceUid;
	}

	/**
	 * @return the segmentNumber
	 */
	public int getSegmentNumber()
	{
		return segmentNumber;
	}

	/**
	 * @return the sopClassUid
	 */
	public String getSopClassUid()
	{
		return sopClassUid;
	}

	/**
	 * @return the sopInstanceUid
	 */
	public String getSopInstanceUid()
	{
		return sopInstanceUid;
	}

	/**
	 * @param referencedSopInstanceUid the referencedSopInstanceUid to set
	 */
	public void setReferencedSopInstanceUid(String referencedSopInstanceUid)
	{
		this.referencedSopInstanceUid = referencedSopInstanceUid;
	}

	/**
	 * @param segmentNumber the segmentNumber to set
	 */
	public void setSegmentNumber(int segmentNumber)
	{
		this.segmentNumber = segmentNumber;
	}

	/**
	 * @param sopClassUid the sopClassUid to set
	 */
	public void setSopClassUid(String sopClassUid)
	{
		this.sopClassUid = sopClassUid;
	}

	/**
	 * @param sopInstanceUid the sopInstanceUid to set
	 */
	public void setSopInstanceUid(String sopInstanceUid)
	{
		this.sopInstanceUid = sopInstanceUid;
	}
	
}
