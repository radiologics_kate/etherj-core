/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class Image extends AbstractDisplayable
{
	private String sopClassUid = "";
	private String sopInstanceUid = "";

	/**
	 * Constructs a new image.
	 */
	public Image()
	{}

	/**
	 * Constructs a new image.
	 * @param sopClassUid
	 * @param sopInstUid
	 * @throws IllegalArgumentException
	 */
	public Image(String sopClassUid, String sopInstUid)
		throws IllegalArgumentException
	{
		if ((sopClassUid == null) || sopClassUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SOP class UID must not be null or empty");
		}
		this.sopClassUid = sopClassUid;
		if ((sopInstUid == null) || sopInstUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SOP instance UID must not be null or empty");
		}
		this.sopInstanceUid = sopInstUid;
	}

	/**
	 * Constructs a copy of the image.
	 * @param image the image to copy
	 */
	public Image(Image image)
	{
		sopClassUid = image.sopClassUid;
		sopInstanceUid = image.sopInstanceUid;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceUid: "+sopInstanceUid);
		ps.println(pad+"SopClassUid: "+sopClassUid);
	}

	/**
	 * @return the sopClassUid
	 */
	public String getSopClassUid()
	{
		return sopClassUid;
	}

	/**
	 * @return the sopInstanceUid
	 */
	public String getSopInstanceUid()
	{
		return sopInstanceUid;
	}

	/**
	 * @param sopClassUid the sopClassUid to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public void setSopClassUid(String sopClassUid)
		throws IllegalArgumentException
	{
		if ((sopClassUid == null) || sopClassUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SOP class UID must not be null or empty");
		}
		this.sopClassUid = sopClassUid;
	}

	/**
	 * @param sopInstanceUid the sopInstanceUid to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public void setSopInstanceUid(String sopInstanceUid)
		throws IllegalArgumentException
	{
		if ((sopInstanceUid == null) || sopInstanceUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SOP instance UID must not be null or empty");
		}
		this.sopInstanceUid = sopInstanceUid;
	}

}
