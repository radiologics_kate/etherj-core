/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author jamesd
 */
public class DefaultXmlWriter implements XmlWriter
{
	private static final Logger logger = LoggerFactory.getLogger(
		DefaultXmlWriter.class);
	private static final Set<String> requiredAttrs = new HashSet<>();

	static
	{
		requiredAttrs.add(AimXml.ATTR_ROOT);
		requiredAttrs.add(AimXml.ATTR_XSI_TYPE);
	}

	@Override
	public void write(ImageAnnotationCollection iac, String path)
		throws IOException
	{
		write(iac, new File(path));
	}

	@Override
	public void write(ImageAnnotationCollection iac, File file)
		throws IOException
	{
		try (FileOutputStream fis = new FileOutputStream(file))
		{
			write(iac, fis);
		}
	}

	@Override
	public void write(ImageAnnotationCollection iac, OutputStream stream)
		throws IOException
	{
		Document doc = null;
		try
		{
			doc = createDocument(iac);
			StreamResult result = new StreamResult(
				new BufferedWriter(new OutputStreamWriter(stream)));
			transform(doc, result);
		}
		catch (ParserConfigurationException | TransformerException ex)
		{
			logger.warn("XML write failed", ex);
		}
	}
	
	@Override
	public void write(ImageAnnotationCollection iac, Writer writer)
		throws IOException
	{
		Document doc = null;
		try
		{
			doc = createDocument(iac);
			StreamResult result = new StreamResult(new BufferedWriter(writer));
			transform(doc, result);
		}
		catch (ParserConfigurationException | TransformerException ex)
		{
			logger.warn("XML write failed", ex);
		}
	}

	private void append2DCoord(Document doc, Element parent,
		TwoDimensionCoordinate coord)
	{
		Element coordNode = doc.createElement(
			AimXml.NODE_TWO_DIMENSION_COORDINATE);
		appendNodeWithAttr(doc, coordNode, AimXml.NODE_COORDINATE_INDEX,
			AimXml.ATTR_VALUE, Integer.toString(coord.getIndex()), true);
		appendNodeWithAttr(doc, coordNode, AimXml.NODE_X, AimXml.ATTR_VALUE,
			Double.toString(coord.getX()), true);
		appendNodeWithAttr(doc, coordNode, AimXml.NODE_Y, AimXml.ATTR_VALUE,
			Double.toString(coord.getY()), true);
		parent.appendChild(coordNode);
	}
	
	private void appendAnnotation(Document doc, Element parent,
		ImageAnnotation ia)
	{
		Element annoNode = doc.createElement(AimXml.NODE_IMAGE_ANNOTATION);
		appendUid(doc, annoNode, ia.getUid());
		for (Map.Entry<String,Code> entry : ia.getTypeCodeMap().entrySet())
		{
			appendTypeCode(doc, annoNode, entry.getValue());
		}
		appendDateTime(doc, annoNode, ia.getDateTime());
		appendName(doc, annoNode, ia.getName());
		appendNodeWithAttr(doc, annoNode, AimXml.NODE_COMMENT, AimXml.ATTR_VALUE,
			ia.getComment());
		if (ia.getMarkupCount() > 0)
		{
			Element markupCollNode = doc.createElement(
				AimXml.NODE_MARKUP_COLLECTION);
			annoNode.appendChild(markupCollNode);
			for (Markup markup : ia.getMarkupList())
			{
				appendMarkup(doc, markupCollNode, markup);
			}
		}
		if (ia.getReferenceCount() > 0)
		{
			Element refCollNode = doc.createElement(
				AimXml.NODE_IMAGE_REFERENCE_COLLECTION);
			annoNode.appendChild(refCollNode);
			for (ImageReference ref : ia.getReferenceList())
			{
				appendReference(doc, refCollNode, ref);
			}
		}
		
		parent.appendChild(annoNode);
	}

	private void appendAnnotations(Document doc, Element parent,
		List<ImageAnnotation> annotationList)
	{
		if (annotationList.isEmpty())
		{
			return;
		}
		Element listNode = doc.createElement(AimXml.NODE_ANNOTATIONS);
		for (ImageAnnotation ia : annotationList)
		{
			appendAnnotation(doc, listNode, ia);
		}
		parent.appendChild(listNode);
	}

	private void appendDateTime(Document doc, Element parent, String dt)
	{
		appendNodeWithAttr(doc, parent, AimXml.NODE_DATE_TIME,
			AimXml.ATTR_VALUE, dt);
	}

	private void appendDescription(Document doc, Element parent,
		String desc)
	{
		appendNodeWithAttr(doc, parent, AimXml.NODE_DESCRIPTION,
			AimXml.ATTR_VALUE, desc);
	}

	private void appendEquipment(Document doc, Element parent,
		Equipment equipment)
	{
		if (equipment == null)
		{
			return;
		}
		Element equipNode = doc.createElement(AimXml.NODE_EQUIPMENT);
		appendNodeWithAttr(doc, equipNode, AimXml.NODE_EQUIPMENT_MANUFACTURER_NAME,
			AimXml.ATTR_VALUE, equipment.getManufacturerName(), true);
		appendNodeWithAttr(doc, equipNode, AimXml.NODE_EQUIPMENT_MANUFACTURER_MODEL_NAME,
			AimXml.ATTR_VALUE, equipment.getManufacturerModelName());
		appendNodeWithAttr(doc, equipNode, AimXml.NODE_EQUIPMENT_SOFTWARE_VERSION,
			AimXml.ATTR_VALUE, equipment.getSoftwareVersion());
		appendNodeWithAttr(doc, equipNode, AimXml.NODE_EQUIPMENT_DEVICE_SERIAL_NUMBER,
			AimXml.ATTR_VALUE, equipment.getDeviceSerialNumber());
		parent.appendChild(equipNode);
	}

	private void appendImage(Document doc, Element parent, Image image)
	{
		Element imageNode = doc.createElement(AimXml.NODE_IMAGE);
		appendNodeWithAttr(doc, imageNode, AimXml.NODE_SOP_CLASS_UID,
			AimXml.ATTR_ROOT, image.getSopClassUid(), true);
		appendNodeWithAttr(doc, imageNode, AimXml.NODE_SOP_INSTANCE_UID,
			AimXml.ATTR_ROOT, image.getSopInstanceUid(), true);
		parent.appendChild(imageNode);
	}

	private void appendImageSeries(Document doc, Element parent,
		ImageSeries series)
	{
		Element seriesNode = doc.createElement(AimXml.NODE_IMAGE_SERIES);
		appendNodeWithAttr(doc, seriesNode, AimXml.NODE_INSTANCE_UID,
			AimXml.ATTR_ROOT, series.getInstanceUid(), true);
		appendModality(doc, seriesNode, series.getModality());
		Element imageCollNode = doc.createElement(AimXml.NODE_IMAGE_COLLECTION);
		for (Image image : series.getImageList())
		{
			appendImage(doc, imageCollNode, image);
		}
		seriesNode.appendChild(imageCollNode);
		parent.appendChild(seriesNode);
	}

	private void appendImageStudy(Document doc, Element parent, ImageStudy study)
	{
		Element studyNode = doc.createElement(AimXml.NODE_IMAGE_STUDY);
		appendNodeWithAttr(doc, studyNode, AimXml.NODE_INSTANCE_UID,
			AimXml.ATTR_ROOT, study.getInstanceUid(), true);
		appendNodeWithAttr(doc, studyNode, AimXml.NODE_START_DATE,
			AimXml.ATTR_VALUE, study.getStartDate(), true);
		appendNodeWithAttr(doc, studyNode, AimXml.NODE_START_TIME,
			AimXml.ATTR_VALUE, study.getStartTime(), true);
		appendImageSeries(doc, studyNode, study.getSeries());
		parent.appendChild(studyNode);
	}

	private void appendMarkup(Document doc, Element parent, Markup markup)
	{
		Element markupNode = doc.createElement(AimXml.NODE_MARKUP);
		if (markup instanceof TwoDimensionGeometricShape)
		{
			if (build2DShape(doc, markupNode, (TwoDimensionGeometricShape) markup))
			{
				parent.appendChild(markupNode);
			}
		}
		else
		{
			logger.warn("Ignoring Markup class: "+
				markup.getClass().getCanonicalName());
		}
	}

	private void appendModality(Document doc, Element parent, Code modality)
	{
		Element modalityNode = doc.createElement(AimXml.NODE_MODALITY);
		modalityNode.setAttribute(AimXml.ATTR_CODE, modality.getCode());
		modalityNode.setAttribute(AimXml.ATTR_CODE_SYSTEM,
			modality.getCodeSystem());
		modalityNode.setAttribute(AimXml.ATTR_CODE_SYSTEM_NAME,
			modality.getCodeSystemName());
		modalityNode.setAttribute(AimXml.ATTR_CODE_SYSTEM_VERSION,
			modality.getCodeSystemVersion());
		parent.appendChild(modalityNode);
	}

	private void appendName(Document doc, Element parent, String name)
	{
		appendNodeWithAttr(doc, parent, AimXml.NODE_NAME,
			AimXml.ATTR_VALUE, name, true);
	}

	private void appendNodeWithAttr(Document doc, Element parent,
		String nodeName, String attrName, String attrValue)
	{
		appendNodeWithAttr(doc, parent, nodeName, attrName, attrValue, false);
	}

	private void appendNodeWithAttr(Document doc, Element parent,
		String nodeName, String attrName, String attrValue, boolean nodeMustExist)
	{
		Element node;
		boolean nonEmpty = !((attrValue == null) || attrValue.isEmpty());
		if (nonEmpty)
		{
			node = doc.createElement(nodeName);
			node.setAttribute(attrName, attrValue);
			parent.appendChild(node);
			return;
		}
		if (nodeMustExist)
		{
			node = doc.createElement(nodeName);
			if (nonEmpty)
			{
				node.setAttribute(attrName, attrValue);
			}
			else
			{
				if (requiredAttrs.contains(attrName))
				{
					node.setAttribute(attrName, "");
				}
			}
			parent.appendChild(node);
		}
	}

	private void appendPerson(Document doc, Element parent, Person person)
	{
		if (person == null)
		{
			return;
		}
		Element personNode = doc.createElement(AimXml.NODE_PERSON);
		appendName(doc, personNode, person.getName());
		appendNodeWithAttr(doc, personNode, AimXml.NODE_PERSON_ID,
			AimXml.ATTR_VALUE, person.getId(), true);
		appendNodeWithAttr(doc, personNode, AimXml.NODE_PERSON_BIRTHDATE,
			AimXml.ATTR_VALUE, person.getBirthDate(), true);
		appendNodeWithAttr(doc, personNode, AimXml.NODE_PERSON_SEX,
			AimXml.ATTR_VALUE, person.getSex(), true);
		appendNodeWithAttr(doc, personNode, AimXml.NODE_PERSON_ETHNIC_GROUP,
			AimXml.ATTR_VALUE, person.getEthnicGroup(), true);
		parent.appendChild(personNode);
	}

	private void appendReference(Document doc, Element parent,
		ImageReference ref)
	{
		Element refNode = doc.createElement(AimXml.NODE_IMAGE_REFERENCE);
		appendUid(doc, refNode, ref.getUid());
		if (ref instanceof DicomImageReference)
		{
			if (buildDicomImageRef(doc, refNode, (DicomImageReference) ref))
			{
				parent.appendChild(refNode);
			}
		}
		else
		{
			logger.warn("Ignoring ImageReference class: "+
				ref.getClass().getCanonicalName());
		}
	}

	private void appendTypeCode(Document doc, Element parent, Code code)
	{
		Element tcNode = doc.createElement(AimXml.NODE_TYPECODE);
		tcNode.setAttribute(AimXml.ATTR_CODE, code.getCode());
		tcNode.setAttribute(AimXml.ATTR_CODE_SYSTEM, code.getCodeSystem());
		tcNode.setAttribute(AimXml.ATTR_CODE_SYSTEM_NAME,
			code.getCodeSystemName());
		tcNode.setAttribute(AimXml.ATTR_CODE_SYSTEM_VERSION,
			code.getCodeSystemVersion());
		parent.appendChild(tcNode);
	}

	private void appendUid(Document doc, Element parent, String uid)
	{
		appendNodeWithAttr(doc, parent, AimXml.NODE_UID,
			AimXml.ATTR_ROOT, uid, true);
	}

	private void appendUser(Document doc, Element parent, User user)
	{
		if (user == null)
		{
			return;
		}
		Element userNode = doc.createElement(AimXml.NODE_USER);
		appendName(doc, userNode, user.getName());
		appendNodeWithAttr(doc, userNode, AimXml.NODE_USER_LOGIN_NAME,
			AimXml.ATTR_VALUE, user.getLoginName(), true);
		appendNodeWithAttr(doc, userNode, AimXml.NODE_USER_ROLE,
			AimXml.ATTR_VALUE, user.getRoleInTrial(), true);
		int number = user.getNumberWithinRoleOfClinicalTrial();
		if (number > 0)
		{
			appendNodeWithAttr(doc, userNode, AimXml.NODE_USER_NUMBER_IN_ROLE,
				AimXml.ATTR_VALUE, Integer.toString(number));
		}
		parent.appendChild(userNode);
	}

	private boolean build2DShape(Document doc, Element markupNode,
		TwoDimensionGeometricShape markup)
	{
		String className = markup.getClass().getSimpleName();
		switch (className)
		{
			case AimXml.TWO_DIMENSION_POLYLINE:
				break;
			default:
				return false;
		}
		markupNode.setAttribute(AimXml.ATTR_XSI_TYPE, className);
		appendUid(doc, markupNode, markup.getUid());
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_SHAPE_ID,
			AimXml.ATTR_VALUE, Integer.toString(markup.getShapeId()), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_INCLUDE_FLAG,
			AimXml.ATTR_VALUE, Boolean.toString(markup.getIncludeFlag()), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_LINE_COLOUR,
			AimXml.ATTR_VALUE, markup.getLineColour(), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_LINE_OPACITY,
			AimXml.ATTR_VALUE, markup.getLineOpacity(), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_LINE_STYLE,
			AimXml.ATTR_VALUE, markup.getLineStyle(), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_LINE_THICKNESS,
			AimXml.ATTR_VALUE, markup.getLineThickness(), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_IMAGE_REFERENCE_UID,
			AimXml.ATTR_ROOT, markup.getImageReferenceUid(), true);
		appendNodeWithAttr(doc, markupNode, AimXml.NODE_REFERENCED_FRAME_NUMBER,
			AimXml.ATTR_VALUE, Integer.toString(markup.getReferencedFrameNumber()),
			true);
		Element twoDCoordCollNode = doc.createElement(
			AimXml.NODE_TWO_DIMENSION_COORDINATE_COLLECTION);
		for (TwoDimensionCoordinate coord : markup.getCoordinateList())
		{
			append2DCoord(doc, twoDCoordCollNode, coord);
		}
		markupNode.appendChild(twoDCoordCollNode);
		return true;
	}

	private boolean buildDicomImageRef(Document doc, Element refNode,
		DicomImageReference ref)
	{
		refNode.setAttribute(AimXml.ATTR_XSI_TYPE,
			AimXml.DICOM_IMAGE_REFERENCE_ENTITY);
		appendImageStudy(doc, refNode, ref.getStudy());
		
		return true;
	}

	private Document createDocument(ImageAnnotationCollection iac)
		throws ParserConfigurationException
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		Element root = doc.createElement(AimXml.NODE_ANNOTATION_COLLECTION);
		root.setAttribute(AimXml.ATTR_XMLNS,
			"gme://caCORE.caCORE/4.4/edu.northwestern.radiology.AIM");
		root.setAttribute(AimXml.ATTR_XMLNS_RDF,
			"http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		root.setAttribute(AimXml.ATTR_XMLNS_XSI,
			"http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute(AimXml.ATTR_AIM_VERSION, "AIMv4_0");
		root.setAttribute(AimXml.ATTR_XSI_SCHEMA_LOCATION,
			 "gme://caCORE.caCORE/4.4/edu.northwestern.radiology.AIM AIM_v4_rv44_XML.xsd");
		doc.appendChild(root);
		appendUid(doc, root, iac.getUid());
		appendDateTime(doc, root, iac.getDateTime());
		appendDescription(doc, root, iac.getDescription());
		appendUser(doc, root, iac.getUser());
		appendEquipment(doc, root, iac.getEquipment());
		appendPerson(doc, root, iac.getPerson());
		appendAnnotations(doc, root, iac.getAnnotationList());

		return doc;
	}

	private void transform(Document doc, StreamResult result)
		throws TransformerException
	{
		TransformerFactory transformerFactory =
			TransformerFactory.newInstance();
		try
		{
			transformerFactory.setAttribute("indent-number", 3);
		}
		catch (IllegalArgumentException ex)
		{
			// MATLAB uses Saxon 6.5 instead of the standard JDK Xalan
			logger.warn("Attribute 'indent-number' not supported by TransformerFactory: "+
				transformerFactory.getClass().getCanonicalName());
		}
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
	}

}
