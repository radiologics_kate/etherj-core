/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicArrowButton;

/**
 *
 * @author jamesd
 */
public final class DropDownButton extends JButton
{
	private final JButton dropButton;
	private final JButton mainButton;
	private final JPopupMenu popupMenu;

	public DropDownButton(Icon icon, final JPopupMenu popupMenu)
	{
		super();
		this.popupMenu = popupMenu;
		mainButton = new JButton(icon);
		dropButton = new BasicArrowButton(SwingConstants.SOUTH);
		dropButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onDropActionPerformed();
			}
		});

		setBorderPainted(false);
		dropButton.setBorderPainted(false);
		mainButton.setBorderPainted(false);
		int height = icon.getIconHeight()+4;
		int width = icon.getIconWidth()+4+16;
		setPreferredSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		setLayout(new BorderLayout());
		setMargin(new Insets(-3, -3,-3,-3));
		add(mainButton, BorderLayout.CENTER);
		add(dropButton, BorderLayout.EAST);
	}

	@Override
	public void addActionListener(ActionListener al)
	{
		super.addActionListener(al);
		mainButton.addActionListener(al);
	}

	@Override
	public void removeActionListener(ActionListener al)
	{
		super.removeActionListener(al);
		mainButton.removeActionListener(al);
	}

	@Override
	public void setEnabled(boolean enable)
	{
		super.setEnabled(enable);
		mainButton.setEnabled(enable);
		dropButton.setEnabled(enable);
	}

	private void onDropActionPerformed()
	{
		if (popupMenu.isVisible())
		{
			popupMenu.setVisible(false);
		}
		else
		{
			popupMenu.show(this, 0, getHeight());
		}
	}

}
