/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.ui;

import icr.etherj.concurrent.TaskMonitor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $Id$
 *
 * @author James d'Arcy
 */
public class TaskMonitorDialog extends JDialog
{
	private static final Logger logger =
		LoggerFactory.getLogger(TaskMonitorDialog.class);
	private TaskMonitor taskMonitor = null;
	private JLabel desc = null;
	private JProgressBar progress = null;
	private JButton cancel = null;
	private IndeterminateTask task = null;
	private PropertyChangeListener taskMonitorListener = null;
	private boolean isInCancel = false;

	public TaskMonitorDialog(Window owner, TaskMonitor taskMonitor)
	{
		super(owner);
		this.taskMonitor = taskMonitor;
		init();
	}

	public TaskMonitorDialog(Window owner, Dialog.ModalityType modalityType,
		TaskMonitor taskMonitor)
	{
		super(owner, modalityType);
		this.taskMonitor = taskMonitor;
		init();
	}

	public TaskMonitorDialog(Window owner, String title, TaskMonitor taskMonitor)
	{
		super(owner, title);
		this.taskMonitor = taskMonitor;
		init();
	}

	public TaskMonitorDialog(Window owner, String title,
		Dialog.ModalityType modalityType, TaskMonitor taskMonitor)
	{
		super(owner, title, modalityType);
		this.taskMonitor = taskMonitor;
		init();
	}

	private void cancelTask()
	{
		isInCancel = true;
		taskMonitor.setCancelled(true);
		dispose();
	}

	@Override
	public void dispose()
	{
		super.dispose();
		task.cancel();
		taskMonitor.removePropertyChangeListener(taskMonitorListener);
	}

	private void init()
	{
		if (taskMonitor == null)
		{
			throw new IllegalArgumentException("taskMonitor must not be null");
		}
		taskMonitorListener = new TaskMonitorListener();
		taskMonitor.addPropertyChangeListener(taskMonitorListener);

		setTitle(taskMonitor.getTitle());

		JComponent content = (JComponent) getContentPane();
		GridBagLayout gbl = new GridBagLayout();
		content.setLayout(gbl);

		desc = new JLabel(taskMonitor.getDescription());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 2;
		gbc.weightx = 1.0;
		gbc.insets = new Insets(5,5,0,5);
		content.add(desc, gbc);

		progress = new JProgressBar(taskMonitor.getMinimum(),
			taskMonitor.getMaximum());
		progress.setValue(taskMonitor.getValue());
		progress.setPreferredSize(new Dimension(300, 24));
		gbc = new GridBagConstraints();
		gbc.gridwidth = taskMonitor.isCancellable() ? 1 : 2;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.insets = new Insets(5,5,5,5);
		content.add(progress, gbc);

		cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				cancelTask();
			}
		});
		if (taskMonitor.isCancellable())
		{
			gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.gridy = 1;
			gbc.fill = GridBagConstraints.NONE;
			gbc.insets = new Insets(5,0,5,0);
			content.add(cancel, gbc);
		}

		task = new IndeterminateTask(progress);

		pack();
	}

	@Override
	public void setVisible(boolean visible)
	{
		Rectangle frameBounds = getOwner().getBounds();
		Rectangle bounds = getBounds();
		setBounds(frameBounds.x+(frameBounds.width-bounds.width)/2,
			frameBounds.y+(frameBounds.height-bounds.height)/2,
			bounds.width, bounds.height);

		super.setVisible(visible);
		if (taskMonitor.isIndeterminate())
		{
			task.start();
		}
	}

	class TaskMonitorListener implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			if (evt.getPropertyName().equals(TaskMonitor.VALUE))
			{
				progress.setValue(taskMonitor.getValue());
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.INDETERMINATE))
			{
				javax.swing.SwingUtilities.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						boolean indeterminate = taskMonitor.isIndeterminate();
						progress.setIndeterminate(indeterminate);
						if (indeterminate)
						{
							task.start();
						}
						else
						{
							task.cancel();
						}
					}
				});
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.MAXIMUM))
			{
				progress.setMaximum(taskMonitor.getMaximum());
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.MINIMUM))
			{
				progress.setMinimum(taskMonitor.getMinimum());
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.DESCRIPTION))
			{
				desc.setText(taskMonitor.getDescription());
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.TITLE))
			{
				setTitle(taskMonitor.getTitle());
				return;
			}
			if (evt.getPropertyName().equals(TaskMonitor.CANCELLED))
			{
				if (taskMonitor.isCancelled() && !isInCancel)
				{
					dispose();
				}
			}
		}
	}
}
