/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.ui;

import icr.etherj.concurrent.AsyncTaskException;
import icr.etherj.concurrent.AsyncTaskResult;
import java.awt.Frame;
import java.util.concurrent.ExecutionException;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $Id$
 *
 * @author adminjamesd
 * @param <T>
 */
public class DialogAsyncTaskResult<T> implements AsyncTaskResult<T>
{
	private static final Logger logger =
		LoggerFactory.getLogger(DialogAsyncTaskResult.class);

	private Frame frame;
	private TaskMonitorDialog monitorDialog;
	private String desc;
	private String title;

	public DialogAsyncTaskResult(Frame frame, TaskMonitorDialog monitorDialog)
	{
		this(frame, monitorDialog, "Process", "Error");
	}

	public DialogAsyncTaskResult(Frame frame, TaskMonitorDialog monitorDialog,
		String description, String title)
	{
		this.frame = frame;
		this.monitorDialog = monitorDialog;
		desc = description;
		this.title = title;
	}

	@Override
	public void failure(AsyncTaskException ex)
	{
		frame.setCursor(null);
		monitorDialog.dispose();
		Throwable asyncCause = ex.getCause();
		if (asyncCause instanceof InterruptedException)
		{
			logger.warn("Thread interrupted. {} aborting.", desc);
			Thread.currentThread().interrupt();
		}
		else if (asyncCause instanceof ExecutionException)
		{
			Throwable cause = asyncCause.getCause();
			cause = (cause == null) ? asyncCause : cause;
			logger.error("Unexpected exception during execution", cause);
			JOptionPane.showMessageDialog(frame,
				"Error: "+cause.getClass().getSimpleName()+"\n\n"+
					cause.getMessage()+"\n\nSee log for details.",
				title, JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void success(T t)
	{
		frame.setCursor(null);
		monitorDialog.dispose();
		logger.info("{} complete.", desc);
	}

	protected String getDescription()
	{
		return desc;
	}

	protected Frame getFrame()
	{
		return frame;
	}

	protected TaskMonitorDialog getTaskMonitorDialog()
	{
		return monitorDialog;
	}
}
