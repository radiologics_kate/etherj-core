/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj;

import java.io.BufferedWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Utility class for XML handling methods.
 * @author jamesd
 */
public class Xml
{
	private static final org.slf4j.Logger logger =
		LoggerFactory.getLogger(Xml.class);

	/**
	 * Converts a <code>Document</code> to a string without throwing an exception.
	 * Any exception thrown will have its message stored in the returned string.
	 * @param doc the document to convert
	 * @return the document as a string
	 */
	public static String dump(Document doc)
	{
		String value;
		try
		{
			value = toString(doc, "UTF-8");
		}
		catch (XmlException ex)
		{
			value = "Exception thrown: "+ex.getMessage();
		}
		return value;
	}

	/**
	 * Returns the named attribute as a boolean, defaulting to false if it does
	 * not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @return the value of the attribute as a boolean or false if it does not
	 * exist
	 */
	public static boolean getAttrBoolean(NamedNodeMap attrs, String name)
	{
		return getAttrBoolean(attrs, name, false);
	}

	/**
	 * Returns the named attribute as a boolean or the default value if it does
	 * not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @param defaultValue the value to return if the attribute does not exist
	 * @return the value of the attribute as a boolean or the default value if it
	 * does not exist
	 */
	public static boolean getAttrBoolean(NamedNodeMap attrs, String name,
		boolean defaultValue)
	{
		boolean value = defaultValue;
		Node node = attrs.getNamedItem(name);
		if (node != null)
		{
			try
			{
				value = Boolean.parseBoolean(node.getNodeValue());
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Attr: '{}' not a valid boolean '{}'", name,
					node.getNodeValue());
			}
		}
		return value;
	}

	/**
	 * Returns the named attribute as a double, defaulting to <code>NaN</code> if
	 * it does not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @return the value of the attribute as a double or <code>NaN</code> if it 
	 * does not exist
	 */
	public static double getAttrDouble(NamedNodeMap attrs, String name)
	{
		return getAttrDouble(attrs, name, Double.NaN);
	}

	/**
	 * Returns the named attribute as a double or the default value if it does
	 * not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @param defaultValue the value to return if the attribute does not exist
	 * @return the value of the attribute as a double or the default value if it
	 * does not exist
	 */
	public static double getAttrDouble(NamedNodeMap attrs, String name,
		double defaultValue)
	{
		double value = defaultValue;
		Node node = attrs.getNamedItem(name);
		if (node != null)
		{
			try
			{
				value = Double.parseDouble(node.getNodeValue());
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Attr: '{}' not a valid double '{}'", name,
					node.getNodeValue());
			}
		}
		return value;
	}

	/**
	 * Returns the named attribute as an integer, defaulting to <code>-1</code> if
	 * it does not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @return the value of the attribute as an integer or <code>-1</code> if it 
	 * does not exist
	 */
	public static int getAttrInt(NamedNodeMap attrs, String name)
	{
		return getAttrInt(attrs, name, -1);
	}

	/**
	 * Returns the named attribute as an integer or the default value if it does
	 * not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @param defaultValue the value to return if the attribute does not exist
	 * @return the value of the attribute as an integer or the default value if it
	 * does not exist
	 */
	public static int getAttrInt(NamedNodeMap attrs, String name,
		int defaultValue)
	{
		int value = defaultValue;
		Node node = attrs.getNamedItem(name);
		if (node != null)
		{
			try
			{
				value = Integer.parseInt(node.getNodeValue());
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Attr: '{}' not a valid int '{}'", name,
					node.getNodeValue());
			}
		}
		return value;
	}
	
	/**
	 * Returns the named attribute as a string, defaulting to <code>null</code> if
	 * it does not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @return the value of the attribute as a string or <code>null</code> if it 
	 * does not exist
	 */
	public static String getAttrStr(NamedNodeMap attrs, String name)
	{
		return getAttrStr(attrs, name, null);
	}
	
	/**
	 * Returns the named attribute as a string or the default value if it does
	 * not exist.
	 * @param attrs the attribute nodes to search
	 * @param name the name of the attribute to search for
	 * @param defaultValue the value to return if the attribute does not exist
	 * @return the value of the attribute as a string or the default value if it
	 * does not exist
	 */
	public static String getAttrStr(NamedNodeMap attrs, String name,
		String defaultValue)
	{
		Node node = attrs.getNamedItem(name);
		return (node != null) ? node.getNodeValue() : defaultValue;
	}

	/**
	 * Returns the first child <code>Node</code> of the <code>Element</code> that
	 * matches the tagPath.
	 * @param element the element to search
	 * @param tagPath the tag path to match
	 * @return the first matching child node
	 */
	public static Node getFirstMatch(Element element, String tagPath)
	{
		String[] tags = tagPath.trim().split("/");
		List<String> list = new ArrayList<>();
		for (String tag : tags)
		{
			if (!tag.isEmpty())
			{
				list.add(tag);
			}
		}
		if (list.isEmpty())
		{
			return null;
		}
		return searchChildren(element, list.toArray(new String[0]));
	}

	/**
	 * Converts a <code>Document</code> to a string using the UTF-8 encoding.
	 * @param doc the document to convert
	 * @return the document as a string
	 * @throws XmlException if an error occurs
	 */
	public static String toString(Document doc)
		throws XmlException
	{
		return toString(doc, "UTF-8");
	}

	/**
	 * Converts a <code>Document</code> to a string.
	 * @param doc the document to convert
	 * @param encoding the encoding to use
	 * @return the document as a string
	 * @throws XmlException if an error occurs
	 */
	public static String toString(Document doc, String encoding)
		throws XmlException
	{
		StringWriter sw = new StringWriter();
		try
		{
			StreamResult result = new StreamResult(new BufferedWriter(sw));
			transform(doc, result);
		}
		catch (TransformerException ex)
		{
			throw new XmlException("Cannot conver Document to String", ex);
		}
		return sw.toString();
	}

	private static Node searchChildren(Node searchNode, String[] tags)
	{
		String tag = tags[0];
		tags = Arrays.copyOfRange(tags, 1, tags.length);
		NodeList children = searchNode.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			if (child.getNodeName().equals(tag))
			{
				if (tags.length > 0)
				{
					Node node = searchChildren(child, tags);
					if (node != null)
					{
						return node;
					}
				}
				else
				{
					return child;
				}
			}
		}
		return null;
	}

	private static void transform(Document doc, StreamResult result)
		throws TransformerException
	{
		TransformerFactory transformerFactory =
			TransformerFactory.newInstance();
		try
		{
			transformerFactory.setAttribute("indent-number", 3);
		}
		catch (IllegalArgumentException ex)
		{
			// MATLAB uses Saxon 6.5 instead of the standard JDK Xalan
			logger.warn("Attribute 'indent-number' not supported by TransformerFactory: "+
				transformerFactory.getClass().getCanonicalName());
		}
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
	}

	private Xml()
	{}
}
