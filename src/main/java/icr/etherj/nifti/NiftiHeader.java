/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface NiftiHeader extends Displayable
{
	public static final int Length_v1_1 = 348;
	public static final int Length_v2 = 540;
	public static final String Magic_v1_1 = "n+1";

	/**
	 * @return the affineX
	 */
	public double[] getAffineX();

	/**
	 * @return the affineY
	 */
	public double[] getAffineY();

	/**
	 * @return the affineZ
	 */
	public double[] getAffineZ();

	/**
	 * @return the auxFileName
	 */
	public String getAuxFileName();

	/**
	 * @return the bitsPerPixel
	 */
	public short getBitsPerPixel();

	/**
	 * @return the dataType
	 */
	public short getDataType();

	/**
	 * @return the description
	 */
	public String getDescription();

	/**
	 * @return returns the dimension info
	 */
	public byte getDimensionInfo();

	/**
	 * @return the dimensions
	 */
	public long[] getDimensions();

	/**
	 * @return the firstSliceIndex
	 */
	public short getFirstSliceIndex();

	/**
	 * @return the intentCode
	 */
	public short getIntentCode();

	/**
	 * @return the intentName
	 */
	public String getIntentName();

	/**
	 * @return the intentParameters
	 */
	public float[] getIntentParameters();

	/**
	 * @return the intercept
	 */
	public float getIntercept();

	/**
	 * @return the lastSliceIndex
	 */
	public short getLastSliceIndex();

	/**
	 * @return the length
	 */
	public int getLength();

	/**
	 * @return the magic
	 */
	public String getMagic();

	/**
	 * @return the maxDisplay
	 */
	public float getMaxDisplay();

	/**
	 * @return the minDisplay
	 */
	public float getMinDisplay();

	/**
	 * @return the pixelSpacing
	 */
	public double[] getPixelSpacing();

	/**
	 * @return the quaternionB
	 */
	public double getQuaternionB();

	/**
	 * @return the quaternionC
	 */
	public double getQuaternionC();

	/**
	 * @return the quaternionD
	 */
	public double getQuaternionD();

	/**
	 * @return the quaternionXShift
	 */
	public double getQuaternionXShift();

	/**
	 * @return the quaternionYShift
	 */
	public double getQuaternionYShift();

	/**
	 * @return the quaternionZShift
	 */
	public double getQuaternionZShift();

	/**
	 * @return the qFormCode
	 */
	public short getQFormCode();

	/**
	 * @return the sFormCode
	 */
	public short getSFormCode();

	/**
	 * @return the sliceDuration
	 */
	public float getSliceDuration();

	/**
	 * @return the sliceTimingOrder
	 */
	public byte getSliceTimingOrder();

	/**
	 * @return the slope
	 */
	public float getSlope();

	/**
	 * @return the timeOffset
	 */
	public float getTimeOffset();

	/**
	 * @return the voxelOffset
	 */
	public float getVoxelOffset();

	/**
	 * @return the xyztUnits
	 */
	public byte getXyztUnits();

	/**
	 * @param affineX the affineX to set
	 */
	public void setAffineX(double[] affineX);

	/**
	 * @param affineY the affineY to set
	 */
	public void setAffineY(double[] affineY);

	/**
	 * @param affineZ the affineZ to set
	 */
	public void setAffineZ(double[] affineZ);

	/**
	 * @param auxFileName the auxFileName to set
	 */
	public void setAuxFileName(String auxFileName);

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(short dataType);

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description);

	/**
	 * @param dimInfo the dimInfo to set
	 */
	public void setDimensionInfo(byte dimInfo);

	/**
	 * @param dimensions the dimensions to set
	 */
	public void setDimensions(long[] dimensions);

	/**
	 * @param firstSliceIndex the firstSliceIndex to set
	 */
	public void setFirstSliceIndex(short firstSliceIndex);

	/**
	 * @param intentCode the intentCode to set
	 */
	public void setIntentCode(short intentCode);

	/**
	 * @param intentName the intentName to set
	 */
	public void setIntentName(String intentName);

	/**
	 * @param intentParameters the intentParameters to set
	 */
	public void setIntentParameters(float[] intentParameters);

	/**
	 * @param intercept the intercept to set
	 */
	public void setIntercept(float intercept);

	/**
	 * @param lastSliceIndex the lastSliceIndex to set
	 */
	public void setLastSliceIndex(short lastSliceIndex);

	/**
	 * @param length the length to set
	 */
	public void setLength(int length);
	
	/**
	 * @param magic the magic to set
	 */
	public void setMagic(String magic);

	/**
	 * @param maxDisplay the maxDisplay to set
	 */
	public void setMaxDisplay(float maxDisplay);

	/**
	 * @param minDisplay the minDisplay to set
	 */
	public void setMinDisplay(float minDisplay);

	/**
	 * @param pixelSpacing the pixelSpacing to set
	 */
	public void setPixelSpacing(double[] pixelSpacing);

	/**
	 * @param quaternionB the quaternionB to set
	 */
	public void setQuaternionB(double quaternionB);

	/**
	 * @param quaternionC the quaternionC to set
	 */
	public void setQuaternionC(double quaternionC);

	/**
	 * @param quaternionD the quaternionD to set
	 */
	public void setQuaternionD(double quaternionD);

	/**
	 * @param quaternionXShift the quaternionXShift to set
	 */
	public void setQuaternionXShift(double quaternionXShift);

	/**
	 * @param quaternionYShift the quaternionYShift to set
	 */
	public void setQuaternionYShift(double quaternionYShift);

	/**
	 * @param quaternionZShift the quaternionZShift to set
	 */
	public void setQuaternionZShift(double quaternionZShift);

	/**
	 * @param qFormCode the qFormCode to set
	 */
	public void setQFormCode(short qFormCode);

	/**
	 * @param sFormCode the sFormCode to set
	 */
	public void setSFormCode(short sFormCode);

	/**
	 * @param sliceDuration the sliceDuration to set
	 */
	public void setSliceDuration(float sliceDuration);

	/**
	 * @param sliceTimingOrder the sliceTimingOrder to set
	 */
	public void setSliceTimingOrder(byte sliceTimingOrder);

	/**
	 * @param slope the slope to set
	 */
	public void setSlope(float slope);

	/**
	 * @param timeOffset the timeOffset to set
	 */
	public void setTimeOffset(float timeOffset);

	/**
	 * @param voxelOffset the voxelOffset to set
	 */
	public void setVoxelOffset(float voxelOffset);

	/**
	 * @param xyztUnits the xyztUnits to set
	 */
	public void setXyztUnits(byte xyztUnits);

}
