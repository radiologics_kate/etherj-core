/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
class DefaultNiftiHeader extends AbstractDisplayable implements NiftiHeader
{
	private double[] affineX = new double[4];
	private double[] affineY = new double[4];
	private double[] affineZ = new double[4];
	private String auxFileName = "";
	private short bitsPerPixel;
	private short dataType;
	private String description = "";
	private long[] dimensions = new long[8];
	private byte dimInfo = 0;
	private short firstSliceIndex;
	private short intentCode;
	private String intentName = "";
	private float[] intentParameters = new float[3];
	private float intercept = 0;
	private short lastSliceIndex;
	private int length = 0;
	private String magic = "";
	private float maxDisplay;
	private float minDisplay;
	private double[] pixelSpacing = new double[8];
	private short qFormCode;
	private double quaternionB;
	private double quaternionC;
	private double quaternionD;
	private double quaternionXShift;
	private double quaternionYShift;
	private double quaternionZShift;
	private short sFormCode;
	private float sliceDuration;
	private byte sliceTimingOrder;
	private float slope = 1;
	private float timeOffset = 0;
	private float voxelOffset = 0;
	private byte xyztUnits;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Length: "+length);
		ps.println(pad+"DimInfo: "+dimInfo);
		ps.println(pad+"Dimensions: "+formatArray(dimensions));
		ps.println(pad+"IntentParameters: "+formatArray(intentParameters));
		ps.println(pad+"IntentCode: "+NiftiUtils.getIntentName(intentCode));
		ps.println(pad+"DataType: "+NiftiUtils.getTypeName(dataType));
		ps.println(pad+"BitsPerPixel: "+bitsPerPixel);
		ps.println(pad+"FirstSliceIndex: "+firstSliceIndex);
		ps.println(pad+"PixelSpacing: "+formatArray(pixelSpacing));
		ps.println(pad+"VoxelOffset: "+voxelOffset);
		ps.println(pad+"Slope: "+slope);
		ps.println(pad+"Intercept: "+intercept);
		ps.println(pad+"LastSliceIndex: "+lastSliceIndex);
		ps.println(pad+"SliceTimingOrder: "+sliceTimingOrder);
		String[] units = NiftiUtils.getUnitNames(xyztUnits);
		ps.println(pad+"XyztUnits: "+formatArray(units));
		ps.println(pad+"MaxDisplay: "+maxDisplay);
		ps.println(pad+"MinDisplay: "+minDisplay);
		ps.println(pad+"SliceDuration: "+sliceDuration);
		ps.println(pad+"TimeOffset: "+timeOffset);
		ps.println(pad+"Description: "+description);
		ps.println(pad+"AuxFileName: "+auxFileName);
		ps.println(pad+"QFormCode: "+qFormCode);
		ps.println(pad+"SFormCode: "+sFormCode);
		ps.println(pad+"QuaternionB: "+quaternionB);
		ps.println(pad+"QuaternionC: "+quaternionC);
		ps.println(pad+"QuaternionD: "+quaternionD);
		ps.println(pad+"QuaternionXShift: "+quaternionXShift);
		ps.println(pad+"QuaternionYShift: "+quaternionYShift);
		ps.println(pad+"QuaternionZShift: "+quaternionZShift);
		ps.println(pad+"AffineX: "+formatArray(affineX));
		ps.println(pad+"AffineY: "+formatArray(affineY));
		ps.println(pad+"AffineZ: "+formatArray(affineZ));
		ps.println(pad+"IntentName: "+intentName);
		ps.println(pad+"Magic: "+magic);
		
	}

	@Override
	public double[] getAffineX()
	{
		return Arrays.copyOf(affineX, 4);
	}

	@Override
	public double[] getAffineY()
	{
		return Arrays.copyOf(affineY, 4);
	}

	@Override
	public double[] getAffineZ()
	{
		return Arrays.copyOf(affineZ, 4);
	}

	@Override
	public String getAuxFileName()
	{
		return auxFileName;
	}

	@Override
	public short getBitsPerPixel()
	{
		return bitsPerPixel;
	}

	@Override
	public short getDataType()
	{
		return dataType;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	@Override
	public byte getDimensionInfo()
	{
		return dimInfo;
	}

	@Override
	public long[] getDimensions()
	{
		return Arrays.copyOf(dimensions, 8);
	}

	@Override
	public short getFirstSliceIndex()
	{
		return firstSliceIndex;
	}

	@Override
	public short getIntentCode()
	{
		return intentCode;
	}

	@Override
	public String getIntentName()
	{
		return intentName;
	}

	@Override
	public float[] getIntentParameters()
	{
		return Arrays.copyOf(intentParameters, 3);
	}

	@Override
	public float getIntercept()
	{
		return intercept;
	}

	@Override
	public short getLastSliceIndex()
	{
		return lastSliceIndex;
	}

	@Override
	public int getLength()
	{
		return length;
	}

	@Override
	public String getMagic()
	{
		return magic;
	}

	@Override
	public float getMaxDisplay()
	{
		return maxDisplay;
	}

	@Override
	public float getMinDisplay()
	{
		return minDisplay;
	}

	@Override
	public double[] getPixelSpacing()
	{
		return Arrays.copyOf(pixelSpacing, 8);
	}

	@Override
	public double getQuaternionB()
	{
		return quaternionB;
	}

	@Override
	public double getQuaternionC()
	{
		return quaternionC;
	}

	@Override
	public double getQuaternionD()
	{
		return quaternionD;
	}

	@Override
	public double getQuaternionXShift()
	{
		return quaternionXShift;
	}

	@Override
	public double getQuaternionYShift()
	{
		return quaternionYShift;
	}

	@Override
	public double getQuaternionZShift()
	{
		return quaternionZShift;
	}

	@Override
	public short getQFormCode()
	{
		return qFormCode;
	}

	@Override
	public short getSFormCode()
	{
		return sFormCode;
	}

	@Override
	public float getSliceDuration()
	{
		return sliceDuration;
	}

	@Override
	public byte getSliceTimingOrder()
	{
		return sliceTimingOrder;
	}

	@Override
	public float getSlope()
	{
		return slope;
	}

	@Override
	public float getTimeOffset()
	{
		return timeOffset;
	}

	@Override
	public float getVoxelOffset()
	{
		return voxelOffset;
	}

	@Override
	public byte getXyztUnits()
	{
		return xyztUnits;
	}

	@Override
	public void setAffineX(double[] affineX)
	{
		this.affineX = Arrays.copyOf(affineX, 4);
	}

	@Override
	public void setAffineY(double[] affineY)
	{
		this.affineY = Arrays.copyOf(affineY, 4);
	}

	@Override
	public void setAffineZ(double[] affineZ)
	{
		this.affineZ = Arrays.copyOf(affineZ, 4);
	}

	@Override
	public void setAuxFileName(String auxFileName)
	{
		this.auxFileName = auxFileName;
	}

	@Override
	public void setDataType(short dataType)
	{
		switch (dataType)
		{
			case Nifti.Uint8:
			case Nifti.Int8:
				bitsPerPixel = 8;
				break;
			case Nifti.Int16:
			case Nifti.Uint16:
				bitsPerPixel = 16;
				break;
			case Nifti.Int32:
			case Nifti.Float32:
			case Nifti.Uint32:
			case Nifti.Rgba32:
				bitsPerPixel = 32;
				break;
			case Nifti.Complex64:
			case Nifti.Float64:
			case Nifti.Int64:
			case Nifti.Uint64:
				bitsPerPixel = 64;
				break;
			case Nifti.Float128:
			case Nifti.Complex128:
				bitsPerPixel = 128;
				break;
			case Nifti.Complex256:
				bitsPerPixel = 256;
				break;
			case Nifti.Rgb24:
				bitsPerPixel = 24;
				break;
			default:
				throw new IllegalArgumentException("Invalid data type: "+dataType);
		}
		this.dataType = dataType;
	}

	@Override
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public void setDimensionInfo(byte dimInfo)
	{
		this.dimInfo = dimInfo;
	}

	@Override
	public void setDimensions(long[] dimensions)
	{
		this.dimensions = Arrays.copyOf(dimensions, 8);
	}

	@Override
	public void setFirstSliceIndex(short firstSliceIndex)
	{
		this.firstSliceIndex = firstSliceIndex;
	}

	@Override
	public void setIntentCode(short intentCode)
	{
		this.intentCode = intentCode;
	}

	@Override
	public void setIntentName(String intentName)
	{
		this.intentName = intentName;
	}

	@Override
	public void setIntentParameters(float[] intentParameters)
	{
		this.intentParameters = intentParameters;
	}

	@Override
	public void setIntercept(float intercept)
	{
		this.intercept = intercept;
	}

	@Override
	public void setLastSliceIndex(short lastSliceIndex)
	{
		this.lastSliceIndex = lastSliceIndex;
	}

	@Override
	public void setLength(int length)
	{
		this.length = length;
	}

	@Override
	public void setMagic(String magic)
	{
		this.magic = magic;
	}

	@Override
	public void setMaxDisplay(float maxDisplay)
	{
		this.maxDisplay = maxDisplay;
	}

	@Override
	public void setMinDisplay(float minDisplay)
	{
		this.minDisplay = minDisplay;
	}

	@Override
	public void setPixelSpacing(double[] pixelSpacing)
	{
		this.pixelSpacing = Arrays.copyOf(pixelSpacing, 8);
	}

	@Override
	public void setQuaternionB(double quaternionB)
	{
		this.quaternionB = quaternionB;
	}

	@Override
	public void setQuaternionC(double quaternionC)
	{
		this.quaternionC = quaternionC;
	}

	@Override
	public void setQuaternionD(double quaternionD)
	{
		this.quaternionD = quaternionD;
	}

	@Override
	public void setQuaternionXShift(double quaternionXShift)
	{
		this.quaternionXShift = quaternionXShift;
	}

	@Override
	public void setQuaternionYShift(double quaternionYShift)
	{
		this.quaternionYShift = quaternionYShift;
	}

	@Override
	public void setQuaternionZShift(double quaternionZShift)
	{
		this.quaternionZShift = quaternionZShift;
	}

	@Override
	public void setQFormCode(short qFormCode)
	{
		this.qFormCode = qFormCode;
	}

	@Override
	public void setSFormCode(short sFormCode)
	{
		this.sFormCode = sFormCode;
	}

	@Override
	public void setSliceDuration(float sliceDuration)
	{
		this.sliceDuration = sliceDuration;
	}

	@Override
	public void setSliceTimingOrder(byte sliceTimingOrder)
	{
		this.sliceTimingOrder = sliceTimingOrder;
	}

	@Override
	public void setSlope(float slope)
	{
		this.slope = slope;
	}

	@Override
	public void setTimeOffset(float timeOffset)
	{
		this.timeOffset = timeOffset;
	}

	@Override
	public void setVoxelOffset(float voxelOffset)
	{
		this.voxelOffset = voxelOffset;
	}

	@Override
	public void setXyztUnits(byte xyztUnits)
	{
		this.xyztUnits = xyztUnits;
	}

	private String formatArray(double[] array)
	{
		if ((array == null) || (array.length == 0))
		{
			return "[]";
		}
		StringBuilder sb = new StringBuilder("[").append(array[0]);
		for (int i=1; i<array.length; i++)
		{
			sb.append(",").append(array[i]);
		}
		sb.append("]");
		return sb.toString();
	}

	private String formatArray(float[] array)
	{
		if ((array == null) || (array.length == 0))
		{
			return "[]";
		}
		StringBuilder sb = new StringBuilder("[").append(array[0]);
		for (int i=1; i<array.length; i++)
		{
			sb.append(",").append(array[i]);
		}
		sb.append("]");
		return sb.toString();
	}

	private String formatArray(long[] array)
	{
		if ((array == null) || (array.length == 0))
		{
			return "[]";
		}
		StringBuilder sb = new StringBuilder("[").append(array[0]);
		for (int i=1; i<array.length; i++)
		{
			sb.append(",").append(array[i]);
		}
		sb.append("]");
		return sb.toString();
	}

	private String formatArray(String[] array)
	{
		if ((array == null) || (array.length == 0))
		{
			return "[]";
		}
		StringBuilder sb = new StringBuilder("[").append(array[0]);
		for (int i=1; i<array.length; i++)
		{
			sb.append(",").append(array[i]);
		}
		sb.append("]");
		return sb.toString();
	}
}
