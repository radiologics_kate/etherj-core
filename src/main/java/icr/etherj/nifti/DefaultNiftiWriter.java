/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import com.google.common.io.LittleEndianDataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class DefaultNiftiWriter implements NiftiWriter
{
	private final static Logger logger = LoggerFactory.getLogger(
		DefaultNiftiWriter.class);

	@Override
	public void write(Nifti nifti, OutputStream os) throws IOException
	{
		writeStream(nifti, os);
	}

	@Override
	public void write(Nifti nifti, String filePath) throws IOException
	{
		Path path = Paths.get(filePath);
		File file = path.toFile();
		if (file.isDirectory())
		{
			throw new IOException("Target is a directory: "+filePath);
		}
		if (file.exists() && !file.canWrite())
		{
			throw new IOException("No write permission to:"+filePath);
		}
		try (OutputStream os = Files.newOutputStream(path))
		{
			writeStream(nifti, os);
		}
	}

	private void writeDataBytes(Nifti nifti, OutputStream os) throws IOException
	{
		LittleEndianDataOutputStream dos = new LittleEndianDataOutputStream(os);
		short dataType = nifti.getHeader().getDataType();
		switch (dataType)
		{
			case Nifti.Int16:
			case Nifti.Uint16:
				short[] shortData = nifti.getShortData();
				logger.debug("Writing "+NiftiUtils.getTypeName(dataType)+" data ("+
					shortData.length+" items)");
				writeShortArray(dos, shortData);
				break;
			default:
				throw new IOException(
					"Data type not supported: "+NiftiUtils.getTypeName(dataType));
		}
	}

	private void writeFloatArray(LittleEndianDataOutputStream dos, float[] array)
		throws IOException
	{
		for (int i=0; i<array.length; i++)
		{
			dos.writeFloat(array[i]);
		}		
	}

	private void writeFloatArray(LittleEndianDataOutputStream dos, double[] array)
		throws IOException
	{
		for (int i=0; i<array.length; i++)
		{
			dos.writeFloat((float)array[i]);
		}		
	}

	private void writeHeader(NiftiHeader header, OutputStream os)
		throws IOException
	{
		if (NiftiHeader.Magic_v1_1.equals(header.getMagic()))
		{
			writeHeader_v1p1(header, os);
		}
		else
		{
			throw new IOException("Versions other than 1.1 not supported yet");
		}
	}

	private void writeHeader_v1p1(NiftiHeader header, OutputStream os)
		throws IOException
	{
		logger.debug("Writing v1.1 header");
		LittleEndianDataOutputStream dos = new LittleEndianDataOutputStream(os);

		dos.writeInt(NiftiHeader.Length_v1_1);
		dos.write(new byte[35]);
		dos.write(header.getDimensionInfo());
		writeShortArray(dos, header.getDimensions());
		writeFloatArray(dos, header.getIntentParameters());
		dos.writeShort(header.getIntentCode());
		dos.writeShort(header.getDataType());
		dos.writeShort(header.getBitsPerPixel());
		dos.writeShort(header.getFirstSliceIndex());
		writeFloatArray(dos, header.getPixelSpacing());
		dos.writeFloat(header.getVoxelOffset());
		dos.writeFloat(header.getSlope());
		dos.writeFloat(header.getIntercept());
		dos.writeShort(header.getLastSliceIndex());
		dos.write(header.getSliceTimingOrder());
		dos.write(header.getXyztUnits());
		dos.writeFloat(header.getMaxDisplay());
		dos.writeFloat(header.getMinDisplay());
		dos.writeFloat(header.getSliceDuration());
		dos.writeFloat(header.getTimeOffset());
		dos.write(new byte[8]);
		writeString(dos, header.getDescription(), 80);
		writeString(dos, header.getAuxFileName(), 24);
		dos.writeShort(header.getQFormCode());
		dos.writeShort(header.getSFormCode());
		dos.writeFloat((float) header.getQuaternionB());
		dos.writeFloat((float) header.getQuaternionC());
		dos.writeFloat((float) header.getQuaternionD());
		dos.writeFloat((float) header.getQuaternionXShift());
		dos.writeFloat((float) header.getQuaternionYShift());
		dos.writeFloat((float) header.getQuaternionZShift());
		writeFloatArray(dos, header.getAffineX());
		writeFloatArray(dos, header.getAffineY());
		writeFloatArray(dos, header.getAffineZ());
		writeString(dos, header.getIntentName(), 16);
		writeString(dos, header.getMagic(), 4);
		// TODO: Handle extensions
		dos.write(new byte[4]);
	}

	private void writeShortArray(LittleEndianDataOutputStream dos, long[] array)
		throws IOException
	{
		for (int i=0; i<array.length; i++)
		{
			dos.writeShort((short)array[i]);
		}
	}

	private void writeShortArray(LittleEndianDataOutputStream dos, short[] array)
		throws IOException
	{
		for (int i=0; i<array.length; i++)
		{
			dos.writeShort(array[i]);
		}
	}

	private void writeStream(Nifti nifti, OutputStream os) throws IOException
	{
		try (GZIPOutputStream gzos = new GZIPOutputStream(os))
		{
			writeHeader(nifti.getHeader(), gzos);
			writeDataBytes(nifti, gzos);
			gzos.finish();
		}
	}

	private void writeString(LittleEndianDataOutputStream dos, String value,
		int length) throws IOException
	{
		byte[] valueBytes = value.getBytes();
		byte[] allBytes = new byte[length];
		int toCopy = (valueBytes.length > length) ? length : valueBytes.length;
		System.arraycopy(valueBytes, 0, allBytes, 0, toCopy);
		dos.write(allBytes);
	}
	
}
