package icr.etherj.nifti;


/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class NiftiUtils
{
	private static final Map<Integer,String> intentNames = new HashMap<>();
	private static final Map<Short,String> typeNames = new HashMap<>();
	private static final Map<Byte,String> unitNames = new HashMap<>();

	static
	{
		typeNames.put(Nifti.Uint8, "Uint8");
		typeNames.put(Nifti.Int16, "Int16");
		typeNames.put(Nifti.Int32, "Int32");
		typeNames.put(Nifti.Float32, "Float32");
		typeNames.put(Nifti.Complex64, "Complex64");
		typeNames.put(Nifti.Float64, "Float64");
		typeNames.put(Nifti.Rgb24, "Rgb24");
		typeNames.put(Nifti.Int8, "Int8");
		typeNames.put(Nifti.Uint16, "Uint16");
		typeNames.put(Nifti.Uint32, "Uint32");
		typeNames.put(Nifti.Int64, "Int64");
		typeNames.put(Nifti.Uint64, "Uint64");
		typeNames.put(Nifti.Float128, "Float128");
		typeNames.put(Nifti.Complex128, "Complex128");
		typeNames.put(Nifti.Complex256, "Complex256");
		typeNames.put(Nifti.Rgba32, "Rgba32");

		intentNames.put(Nifti.IntentNone, "None");

		unitNames.put(Nifti.UnitsUnknown, "Unknown");
		unitNames.put(Nifti.UnitsMeters, "m");
		unitNames.put(Nifti.UnitsMillimeters, "mm");
		unitNames.put(Nifti.UnitsMicrons, "um");
		unitNames.put(Nifti.UnitsSeconds, "s");
		unitNames.put(Nifti.UnitsMilliseconds, "ms");
		unitNames.put(Nifti.UnitsMicroseconds, "us");
		unitNames.put(Nifti.UnitsHz, "s");
		unitNames.put(Nifti.UnitsPpm, "ppm");
		unitNames.put(Nifti.UnitsRadPerSec, "rad/s");
	}

	/**
	 *
	 * @param intent
	 * @return
	 */
	public static String getIntentName(int intent)
	{
		return intentNames.getOrDefault(intent, "Unknown");
	}

	/**
	 *
	 * @param type
	 * @return
	 */
	public static String getTypeName(short type)
	{
		return typeNames.getOrDefault(type, "Unknown");
	}

	/**
	 *
	 * @param xyztUnits
	 * @return
	 */
	public static String[] getUnitNames(byte xyztUnits)
	{
		String[] units = new String[]{
			unitNames.getOrDefault((byte)(xyztUnits & 0x07), "Unknown"),
			unitNames.getOrDefault((byte)(xyztUnits & 0x38), "Unknown")
		};
		return units;
	}

	public static Nifti readNiftiFile(String path) throws IOException
	{
		Nifti nifti;
		try (InputStream is = Files.newInputStream(Paths.get(path)))
		{
			NiftiReader reader = NiftiToolkit.getToolkit().createReader();
			nifti = reader.read(is);
		}
		return nifti;
	}

	private NiftiUtils()
	{}
}
