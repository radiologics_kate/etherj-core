/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface Nifti extends Displayable
{
	public static final short Uint8 = 2;
	public static final short Int16 = 4;
	public static final short Int32 = 8;
	public static final short Float32 = 16;
	public static final short Complex64 = 32;
	public static final short Float64 = 64;
	public static final short Rgb24 = 128;
	public static final short Int8 = 256;
	public static final short Uint16 = 512;
	public static final short Uint32 = 768;
	public static final short Int64 = 1024;
	public static final short Uint64 = 1280;
	public static final short Float128 = 1536;
	public static final short Complex128 = 1792;
	public static final short Complex256 = 2048;
	public static final short Rgba32 = 2304;

	public static final int IntentNone = 0;

	public static final byte UnitsUnknown = 0;
	public static final byte UnitsMeters = 1;
	public static final byte UnitsMillimeters = 2;
	public static final byte UnitsMicrons = 3;
	public static final byte UnitsSeconds = 8;
	public static final byte UnitsMilliseconds = 16;
	public static final byte UnitsMicroseconds = 24;
	public static final byte UnitsHz = 32;
	public static final byte UnitsPpm = 40;
	public static final byte UnitsRadPerSec = 48;

	public static final short TransformUnknown = 0;
	public static final short TransformScanner = 1;
	public static final short TransformAligned = 2;
	public static final short TransformTalairach = 3;
	public static final short TransformMni152 = 4;
	public static final short TransformOther = 5;

	/**
	 * Returns the number of data elements.
	 * @return
	 */
	int getDataLength();

	/**
	 * Returns the header.
	 * @return
	 */
	NiftiHeader getHeader();

	/**
	 *	Returns the data or null if data type is not 16 bit integers.
	 * @return
	 */
	short[] getShortData();

	/**
	 *	Sets the data or throws if data type is not 16 bit integers or is
	 * incorrect length.
	 * @param data
	 */
	void setShortData(short[] data) throws IllegalArgumentException;
}
