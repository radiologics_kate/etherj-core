/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
class DefaultNifti extends AbstractDisplayable implements Nifti
{
	private final NiftiHeader header;
	
	private short[] shortData = null;

	DefaultNifti(NiftiHeader header)
	{
		this.header = header;
		buildDataArray();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		header.display(ps, indent+"  ");
		if (shortData != null)
		{
			ps.println(pad+"ShortData: "+shortData.length+" element"+
				((shortData.length != 1) ? "s" : ""));
		}
	}

	@Override
	public int getDataLength()
	{
		if (shortData != null)
		{
			return shortData.length;
		}
		return -1;
	}

	@Override
	public NiftiHeader getHeader()
	{
		return header;
	}

	@Override
	public short[] getShortData()
	{
		return (shortData != null)
			? Arrays.copyOf(shortData, shortData.length)
			: null;
	}

	@Override
	public void setShortData(short[] data) throws IllegalArgumentException
	{
		if (shortData == null)
		{
			throw new IllegalArgumentException("Data type is not (U)Int16");
		}
		if ((data == null) || (data.length != shortData.length))
		{
			throw new IllegalArgumentException("Data null or of incorrect length");
		}
		shortData = Arrays.copyOf(data, data.length);
	}

	private void buildDataArray()
	{
		short dataType = header.getDataType();
		long[] dims = header.getDimensions();
		int nDims = (int) dims[0];
		long nValues = dims[1];
		for (int i=1; i<nDims; i++)
		{
			nValues *= dims[i+1];
		}
		if (nValues > Integer.MAX_VALUE)
		{
			throw new IllegalArgumentException(
				"Data array has too many elements: "+nValues);
		}
		switch (dataType)
		{
			case Nifti.Int16:
			case Nifti.Uint16:
				shortData = new short[(int) nValues];
				break;
			default:
				throw new IllegalArgumentException(
					"Unsupported data type: "+NiftiUtils.getTypeName(dataType));
		}
	}
	
}
