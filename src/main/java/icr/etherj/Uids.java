/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Enumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for generating unique identifiers.
 * @author jamesd
 */
public class Uids
{
	private static final String prefix = "1.2.826.0.1.534147.";
	private static int macInt = 0;
	private static long macLong = 0;
	private static short sequence = 0;
	private static Calendar dateTime = null;
	private static boolean initDone = false;
	private static final Logger logger = LoggerFactory.getLogger(Uids.class);
	private static final SecureRandom sc = new SecureRandom();

	/**
	 * Creates a short unique identifier.
	 * @return the UID
	 */
	public static String createShortUnique()
	{
		// Adapted from Simon Doran's UidGenerator class, allows more than one UID
		// per 1ms
		long macAsLong = -1;
		try
		{
			Enumeration<NetworkInterface> niEnum =
				NetworkInterface.getNetworkInterfaces();
			while (niEnum.hasMoreElements())
			{
				NetworkInterface ni = niEnum.nextElement();
				byte[] mac = ni.getHardwareAddress();
				if (ni.isVirtual() || (mac == null))
				{
					continue;
				}
				macAsLong = 0;
				for (int idx=0; idx<mac.length; idx++)
				{
					long b = 0xff & mac[mac.length-1-idx];
					macAsLong += b << (idx*8);
				}
				break;
			}
		}
		catch (SocketException ex)
		{
			logger.warn("Cannot use network interface for uniqueness", ex);
		}
		long timeRandom = (System.currentTimeMillis() & 0x00000000ffffffffL) |
			(sc.nextLong() & 0x7fffffff00000000L);
		String uid = (macAsLong == -1)
			? "zzzzzzzzz"
			: convertDecimalToIntegerBase(macAsLong, 62);
		uid += "_"+convertDecimalToIntegerBase(timeRandom, 62);

		return uid;
	}

	/**
	 * Generate a UID compliant with the DICOM standard.
	 * @return the UID
	 */
	public synchronized static String generateDicomUid()
	{
		/* UID comprised of:
		 *
		 * prefix (19)
		 * macInt (6)
		 * macLong (11)
		 * dateTime (22)
		 * sequence (6) - depends on UID request rate
		 */
		if (!initDone)
		{
			doInit();
		}
		Calendar now = Calendar.getInstance();
		if (now.equals(dateTime))
		{
			sequence++;
		}
		else
		{
			sequence = 0;
		}

		StringBuilder sb = new StringBuilder();
		sb.setLength(0);
		sb.append(prefix).append(macInt).append(".").append(macLong).append(".");
		sb.append(now.get(Calendar.YEAR));
		sb.append(now.get(Calendar.MONTH));
		sb.append(now.get(Calendar.DAY_OF_MONTH));
		sb.append(now.get(Calendar.HOUR_OF_DAY));
		sb.append(now.get(Calendar.MINUTE));
		sb.append(now.get(Calendar.SECOND));
		sb.append(now.get(Calendar.MILLISECOND));
		if (sequence > 0)
		{
			sb.append(".").append(sequence);
		}
		dateTime = now;

		return sb.toString();
	}

	// Nabbed from Simon Doran's UidGenerator class
	/*
	 * Convert a decimal integer to a character string in base b.
	 * Sensible ASCII string values containing only alphanumerics are obtained
	 * for all values of b from 2 to 62. I wrote this in order to extend the
	 * standard Java code in Long.toString(long i, int radix) in order to cope
	 * with a larger range of radix values, hence shortening the output string.
	 * @param a integer containing decimal number to be converted
	 * @param b integer containing the base
	 * @return String containing base b conversion
	 */
	private static String convertDecimalToIntegerBase(long a, int b)
	{
		StringBuilder s = new StringBuilder();
		while (a > 0)
		{
			int r = (int) (a % b);
			char rch = (char) (r+48+(r > 9 ? 7 : 0)+(r > 35 ? 6 : 0));
			s.insert(0, rch);
			a /= b;
		}
		return s.toString();
	}

	private static void doInit()
	{
		try
		{
			Enumeration<NetworkInterface> enumNI =
				NetworkInterface.getNetworkInterfaces();
			while (enumNI.hasMoreElements())
			{
				NetworkInterface ni = enumNI.nextElement();
				byte[] mac = ni.getHardwareAddress();
				if (ni.isVirtual() || (mac == null) || (mac.length < 6))
				{
					continue;
				}
				// macInt and macLong are twice as wide as the bytes put into them
				// to ensure the result is always positive for UID use.
				int anInt = (mac[0] << 8 & 0xff00) +
					(mac[1] & 0x00ff);
				long aLong = ((((long) mac[2]) << 24) & 0xff000000L) +
					((((long) mac[3]) << 16) & 0x00ff0000L) +
					((((long) mac[4]) << 8) & 0x0000ff00L) +
					(((long) mac[5]) & 0x000000ffL);
				// NIC MACs should have both values non-zero
				if ((anInt != 0) && (aLong != 0))
				{
					macInt = anInt;
					macLong = aLong;
					logger.debug("NetworkInterface for UIDs: "+ni.getDisplayName());
					break;
				}
			}
			initDone = true;
		}
		catch (SocketException exSock)
		{
			throw new RuntimeException(exSock);
		}
	}

	private Uids()
	{}

}
